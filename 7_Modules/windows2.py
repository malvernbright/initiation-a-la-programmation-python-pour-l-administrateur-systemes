#!/usr/bin/env python3
# -*- coding: utf-8 -*- 

import win32evtlog 
import win32api 
import win32con 
import win32security # Pour traduire NT Sids en noms de compte. 
import win32evtlogutil 

def ReadLog(computer, logType="Application", dumpEachRecord = 0): 
 # relie l'intégralité du journal. 
 h=win32evtlog.OpenEventLog(computer, logType) 
 numRecords = win32evtlog.GetNumberOfEventLogRecords(h) 
 # print("Il y a% d'enregistrements"% numRecords) 

 num=0 
 while 1: 
 objects = win32evtlog.ReadEventLog(h, win32evtlog.EVENTLOG_BACKWARDS_READ|win32evtlog.EVENTLOG_SEQUENTIAL_READ, 0) 
 if not objects: 
 break 
 for object in objects: 
 # obtenez-le à des fins de test, mais ne l'imprimez pas. 
 msg = win32evtlogutil.SafeFormatMessage(object, logType) 
 if object.Sid is not None: 
 try: 
 domain, user, typ = win32security.LookupAccountSid(computer, object.Sid) 
 sidDesc = "%s/%s" % (domain, user) 
 except win32security.error: 
 sidDesc = str(object.Sid) 
 user_desc = "Événement associé à l'utilisateur %s" % (sidDesc,) 
 else: 
 user_desc = None 
 if dumpEachRecord: 
 print "Enregistrement d'événement de %r généré à %s" % (object.SourceName, object.TimeGenerated.Format()) 
 if user_desc: 
 print user_desc 
 try: 
 print msg 
 except UnicodeError: 
 print "(message d'impression d'erreur unicode: repr() suit…)" 
 print repr(msg) 

 num = num + len(objects) 

 if numRecords == num: 
 print "Succès de la lecture complète", numRecords, "enregistrements" 
 else: 
 print "Impossible d'obtenir tous les enregistrements - signalé %d, mais trouvé %d" % (numRecords, num) 
 print "(Notez d'autres applications peuvent avoir écrit des enregistrements pendant l'exécution!)" 
 win32evtlog.CloseEventLog(h)

def usage(): 
 print "Écrit un événement dans le journal des événements." 
 print "-w : N'écrire aucun enregistrement de test." 
 print "-r : Ne pas lire le journal des événements" 
 print "-c : nomOrdinateur : Traiter le journal sur l'ordinateur spécifié" 
   print "-v : Verbeux" 
   print "-t : LogType - Utiliser le journal spécifié - défaut = 'Application'" 

def test(): 
   # vérifier s'il fonctionne sous Windows NT, sinon, afficher un avis et terminer 
   if win32api.GetVersion() & 0x80000000: 
       print "Cet exemple ne fonctionne que sur NT" 
       return 

   import sys, getopt 
   opts, args = getopt.getopt(sys.argv[1:], "rwh?c:t:v") 
   computer = None 
   do_read = do_write = 1 

   logType = "Application" 
   verbose = 0 

   if len(args)>0: 
       print "Arguments non valides" 
       usage() 
       return 1 
   for opt, val in opts: 
       if opt == '-t': 
           logType = val 
       if opt == '-c': 
           computer = val 
       if opt in ['-h', '-?']: 
           usage() 
           return 
       if opt=='-r': 
           do_read = 0 
       if opt=='-w': 
           do_write = 0 
       if opt=='-v': 
           verbose = verbose + 1 
   if do_write: 
       ph=win32api.GetCurrentProcess() 
       th = win32security.OpenProcessToken(ph,win32con.TOKEN_READ) 
       my_sid = win32security.GetTokenInformation(th,win32security.TokenUser)[0] 

       win32evtlogutil.ReportEvent(logType, 2, 
           strings=["Le texte du message pour l'événement 2","Un autre insert"], 
           data = "Raw\0Data".encode("ascii"), sid = my_sid) 
       win32evtlogutil.ReportEvent(logType, 1, eventType=win32evtlog.EVENTLOG_WARNING_TYPE, 
           strings=["Un avertissement","Un avertissement encore plus grave"], 
           data = "Raw\0Data".encode("ascii"), sid = my_sid) 
       win32evtlogutil.ReportEvent(logType, 1, eventType=win32evtlog.EVENTLOG_INFORMATION_TYPE, 
           strings=["Une info","Trop d'informations"], 
           data = "Raw\0Data".encode("ascii"), sid = my_sid) 
       print("Écriture réussie de 3 enregistrements dans le journal")

   if do_read: 
       ReadLog(computer, logType, verbose > 0) 

if __name__=='__main__': 
   test() 
