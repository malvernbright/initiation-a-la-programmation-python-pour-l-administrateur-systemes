Les mots clé
************

.. glossary:: Les mots clé

  and
    Description : Le mot-clef 'and' est un booléen logique, signifiant 'ET'. S'utilise le plus souvent dans une condition.
    Exemple:
      En effet, son nom est bien 'bob' ET son argent (1000) est supérieur à 500.

      .. code-block:: python

        nom, argent = "bob", 1000

        if nom == "bob" and argent > 500:
            print("Vous pouvez achetez cette télévision !")
        else:
            print("Vous n'avez pas assez de sous.")

        >>> Vous pouvez achetez cette télévision !

  as
    Description : Le mot-clef 'as' signifie 'en tant que'; parfois utile dans un gros projet. Contrôleur d'espaces noms. Il permet entre autre, de renommer un module ou le nom d'un fichier pour éviter les conflits (entre des modules qui pourraient avoir le même nom)
    Exemple:
      Ici, ctime() nous donne la date actuelle.

      .. code-block:: python

        #On importe le module time en tant que t
        import time as t

        t.ctime()
        time.ctime()

        >>> 'Sat Sep 11 19:49:40 2010'
        >>> 'Sat Sep 11 19:49:40 2010'

  assert
    Description : Le mot-clef 'assert', permet de rajouter des contrôles pour le débogage d'un programme.
    Exemple:

      .. code-block:: python

        def assert(test, données)


      On peut omettre l'argument 'données'. 'assert' lève une exception AssertionError.

      .. code-block:: python

        if __debug__ :
            if not test :
                raise AssertionError, données

  async
    Description: Permet de marquer une fonction comme étant une coroutine asynchrone.
    Exemple:

      .. code-block:: python

        async for row in GetData():
            print(row)

  await
    Description: Permet de bloquer l’exécution d’une coroutine jusqu’à ce que le traitement de cette instruction soit accompli.
    Exemple:
      L’instruction await va suspendre l’exécution de la fonction asynchrone read_data jusqu’à ce que db.fetch finisse son exécution et retournera un résultat. 

      .. code-block:: python

        async def read_data(db):
            data = await db.fetch("SELECT …")
            …

  break
    Description : Le mot-clef 'break', permet de sortir d'une boucle.
    Exemple:
      Lorsque le compteur atteint '10', on sort de la boucle grâce à celui-ci

      .. code-block:: python

        compteur = 0

        #On commence par une boucle infinie
        while True:
            compteur += 1
            print(compteur, end = '')

            #Quand le compteur vaut 10, on sort de la boucle
            if compteur >= 10:
                break

        >>> 12345678910

  class
    Description : Le mot-clef 'class' est indispensable pour créer un classe en programmant Objet. OOP
    Exemple:
      On créer une classe personnage avec quelques attributs.

      .. code-block:: python

        class Personnage(object):
            """Création d'une classe <Personnage>"""
            def __init__(self, nom):
                """Les attributs"""
                self.nom = nom
                self.prenom = "bob"

                #Pour le fun
                self.force = 0
                self.magie = 0
                self.pa    = 10
                self.pm    = 5

  continue
    Description : Le mot-clef 'continue' permet de repartir au début de la boucle courante.
    Exemple:
      On exécute les instructions une à une, dès que l'on tombe sur 'continue' on repart au début de la boucle sans exécuter les instructions qui suivent

      .. code-block:: python

        while compteur <= 10:
            avancer(x, y)
            lancerPouvoirMagique(enemieProche)

            #Quel acharnement !
            if ennemieProche is not mort:
                continue

            compteur += 1

  def
    Description : Le mot-clef 'def' permet de construire une fonction.
    Exemple:
      On définit la fonction 'addition' grâce à 'def'

      .. code-block:: python

        def addition(nombre1, nombre2):
            return nombre1 + nombre2

        print(addition(1, 1))

        >>> 2

  del
    Description : Le mot-clef 'del' prend un argument et le supprime.
    Exemple:
      Il peut effacer des variables, des fonctions et même des classes !

      .. code-block:: python

        maListe = ["Bonbon", "Chocolat", "Fruits", "Steak"]
        del(maListe[0])
        print(maListe)

        del(maListe)
        print(maListe)

        >>> ["Chocolat", "Fruits", "Steak"]

        >>> Traceback (most recent call last):
          File "<pyshell#91>", line 1, in <module>
            print(maListe)
        NameError: name 'maListe' is not defined

  elif
    Description : Le mot-clef 'elif' 'sinon si' s'utilise pour une condition, notamment après un 'if' ou un autre 'elif'; c'est une alternative secondaire.
    Exemple:
      - si ...
      --- ...
      - sinon-si (elif) ...
      --- ...
      C'est soit l'un, soit l'autre

      .. code-block:: python

        couleur = "rouge"

        if couleur == "verte":
            #Instruction(s)

        elif couleur == "rouge":
            #Elle est d'occasion ;p
            acheterVoiture(1900)
            print("Voiture acheté")

        >>> Voiture acheté

  else
    Description : Le mot-clef 'else' 'sinon' s'utilise pour une condition, l' alternative dernière.
    Exemple:
      - si ...
      --- ...
      - sinon-si (elif) ...
      --- ...
      - sinon (else)
      --- ...
      Si le bloc 'si' et 'sinon-si' n'est pas exécuté, alors le bloc 'sinon' (else) le sera

      .. code-block:: python

        couleur = "grise"

        if couleur == "verte":
            #Instruction(s)

        elif couleur == "rouge":
            #Elle est d'occasion ;p
            acheterVoiture(1900)

        #Si elle n'est ni verte, ni rouge; elle est probablement grise
        else:
            #Ce bloque sera exécuté
            vendreVoiture(1900)
            print("Voiture vendue")

        >>> Voiture vendue

  except
    Description : Le mot-clef 'except' permet de gérer une ou plusieurs exceptions.
    Exemple:
      Il est préférable de préciser de quelle(s) exception(s) il s'agit

      .. code-block:: python

        a = 18
        b = 0

        try:
            a/b
        except ZeroDivisionError:
            print("La division par 0 est impossible")

        >>> La division par 0 est impossible

  False
    Description: Valeur booléenne faux.

  finally
    Description : Le mot-clef 'finally' s'utilise avec les bloc 'try' et 'except'. Le code dans le bloc 'finally' est exécuté quoi qu'il arrive, exception ou non.
    Exemple:
      - try:
      --- On essaye ce code
      - except:
      --- S'il y a une exception, ce code est exécuté
      - finally:
      --- Quoi qu'il arrive, ce code sera automatiquement exécuté après ceux-là

      .. code-block:: python

        a = 18
        b = 0

        try:
            a/b
        except ZeroDivisionError:
            print("La division par 0 est impossible")
        finally:
            print("Le code après finally est exécuté")

        >>> La division par 0 est impossible
        >>> Le code après finally est exécuté

  for
    Description : Le mot-clef 'for' signifiant 'pour' s'utilise pour créer des boucles.
    Exemple:
      Dans une boucle du type 'for' il n'y a pas besoin de créer des variables au préalable pour la parcourir.

      .. code-block:: python

        for i in range(5):
            print(i)

        >>> 01234

  from
    Description : Le mot-clef 'from' signifiant 'de', est utilisé lors d'un import de module.
    Exemple:
      S'utilise sous la forme de: from nomDuModule import *

      .. code-block:: python

        from Tkinter import *

  global
    Description: Le mot-clef 'global' s'utilise pour modifier une valeur dans une fonction depuis le programme principal, où les variables sont dites 'locales'
    Exemple:
      La valeur de la variable 'var' située dans la fonction maFonction change car elle est 'globale'

      .. code-block:: python

        def maFonction():
            global var
            print("var =", var)

        #Programme principal
        var = 5
        maFonction()

        >>>> var = 5

  if
    Description : Le mot-clef 'if' signifiant 'si', est une condition.
    Exemple:
      - si la condition est vérifiée
      --- Ce bloc est exécuté

      .. code-block:: python

        pomme = "oui"

        if pomme == "oui":
            print("J'achète cette pomme")
        else:
            print("Je ne prends pas cette pomme")

        >>> J'achète cette pomme

  import
    Description : Le mot-clef 'import' permet d'importer un module.
    Exemple:
      Importation du type: import math. Importation de toutes les méthodes du module math

      .. code-block:: python

        import math
        math.sqrt(10)

        >>> 3.1622776601683795

  in
    Description : Le mot-clef 'in' signifiant 'dans' ou même 'contient'.
    Exemple:
      Pour chaque lettre dans maChaine afficher la lettre

      .. code-block:: python

        maChaine = "abcde"

        for lettre in maChaine:
            print(lettre, end = '')

        >>> abcde

  is
    Description : Le mot-clef 'is' signifiant 'est'. Contrairement à ce que l'on pourrait penser, ce n'est pas une façon de représenter en lettre le signe '==', et encore moins avec des variables du (type: string). C’est le test d’identité.
    Exemple:
      On voit clairement qu'il ne faut pas confondre '==' et 'is' dans l'exemple ci-dessous

      .. code-block:: python

        class MaClasse:
            def __init__(self, a, b):
                self.a = a
                self.b = b

            def __eq__(self, other):
                return self.a == other.a and self.b == other.b

        a = MaClasse(2, 3)
        b = MaClasse(2, 3)

        a is b #False
        a == b #True

        b = a
        a is b #True

        >>> False
        >>> True
        >>> True

  lambda
    Description : Le mot-clef 'lambda' ou plus communément dit 'à la volée' permet de créer rapidement une fonction d'une ligne.
    Exemple:
      Dans cette exemple, on ré-écrit la fonction 'ajouterUn' à la volée.

      .. code-block:: python

        def ajouterUn(nombre):
            return nombre + 1

        resultat = print(ajouterUn(10))

        #Maintenant avec le mot-clef 'lambda'
        resultat = lambda nombre: nombre + 1
        print(resultat(25))

        >>> 11
        >>> 26

  None
    Description: Valeur d’état vide.

  nonlocal
    Description: Le mot clé nonlocal est utilisé pour travailler avec des variables à l'intérieur de fonctions imbriquées, où la variable ne doit pas appartenir à la fonction interne.
    Exemple:

      .. code-block:: python

        def mafonction1():
            x = "abcde"
            def mafonction2():
                nonlocal x
                x = "bonjour"
            mafonction2()
            return x

        print(mafonction1())

        >>> bonjour

  not
    Description : Le mot-clef 'not' exprime une négation.
    Exemple:
      S'utilise le plus souvent pour dire: Si tu n'est pas/n'a pas...

      .. code-block:: python

        motSecret = "Love_Python"
        if not "a" in motSecret:
            print("le mot {0} ne contient pas cette lettre".format(motSecret))

        >>> Le mot Love_Python ne contient pas cette lettre.

  or
    Description : Le mot-clef 'or' est un booléen logique signifiant 'ou'. Il s'utilise le plus souvent dans une condition.
    Exemple:
      - Si bob est fort en Programmation ou en Français
      --- Afficher: Je l'embauche !

      Cela tombe bien parce que bob n'est fort qu'en Français

      .. code-block:: python

        Mr_bob = "Fort en Français"
        if (Mr_bob is "Fort en Français") or (Mr_bob is "Fort en Programmation"):
            print("Je l'embauche !")

        >>> Je l'embauche !

  pass
    Description : Le mot-clef 'pass' permet de rendre un bloc passif.
    Exemple:
      On ne peut pas par exemple laisser le bloc suivant sans rien à l'intérieur; sinon il plante.

      .. code-block:: python

        nbrDePommes = 60
        if nbrDePommes == 60:
            pass 

        #Le reste du code

  raise
    Description : Le mot-clef 'raise' permet de lever une exception.
    Exemple:
      L'exception ZeroDivisionError est levée.

      .. code-block:: python

        a, b = 10, 0

        try:
            a/b
            raise ZeroDivisionError("Impossible de diviser par 0")
        except:
            print("Vous ne pouvez pas diviser", a, "par", b)

  return
    Description : Le mot-clef 'return' permet de retourner 0 ou plusieurs argument(s) dans une fonction (principalement).
    Exemple:
      On retourne var²

      .. code-block:: python

        def fonction(var):
            return var * var

        print(resultat = fonction(5))

        >>> 25

  True
    Description: Valeur booléenne vraie.

  try
    Description : Le mot-clef 'try' signifiant 'essayer' permet de tester un bloc d'instruction.
    Exemple:
      On essaye de convertir la valeur numérique entrée par l'utilisateur

      .. code-block:: python

        valeur = input("Entrez un valeur\n>")

        try:
            valeur = int(valeur)

        #except TypeError:
            #Du code

        finally:
            print("La valeur entrée est", valeur)

        >>> Entrez une valeur
        >>> 6
        >>> La valeur entrée est 6

  while
    Description : Le mot-clef 'while' signifiant 'tant que' permet de construire une boucle.
    Exemple:
      On créer une boucle grâce à lui. On n'oublie pas d'implémenter une condition pour éviter les boucles infinies.

      .. code-block:: python

        compteur = 0

        while compteur <= 5:
            print(compteur, end = '')
            compteur += 1

        >>> 12345

  with
    Description : Le mot-clef 'with' signifiant 'avec' est utilisé pour les manipulations de fichiers et d'objets.
    Exemple:
      Dans ce cas, le mot-clef 'with' permet d'utiliser le fichier déjà ouvert.

      .. code-block:: python

        with open(monFichier, r+) as f:
            #Le reste du code

  yield
    Description : Le mot-clef 'yield' retourne successivement plusieurs valeurs. (Connaître ce que sont les Itérateurs et les Générateurs)
    Exemple:
      Dans cette exemple, la fonction pommeItérateur renverra successivement 'prendrePomme', puis 'laverPomme' au prochain appel, puis 'mangerPomme'; et cela ainsi de suite

      .. code-block:: python

        def pommeIterateur():

            while True:
                for i in ("prendrePomme", "laverPomme", "mangerPomme"):
                    yield i

        #Programme principal
        action = pommeIterateur()

        for i in range(6):
            print(action.next())

        >>> prendrePomme
        >>> laverPomme
        >>> mangerPomme
        >>> prendrePomme
        >>> laverPomme
        >>> mangerPomme