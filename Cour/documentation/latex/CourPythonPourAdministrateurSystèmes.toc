\babel@toc {french}{}
\contentsline {chapter}{\numberline {1}Accueil des stagiaires}{3}{chapter.1}%
\contentsline {chapter}{\numberline {2}Présentation et installation des outils du langage}{5}{chapter.2}%
\contentsline {section}{\numberline {2.1}Conception et modélisations}{6}{section.2.1}%
\contentsline {subsection}{\numberline {2.1.1}Réalités des développeurs}{6}{subsection.2.1.1}%
\contentsline {subsection}{\numberline {2.1.2}Bonnes pratiques}{6}{subsection.2.1.2}%
\contentsline {section}{\numberline {2.2}Les environnements systèmes}{7}{section.2.2}%
\contentsline {subsection}{\numberline {2.2.1}EnvironnementUNIX/Linux sous Windows}{7}{subsection.2.2.1}%
\contentsline {subsubsection}{SSH et Telnet}{7}{subsubsection*.3}%
\contentsline {subsubsection}{Serveur X}{7}{subsubsection*.4}%
\contentsline {subsubsection}{RDP}{7}{subsubsection*.5}%
\contentsline {subsubsection}{Nomachine/freenx ou X2GO (optimisation X)}{7}{subsubsection*.6}%
\contentsline {subsubsection}{VNC}{7}{subsubsection*.7}%
\contentsline {subsection}{\numberline {2.2.2}Linux sous Windows}{8}{subsection.2.2.2}%
\contentsline {subsubsection}{Activer le mode développeur de Windows 10}{8}{subsubsection*.8}%
\contentsline {subsubsection}{Installer le sous\sphinxhyphen {}système Windows pour Linux}{9}{subsubsection*.9}%
\contentsline {subsubsection}{Choisir une distribution Linux pour Windows}{10}{subsubsection*.10}%
\contentsline {subsubsection}{Installer Linux}{11}{subsubsection*.11}%
\contentsline {subsection}{\numberline {2.2.3}Environnements Windows sous MAC OS}{13}{subsection.2.2.3}%
\contentsline {subsubsection}{PowerShell}{13}{subsubsection*.12}%
\contentsline {subsection}{\numberline {2.2.4}Environnements Windows sous Linux}{14}{subsection.2.2.4}%
\contentsline {subsubsection}{PowerShell}{14}{subsubsection*.13}%
\contentsline {paragraph}{La méthode recommandée est la suivante pour une distribution Debian}{14}{paragraph*.14}%
\contentsline {subsubsection}{Serveurs RDP (TSE)}{15}{subsubsection*.15}%
\contentsline {section}{\numberline {2.3}L’édition de code Python}{16}{section.2.3}%
\contentsline {subsection}{\numberline {2.3.1}Installer un éditeur de code}{16}{subsection.2.3.1}%
\contentsline {section}{\numberline {2.4}Interpréteurs}{18}{section.2.4}%
\contentsline {subsection}{\numberline {2.4.1}Installer Python}{18}{subsection.2.4.1}%
\contentsline {subsection}{\numberline {2.4.2}Mode Interactif}{18}{subsection.2.4.2}%
\contentsline {subsection}{\numberline {2.4.3}Les mots clé}{21}{subsection.2.4.3}%
\contentsline {subsection}{\numberline {2.4.4}Les fonctions de base de Python}{22}{subsection.2.4.4}%
\contentsline {subsection}{\numberline {2.4.5}Mode interprété}{23}{subsection.2.4.5}%
\contentsline {subsubsection}{Conversion Python 2 vers Python 3}{23}{subsubsection*.16}%
\contentsline {subsection}{\numberline {2.4.6}Mode Compilé}{25}{subsection.2.4.6}%
\contentsline {section}{\numberline {2.5}Révision de code}{26}{section.2.5}%
\contentsline {subsection}{\numberline {2.5.1}Installer un logiciel de révision de code sur le poste de développement}{27}{subsection.2.5.1}%
\contentsline {subsubsection}{Installer git}{27}{subsubsection*.17}%
\contentsline {subsubsection}{Configurer git}{27}{subsubsection*.18}%
\contentsline {section}{\numberline {2.6}Environnement virtuel PYTHON 3}{29}{section.2.6}%
\contentsline {subsection}{\numberline {2.6.1}venv}{29}{subsection.2.6.1}%
\contentsline {subsection}{\numberline {2.6.2}pipenv}{30}{subsection.2.6.2}%
\contentsline {section}{\numberline {2.7}Documentation}{31}{section.2.7}%
\contentsline {subsection}{\numberline {2.7.1}Sauvegarder la structure de documentation}{33}{subsection.2.7.1}%
\contentsline {section}{\numberline {2.8}Débogages}{35}{section.2.8}%
\contentsline {subsection}{\numberline {2.8.1}Le débogueur Python pdb}{35}{subsection.2.8.1}%
\contentsline {subsubsection}{Installation de pdb}{35}{subsubsection*.19}%
\contentsline {subsubsection}{Déboguer avec pdb}{35}{subsubsection*.20}%
\contentsline {paragraph}{Postmortem}{36}{paragraph*.21}%
\contentsline {paragraph}{Lancez le module avec le débogueur}{36}{paragraph*.22}%
\contentsline {paragraph}{Exécution pas à pas du débogueur}{36}{paragraph*.23}%
\contentsline {paragraph}{Appeler le débogueur à l’intérieur du module}{37}{paragraph*.24}%
\contentsline {paragraph}{Les commandes du débogueur}{37}{paragraph*.25}%
\contentsline {subsection}{\numberline {2.8.2}La Gestion des Warnings d’exécution}{39}{subsection.2.8.2}%
\contentsline {section}{\numberline {2.9}Tests Unitaires}{40}{section.2.9}%
\contentsline {subsection}{\numberline {2.9.1}Sauvegarder la structure de tests}{40}{subsection.2.9.1}%
\contentsline {section}{\numberline {2.10}L’industrialisation du code DEVOPS}{41}{section.2.10}%
\contentsline {subsection}{\numberline {2.10.1}Configurer le DNS local}{42}{subsection.2.10.1}%
\contentsline {subsubsection}{Installer une interface réseau virtuelle}{42}{subsubsection*.26}%
\contentsline {subsubsection}{Configuration du client dhcp adaptée au DNS local}{42}{subsubsection*.27}%
\contentsline {subsubsection}{Définir le domaine local de la machine Ubuntu}{43}{subsubsection*.28}%
\contentsline {subsubsection}{Installer les applications de base}{43}{subsubsection*.29}%
\contentsline {subsubsection}{Configuration du DNS local}{43}{subsubsection*.30}%
\contentsline {subsubsection}{Tests du serveur DNS}{44}{subsubsection*.31}%
\contentsline {paragraph}{Vérifications du serveur}{44}{paragraph*.32}%
\contentsline {paragraph}{Vérifier la résolution DNS}{48}{paragraph*.33}%
\contentsline {paragraph}{Vérifier la résolution externe}{49}{paragraph*.34}%
\contentsline {paragraph}{Vérifier la résolution inverse}{49}{paragraph*.35}%
\contentsline {subsubsection}{Paramétrer définitivement votre DNS pour gitlab}{49}{subsubsection*.36}%
\contentsline {subsection}{\numberline {2.10.2}Installer Docker}{52}{subsection.2.10.2}%
\contentsline {subsection}{\numberline {2.10.3}Tester Docker}{55}{subsection.2.10.3}%
\contentsline {subsection}{\numberline {2.10.4}Installer GitLab}{56}{subsection.2.10.4}%
\contentsline {subsubsection}{Téléchargez le paquet d’installation GitLab pour Ubuntu et l’installer}{56}{subsubsection*.37}%
\contentsline {subsubsection}{Paramétrer GitLab}{56}{subsubsection*.38}%
\contentsline {subsection}{\numberline {2.10.5}Configurer et tester GitLab}{57}{subsection.2.10.5}%
\contentsline {subsection}{\numberline {2.10.6}Autorisations pour Docker et le Runner}{65}{subsection.2.10.6}%
\contentsline {subsection}{\numberline {2.10.7}Configurer et tester le Runner}{66}{subsection.2.10.7}%
\contentsline {subsubsection}{Tester le fonctionnement du runner}{71}{subsubsection*.39}%
\contentsline {subsection}{\numberline {2.10.8}Tester les Pages GitLab}{76}{subsection.2.10.8}%
\contentsline {subsubsection}{Créer un projet de rendu de pages HTML}{76}{subsubsection*.40}%
\contentsline {subsubsection}{Créer le «runner» pour ce projet}{79}{subsubsection*.41}%
\contentsline {paragraph}{Déployer et tester le HTML dans une Pages GitLab}{82}{paragraph*.42}%
\contentsline {chapter}{\numberline {3}Les principaux éléments de syntaxe}{87}{chapter.3}%
\contentsline {section}{\numberline {3.1}Syntaxe et grammaire Python}{87}{section.3.1}%
\contentsline {subsection}{\numberline {3.1.1}L’identation}{88}{subsection.3.1.1}%
\contentsline {subsection}{\numberline {3.1.2}Les commentaires}{90}{subsection.3.1.2}%
\contentsline {subsection}{\numberline {3.1.3}Chaînes de caractères}{91}{subsection.3.1.3}%
\contentsline {subsection}{\numberline {3.1.4}Majuscules et Minuscules (Variables, instructions, fonctions, objets)}{92}{subsection.3.1.4}%
\contentsline {subsection}{\numberline {3.1.5}Gestion des espaces}{93}{subsection.3.1.5}%
\contentsline {subsection}{\numberline {3.1.6}Les règles de base d’écriture des fonctions/procédures}{94}{subsection.3.1.6}%
\contentsline {section}{\numberline {3.2}Utilisation de Python comme calculatrice}{96}{section.3.2}%
\contentsline {section}{\numberline {3.3}Les variables}{98}{section.3.3}%
\contentsline {subsection}{\numberline {3.3.1}L’affectation dans le code}{98}{subsection.3.3.1}%
\contentsline {subsection}{\numberline {3.3.2}L’affectation au clavier}{99}{subsection.3.3.2}%
\contentsline {subsubsection}{La fonction input}{99}{subsubsection*.43}%
\contentsline {subsection}{\numberline {3.3.3}L’affectation par variables passées à un script}{100}{subsection.3.3.3}%
\contentsline {subsubsection}{Passage d’arguments en ligne de commande}{100}{subsubsection*.44}%
\contentsline {section}{\numberline {3.4}L’affichage d’informations}{101}{section.3.4}%
\contentsline {subsection}{\numberline {3.4.1}À l’écran du terminal}{101}{subsection.3.4.1}%
\contentsline {subsubsection}{Formatage de données}{101}{subsubsection*.45}%
\contentsline {paragraph}{Les expressions formatées f' \{\} '}{101}{paragraph*.46}%
\contentsline {paragraph}{La méthode str.format()}{101}{paragraph*.47}%
\contentsline {paragraph}{Concaténations de tranches de chaînes}{102}{paragraph*.48}%
\contentsline {subsubsection}{La sortie en chaînes de caractères}{102}{subsubsection*.49}%
\contentsline {subsubsection}{Les \sphinxstyleemphasis {chaînes} de caractères formatées (f\sphinxhyphen {}strings)}{103}{subsubsection*.50}%
\contentsline {subsubsection}{La méthode de chaîne de caractères format()}{104}{subsubsection*.51}%
\contentsline {subsection}{\numberline {3.4.2}Logs Système}{106}{subsection.3.4.2}%
\contentsline {subsubsection}{Utiliser logging}{106}{subsubsection*.52}%
\contentsline {subsubsection}{Un exemple simple}{107}{subsubsection*.53}%
\contentsline {subsubsection}{Enregistrer les évènements dans un fichier}{107}{subsubsection*.54}%
\contentsline {subsubsection}{Régler le niveau de journalisation d’un script}{108}{subsubsection*.55}%
\contentsline {subsubsection}{Modifier le format du message affiché}{110}{subsubsection*.56}%
\contentsline {subsection}{\numberline {3.4.3}GUI}{111}{subsection.3.4.3}%
\contentsline {section}{\numberline {3.5}Les types de variables}{112}{section.3.5}%
\contentsline {subsection}{\numberline {3.5.1}Logique}{112}{subsection.3.5.1}%
\contentsline {subsubsection}{Booléen}{112}{subsubsection*.57}%
\contentsline {subsection}{\numberline {3.5.2}Chaînes de caractères}{113}{subsection.3.5.2}%
\contentsline {subsubsection}{Chaîne de caractère ASCII}{113}{subsubsection*.58}%
\contentsline {subsubsection}{Chaîne de caractère Unicode}{113}{subsubsection*.59}%
\contentsline {subsubsection}{Manipulation des chaînes de caractères}{113}{subsubsection*.60}%
\contentsline {subsection}{\numberline {3.5.3}Nombres}{119}{subsection.3.5.3}%
\contentsline {subsubsection}{Nombre entier optimisé (int)}{119}{subsubsection*.61}%
\contentsline {subsubsection}{Nombre entier de taille arbitraire (long int)}{119}{subsubsection*.62}%
\contentsline {subsubsection}{Nombre à virgule flottante}{119}{subsubsection*.63}%
\contentsline {subsubsection}{Nombre complexe}{119}{subsubsection*.64}%
\contentsline {subsection}{\numberline {3.5.4}Données multiples}{120}{subsection.3.5.4}%
\contentsline {subsubsection}{Liste de longueur fixe}{120}{subsubsection*.65}%
\contentsline {subsubsection}{Les ensembles}{121}{subsubsection*.66}%
\contentsline {subsubsection}{Liste de longueur variable}{122}{subsubsection*.67}%
\contentsline {paragraph}{Premiers pas vers la programmation}{124}{paragraph*.68}%
\contentsline {subsubsection}{Dictionnaire}{127}{subsubsection*.69}%
\contentsline {subsection}{\numberline {3.5.5}Autres}{129}{subsection.3.5.5}%
\contentsline {subsubsection}{Fichier}{129}{subsubsection*.70}%
\contentsline {subsubsection}{Absence de type}{129}{subsubsection*.71}%
\contentsline {subsubsection}{Absence d’implémentation}{129}{subsubsection*.72}%
\contentsline {subsubsection}{fonction}{129}{subsubsection*.73}%
\contentsline {subsubsection}{module}{129}{subsubsection*.74}%
\contentsline {section}{\numberline {3.6}Les fonctions intégrées}{130}{section.3.6}%
\contentsline {subsection}{\numberline {3.6.1}Détermination du type d’une variable}{130}{subsection.3.6.1}%
\contentsline {subsection}{\numberline {3.6.2}Conversion de types}{130}{subsection.3.6.2}%
\contentsline {subsection}{\numberline {3.6.3}Voir les propriétés des fonctions}{131}{subsection.3.6.3}%
\contentsline {subsubsection}{Fonction d’aide sur les fonctions Python}{131}{subsubsection*.75}%
\contentsline {subsubsection}{Fonction de visualisation des propriétés et méthodes des fonctions Python}{131}{subsubsection*.76}%
\contentsline {subsubsection}{L’identification des objets}{132}{subsubsection*.77}%
\contentsline {subsection}{\numberline {3.6.4}Zoom sur fonctions et propriétés}{133}{subsection.3.6.4}%
\contentsline {subsubsection}{Range}{133}{subsubsection*.78}%
\contentsline {subsubsection}{Chaînes de caractères}{135}{subsubsection*.79}%
\contentsline {paragraph}{split() : sépare une chaîne en liste}{135}{paragraph*.80}%
\contentsline {paragraph}{join() : Concatène une liste de caractères}{135}{paragraph*.81}%
\contentsline {section}{\numberline {3.7}Les modules}{137}{section.3.7}%
\contentsline {subsection}{\numberline {3.7.1}\_\_name\_\_ et \_\_main\_\_}{137}{subsection.3.7.1}%
\contentsline {subsection}{\numberline {3.7.2}Gestion des imports}{138}{subsection.3.7.2}%
\contentsline {section}{\numberline {3.8}Les bibliothèques de fonctions ou d’objets}{139}{section.3.8}%
\contentsline {subsection}{\numberline {3.8.1}Les modules PYTHON}{139}{subsection.3.8.1}%
\contentsline {subsection}{\numberline {3.8.2}Le dépôt de modules Python}{140}{subsection.3.8.2}%
\contentsline {subsubsection}{Pip}{140}{subsubsection*.82}%
\contentsline {paragraph}{PIP c’est quoi ?}{140}{paragraph*.83}%
\contentsline {subsection}{\numberline {3.8.3}Les modules de gestion des paramètres de la ligne de commande}{142}{subsection.3.8.3}%
\contentsline {subsubsection}{Modules dépréciés}{142}{subsubsection*.84}%
\contentsline {subsubsection}{Argparse}{143}{subsubsection*.85}%
\contentsline {paragraph}{Utilisation du module}{143}{paragraph*.86}%
\contentsline {paragraph}{Ajout d’options}{144}{paragraph*.87}%
\contentsline {paragraph}{Exemples}{144}{paragraph*.88}%
\contentsline {subsection}{\numberline {3.8.4}Gestion des dates et du temps}{163}{subsection.3.8.4}%
\contentsline {subsubsection}{Date et heure}{163}{subsubsection*.89}%
\contentsline {subsubsection}{Durée}{164}{subsubsection*.90}%
\contentsline {subsubsection}{Calendrier}{165}{subsubsection*.91}%
\contentsline {subsection}{\numberline {3.8.5}Module Windows}{167}{subsection.3.8.5}%
\contentsline {section}{\numberline {3.9}L’organisation du code}{171}{section.3.9}%
\contentsline {subsection}{\numberline {3.9.1}Structures de contrôles}{171}{subsection.3.9.1}%
\contentsline {subsubsection}{Structures alternatives}{171}{subsubsection*.92}%
\contentsline {paragraph}{If, else, elif}{171}{paragraph*.93}%
\contentsline {subsubsection}{Structures itératives}{171}{subsubsection*.94}%
\contentsline {paragraph}{For in}{171}{paragraph*.95}%
\contentsline {paragraph}{While}{173}{paragraph*.96}%
\contentsline {subparagraph}{break, continue}{173}{subparagraph*.97}%
\contentsline {subparagraph}{pass}{174}{subparagraph*.98}%
\contentsline {paragraph}{alternative do while}{175}{paragraph*.99}%
\contentsline {paragraph}{Techniques de boucles des dictionnaires}{175}{paragraph*.100}%
\contentsline {chapter}{\numberline {4}La génération de la documentation du programme}{179}{chapter.4}%
\contentsline {section}{\numberline {4.1}Configurer Sphinx pour Python}{180}{section.4.1}%
\contentsline {subsection}{\numberline {4.1.1}Renseigner la racine des fichiers de CODE}{180}{subsection.4.1.1}%
\contentsline {subsection}{\numberline {4.1.2}Configurer les paramètres de documentation}{180}{subsection.4.1.2}%
\contentsline {subsection}{\numberline {4.1.3}Changer de thème pour votre documentation}{182}{subsection.4.1.3}%
\contentsline {subsubsection}{Le thème sphinx book}{182}{subsubsection*.101}%
\contentsline {subsection}{\numberline {4.1.4}Inclure les extensions utiles pour Python}{186}{subsection.4.1.4}%
\contentsline {subsubsection}{Ajout des extensions utiles pour Python et Sphinx}{187}{subsubsection*.102}%
\contentsline {subsection}{\numberline {4.1.5}Configuration des extensions}{187}{subsection.4.1.5}%
\contentsline {subsubsection}{Intersphinx}{187}{subsubsection*.103}%
\contentsline {subsubsection}{Extlinks}{188}{subsubsection*.104}%
\contentsline {subsubsection}{Autodoc}{188}{subsubsection*.105}%
\contentsline {subsubsection}{Inline Syntax highlight}{188}{subsubsection*.106}%
\contentsline {subsubsection}{Graphviz}{188}{subsubsection*.107}%
\contentsline {subsubsection}{Copybutton}{189}{subsubsection*.108}%
\contentsline {subsubsection}{Tabs}{189}{subsubsection*.109}%
\contentsline {subsubsection}{Ifconfig}{190}{subsubsection*.110}%
\contentsline {subsubsection}{LaTeX}{190}{subsubsection*.111}%
\contentsline {subsubsection}{Pages de manuel}{191}{subsubsection*.112}%
\contentsline {subsubsection}{Texinfo}{191}{subsubsection*.113}%
\contentsline {subsubsection}{Epub}{192}{subsubsection*.114}%
\contentsline {subsection}{\numberline {4.1.6}Générer la documentation}{192}{subsection.4.1.6}%
\contentsline {subsubsection}{Générer les formats odt ou docx}{193}{subsubsection*.115}%
\contentsline {subsubsection}{Générer l’internationalisation}{193}{subsubsection*.116}%
\contentsline {section}{\numberline {4.2}Rédiger la documentation}{194}{section.4.2}%
\contentsline {subsection}{\numberline {4.2.1}Parties, chapitres, sections, paragraphes}{195}{subsection.4.2.1}%
\contentsline {subsection}{\numberline {4.2.2}Mettre en forme du texte}{197}{subsection.4.2.2}%
\contentsline {subsection}{\numberline {4.2.3}Insertion de texte d’échappement}{199}{subsection.4.2.3}%
\contentsline {subsection}{\numberline {4.2.4}Les listes}{200}{subsection.4.2.4}%
\contentsline {subsection}{\numberline {4.2.5}Les tableaux}{201}{subsection.4.2.5}%
\contentsline {subsection}{\numberline {4.2.6}Insertion de blocs, code, image, graphviz, liens, mathématiques, notes, citations}{203}{subsection.4.2.6}%
\contentsline {subsection}{\numberline {4.2.7}Boites et conditions d’affichages}{205}{subsection.4.2.7}%
\contentsline {subsection}{\numberline {4.2.8}Documenter le code Python}{209}{subsection.4.2.8}%
\contentsline {subsubsection}{Balise de méta\sphinxhyphen {}information}{211}{subsubsection*.117}%
\contentsline {paragraph}{sectionauthor}{211}{paragraph*.118}%
\contentsline {subsubsection}{Balisages spécifiques au module}{211}{subsubsection*.119}%
\contentsline {paragraph}{module}{212}{paragraph*.120}%
\contentsline {paragraph}{moduleauthor}{212}{paragraph*.121}%
\contentsline {subsubsection}{Pour le code, nos fonctions et nos classes}{213}{subsubsection*.122}%
\contentsline {section}{\numberline {4.3}Mise en œuvre de la documentation avec Gitlab}{218}{section.4.3}%
\contentsline {subsection}{\numberline {4.3.1}Définition de la structure du document}{218}{subsection.4.3.1}%
\contentsline {subsection}{\numberline {4.3.2}Générer la documentation avec un script}{219}{subsection.4.3.2}%
\contentsline {subsection}{\numberline {4.3.3}Générer la documentation avec Gitlab}{221}{subsection.4.3.3}%
\contentsline {subsubsection}{Génération de la documentation dans Gitlab}{221}{subsubsection*.123}%
\contentsline {chapter}{\numberline {5}La qualité du code}{225}{chapter.5}%
\contentsline {section}{\numberline {5.1}Test de l’environnement Python dans Gitlab}{227}{section.5.1}%
\contentsline {section}{\numberline {5.2}Test de l’obsolescence du code Python dans Gitlab}{228}{section.5.2}%
\contentsline {section}{\numberline {5.3}Test de qualité du code dans Gitlab}{234}{section.5.3}%
\contentsline {chapter}{\numberline {6}Tests unitaire Python}{243}{chapter.6}%
\contentsline {section}{\numberline {6.1}Le module Unittest}{243}{section.6.1}%
\contentsline {subsection}{\numberline {6.1.1}Les valeurs de retour des tests}{245}{subsection.6.1.1}%
\contentsline {subsubsection}{Tests réussits}{245}{subsubsection*.124}%
\contentsline {subsubsection}{Modification engendrant un échec}{246}{subsubsection*.125}%
\contentsline {subsubsection}{Modification engendrant une erreur}{246}{subsubsection*.126}%
\contentsline {subsection}{\numberline {6.1.2}Exécution de l’ensemble de tests}{247}{subsection.6.1.2}%
\contentsline {subsection}{\numberline {6.1.3}Présentation et architecture des tests}{248}{subsection.6.1.3}%
\contentsline {subsection}{\numberline {6.1.4}Les différents tests de Unittest}{250}{subsection.6.1.4}%
\contentsline {section}{\numberline {6.2}Mise en œuvre avec Gitlab}{251}{section.6.2}%
\contentsline {chapter}{\numberline {7}Les procédures et fonctions}{257}{chapter.7}%
\contentsline {section}{\numberline {7.1}Définir une procédure/fonction}{257}{section.7.1}%
\contentsline {subsection}{\numberline {7.1.1}Corps de la procédure/fonction}{258}{subsection.7.1.1}%
\contentsline {subsection}{\numberline {7.1.2}Procédure sans paramètres}{258}{subsection.7.1.2}%
\contentsline {subsection}{\numberline {7.1.3}Fonction sans paramètres}{258}{subsection.7.1.3}%
\contentsline {subsection}{\numberline {7.1.4}Paramètres d’une fonction/procédure}{259}{subsection.7.1.4}%
\contentsline {subsubsection}{Utilisation d’une variable comme paramètre}{259}{subsubsection*.127}%
\contentsline {subsubsection}{Plusieurs paramètres}{259}{subsubsection*.128}%
\contentsline {subsubsection}{Valeurs par défaut des paramètres}{260}{subsubsection*.129}%
\contentsline {subsection}{\numberline {7.1.5}Variables locales ou globales}{262}{subsection.7.1.5}%
\contentsline {subsubsection}{Utilisation d’une variable globale}{263}{subsubsection*.130}%
\contentsline {subsection}{\numberline {7.1.6}Gestion des arguments nommés}{263}{subsection.7.1.6}%
\contentsline {subsection}{\numberline {7.1.7}Paramètres spéciaux}{265}{subsection.7.1.7}%
\contentsline {section}{\numberline {7.2}Documenter les fonctions}{268}{section.7.2}%
\contentsline {subsection}{\numberline {7.2.1}Annotations de fonctions}{269}{subsection.7.2.1}%
\contentsline {section}{\numberline {7.3}Création de bibliothèques de fonctions}{270}{section.7.3}%
\contentsline {subsection}{\numberline {7.3.1}Les modules en détail}{270}{subsection.7.3.1}%
\contentsline {subsection}{\numberline {7.3.2}Exécuter des modules comme des scripts}{272}{subsection.7.3.2}%
\contentsline {subsection}{\numberline {7.3.3}Les dossiers de recherche de modules}{272}{subsection.7.3.3}%
\contentsline {subsection}{\numberline {7.3.4}Modules Python «compilés»}{273}{subsection.7.3.4}%
\contentsline {subsection}{\numberline {7.3.5}Astuces pour les experts}{273}{subsection.7.3.5}%
\contentsline {subsection}{\numberline {7.3.6}Les paquets}{274}{subsection.7.3.6}%
\contentsline {subsubsection}{Importer un paquet}{275}{subsubsection*.131}%
\contentsline {subsubsection}{Références internes dans un paquet}{276}{subsubsection*.132}%
\contentsline {subsubsection}{Paquets dans plusieurs dossiers}{277}{subsubsection*.133}%
\contentsline {chapter}{\numberline {8}Les objets}{279}{chapter.8}%
\contentsline {section}{\numberline {8.1}Objet et caractéristiques}{279}{section.8.1}%
\contentsline {subsection}{\numberline {8.1.1}Type}{280}{subsection.8.1.1}%
\contentsline {subsection}{\numberline {8.1.2}Les attributs}{280}{subsection.8.1.2}%
\contentsline {subsection}{\numberline {8.1.3}Les méthodes}{281}{subsection.8.1.3}%
\contentsline {section}{\numberline {8.2}Classes}{282}{section.8.2}%
\contentsline {subsection}{\numberline {8.2.1}Portées et espaces de nommage en Python}{283}{subsection.8.2.1}%
\contentsline {subsubsection}{Exemple de portées et d’espaces de nommage}{285}{subsubsection*.134}%
\contentsline {subsection}{\numberline {8.2.2}Passages d’arguments}{285}{subsection.8.2.2}%
\contentsline {subsection}{\numberline {8.2.3}Méthodes spéciales \_\_repr\_\_ et \_\_str\_\_}{286}{subsection.8.2.3}%
\contentsline {subsection}{\numberline {8.2.4}Variables privées, l’encapsulation}{288}{subsection.8.2.4}%
\contentsline {subsection}{\numberline {8.2.5}Duck\sphinxhyphen {}typing}{290}{subsection.8.2.5}%
\contentsline {section}{\numberline {8.3}Documenter les classes d’objets}{293}{section.8.3}%
\contentsline {section}{\numberline {8.4}Notions avancées en objet}{296}{section.8.4}%
\contentsline {subsection}{\numberline {8.4.1}Extension de classes}{296}{subsection.8.4.1}%
\contentsline {subsubsection}{Hériter}{296}{subsubsection*.135}%
\contentsline {paragraph}{Sous\sphinxhyphen {}typage}{298}{paragraph*.136}%
\contentsline {subsubsection}{Redéfinition de méthodes, la surcharge}{298}{subsubsection*.137}%
\contentsline {subsubsection}{Héritage Conditionnel}{299}{subsubsection*.138}%
\contentsline {paragraph}{Proxy de classes}{299}{paragraph*.139}%
\contentsline {paragraph}{Héritage conditionnel}{300}{paragraph*.140}%
\contentsline {subsubsection}{Héritages Multiples}{301}{subsubsection*.141}%
\contentsline {paragraph}{Ordre d’héritage}{302}{paragraph*.142}%
\contentsline {paragraph}{Mixins}{304}{paragraph*.143}%
\contentsline {subsection}{\numberline {8.4.2}L’héritage conditionnel}{307}{subsection.8.4.2}%
\contentsline {subsection}{\numberline {8.4.3}Opérateurs}{310}{subsection.8.4.3}%
\contentsline {subsubsection}{Opérateurs arithmétiques}{310}{subsubsection*.144}%
\contentsline {subsubsection}{Opérateurs arithmétiques unaires}{312}{subsubsection*.145}%
\contentsline {subsubsection}{Opérateurs de comparaison}{312}{subsubsection*.146}%
\contentsline {subsubsection}{Autres opérateurs}{313}{subsubsection*.147}%
\contentsline {subsubsection}{Exercice Arithmétique simple}{313}{subsubsection*.148}%
\contentsline {subsection}{\numberline {8.4.4}Les attributs de classe}{316}{subsection.8.4.4}%
\contentsline {subsection}{\numberline {8.4.5}Méthodes de Classes}{317}{subsection.8.4.5}%
\contentsline {subsection}{\numberline {8.4.6}Méthodes statiques}{319}{subsection.8.4.6}%
\contentsline {subsection}{\numberline {8.4.7}Recherche d’attributs}{319}{subsection.8.4.7}%
\contentsline {subsection}{\numberline {8.4.8}Les propriétés}{320}{subsection.8.4.8}%
\contentsline {subsection}{\numberline {8.4.9}Classes abstraites}{322}{subsection.8.4.9}%
\contentsline {subsubsection}{Exercice : Base de données}{323}{subsubsection*.149}%
\contentsline {section}{\numberline {8.5}La visualisation de l’architecture objets}{326}{section.8.5}%
\contentsline {subsection}{\numberline {8.5.1}Pyreverse}{326}{subsection.8.5.1}%
\contentsline {subsubsection}{Les options de la ligne de commande}{327}{subsubsection*.150}%
\contentsline {subsection}{\numberline {8.5.2}Mise en œuvre dans GitLab}{331}{subsection.8.5.2}%
