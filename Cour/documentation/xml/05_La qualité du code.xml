<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE document PUBLIC "+//IDN docutils.sourceforge.net//DTD Docutils Generic//EN//XML" "http://docutils.sourceforge.net/docs/ref/docutils.dtd">
<!-- Generated by Docutils 0.16 -->
<document source="/home/franc/Documents/Bureautique/Formation/Projet/initiation-a-la-programmation-python-pour-l-administrateur-systemes/Cour/sources-documents/05_La qualité du code.rst">
    <section ids="la-qualite-du-code" names="la\ qualité\ du\ code">
        <title>La qualité du code</title>
        <paragraph>Vous avez écrit votre code de votre projet, mais avez vous :</paragraph>
        <bullet_list bullet="-">
            <list_item>
                <paragraph>utilisé des modules odsolètes ?</paragraph>
            </list_item>
            <list_item>
                <paragraph>respecté les standards d’écriture Python définis dans les spécifications de la <strong>PEP 8</strong> ?</paragraph>
            </list_item>
        </bullet_list>
        <paragraph>Pour cela, en plus de la commande <literal classes="code python" language="python">python3 -Wd</literal> pour vérifier l’obsolescence du code, nous avons plusieurs outils <strong>Pylint</strong>, <strong>pyflakes</strong>, <strong>pychecker</strong>, <strong>pep8</strong> ou <strong>flake8</strong>, qui permettent de vérifier la conformité de votre code avec la <strong>PEP 8</strong>.</paragraph>
        <paragraph>Dans ce cours nous allons utiliser <strong>Pylint</strong>.</paragraph>
        <paragraph>Voyons comment fonctionne cet outil ?</paragraph>
        <literal_block force="False" highlight_args="{}" language="console" linenos="False" xml:space="preserve">utilisateur@MachineUbuntu:~/repertoire_de_developpement$ sudo apt install pylint
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ pylint 1_Mode_interprété/mon_1er_programme.py
************ Module mon_1er_programme
1_Mode_interprété/mon_1er_programme.py:1:0: C0114: Missing module docstring (missing-module-docstring)
-----------------------------------
Your code has been rated at 0.00/10</literal_block>
        <paragraph>Comment éviter un message d’erreur de documentation lorsque l’on ne veut pas de documentation dans son code ?</paragraph>
        <literal_block force="False" highlight_args="{}" language="console" linenos="False" xml:space="preserve">utilisateur@MachineUbuntu:~/repertoire_de_developpement$ pylint --disable=missing-module-docstring
1_Mode_interprété/mon_1er_programme.py
---------------------
Your code has been rated at 10.00/10 (previous run: 0.00/10, +10.00)</literal_block>
        <paragraph>Comment éviter des fichiers, ou répertoires, que l’on ne veut pas tester avec <literal classes="code terminal console" language="console">pylint</literal> ?</paragraph>
        <paragraph>Nous allons d’abord tester dans un terminal la bonne remontée des fichiers à tester pour <literal classes="code terminal console" language="console">pylint</literal> avec un script shell.</paragraph>
        <paragraph>Créer un fichier <strong>choix-fichiers-a-tester</strong>.</paragraph>
        <literal_block force="False" highlight_args="{}" language="bash" linenos="False" xml:space="preserve">#! /usr/bin/env bash

find -type f -name "*.py" ! -path "*1_Mode_interprété*" ! -path "*2_Debug*" ! -path "*3_Interpreteur_alerts*" ! -path "*4_Passage_paramètres*" ! -path "*5_Niveau_journalisation*" ! -path "*6_Chaines_split_join*" ! -path "*7_Modules*" ! -path "*/.env/*" ! -path "*docs*"</literal_block>
        <literal_block force="False" highlight_args="{}" language="console" linenos="False" xml:space="preserve">utilisateur@MachineUbuntu:~/repertoire_de_developpement$ chmod u+x choix-fichiers-a-tester ; ./choix-fichiers-a-tester
./Documentation/mon_module.py</literal_block>
        <literal_block force="False" highlight_args="{}" language="console" linenos="False" xml:space="preserve">utilisateur@MachineUbuntu:~/repertoire_de_developpement$ pylint $(find -type f -name "*.py" ! -path "*1_Mode_interprété*" ! -path "*2_Debug*" ! -path "*3_Interpreteur_alerts*" ! -path "*4_Passage_paramètres*" ! -path "*5_Niveau_journalisation*" ! -path "*6_Chaines_split_join*" ! -path "*7_Modules*" ! -path "*/.env/*" ! -path "*docs*")
************ Module mon_module
Documentation/mon_module.py:72:59: C0303: Trailing whitespace (trailing-whitespace)
Documentation/mon_module.py:127:31: C0303: Trailing whitespace (trailing-whitespace)
Documentation/mon_module.py:19:0: R0205: Class 'ClasseExemple' inherits from object, can be safely removed from bases in python3 (useless-object-inheritance)
Documentation/mon_module.py:79:4: R0201: Method could be a function (no-self-use)
Documentation/mon_module.py:19:0: R0903: Too few public methods (1/2) (too-few-public-methods)

------------------------------------------------------------------
Your code has been rated at 5.00/10</literal_block>
        <paragraph>Nous allons maintenant voir comment mettre ces tests d’obsolescence et de qualité du code dans <strong>GitLab</strong>.</paragraph>
        <section ids="test-de-lenvironnement-python-dans-gitlab" names="test\ de\ l’environnement\ python\ dans\ gitlab">
            <title>Test de l’environnement Python dans Gitlab</title>
            <paragraph>Ici nous allons tester le déploiement de l’environnement <strong>Python 3</strong> pour notre code, avec le déploiement d’une page de documentation.</paragraph>
            <paragraph>Modifier <strong>.gitlab-ci.yml</strong> :</paragraph>
            <literal_block force="False" highlight_args="{}" language="yaml" linenos="False" xml:space="preserve">image: python:latest

stages:
  - build
  - deploy

construction-environnement:
  stage: build
  script:
    - echo "Bonjour $GITLAB_USER_LOGIN !"
    - echo "** Mises à jour et installation des applications supplémentaires **"
    - echo "Mises à jour système"
    - apt -y update
    - apt -y upgrade
    - echo "Installation des applications supplémentaires"
    - cat packages.txt | xargs apt -y install
    - echo "Mise à jour de PIP"
    - pip install --upgrade pip
    - echo "Installation des dépendances de modules python"
    - pip install -U -r requirements.txt
  only:
    - master

pages:
  stage: deploy
  script:
    - echo "$GITLAB_USER_LOGIN déploiement de la documentation"
    - echo "** Mises à jour et installation des applications supplémentaires **"
    - echo "Mises à jour système"
    - apt -y update
    - apt -y upgrade
    - echo "Installation des applications supplémentaires"
    - cat docs-packages.txt | xargs apt -y install
    - echo "Mise à jour de PIP"
    - pip install --upgrade pip
    - echo "Installation des dépendances de modules python"
    - pip install -U -r docs-requirements.txt
    - echo "** Génération des diagrammes de classes **"
    - ./makediagrammes
    - echo "** Génération de la documentation HTML **"
    - sphinx-build -b html ./docs/sources-document public
  artifacts:
    paths:
      - public
  only:
    - master</literal_block>
            <image align="center" alt="Tâche pages en cours" candidates="{'*': 'images/QualitéCode_1.png'}" scale="100" uri="images/QualitéCode_1.png"></image>
            <image align="center" alt="Tâche pages OK" candidates="{'*': 'images/QualitéCode_2.png'}" scale="100" uri="images/QualitéCode_2.png"></image>
        </section>
        <section ids="test-de-l-obsolescence-du-code-python-dans-gitlab" names="test\ de\ l'obsolescence\ du\ code\ python\ dans\ gitlab">
            <title>Test de l’obsolescence du code Python dans Gitlab</title>
            <paragraph>Ici nous allons utiliser la commande <literal classes="code terminal console" language="console">python3 -Wd</literal> pour générer une image d’obsolescence du code. Nous allons le tester avec le fichier «<strong>3_Interpreteur_alerts/monscript.py</strong>».</paragraph>
            <paragraph>Modifier le fichier <strong>.gitlab-ci.yml</strong>. Voici à quoi ressemble le fichier <strong>.gitlab-ci.yml</strong> pour ce projet :</paragraph>
            <literal_block force="False" highlight_args="{}" language="yaml" linenos="False" xml:space="preserve">image: python:latest

stages:
  - build
  - Static Analysis
  - deploy

construction-environnement:
  stage: build
  script:
    - echo "Bonjour $GITLAB_USER_LOGIN !"
    - echo "** Mises à jour et installation des applications supplémentaires **"
    - echo "Mises à jour système"
    - apt -y update
    - apt -y upgrade
    - echo "Installation des applications supplémentaires"
    - cat packages.txt | xargs apt -y install
    - echo "Mise à jour de PIP"
    - pip install --upgrade pip
    - echo "Installation des dépendances de modules python"
    - pip install -U -r requirements.txt
  only:
    - master

obsolescence-code:
  stage: Static Analysis
  allow_failure: true
  script:
    - echo "$GITLAB_USER_LOGIN test de l'obsolescence du code"
    - python3 -Wd Documentation/mon_module.py 2&gt; /tmp/output.txt
    - sed -n '/DeprecationWarning:/p' /tmp/output.txt &gt; /tmp/obsolescence.txt
    - \[ -s /tmp/obsolescence.txt \] &amp;&amp; cat /tmp/obsolescence.txt || echo "Pas d'obsolescences"

pages:
  stage: deploy
  before_script:
    - echo "** Mises à jour et installation des applications supplémentaires **"
    - echo "Mises à jour système"
    - apt -y update
    - apt -y upgrade
    - echo "Installation des applications supplémentaires"
    - cat docs-packages.txt | xargs apt -y install
    - echo "Mise à jour de PIP"
    - pip install --upgrade pip
    - echo "Installation des dépendances de modules python"
    - pip install -U -r docs-requirements.txt
    - echo "Création de l’infrastructure pour l'obsolescence du code"
    - mkdir -p public/obsolescence public/badges
    - echo undefined &gt; public/obsolescence/obsolescence.score
  script:
    - echo "** $GITLAB_USER_LOGIN déploiement de la documentation **"
    - python3 -Wd Documentation/mon_module.py 2&gt; /tmp/output.txt
    - sed -n '/DeprecationWarning:/p' /tmp/output.txt &gt; /tmp/obsolescence.txt
    - \[ -s /tmp/obsolescence.txt \] &amp;&amp; cat /tmp/obsolescence.txt || echo "Pas d'obsolescences"
    - \[ -s /tmp/obsolescence.txt \] &amp;&amp; echo oui &gt; public/obsolescence/obsolescence.score || echo non &gt; public/obsolescence/obsolescence.score
    - echo "Obsolescence $(cat public/obsolescence/obsolescence.score)"
    - echo "Génération des diagrammes de classes"
    - ./makediagrammes
    - echo "Création du logo SVG d'obsolescence de code"
    - anybadge --overwrite --label "Obsolescence du code" --value=$(cat public/obsolescence/obsolescence.score) --file=public/badges/obsolescence.svg oui=red non=green
    - echo "Génération de la documentation HTML"
    - sphinx-build -b html ./docs/sources-document public
  artifacts:
    paths:
      - public
  only:
    - master</literal_block>
            <paragraph>Modifier le fichier <strong>repertoire_de_developpement/docs-requirements.txt</strong>.</paragraph>
            <paragraph>Et ajouter à la fin du fichier :</paragraph>
            <literal_block force="False" highlight_args="{}" language="rest" linenos="False" xml:space="preserve">anybadge</literal_block>
            <paragraph>Modifier le fichier <strong>repertoire_de_developpement/docs/sources-documents/index.rst</strong>.</paragraph>
            <literal_block force="False" highlight_args="{}" language="rest" linenos="False" xml:space="preserve">.. |date| date::

:Date: |date|
:Revision: 1.0
:Author: Prénom NOM &lt;prénom.nom@fai.fr&gt;
:Description: Documentation sur l'initiation à la programmation Python pour l'administrateur systèmes
:Info: Voir &lt;http://gitlab.domaine-perso.fr/utilisateur/initiation_developpement_python_pour_administrateur&gt; pour la mise à jour de ce cours.

.. toctree::
   :maxdepth: 2
   :caption: Contenu

.. include:: cours/InitiationProgrammationPythonPourAdministrateurSystemes.rst


.. only:: html

  .. image:: ./badges/obsolescence.svg
     :alt: Obsolescence du code Python
     :align: left
     :width: 200px

Modules
*******

.. automodule:: Documentation.mon_module
   :members:</literal_block>
            <paragraph>Déployez les fichiers dans gitlab.</paragraph>
            <image align="center" alt="Tâche construction environnement début" candidates="{'*': 'images/QualitéCode_3.png'}" scale="100" uri="images/QualitéCode_3.png"></image>
            <image align="center" alt="Log construction environnement" candidates="{'*': 'images/QualitéCode_4.png'}" scale="100" uri="images/QualitéCode_4.png"></image>
            <image align="center" alt="Tâche qualité du code début" candidates="{'*': 'images/QualitéCode_5.png'}" scale="100" uri="images/QualitéCode_5.png"></image>
            <image align="center" alt="Tâche qualité du code en cours" candidates="{'*': 'images/QualitéCode_6.png'}" scale="100" uri="images/QualitéCode_6.png"></image>
            <image align="center" alt="Log tâche qualité du code" candidates="{'*': 'images/QualitéCode_7.png'}" scale="100" uri="images/QualitéCode_7.png"></image>
            <image align="center" alt="Tâche qualité du code en erreur et début tâche pages" candidates="{'*': 'images/QualitéCode_8.png'}" scale="100" uri="images/QualitéCode_8.png"></image>
            <image align="center" alt="Tâche pages en cours" candidates="{'*': 'images/QualitéCode_9.png'}" scale="100" uri="images/QualitéCode_9.png"></image>
            <image align="center" alt="Log tâche pages" candidates="{'*': 'images/QualitéCode_10.png'}" scale="100" uri="images/QualitéCode_10.png"></image>
            <image align="center" alt="Tâche pages OK avec qualité du code en erreur" candidates="{'*': 'images/QualitéCode_11.png'}" scale="100" uri="images/QualitéCode_11.png"></image>
            <image align="center" alt="Pages HTML de la documentation générée avec la qualité du code" candidates="{'*': 'images/QualitéCode_12.png'}" scale="100" uri="images/QualitéCode_12.png"></image>
        </section>
        <section ids="test-de-qualite-du-code-dans-gitlab" names="test\ de\ qualité\ du\ code\ dans\ gitlab">
            <title>Test de qualité du code dans Gitlab</title>
            <paragraph>Ici nous allons tester avec <literal classes="code terminal console" language="console">pylint</literal> la conformance du code de votre projet avec le standard <strong>PEP 8</strong>.</paragraph>
            <paragraph>Voici à quoi ressemble le fichier <strong>.gitlab-ci.yml</strong> pour ce projet :</paragraph>
            <literal_block force="False" highlight_args="{}" language="yaml" linenos="False" xml:space="preserve">image: python:latest

stages:
  - build
  - Static Analysis
  - deploy

construction-environnement:
  stage: build
  script:
    - echo "Bonjour $GITLAB_USER_LOGIN !"
    - echo "** Mises à jour et installation des applications supplémentaires **"
    - echo "Mises à jour système"
    - apt -y update
    - apt -y upgrade
    - echo "Installation des applications supplémentaires"
    - cat packages.txt | xargs apt -y install
    - echo "Mise à jour de PIP"
    - pip install --upgrade pip
    - echo "Installation des dépendances de modules python"
    - pip install -U -r requirements.txt
  only:
    - master

obsolescence-code:
  stage: Static Analysis
  allow_failure: true
  script:
    - echo "$GITLAB_USER_LOGIN test de l'obsolescence du code"
    - python3 -Wd Documentation/mon_module.py 2&gt; /tmp/output.txt
    - sed -n '/DeprecationWarning:/p' /tmp/output.txt &gt; /tmp/obsolescence.txt
    - \[ -s /tmp/obsolescence.txt \] &amp;&amp; cat /tmp/obsolescence.txt || echo "Pas d'obsolescences"

qualité-du-code:
  stage: Static Analysis
  allow_failure: true
  before_script:
    - echo "Installation de Pylint"
    - pip install -U pylint-gitlab
  script:
    - echo "$GITLAB_USER_LOGIN test de la qualité du code"
    - pylint --output-format=text $(bash choix-fichiers-a-tester) | tee /tmp/pylint.txt
  after_script:
    - sed -n 's/^Your code has been rated at \([-0-9.]*\)\/.*/\1/p' /tmp/pylint.txt &gt; pylint.score
    - echo "Votre score de qualité de code Pylint est de $(cat pylint.score)"

pages:
  stage: deploy
  before_script:
    - echo "** Mises à jour et installation des applications supplémentaires **"
    - echo "Mises à jour système"
    - apt -y update
    - apt -y upgrade
    - echo "Installation des applications supplémentaires"
    - cat docs-packages.txt | xargs apt -y install
    - echo "Mise à jour de PIP"
    - pip install --upgrade pip
    - echo "Installation des dépendances de modules python"
    - pip install -U -r docs-requirements.txt
    - echo "Création de l’infrastructure pour l'obsolescence et la qualité de code"
    - mkdir -p public/obsolescence public/quality public/badges public/pylint
    - echo undefined &gt; public/obsolescence/obsolescence.score
    - echo undefined &gt; public/quality/pylint.score
    - pip install -U pylint-gitlab
  script:
    - echo "** $GITLAB_USER_LOGIN déploiement de la documentation **"
    - python3 -Wd Documentation/mon_module.py 2&gt; /tmp/output.txt
    - sed -n '/DeprecationWarning:/p' /tmp/output.txt &gt; /tmp/obsolescence.txt
    - \[ -s /tmp/obsolescence.txt \] &amp;&amp; cat /tmp/obsolescence.txt || echo "Pas d'obsolescences"
    - \[ -s /tmp/obsolescence.txt \] &amp;&amp; echo oui &gt; public/obsolescence/obsolescence.score || echo non &gt; public/obsolescence/obsolescence.score
    - echo "Obsolescence $(cat public/obsolescence/obsolescence.score)"
    - pylint --exit-zero --output-format=text $(bash choix-fichiers-a-tester) | tee /tmp/pylint.txt
    - sed -n 's/^Your code has been rated at \([-0-9.]*\)\/.*/\1/p' /tmp/pylint.txt &gt; public/quality/pylint.score
    - echo "Votre score de qualité de code Pylint est de $(cat public/quality/pylint.score)"
    - echo "Création du rapport HTML de qualité de code"
    - pylint --exit-zero --output-format=pylint_gitlab.GitlabPagesHtmlReporter $(bash choix-fichiers-a-tester) &gt; public/pylint/index.html
    - echo "Génération des diagrammes de classes"
    - ./makediagrammes
    - echo "Création du logo SVG d'obsolescence de code"
    - anybadge --overwrite --label "Obsolescence du code" --value=$(cat public/obsolescence/obsolescence.score) --file=public/badges/obsolescence.svg oui=red non=green
    - echo "Création du logo SVG de qualité de code"
    - anybadge --overwrite --label "Qualité du code avec Pylint" --value=$(cat public/quality/pylint.score) --file=public/badges/pylint.svg 4=red 6=orange 8=yellow 10=green
    - echo "Génération de la documentation HTML"
    - sphinx-build -b html ./docs/sources-document public
  artifacts:
    paths:
      - public
  only:
    - master</literal_block>
            <paragraph>Modifier le fichier <strong>repertoire_de_developpement/docs/sources-documents/index.rst</strong>.</paragraph>
            <literal_block force="False" highlight_args="{}" language="rest" linenos="False" xml:space="preserve">.. |date| date::

:Date: |date|
:Revision: 1.0
:Author: Prénom NOM &lt;prénom.nom@fai.fr&gt;
:Description: Documentation sur l'initiation à la programmation Python pour l'administrateur systèmes
:Info: Voir &lt;http://gitlab.domaine-perso.fr/utilisateur/initiation_developpement_python_pour_administrateur&gt; pour la mise à jour de ce cours.

.. toctree::
   :maxdepth: 2
   :caption: Contenu

.. include:: cours/InitiationProgrammationPythonPourAdministrateurSystemes.rst


.. only:: html

  .. image:: ./badges/obsolescence.svg
     :alt: Obsolescence du code Python
     :align: left
     :width: 200px

  .. image:: ./badges/pylint.svg
     :alt: Cliquez pour voir le rapport
     :align: left
     :width: 200px
     :target: ./pylint/index.html


----


Modules
*******

.. automodule:: Documentation.mon_module
   :members:</literal_block>
            <paragraph>Déployez les fichiers dans gitlab.</paragraph>
            <image align="center" alt="Tâche construction environnement début" candidates="{'*': 'images/QualitéCode_13.png'}" scale="100" uri="images/QualitéCode_13.png"></image>
            <image align="center" alt="Log construction environnement" candidates="{'*': 'images/QualitéCode_14.png'}" scale="100" uri="images/QualitéCode_14.png"></image>
            <image align="center" alt="Tâche qualité du code début" candidates="{'*': 'images/QualitéCode_15.png'}" scale="100" uri="images/QualitéCode_15.png"></image>
            <image align="center" alt="Tâche qualité du code en cours" candidates="{'*': 'images/QualitéCode_16.png'}" scale="100" uri="images/QualitéCode_16.png"></image>
            <image align="center" alt="Log tâche qualité du code" candidates="{'*': 'images/QualitéCode_17.png'}" scale="100" uri="images/QualitéCode_17.png"></image>
            <image align="center" alt="Tâche qualité du code en erreur et début tâche pages" candidates="{'*': 'images/QualitéCode_18.png'}" scale="100" uri="images/QualitéCode_18.png"></image>
            <image align="center" alt="Tâche pages en cours" candidates="{'*': 'images/QualitéCode_19.png'}" scale="100" uri="images/QualitéCode_19.png"></image>
            <image align="center" alt="Log tâche pages" candidates="{'*': 'images/QualitéCode_20.png'}" scale="100" uri="images/QualitéCode_20.png"></image>
            <image align="center" alt="Tâche pages OK avec qualité du code en erreur" candidates="{'*': 'images/QualitéCode_21.png'}" scale="100" uri="images/QualitéCode_21.png"></image>
            <image align="center" alt="Pages HTML de la documentation générée avec la qualité du code" candidates="{'*': 'images/QualitéCode_22.png'}" scale="100" uri="images/QualitéCode_22.png"></image>
            <comment xml:space="preserve"></comment>
        </section>
    </section>
</document>
