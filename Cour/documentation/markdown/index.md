# [Initiation à la programmation PYTHON pour l’administrateur systèmes](https://sefran.frama.io/initiation-a-la-programmation-python-pour-l-administrateur-systemes/)

# Accueil des stagiaires


* Présentation du centre de formation


* Présentation du formateur


* Règles de vie


    * Individuelles


    * Collectives


* Présentation interactive des stagiaires


* Horaires et logistique (pauses, repas, locaux, service administratif,
etc.)


* Présentation de la formation


    * Prérequis (Utilisation poste travail Windows ou Unix, savoir
installer une application, bases de l’algorithmique et de l’administration systèmes)


    * Attentes sur la formation


    * Plan du cours

# Présentation et installation des outils du langage

Approche interactive avec les stagiaires pour créer le groupe.

Avez-vous déjà programmé ?

Avez-vous déjà programmé en Python ?

## Conception et modélisations

Comment approchez-vous le développement d’une application ?


* **Structure** (UML : Visual paradigm, StarUML 3, PyUML pour éclipse,
PlantUML pour visual studio, Pynsource, Graphor, Umbrello, le papier
et le crayon)


* **Interactions** (Concurrences : SA-RT, logique avec l’algèbre de
Boole, etc.)


* **Données** (Sql avec Merise, nosql, bases graphes, etc.)


* **Optimisations** (recettes, patrons de conception, etc.)

### Réalités des développeurs


* Développement logiciel (**approche produit**) : Modélisation
conceptuelle vers code.


* Développeurs ingénieurs systèmes déploiements/exploitation/matériels
(**approche opérationnelle**) : Fonctions vers code.


* Développeur WEB (**approche interfaces**) : Apparence/Ergonomie vers
code.

### Bonnes pratiques


* Modèle (**données**) Vues (**interfaces utilisateurs ou
applicatives**) Contrôleur (**opérations entre les données et les
interfaces**)


* **K**eep **I**t **S**imple **S**tupid et «**modulaire**»
(découpage en plus petit programmes ou en objets simples)


* **Respecter les standards des normes d’interopérabilités** (lire les
normes), et ne pas réinventer la roue avec le code (voir la
catastrophe des clients de messagerie avec maildir et l’obligation de
passer par imap pour en avoir les fonctionnalités sur le client MUA).


* **Maintenabilité et compréhension du code pour les autres**
(documentation, composants, déploiement, maintenance).

Lire [https://www.laurentbloch.org/Data/SI-Projets-extraits/livre008.html](https://www.laurentbloch.org/Data/SI-Projets-extraits/livre008.html)
pour ceux qui veulent aller plus loin sur le sujet.

Distribuer sous forme papier

## Les environnements systèmes

### EnvironnementUNIX/Linux sous Windows

#### SSH et Telnet

Vous pouvez vous connecter à distance avec un terminal texte sur un serveur Unix/Linux [en telnet](https://www.pcastuces.com/pratique/astuces/4519.htm) (sans sécurité), ou de façon très sécurisé [en ssh](http://www.kevinsubileau.fr/informatique/astuces-tutoriels/windows-10-client-serveur-ssh-natif.html) en l’installant directement depuis Windows. Ceci est utile si l’on veut dans un environnement Unix/Linux administrer, développer ou faire des tests de qualifications avec une infrastructure proche de celle réelle de production (Modèle V ou DEVOPS).

#### Serveur X

Vous pouvez vous connecter à un un client graphique Unix/Linux sous Windows en installant le serveur graphique [VcXsrv](https://sourceforge.net/projects/vcxsrv/). Utile si on veut tester des interfaces graphiques Unix/Linux à distance sous Windows ou se connecter en mode graphique sur une application en tant qu’utilisateur Unix/Linux.

#### RDP

Unix/Linux supporte l’installation d’un client RDP sur vos serveurs pour se connecter avec une session terminal serveur Windows en tant que client utilisateur Unix graphique.

#### Nomachine/freenx ou X2GO (optimisation X)

Un serveur graphique Windows Nomachine ou X2GO doit être installé sous Windows pour avoir une connexion client Unix/Linux graphique.

#### VNC

Linux permet aussi le partage de session graphique active d’un utilisateur Unix/Linux. C’est alors l’utilisation d’un serveur Unix/Linux (TightVNC, X11Vnc ou Vino) avec un client VNC à installer sous Windows.

Phase de test de la maîtrise par les stagiaires du poste de travail, et réglages des problèmes techniques des postes de travail.

Réglages des problèmes techniques de début de cours.

### Linux sous Windows

Lorsque nous souhaitons utiliser Unix/Linux sous Windows pour administrer, développer, tester nous passons habituellement par une solution de virtualisation. Les logiciels [Hyper-V](https://docs.microsoft.com/fr-fr/virtualization/hyper-v-on-windows/about/) ou [VirtualBox](https://www.zebulon.fr/telechargements/utilitaires/systeme-utilitaires/virtualbox.html) permettent de virtualiser une distribution Unix/Linux de son choix avec Windows.
Néanmoins, Windows 10 permet d’accéder à Linux depuis Windows assez simplement.

#### Activer le mode développeur de Windows 10

Avant toutes choses, il convient d’activer le mode développeur dans Windows. Cliquer sur le bouton **Démarrer**, aller dans **Paramètres** puis choisissez **Mise à jour et sécurité**.



![image](images/env_system_1.png)

Cliquer ensuite sur **Pour les développeurs** dans la colonne de gauche.



![image](images/env_system_2.png)

Sélectionner le **Mode développeur**, une fenêtre vous demandera d’*activer le mode développeur* :



![image](images/env_system_3.png)

Cliquer sur **Oui**. La recherche du package en mode développeur débute :



![image](images/env_system_4.png)

Il vous sera ensuite demandé de redémarrer l’ordinateur. Après le redémarrage, le **package en mode développeur** est installé et les **outils à distance pour le Bureau** sont désormais activés.

#### Installer le sous-système Windows pour Linux

Nous devons maintenant installer un sous-système Windows pour faire fonctionner Linux. Cliquer sur le bouton **Démarrer**, **Paramètres** puis **Applications** :



![image](images/env_system_5.png)

Dans la colonne de droite, cliquer sur **Programmes et fonctionnalités** dans la section Paramètres associés. Dans la colonne de gauche, cliquer
sur **Activer ou désactiver des fonctionnalités Windows**. Cochez l’option **Sous-système Windows pour Linux** puis cliquer sur le bouton **OK**.



![image](images/env_system_6.png)

Les fichiers vont être installés, puis Windows vous demande de redémarrer l’ordinateur. Cliquer sur le bouton **Redémarrer maintenant**.

#### Choisir une distribution Linux pour Windows

Nous avons activé le mode développeur et installé le sous-système Windows pour Linux, nous devons maintenant installer une distribution Linux fonctionnant avec Windows 10. Pour lancer Linux, il nous suffira de taper la commande **Bash** dans le champ de recherche en bas à gauche :



![image](images/env_system_7.png)

La fenêtre bash.exe s’ouvre alors :



![image](images/env_system_8.png)

Nous n’avons pas encore installé Linux, mais nous avons le shell Unix Bash sous Windows.

Windows nous invite alors à installer Linux en indiquant un lien. Dans votre navigateur, saisissez alors l’URL [https://aka.ms/wslstore](https://aka.ms/wslstore).
Microsoft Store va alors s’ouvrir et vous demander de choisir votre distribution Linux compatible avec Windows.

Les distributions suivantes sont proposées Ubuntu, Debian, Fedora, openSUSE, SUSE Linux Enterprise Server, Kali Linux, etc.



![image](images/env_system_9.png)

Nous choisissons Ubuntu pour ce cours (plus modèle en V avec une base Debian).

Après avoir cliqué sur Ubuntu, cliquer sur le bouton **Télécharger**. Après téléchargement et installation, cliquer sur le bouton **Lancer**.

#### Installer Linux

Voilà maintenant votre système Linux prêt à être installé sous Windows.

Lancer à nouveau la commande **bash** dans le champ de recherche. Le premier lancement permettra d’installer définitivement Ubuntu sous Windows :



![image](images/env_system_10.png)

Vous devrez alors saisir un login de votre choix ainsi qu’un mot de passe :



![image](images/env_system_11.png)

Ubuntu est maintenant prêt à être utilisé en ligne de commande, votre disque dur étant déjà monté.

### Environnements Windows sous MAC OS

#### PowerShell

installer **PowerShell** :

Lancer un shell

```
$ brew cask install powershell
```

Enfin, vérifiez que votre installation fonctionne correctement :

```
$ pwsh
```

Quand de nouvelles versions de **PowerShell** sont publiées, mettez à jour les formules de **Homebrew** et mettez à niveau **PowerShell** :

```
$ brew update
$ brew upgrade powershell –cask
```

### Environnements Windows sous Linux

#### PowerShell

PowerShell pour Linux est distribué par Microsoft pour les référentiels de packages afin de faciliter l’installation et les mises à jour à partir de Windows.

```
utilisateur@MachineUbuntu:~$ sudo snap install --classic powershell
utilisateur@MachineUbuntu:~$ sudo snap remove powershell
```

##### La méthode recommandée est la suivante pour une distribution Debian

**Inscrivez le référentiel de logiciels Microsoft pour Ubuntu**

*Téléchargements des clés de cryptages CPG des dépôts de Microsoft*

```
utilisateur@MachineUbuntu:~$ wget -q
https://packages.microsoft.com/config/ubuntu/21.04/packages-microsoft-prod.deb
```

*Enregistrer ces clés Microsoft dans le répertoire d’installation de logiciels*

```
utilisateur@MachineUbuntu:~$ sudo dpkg -i packages-microsoft-prod.deb
```

*Mise à jour de la liste des logiciels installables*

```
utilisateur@MachineUbuntu:~$ sudo apt update
```

**Installer Powershell**

Après l’inscription du dépôt logiciel Microsoft, vous pouvez installer **PowerShell**

*Installation*

```
utilisateur@MachineUbuntu:~$ sudo apt install -y powershell liblttng-ust0 liburcu6 liblttng-ust-ctl4
```

*Démarrer PowerShell*

```
utilisateur@MachineUbuntu:~$ pwsh
PowerShell 7.1.3
Copyright (c) Microsoft Corporation.

https://aka.ms/powershell
Type 'help' to get help.

PS /home/utilisateur> exit
```

#### Serveurs RDP (TSE)

Vous pouvez vous connecter en mode graphique sur un serveur Linux distant en TSE avec le protocole RDP en installant XRDP :

```
utilisateur@MachineUbuntu:~$ sudo apt install gnome-session gnome-terminal
utilisateur@MachineUbuntu:~$ sudo apt -y install xrdp
utilisateur@MachineUbuntu:~$ sudo systemctl status xrdp
utilisateur@MachineUbuntu:~$ sudo adduser xrdp ssl-cert
```

Pensez à désinstaller le serveur graphique de vos serveurs Linux de production pour la sécurité (sous Ubuntu `sudo apt remove xserver-xorg-video-all` ou `sudo apt remove xserver-xorg-driver-all`)

## L’édition de code Python

L’édition de code est une question d’ergonomie personnelle.

Certains préfèrent la méthode manuelle pour tout contrôler de leur poste de travail (système et comprendre ce qu’ils utilisent et font). Pour ne pas s’enfermer dans un environnement de travail fournisseur logiciel et permettre l’interopérabilité. Ils se tourneront alors vers un éditeur de texte évolué avec des plugins plus ou moins automatisés pour garder le contrôle de leur poste de travail (**ingénieurs systèmes**).

D’autres adorent l’automatisation de leur production de développement et ne veulent se concentrer que sur le code. Ils se tourneront alors vers un «**I**ntegrated **D**eveloppement **E**nvironnement» le plus intégré que possible et standard (**développeurs**).

Et encore d’autres aiment s’enferment dans des technologies fournisseurs et se tournent vers des **R**apid **A**pplication **D**éveloppement (**informatique non cœur de métier**) qui ont le défaut de la non optimisation du code et d’être des usines à gaz.


* **Éditeurs de texte avec coloration syntaxique et plugins** (l’IDE c’est le système d’exploitation. Pour les geeks comme moi ;-p)


* **Idle** (IDE minimaliste natif de python)


* **Pyscripter** (IDE gratuit débutants pour Windows)


* **Eric** (IDE purement python)


* **Éclipse** (IDE professionnel industriel avec l’extension PyDev pour le développement Python)


* **Visual studio** (IDE/RAD .Net professionnel Windows avec l’extension PTVS=Python Tools for Visual Studio)


* **Boaconstructor** (RAD Python + wxPython)


* **Visual python** (RAD Python + Tkinter)

Voir pour une liste plus exhaustive des éditeurs [https://wiki.python.org/moin/PythonEditors](https://wiki.python.org/moin/PythonEditors) et pour les IDE voir [https://wiki.python.org/moin/IntegratedDevelopmentEnvironments](https://wiki.python.org/moin/IntegratedDevelopmentEnvironments)### Installer un éditeur de code

Exercice :

Le stagiaire installe l’éditeur de son choix.

Distribuer sous forme papier la procédure pour **Visual studio** voir
[https://docs.microsoft.com/fr-fr/visualstudio/python/installing-python-support-in-visual-studio?view=vs-2019](https://docs.microsoft.com/fr-fr/visualstudio/python/installing-python-support-in-visual-studio?view=vs-2019)

Présentation de Visual studio ?

Distribuer sous forme papier la procédure pour **éclipse avec PyDev** :


* installer [https://www.liclipse.com/download.html](https://www.liclipse.com/download.html)


* Pour éclipse seul voir
[https://www.eclipse.org/downloads/packages/installer](https://www.eclipse.org/downloads/packages/installer)


* Pour le plugin voir
[https://koor.fr/Python/Tutorial/python_ide_pydev.wp](https://koor.fr/Python/Tutorial/python_ide_pydev.wp)

Ou pour les manuels et les pros de l’éditeur texte (allergiques aux IDE et qui veulent contrôler ce qu’il y a sous le capot), installation de notepad++ par exemple [https://notepad-plus-plus.org/downloads/](https://notepad-plus-plus.org/downloads/).

## Interpréteurs

Python est un langage de haut niveau, c’est à dire que l’on n’a pas à
tenir compte des contraintes du système d’exploitation, comme la gestion
du matériel ou de la mémoire avec le code par exemple.

Python est un langage interprété, c’est-à-dire que son code pour
s’exécuter n’a pas besoin d’être «compilé» (traduit dans le langage
machine) pour une architecture matérielle. Il s’exécute avec
l’interpréteur Python de l’architecture matérielle.

En tant que langage interprété, lorsque nous installons Python, nous
installons un interpréteur.

En réalité Python est un langage semi-interprété, l’interpréteur Python
va passer par une étape de compilation qui ne produira pas un code
adapté à la machine, mais un code intermédiaire. Souvent appelé byte
code, celui-ci sera le code réel interprété par l’interpréteur Python de
l’environnement matériel du système d’exploitation.

Il existe de nombreux interpréteurs Python écrits dans différents
langages qui fonctionnent sur différentes architectures matérielles et
systèmes d’exploitations.


* **Cpython** : L’interpréteur «classique» écrit en C


* **Pypy** : Un interpréteur écrit en… Python


* **Jython** : Un interpréteur écrit en Java qui permet d’accéder en
Python aux bibliothèques d’objets Java


* **IronPython** : Un interpréteur écrit en .Net et intégré à Visual
Studio


* **PythonNet** (.Net) : Un interpréteur distribué avec vos
développements d’applications .Net


* **Rustpython** : Un interpréteur écrit en Rust, langage système bas
niveau (comme le C, mais plus moderne et très à la mode actuellement)


* etc.

### Installer Python

Exercice :

Distribuer la procédure sous forme papier (Windows, MAC, Linux) [https://openclassrooms.com/fr/courses/4262331-demarrez-votre-projet-avec-python/4262506-installez-python](https://openclassrooms.com/fr/courses/4262331-demarrez-votre-projet-avec-python/4262506-installez-python)

Voir la doc [https://docs.python.org/fr/3/using/index.html](https://docs.python.org/fr/3/using/index.html)

### Mode Interactif

On peut essentiellement distinguer trois types d’interpréteurs
interactifs Python :


* **python** : l’interpréteur interactif classique et basique intégré à
Python.


* **IPython** (intégré avec Jupyter Notebook, le mode ordinateur de
présentations scientifiques ou d’Intelligence Artificielle) : Un
interpréteur interactif adapté à l’affichage en temps réel de courbes
et graphiques dessinés avec Matplotlib.


* **BPython** (le mode test de codes ou d’exposés pédagogiques de
codes) : Un interpréteur interactif amélioré grâce à l’utilisation de
la coloration syntaxique, la mise à disposition d’un historique des
commandes, la complétion automatique, l’auto indentation, etc.

Suivant nos besoins d’utilisation de Python en mode interactif nous
pourrons être amenés à évoluer de l’interpréteur python classique vers
un des deux autres types (IPython ou BPython).

Exercice :

```
utilisateur@MachineUbuntu:~$ python3
Python 3.9.4 (default, Apr 4 2021, 19:38:44)
[GCC 10.2.1 20210401] on linux
Type "help", "copyright", "credits" or "license" for more information.
```

```
>>> help()
Welcome to Python 3.9's help utility!

If this is your first time using Python, you should definitely check out
the tutorial on the Internet at https://docs.python.org/3.8/tutorial/.

Enter the name of any module, keyword, or topic to get help on writing
Python programs and using Python modules. To quit this help utility and
return to the interpreter, just type "quit".

To get a list of available modules, keywords, symbols, or topics, type
"modules", "keywords", "symbols", or "topics". Each module also comes
with a one-line summary of what it does; to list the modules whose name
or summary contain a given string such as "spam", type "modules spam".

help> quit
You are now leaving help and returning to the Python interpreter.
If you want to ask for help on a particular object directly from the
interpreter, you can type "help(object)". Executing "help('string')"
has the same effect as typing a particular string at the help> prompt.
>>> help(quit)
Help on Quit in module _sitebuiltins object:

class Quit(builtins.object)
 | Quit(name, eof)
 |
 | Methods defined here:
 |
 | __call__(self, code=None)
 | Call self as a function.
 |
 | __init__(self, name, eof)
 | Initialize self. See help(type(self)) for accurate signature.
 |
 | __repr__(self)
 | Return repr(self).
 |
 |
 | Data descriptors defined here:
 |
 | __dict__
 | dictionary for instance variables (if defined)
 |
 | __weakref__
 | list of weak references to the object (if defined)
(END)
q
>>> quit()
```

### Les mots clé

[Distribuer Lexique Mots Clé](../InitiationALaProgrammationPythonPourLAdministrateurSystèmesLexiqueMotsClé.odt)

### Les fonctions de base de Python

[Distribuer Lexique Les Fonctions de base](../InitiationALaProgrammationPythonPourLAdministrateurSystèmesLesFonctionsDeBase.odt)

### Mode interprété

Exercice :

Créer le répertoire répertoire_de_développement :

```
utilisateur@MachineUbuntu:~$ mkdir -p repertoire_de_developpement/1_Mode_interprété; cd repertoire_de_developpement/1_Mode_interprété
```

Créer dans ce répertoire le fichier **mon_1er_programme.py** avec l’éditeur de code choisi, et le modifier comme suit :

```
#! /usr/bin/env python3
# -*- coding: utf8 -*-

print('Bonjour à toutes et tous !')
```

Le **shebang**, représenté par **#!**, c’est un en-tête d’un fichier texte qui indique au système d’exploitation (de type Unix) que ce fichier n’est pas un fichier binaire mais un script (ensemble de commandes) ; sur la même ligne est précisé l’interpréteur permettant d’exécuter ce script.

Exécuter le programme :

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement/1_Mode_interprété$ python3 mon_1er_programme.py
```

Ou sur Unix le rendre exécutable (chmod u+x) et le lancer en ligne de commande comme une simple application :

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement/1_Mode_interprété$ chmod u+x mon_1er_programme.py ; ./mon_1er_programme.py
```

#### Conversion Python 2 vers Python 3

```
$ python2.7
Python 2.7.18 (default, Sep  5 2020, 11:17:26)
[GCC 10.2.0] on linux2
Type "help", "copyright", "credits" or "license" for more information.
```

```
>>> type('chaine') # bits => encodée
<type 'str'>
>>> type(u'chaine') # unicode => décodée
<type 'unicode'>
```

```
$ python3
Python 3.8.5 (default, Sep  5 2020, 10:50:12)
[GCC 10.2.0] on linux2
Type "help", "copyright", "credits" or "license" for more information.
```

```
>>> type("chaine") # unicode => decodée
<class 'str'>
>>> type(b"chaine") # bits => encodée
<class 'bytes'>
```

Votre but, c’est de n’avoir dans votre code que des chaînes de type ‘unicode’.

En Python 3, c’est automatique. Toutes les chaînes sont de type ‘unicode’ (appelé ‘str’ dans cette version) par défaut. En Python 2 en revanche, il faut préfixer la chaîne par un u pour avoir de l’unicode.

Python 2 vient de prendre fin le 1<sup>er</sup> janvier 2020.

Donc si vous utilisez un interpréteur Python 2, dans votre code, TOUTES vos chaînes unicode doivent être déclarées ainsi :

```
u"votre chaîne"
```

Si vous voulez, vous pouvez activer le comportement de Python 3 dans Python 2 en mettant ceci au début de CHACUN de vos modules pour vous aider à migrer vos scripts et programmes :

```
from __future__ import unicode_literals
```

Ceci n’affecte que le fichier en cours, jamais les autres modules. On peut également le mettre au démarrage d’iPython.

Résumé pour migrer Python 2 :


1. Réglez votre éditeur sur UTF8.


2. Mettez # coding: utf8 au début de vos modules.


3. Préfixez toutes vos chaînes de **u** ou faites `from __future__ import unicode_literals` en début de chaque module.

Si vous ne faites pas cela, votre code marchera uniquement avec Python 2. Et un jour, quand Python 2 ne pourra plus être déployer, il ne marchera plus. Plus du tout.

Donner sous forme papier [http://sametmax.com/lencoding-en-python-une-bonne-fois-pour-toute/](http://sametmax.com/lencoding-en-python-une-bonne-fois-pour-toute/) si besoins de migrations de python2 vers python3

### Mode Compilé

La compilation en python existe, c’est «**Cython**».

## Révision de code

Lorsque l’on développe un logiciel, ce dernier est voué à évoluer. On ne part malheureusement pas de l’idée pour aboutir immédiatement au programme fini.

Même si les spécifications sont précises, il y aura toujours de petits bugs à corriger et donc des lignes de codes seront modifiées, supprimées ou ajoutées. Mais que se passe-t-il lorsque plusieurs développeurs travaillent sur le même fichier ou programme, ou lorsqu’une correction n’en est pas une et qu’il faut revenir en arrière ?

C’est là qu’interviennent **les logiciels de gestion de versions concurrentes**, vision collective, **ou de révision de code**, vision individuelle.


* **Git** : le standard de fait en mode décentralisé.


* **Visualsource** : celui de Microsoft


* Bazaar


* Mercurial


* dinosaures (**rcs**, **svn**) etc.

### Installer un logiciel de révision de code sur le poste de développement

Exercice :

Distribuer procédure installation de git voir [https://openclassrooms.com/fr/courses/5641721-utilisez-git-et-github-pour-vos-projets-de-developpement/6113016-installez-git-sur-votre-ordinateur](https://openclassrooms.com/fr/courses/5641721-utilisez-git-et-github-pour-vos-projets-de-developpement/6113016-installez-git-sur-votre-ordinateur)

Documentation voir [https://git-scm.com/book/fr/v2](https://git-scm.com/book/fr/v2)

#### Installer git

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement/1_Mode_interprété$ cd .. ; sudo apt update; sudo apt upgrade
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ sudo apt install git
```

#### Configurer git

Récupérer le fichier [https://github.com/github/gitignore/blob/master/Python.gitignore](https://github.com/github/gitignore/blob/master/Python.gitignore) et le renommer en **.gitignore** dans le répertoire :

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ wget https://raw.githubusercontent.com/github/gitignore/master/Python.gitignore ; mv Python.gitignore .gitignore
```

Ajouter en début de fichier de **.gitignore** :

```
# Ignore itself
.gitignore
```

Mettre en place la coloration syntaxique dans git :

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git config --global color.ui auto
```

Définir l’utilisateur de git avec son adresse courriel :

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git config --global user.name "Prénom NOM"
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git config --global user.email "utilisateur@domaine-perso.fr"
```

Configurer les paramètres de la sauvegarde des identifiants de connections aux dépôts distants :

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git config --global http.sslVerify false
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git  config --global http.postBuffer 524288000
```

Distribuer un lexique sur Git «commandes git».

Initialiser le dépôt git et ajouter le fichier Python «**mon_1er_programme.py**» :

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git init
…
Dépôt Git vide initialisé dans /home/utilisateur/repertoire_de_developpement/.git/
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git status
Sur la branche master

Aucun commit

Fichiers non suivis:
 (utilisez "git add <fichier>…" pour inclure dans ce qui sera validé)
 "1_Mode_interpr\303\251t\303\251/"

 aucune modification ajoutée à la validation mais des fichiers non suivis sont présents (utilisez "git add" pour les suivre)
 utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git add .
 utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git status
 Sur la branche master

 Aucun commit

Modifications qui seront validées :
 (utilisez "git rm --cached <fichier>…" pour désindexer)
 nouveau fichier : "1_Mode_interpr\303\251t\303\251/mon_1er_programme.py"
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git commit -m "Ajout du fichier mon_1er_programme.py"
[master (commit racine) dd36b76] Ajout du fichier mon_1er_programme.py
 1 file changed, 4 insertions(+)
 create mode 100755 "1_Mode_interpr\303\251t\303\251/mon_1er_programme.py"
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git status
Sur la branche master
rien à valider, la copie de travail est propre
```

## Environnement virtuel PYTHON 3

L’ensemble des paquets Python installés par votre distribution, dans votre système Linux, a été bien testé par les intégrateurs de la distribution. Il faut donc autant que possible installer les outils/bibliothèques Python avec les outils d’administration des paquets de votre système pour vos applications informatiques Python du poste de travail .

L’installation de paquets Python par l’intermédiaire d’outils tierces risque de casser cet écosystème système bien testé.

Lorsque l’on fait du développement le besoin d’ajouter des paquets d’outils/bibliothèques Python au delà de votre système d’exploitation est une nécessité. C’est votre projet de programmation Python qui l’impose.

Donc l’utilisation de ces outils/bibliothèques sont propre à vos projets de développements Python. Ils peuvent alors rentrer en conflit de versions avec les applications Python de votre système Linux, Mac, Windows ou autres.

Afin d’isoler ces ajouts d’outils/bibliothèques du système de votre environnement poste de travail, nous allons créer pour vos projets des environnements virtuels de développement Python, avec des outils et des bibliothèques propre à ces environnements.

### venv

C’est l’environnement virtuel standard de Python. Pour l’installer :

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ sudo apt install -y python3-venv python3-pip
```

Création de l’environnement virtuel Python :

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ python3 -m venv .env
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ source .env/bin/activate
(.env) utilisateur@MachineUbuntu:~/repertoire_de_developpement$ deactivate
```

L’application `pip` servira alors d’outil d’installation des outils et bibliothèque Python pour ces environnements virtuels.

### pipenv

Installation de pipenv :

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ sudo apt install pipenv
```

Création de l’environnement virtuel Python :

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ pipenv shell
Creating a virtualenv for this project…
Using /usr/bin/python3 (3.9.4) to create virtualenv…
⠋created virtual environment CPython3.9.4.final.0-64 in 232ms creator
CPython3Posix(dest=/home/utilisateur/.local/share/virtualenvs/repertoire_de_developpement-hIqPJnF9, clear=False, no_vcs_ignore=False, global=False)
seeder FromAppData(download=False, pip=bundle, setuptools=bundle, wheel=bundle, via=copy, app_data_dir=/home/utilisateur/.local/share/virtualenv)
added seed packages: pip==20.3.4, pkg_resources==0.0.0, setuptools==44.1.1, wheel==0.34.2
activators BashActivator,CShellActivator,FishActivator,PowerShellActivator,PythonActivator,XonshActivator

Virtualenv location: /home/utilisateur/.local/share/virtualenvs/repertoire_de_developpement-hIqPJnF9
Creating a Pipfile for this project…
Spawning environment shell (/bin/bash). Use 'exit' to leave.
(repertoire_de_developpement-hIqPJnF9) utilisateur@MachineUbuntu:~/repertoire_de_developpement$ exit
```

## Documentation


* **Documenter le code** (annotations des variables, docstring)


* **Génération de la documentation** (Sphinx, doxygen, docutil, pdoc3, pydoctor, etc.)


* **Syntaxes** (restructured text, markdown, asciidoc, mediawiki, html, etc.)

Fournir un lexique sur la syntaxe pour la doc.

Nous reviendrons plus tard dans ce cours sur l’utilisation de la documentation dans Python.

Mise en place du système de documentation du code, architecture et scripts (Sphinx).

Installer les logiciels de la documentation :

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ source .env/bin/activate
(.env) utilisateur@MachineUbuntu:~/repertoire_de_developpement$ pip install sphinx sphinx-intl
```

Créer la documentations :

```
(.env) utilisateur@MachineUbuntu:~/repertoire_de_developpement$ mkdir docs; cd docs
(.env) utilisateur@MachineUbuntu:~/repertoire_de_developpement/docs$ sphinx-quickstart
Bienvenue dans le kit de démarrage rapide de Sphinx 3.2.1.

Please enter values for the following settings (just press Enter to
accept a default value, if one is given in brackets).

Selected root path: .

You have two options for placing the build directory for Sphinx output.
"source" and "build" directories within the root path.

> Séparer les répertoires build et source (y/n) [n]: y

The project name will occur in several places in the built documentation.

> Nom du projet: Documentation sur l’initiation à la programmation Python pour l’administrateur systèmes
> Nom(s) de l\'auteur: Prénom NOM
> version du projet []:

If the documents are to be written in a language other than English,
you can select a language here by its language code. Sphinx will then
translate text that it generates into that language.

For a list of supported codes, see https://www.sphinx-doc.org/en/master/usage/configuration.html#confval-language.

> Langue du projet [en]: fr

Fichier en cours de création /home/utilisateur/repertoire_de_developpement/docs/source/conf.py.
Fichier en cours de création /home/utilisateur/repertoire_de_developpement/docs/source/index.rst.
Fichier en cours de création /home/utilisateur/repertoire_de_developpement/docs/Makefile.
Fichier en cours de création /home/utilisateur/repertoire_de_developpement/docs/make.bat.

Terminé : la structure initiale a été créée.

You should now populate your master file /home/utilisateur/repertoire_de_developpement/docs/source/index.rst and create other documentation
source files. Use the Makefile to build the docs, like so:
    make builder
where "builder" is one of the supported builders, e.g. html, latex or linkcheck.
(.env) utilisateur@MachineUbuntu:~/repertoire_de_developpement/docs$ rmdir build ; mv source sources-documents
```

modifier les fichiers «**Makefile**» et «**make.bat**», dans lesquels il faudra adapter le contenu de la variable «**SOURCEDIR**».

**Makefile** :

```
SOURCEDIR = sources-documents
BUILDDIR = documentation
```

**make.bat** :

```
set SOURCEDIR=sources-documents
set BUILDDIR=documentation
```

Pour générer la doc sous Linux, c’est très simple, il suffit d’ouvrir un terminal dans le dossier du projet et de taper la commande suivante :

```
(.env) utilisateur@MachineUbuntu:~/repertoire_de_developpement/docs$ make html ; cd ..
(.env) utilisateur@MachineUbuntu:~/repertoire_de_developpement$ deactivate
```

Si vous n’avez pas la commande `make`, il vous faudra l’installer. Ça peut se faire avec la commande suivante si vous utilisez Debian, Ubuntu ou l’un de leurs dérivés :

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement/docs$ sudo apt install build-essential
```

Si vous êtes sous Windows et que vous utilisez Git Bash, il vous faudra utiliser la commande suivante pour générer votre documentation :

```
$ ./make.bat html
```

Voir la documentation générée «**…/repertoire_de_developpement/docs/documentation/html/index.html**» avec un navigateur web.

### Sauvegarder la structure de documentation

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git add .
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git status
Sur la branche master
Votre branche est à jour avec 'origin/master'.
Modifications qui seront validées :
(utilisez "git restore --staged <fichier>..." pour désindexer)
 nouveau fichier : docs/Makefile
 nouveau fichier : docs/documentation/doctrees/environment.pickle
 nouveau fichier : docs/documentation/doctrees/index.doctree
 nouveau fichier : docs/documentation/html/.buildinfo
 nouveau fichier : docs/documentation/html/_sources/index.rst.txt
 nouveau fichier : docs/documentation/html/_static/_stemmer.js
 nouveau fichier : docs/documentation/html/_static/alabaster.css
 nouveau fichier : docs/documentation/html/_static/basic.css
 nouveau fichier : docs/documentation/html/_static/custom.css
 nouveau fichier : docs/documentation/html/_static/doctools.js
 nouveau fichier : docs/documentation/html/_static/documentation_options.js
 nouveau fichier : docs/documentation/html/_static/file.png
 nouveau fichier : docs/documentation/html/_static/jquery-3.5.1.js
 nouveau fichier : docs/documentation/html/_static/jquery.js
 nouveau fichier : docs/documentation/html/_static/language_data.js
 nouveau fichier : docs/documentation/html/_static/minus.png
 nouveau fichier : docs/documentation/html/_static/plus.png
 nouveau fichier : docs/documentation/html/_static/pygments.css
 nouveau fichier : docs/documentation/html/_static/searchtools.js
 nouveau fichier : docs/documentation/html/_static/translations.js
 nouveau fichier : docs/documentation/html/_static/underscore-1.3.1.js
 nouveau fichier : docs/documentation/html/_static/underscore.js
 nouveau fichier : docs/documentation/html/genindex.html
 nouveau fichier : docs/documentation/html/index.html
 nouveau fichier : docs/documentation/html/objects.inv
 nouveau fichier : docs/documentation/html/search.html
 nouveau fichier : docs/documentation/html/searchindex.js
 nouveau fichier : docs/make.bat
 nouveau fichier : docs/sources-documents/conf.py
 nouveau fichier : docs/sources-documents/index.rst
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git commit -m "Structure de la documentation du projet"
[master 31c720b] Structure de la documentation du projet
31 files changed, 17692 insertions(+)
create mode 100644 docs/Makefile
create mode 100644 docs/documentation/doctrees/environment.pickle
create mode 100644 docs/documentation/doctrees/index.doctree
create mode 100644 docs/documentation/html/.buildinfo
create mode 100644 docs/documentation/html/_sources/index.rst.txt
create mode 100644 docs/documentation/html/_static/alabaster.css
create mode 100644 docs/documentation/html/_static/base-stemmer.js
create mode 100644 docs/documentation/html/_static/basic.css
create mode 100644 docs/documentation/html/_static/custom.css
create mode 100644 docs/documentation/html/_static/doctools.js
create mode 100644 docs/documentation/html/_static/documentation_options.js
create mode 100644 docs/documentation/html/_static/file.png
create mode 100644 docs/documentation/html/_static/french-stemmer.js
create mode 100644 docs/documentation/html/_static/jquery-3.5.1.js
create mode 100644 docs/documentation/html/_static/jquery.js
create mode 100644 docs/documentation/html/_static/language_data.js
create mode 100644 docs/documentation/html/_static/minus.png
create mode 100644 docs/documentation/html/_static/plus.png
create mode 100644 docs/documentation/html/_static/pygments.css
create mode 100644 docs/documentation/html/_static/searchtools.js
create mode 100644 docs/documentation/html/_static/translations.js
create mode 100644 docs/documentation/html/_static/underscore-1.12.0.js
create mode 100644 docs/documentation/html/_static/underscore.js
create mode 100644 docs/documentation/html/genindex.html
create mode 100644 docs/documentation/html/index.html
create mode 100644 docs/documentation/html/objects.inv
create mode 100644 docs/documentation/html/search.html
create mode 100644 docs/documentation/html/searchindex.js
create mode 100644 docs/make.bat
create mode 100644 docs/sources-documents/conf.py
create mode 100644 docs/sources-documents/index.rst
```

## Débogages

Si vous avez un bogue non banal, c’est là que les stratégies de débogage vont rentrer en ligne de compte. Le problème doit être isolé dans un petit nombre de lignes de code, hors frameworks ou code applicatif.

Pour déboguer un problème donné :


1. Faites échouer le code de façon fiable : trouvez un cas de test qui fait échouer le code à chaque fois.


2. Diviser et conquérir : une fois que vous avez un cas de test échouant, isolez le code coupable.

> 
>     1. Quel module.


>     2. Quelle fonction.


>     3. Quelle ligne de code.


>     4. Isolez une petite erreur reproductible (permet de définir un cas de test à implémenter).


3. Changez une seule chose à chaque fois, l’archiver dans la révision de code, et ré-exécutez le cas de test d’échec.


4. Utilisez le débogueur (pour Python pdb) pour comprendre ce qui ne va pas.


5. Prenez des notes et soyez patient, ça peut prendre un moment.

Une fois que vous avez procédé à cette étape, isolez un petit bout de code reproduisant le bogue et corrigez celui-ci en utilisant ce bout de code, ajoutez le code de test dans votre suite de test (Unittest).

### Le débogueur Python pdb

#### Installation de pdb

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement$sudo apt install python3-ipdb
```

#### Déboguer avec pdb

Les façons de lancer le débogueur :

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ mkdir 2_Debug ; cd 2_Debug
```

Créer le fichier «**error.py**» dans le dossier «**repertoire_de_developpement/2_Debug**»

```
#! /usr/bin/env python3
# -*- coding: utf8 -*-

dividende = 5
nombres = [5, 4, 3, 2, 1, 0]
for diviseur in nombres:
  print('Valeur du rapport : %s' % (dividende/diviseur))
```

##### Postmortem

`pdb` est invoqué (exécuté) pour déboguer un script.

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement/2_Debug$ python3 -m pdb error.py
>/home/utilisateur/repertoire_de_developpement/2_Debug/error.py(4)<module>()
-> dividende = 5
(pdb) q
```

Pour arrêter le débogage (prompt `pdb`) tapez `q`.

##### Lancez le module avec le débogueur

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement/2_Debug$ ipython3 error.py
```

ou

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement/2_Debug$ ipython3
In [1]:%run error.py
```

pour sortir du débogueur (prompt `In [num]:%`) tapez `quit`.

##### Exécution pas à pas du débogueur

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement/2_Debug$ ipython3 -c '%run -d error.py'
```

ou

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement/2_Debug$ ipython3
In [1]: %run -d error.py
```

Continuez dans le code avec `n`(ext), next saute à la prochaine déclaration de code dans le contexte d’exécution courant  :

```
ipdb> n
```

Placez un point d’arrêt à la ligne 7 en utilisant `b 7` :

```
ipdb> b 7
```

Continuez l’exécution jusqu’au prochain point d’arrêt avec `c`(ontinue) :

```
ipdb> c
```

Continuez dans le code avec `s`(tep), step va traverser les contextes d’exécution, c’est-à-dire permettre l’exploration à l’intérieur des appels de fonction :

```
ipdb> s
```

Visualiser l’état d’une variable avec `print()` :

```
ipdb> print(diviseur)
```

Arrêter le débogage :

```
ipdb> q
```

Quitter le débogueur :

```
In [3]: quit
```

##### Appeler le débogueur à l’intérieur du module

```
import pdb; pdb.set_trace()
```

##### Les commandes du débogueur

| l (list)

 | Liste le code à la position courante

 |
| u(p)

                                                                                                                                                          | Monte à la pile d’appel

                                                                                                                                                                                                                                                                           |
| d(own)

                                                                                                                                                        | Descend à la pile d’appel

                                                                                                                                                                                                                                                                         |
| n(ext)

                                                                                                                                                        | Exécute la prochaine ligne (ne va pas à l’intérieur
d’une nouvelle fonction)

                                                                                                                                                                                                                      |
| s(tep)

                                                                                                                                                        | Exécute la prochaine déclaration (va à l’intérieur d’une
nouvelle fonction)

                                                                                                                                                                                                                       |
| bt

                                                                                                                                                            | Affiche la pile d’appel

                                                                                                                                                                                                                                                                           |
| a

                                                                                                                                                             | Affiche les variables locales

                                                                                                                                                                                                                                                                     |
| !command

                                                                                                                                                      | Exécute la commande **Python** donnée (par opposition à
une commande pdb)

                                                                                                                                                                                                                             |
```
utilisateur@MachineUbuntu:~/repertoire_de_developpement/2_Debug$ cd ..
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git add .
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git commit -m "Ajout des exemples de débogages"
```

### La Gestion des Warnings d’exécution

Attention cet exemple fonctionne jusqu’à Python 3.9. Pour les versions postérieures les modules obsolètes hérités de Python2 ne sont plus pris en charge.

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ mkdir 3_Interpreteur_alerts ; cd 3_Interpreteur_alerts
```

Créer le fichier «**monscript.py**» dans le dossier «**repertoire_de_developpement/3_Interpreteur_alerts**»

```
#! /usr/bin/env python3
# -*- coding: utf8 -*-

import formatter

print('Bonjour %s' % 'Moi')
```

Exécution de python avec les Warnings :

```
python3 -Wd monscript.py
```

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement/3_Interpreteur_alerts$ python3 -Wd monscript.py
monscript.py:4: DeprecationWarning: the formatter module is deprecated
  import formatter
Bonjour Moi
```

Pour l’activer par défaut pour toutes les alertes :

```
python3 -Wa
```

À chaque mise à jour de version de python, pour son code il est important de vérifier les warnings.

Ceux-ci nous informe de l’obsolescence des bibliothèques ou des fonctions de python que nous utilisons. Cela permet de préparer et corriger le code python de nos applications développées pour les migrations futures de vos systèmes informatiques et de leurs bibliothèques/frameworks.

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement/3_Interpreteur_alerts$ cd ..
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git add .
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git commit -m "Ajout des exemples de warnings d’exécution"
```

## Tests Unitaires


* **Tests unitaires en python** (Unittest et doctest)


* **Frameworks de tests** (Unittest, Robot, Pytest, Doctest, Nose2, Testify)

Les tests unitaires permettent de vérifier (tester) des éléments particuliers d’un programme.

Par exemple si un programme contient plusieurs parties de code autonome, les tests unitaires permettront de vérifier leurs présences, le fonctionnement de chacune des parties suivant un comportement attendu.

La mise en place de tests unitaires permet de s’assurer que la correction de bugs, ou le développement de nouvelles fonctions, n’entraînera pas de régressions au niveau du code.

Nous verrons ultérieurement au cours de cette formation le module Python Unittest


* Architecture des tests


* Les valeurs de retour des tests


* Les différents tests de Unittest


* Exécution de l’ensemble des tests

Mise en place de l’infrastructure, créer le répertoire tests :

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ mkdir tests ; touch tests/README.md
```

Contenu du fichier «**README.md**»

```
# Tests unitaires du code Python
```

### Sauvegarder la structure de tests

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git add .
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git status
Sur la branche master
Votre branche est à jour avec 'origin/master'.
Modifications qui seront validées :
(utilisez "git restore --staged <fichier>..." pour désindexer)
 nouveau fichier : tests/README.md
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git commit -m "Ajout de la structure de tests"
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ cd ..
```

## L’industrialisation du code DEVOPS


* Gitlabs


* Github/Azure


* BitbucketInstaller l’infrastructure DEV/OPS

Nous allons installer GitLab avec Docker. De plus, nous utiliserons Ubuntu 21.04 comme système d’exploitation principal.

Prérequis:


* Serveur Ubuntu 21.04


* Min 4 Go de RAM


* Privilèges root

Qu’allons nous faire?


1. Configurer le DNS local


2. Installer Docker


3. Tester Docker


4. Installer Gitlab


5. Configurer et tester Gitlab


6. Autorisations pour Docker et le Runner


7. Configurer et tester le Runner


8. Tester les Pages de Gitlab

### Configurer le DNS local

Vous avez besoin d’un nom de domaine avec un enregistrement A valide pointant vers votre serveur GitLab.

#### Installer une interface réseau virtuelle

Éditer «**/etc/systemd/network/10-virtualeth0.netdev**»

```
[NetDev]
Name = virtualeth0
Kind = dummy
```

Éditer «**/etc/systemd/network/10-virtualeth0.network**»

```
[Match]
Name = virtualeth0

[Network]
Address = 10.10.10.1/24
Address = fd00::/8
```

```
utilisateur@MachineUbuntu:~$ sudo systemctl start systemd-networkd
utilisateur@MachineUbuntu:~$ sudo systemctl enable systemd-networkd
utilisateur@MachineUbuntu:~$ ip a
…
3: virtualeth0: <BROADCAST,NOARP,UP,LOWER_UP> mtu 1500 qdisc noqueue state UNKNOWN group default qlen 1000
  link/ether 9a:3c:56:42:f5:c9 brd ff:ff:ff:ff:ff:ff
  inet 10.10.10.1/24 brd 10.10.10.255 scope global virtualeth0
    valid_lft forever preferred_lft forever
  inet6 fd00::/8 scope global
    valid_lft forever preferred_lft forever
  inet6 fe80::983c:56ff:fe42:f5c9/64 scope link
    valid_lft forever preferred_lft forever
…
```

#### Configuration du client dhcp adaptée au DNS local

Pour pouvoir ajouter le serveur DNS local à «**/etc/resolv.conf**» il faut renseigner l’option «**prepend**» qui permet l’ajout du serveur DNS local **en début de** la liste des **serveurs DNS** fournit automatiquement par DHCP.

Éditer «**/etc/dhcp/dhclient.conf**»

```
prepend domaine-perso.fr 10.10.10.1 fd00::
```

Vérifier les DNS présents :

```
utilisateur@MachineUbuntu:~$ nmcli dev show | grep DNS
IP4.DNS[1]: yyy.yyy.yyy.yyy
IP4.DNS[1]: yyy.yyy.yyy.yyy
IP6.DNS[1]: yyyy:yyyy:yyyy::yyyy
IP6.DNS[2]: yyyy:yyyy:yyyy::yyyy
IP6.DNS[3]: yyyy:yyyy:yyyy::yyyy
utilisateur@MachineUbuntu:~$ resolvectl dns
Global:
Link 2 (enp0sxx):
Link 3 (wlx803xxxxx): yyyy:yyyy:yyyy::yyyy yyyy:yyyy:yyyy::yyyy yyyy:yyyy:yyyy::yyyy yyy.yyy.yyy.yyy
Link 4 (wlo1): yyy.yyy.yyy.yyy
Link 6 (virtualeth0):
```

#### Définir le domaine local de la machine Ubuntu

```
utilisateur@MachineUbuntu:~$ sudo hostnamectl set-hostname MachineUbuntu.domaine-perso.fr --static
utilisateur@MachineUbuntu:~$ hostname -d
domaine-perso.fr
```

#### Installer les applications de base

```
utilisateur@MachineUbuntu:~$ sudo apt install bind9 bind9utils bind9-dnsutils bind9-doc bind9-host net-tools
utilisateur@MachineUbuntu:~$ sudo systemctl status named
utilisateur@MachineUbuntu:~$ sudo systemctl enable named
```

#### Configuration du DNS local

Éditer «**/etc/bind/named.conf.options**»

```
options {
  directory "/var/cache/bind";

  // Pour des raisons de sécurité.
  // Cache la version du serveur DNS pour les clients.
  version "Pas pour les crackers";

  listen-on { 127.0.0.1; 10.10.10.1; };
  listen-on-v6 { ::1; fd00::; };

  allow-query { 127.0.0.1; 10.10.10.1; ::1; fd00::; };

  // Optionnel - Comportement par défaut de BIND en récursions.
  recursion yes;

  // Récursions autorisées seulement pour les interfaces clients
  allow-recursion { 127.0.0.1; 10.10.10.0/24; ::1; fd00::/8; };

  dnssec-validation auto;

  // Activer la journalisation des requêtes DNS
  querylog yes;

};
```

Vérifier la validité de la configuration, et redémarrer le serveur DNS si la configuration est OK.

```
utilisateur@MachineUbuntu:~$ sudo named-checkconf
utilisateur@MachineUbuntu:~$ sudo systemctl restart named
```

Ajout du server DNS local à la liste des serveurs DNS de systemd-resolved.

Éditer «**/etc/systemd/resolved.conf**»

```
DNS=10.10.10.1 fd00::
```

```
utilisateur@MachineUbuntu:~$ sudo systemctl restart systemd-resolved
utilisateur@MachineUbuntu:~$ nmcli general reload
```

#### Tests du serveur DNS

##### Vérifications du serveur

```
utilisateur@MachineUbuntu:~$ sudo rndc status
version: BIND 9.16.8-Ubuntu (Stable Release) <id:539f9f0> (Pas pour les crackers)
running on MachineUbuntu.domaine-perso.fr: Linux x86_64 5.11.0-31-generic #33-Ubuntu SMP Wed Aug 11 13:19:04 UTC 2021
boot time: Thu, 26 Aug 2021 06:13:19 GMT
last configured: Thu, 26 Aug 2021 06:13:19 GMT
configuration file: /etc/bind/named.conf
CPUs found: 4
worker threads: 4
UDP listeners per interface: 4
number of zones: 102 (97 automatic)
debug level: 0
xfers running: 0
xfers deferred: 0
soa queries in progress: 0
query logging is ON
recursive clients: 0/900/1000
tcp clients: 0/150
TCP high-water: 0
server is up and running
```

Vérifier le fonctionnement de bind sur le port 53

```
utilisateur@MachineUbuntu:~$ sudo lsof -i:53
COMMAND PID USER FD TYPE DEVICE SIZE/OFF NODE NAME
named 5624 bind 37u IPv4 54315 0t0 UDP localhost:domain
named 5624 bind 38u IPv4 54316 0t0 UDP localhost:domain
named 5624 bind 39u IPv4 54317 0t0 UDP localhost:domain
named 5624 bind 40u IPv4 54318 0t0 UDP localhost:domain
named 5624 bind 42u IPv4 51987 0t0 TCP localhost:domain (LISTEN)
named 5624 bind 43u IPv4 54319 0t0 UDP MachineUbuntu.domaine-perso.fr:domain
named 5624 bind 44u IPv4 54320 0t0 UDP MachineUbuntu.domaine-perso.fr:domain
named 5624 bind 45u IPv4 54321 0t0 UDP MachineUbuntu.domaine-perso.fr:domain
named 5624 bind 46u IPv4 54322 0t0 UDP MachineUbuntu.domaine-perso.fr:domain
named 5624 bind 47u IPv4 51988 0t0 TCP MachineUbuntu.domaine-perso.fr:domain (LISTEN)
named 5624 bind 48u IPv6 54323 0t0 UDP ip6-localhost:domain
named 5624 bind 49u IPv6 54324 0t0 UDP ip6-localhost:domain
named 5624 bind 50u IPv6 54325 0t0 UDP ip6-localhost:domain
named 5624 bind 51u IPv6 54326 0t0 UDP ip6-localhost:domain
named 5624 bind 52u IPv6 51989 0t0 TCP ip6-localhost:domain (LISTEN)
named 5624 bind 53u IPv6 54327 0t0 UDP MachineUbuntu.domaine-perso.fr:domain
named 5624 bind 54u IPv6 54328 0t0 UDP MachineUbuntu.domaine-perso.fr:domain
named 5624 bind 55u IPv6 54329 0t0 UDP MachineUbuntu.domaine-perso.fr:domain
named 5624 bind 56u IPv6 54330 0t0 UDP MachineUbuntu.domaine-perso.fr:domain
named 5624 bind 58u IPv6 54331 0t0 TCP MachineUbuntu.domaine-perso.fr:domain (LISTEN)
systemd-r 5799 systemd-resolve 12u IPv4 52844 0t0 UDP localhost:domain
systemd-r 5799 systemd-resolve 13u IPv4 52845 0t0 TCP localhost:domain (LISTEN)
```

Vérifier l’écoute réseau sur le port 53

```
utilisateur@MachineUbuntu:~$ sudo netstat -alnp | grep -i :53
tcp  0 0 127.0.0.53:53 0.0.0.0:* LISTEN 5799/systemd-resol
tcp  0 0 127.0.0.1:53  0.0.0.0:* LISTEN 5624/named
tcp  0 0 10.10.10.1:53 0.0.0.0:* LISTEN 5624/named
tcp6 0 0 fd00:::53     :::*      LISTEN 5624/named
tcp6 0 0 ::1:53        :::*      LISTEN 5624/named
udp  0 0 127.0.0.53:53 0.0.0.0:*        5799/systemd-resol
udp  0 0 127.0.0.1:53  0.0.0.0:*        5624/named
udp  0 0 10.10.10.1:53 0.0.0.0:*        5624/named
udp  0 0 0.0.0.0:5353  0.0.0.0:*        771/avahi-daemon: r
udp6 0 0 fd00:::53     :::*             5624/named
udp6 0 0 ::1:53        :::*             5624/named
udp6 0 0 :::5353       :::*             771/avahi-daemon: r
```

Vérifier que le système Ubuntu écoute le serveur DNS

```
utilisateur@MachineUbuntu:~$ resolvectl dns
Global: 10.10.10.1 fd00::
Link 2 (enp0sxx): yyyy:yyyy:yyyy::yyyy yyy.yyy.yyy.yyy
Link 3 (virtualeth0):
Link 4 (wlx803xxxxx): yyyy:yyyy:yyyy::yyyy yyyy:yyyy:yyyy::yyyy yyyy:yyyy:yyyy::yyyy yyy.yyy.yyy.yyy
Link 5 (wlox): yyy.yyy.yyy.yyy
utilisateur@MachineUbuntu:~$ dig MachineUbuntu +noall +answer
MachineUbuntu.                  0 IN A 127.0.1.1
utilisateur@MachineUbuntu:~$ dig MachineUbuntu.domaine-perso.fr +noall +answer
MachineUbuntu.domaine-perso.fr. 0 IN A 10.10.10.1
MachineUbuntu.domaine-perso.fr. 0 IN A aaa.aaa.aaa.aaa
MachineUbuntu.domaine-perso.fr. 0 IN A bbb.bbb.bbb.bbb
…
utilisateur@MachineUbuntu:~$ dig bidon +noall +answer
utilisateur@MachineUbuntu:~$ dig bidon.domaine-perso.fr +noall +answer
```

Si UFW est activé, ouvrir le port DNS sur UFW.

```
utilisateur@MachineUbuntu:~$ sudo ufw allow from 192.168.0.0/16 to any port 53 proto udp
```

Éditer «**/etc/bind/named.conf.local**» pour définir **la zone DNS**

```
zone "domaine-perso.fr" {
  type master;
  file "/etc/bind/db.domaine-perso.fr";
};
zone "10.10.10.in-addr.arpa" {
  type master;
  file "/etc/bind/db.10.10.10";
};
zone "0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.d.f.ip6.arpa." {
  type master;
  file "/etc/bind/db.fd00";
};
```

```
utilisateur@MachineUbuntu:~$ sudo named-checkconf
```

Éditer «**/etc/bind/db.domaine-perso.fr**» pour définir **les alias DNS**

```
$TTL 15m
@             IN SOA     @ root (
          2021082512     ; n° série
                  1h     ; intervalle de rafraîchissement esclave
                 15m     ; intervalle de réessaie pour l’esclave
                  1w     ; temps d’expiration de la copie esclave
                  1h )   ; temps de cache NXDOMAIN

              IN NS      @
              IN A       10.10.10.10
              IN AAAA    fd00::a
              IN MX      2 courriel
; domaine vers adresse IP
gitlab        IN A       10.10.10.1
gitlab        IN AAAA    fd00::
courriel      IN A       10.10.10.2
courriel      IN AAAA    fd00::2
documentation IN A       10.10.10.3
documentation IN AAAA    fd00::3
*             IN A       10.10.10.10
*             IN AAAA    fd00::a
```

Éditer «**/etc/bind/db.10.10.10**» pour définir **les alias inverse DNS**

```
$TTL 15m
@             IN SOA     gitlab.domaine-perso.fr. root.domaine-perso.fr. (
          2021082512     ; n° série
                  1h     ; intervalle de rafraîchissement esclave
                 15m     ; intervalle de réessaie pour l’esclave
                  1w     ; temps d’expiration de la copie esclave
                  1h )   ; temps de cache NXDOMAIN

               IN NS     gitlab.domaine-perso.fr.

; IP vers nom de domaine DNS
1             IN PTR     gitlab.domaine-perso.fr.
2             IN PTR     courriel.domaine-perso.fr.
3             IN PTR     documentation.domaine-perso.fr.
10            IN PTR     domaine-perso.fr.
```

Éditer «**/etc/bind/db.fd00**» pour définir **les alias inverse DNS**

```
$TTL 15m
@             IN SOA     gitlab.domaine-perso.fr. root.domaine-perso.fr. (
          2021082512     ; n° série
                  1h     ; intervalle de rafraîchissement esclave
                 15m     ; intervalle de réessaie pour l’esclave
                  1w     ; temps d’expiration de la copie esclave
                  1h )   ; temps de cache NXDOMAIN

               IN NS     gitlab.domaine-perso.fr.

; IPv6 vers nom de domaine DNS
0             IN PTR     gitlab.domaine-perso.fr.
2             IN PTR     courriel.domaine-perso.fr.
3             IN PTR     documentation.domaine-perso.fr.
a             IN PTR     domaine-perso.fr.
```

```
utilisateur@MachineUbuntu:~$ sudo systemctl restart named
```

##### Vérifier la résolution DNS

```
utilisateur@MachineUbuntu:~$ dig ANY domaine-perso.fr +noall +answer
domaine-perso.fr. 6444 IN SOA domaine-perso.fr. root.domaine-perso.fr. 2021082512 3600 900 604800 3600
domaine-perso.fr. 6444 IN NS domaine-perso.fr.
domaine-perso.fr. 6444 IN A 10.10.10.10
domaine-perso.fr. 6444 IN AAAA fd00::a
domaine-perso.fr. 6444 IN MX 2 courriel.domaine-perso.fr.
utilisateur@MachineUbuntu:~$ dig ANY gitlab.domaine-perso.fr +noall +answer
gitlab.domaine-perso.fr. 6444 IN A 10.10.10.1
gitlab.domaine-perso.fr. 6444 IN AAAA fd00::
utilisateur@MachineUbuntu:~$ dig ANY courriel.domaine-perso.fr +noall +answer
courriel.domaine-perso.fr. 6444 IN A 10.10.10.2
courriel.domaine-perso.fr. 6444 IN AAAA fd00::2
utilisateur@MachineUbuntu:~$ dig ANY documentation.domaine-perso.fr +noall +answer
documentation.domaine-perso.fr. 6444 IN A 10.10.10.3
documentation.domaine-perso.fr. 6444 IN AAAA fd00::3
utilisateur@MachineUbuntu:~$ dig ANY bidon.domaine-perso.fr +noall +answer
bidon.domaine-perso.fr. 6444 IN A 10.10.10.10
bidon.domaine-perso.fr. 6444 IN AAAA fd00::a
```

##### Vérifier la résolution externe

```
utilisateur@MachineUbuntu:~$ dig google.com +noall +answer
google.com. 16 IN A 216.58.223.110
google.com. 32 IN AAAA 2a00:…::200e
…
```

##### Vérifier la résolution inverse

Vous pouvez utiliser la commande `host` ou `dig -x`

```
utilisateur@MachineUbuntu:~$ host 10.10.10.1
1.10.10.10.in-addr-arpa domain name pointer gitlab.domaine-perso.fr.
utilisateur@MachineUbuntu:~$ dig -x 10.10.10.1 +noall +answer
1.10.10.10.in-addr.arpa. 900 IN PTR gitlab.domaine-perso.fr.
utilisateur@MachineUbuntu:~$ dig -x fd00:: +noall +answer
a.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.d.f.ip6.arpa. 900 IN PTR gitlab.domaine-perso.fr.
utilisateur@MachineUbuntu:~$ dig -x 10.10.10.2 +noall +answer
1.10.10.10.in-addr.arpa. 900 IN PTR courriel.domaine-perso.fr.
utilisateur@MachineUbuntu:~$ dig -x fd00::2 +noall +answer
2.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.d.f.ip6.arpa. 900 IN PTR courriel.domaine-perso.fr.
utilisateur@MachineUbuntu:~$ dig -x 10.10.10.3 +noall +answer
1.10.10.10.in-addr.arpa. 900 IN PTR documentation.domaine-perso.fr.
utilisateur@MachineUbuntu:~$ dig -x fd00::3 +noall +answer
3.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.d.f.ip6.arpa. 900 IN PTR documentation.domaine-perso.fr.
utilisateur@MachineUbuntu:~$ dig -x 10.10.10.10 +noall +answer
1.10.10.10.in-addr.arpa. 900 IN PTR domaine-perso.fr.
utilisateur@MachineUbuntu:~$ dig -x fd00::a +noall +answer
a.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.d.f.ip6.arpa. 900 IN PTR domaine-perso.fr.
```

#### Paramétrer définitivement votre DNS pour gitlab

Éditer «**/etc/bind/db.domaine-perso.fr**» pour définir **les alias DNS** définitifs

```
…
               IN NS     @
               IN A      10.10.10.1
               IN AAAA   fd00::
               IN MX     1 courriel
; domaine vers adresse IP
gitlab         IN A      10.10.10.1
gitlab         IN AAAA   fd00::
courriel       IN A      10.10.10.1
courriel       IN AAAA   fd00::1
*              IN A      10.10.10.1
*              IN AAAA   fd00::
```

Éditer «**/etc/bind/db.10.10.10**» pour définir **les alias inverse DNS**

```
$TTL 15m
@              IN SOA    gitlab.domaine-perso.fr. root.domaine-perso.fr. (
           2021082512    ; n° série
                   1h    ; intervalle de rafraîchissement esclave
                  15m    ; intervalle de réessaie pour l’esclave
                   1w    ; temps d’expiration de la copie esclave
                   1h )  ; temps de cache NXDOMAIN

                IN NS    gitlab.domaine-perso.fr.

; IP vers nom de domaine DNS
1               IN PTR   gitlab.domaine-perso.fr.
1               IN PTR   courriel.domaine-perso.fr.
1               IN PTR   domaine-perso.fr.
```

Éditer «**/etc/bind/db.fd00**» pour définir **les alias inverse DNS**

```
$TTL 15m
@              IN SOA    gitlab.domaine-perso.fr. root.domaine-perso.fr. (
           2021082512    ; n° série
                   1h    ; intervalle de rafraîchissement esclave
                  15m    ; intervalle de réessaie pour l’esclave
                   1w    ; temps d’expiration de la copie esclave
                   1h )  ; temps de cache NXDOMAIN

                IN NS   gitlab.domaine-perso.fr.

; IPv6 vers nom de domaine DNS
0               IN PTR   gitlab.domaine-perso.fr.
0               IN PTR   domaine-perso.fr.
1               IN PTR   courriel.domaine-perso.fr.
```

```
utilisateur@MachineUbuntu:~$ sudo systemctl restart named
utilisateur@MachineUbuntu:~$ dig -x 10.10.10.1 +noall +answer
1.10.10.10.in-addr.arpa. 900 IN PTR gitlab.domaine-perso.fr.
1.10.10.10.in-addr.arpa. 900 IN PTR domaine-perso.fr.
1.10.10.10.in-addr.arpa. 900 IN PTR courriel.domaine-perso.fr.
utilisateur@MachineUbuntu:~$ dig -x fd00:: +noall +answer
0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.d.f.ip6.arpa. 900 IN PTR gitlab.domaine-perso.fr.
0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.d.f.ip6.arpa. 900 IN PTR domaine-perso.fr.
utilisateur@MachineUbuntu:~$ dig -x fd00::1 +noall +answer
1.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.d.f.ip6.arpa. 900 IN PTR courriel.domaine-perso.fr.
```

### Installer Docker

Installer les applications de base :

```
utilisateur@MachineUbuntu:~$ sudo apt install docker.io curl openssh-server ca-certificates postfix mailutils
```



![image](images/postfix_1.png)



![image](images/postfix_2.png)



![image](images/postfix_3.png)

Autorisez le compte utilisateur à utiliser docker :

```
utilisateur@MachineUbuntu:~$ sudo usermod -aG docker $USER
```

Démarrez le service docker et ajoutez-le au démarrage du système :

```
utilisateur@MachineUbuntu:~$ sudo systemctl start docker
```

Vérifiez le bon fonctionnement du service docker à l’aide de la commande `systemctl` ci-dessous.

```
utilisateur@MachineUbuntu:~$ systemctl status docker
● docker.service - Docker Application Container Engine
     Loaded: loaded (/lib/systemd/system/docker.service; enabled; vendor preset: enabled)
     Active: active (running) since Fri 2020-10-09 11:07:10 CEST; 47s ago
TriggeredBy: ● docker.socket
       Docs: https://docs.docker.com
   Main PID: 6241 (dockerd)
      Tasks: 12
     Memory: 38.6M
     CGroup: /system.slice/docker.service
             └─6241 /usr/bin/dockerd -H fd://
--containerd=/run/containerd/containerd.sock
q
```

Activez le service au démarrage.

```
utilisateur@MachineUbuntu:~$ sudo systemctl enable docker
utilisateur@MachineUbuntu:~$ ip a
…
4: docker0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc noqueue state DOWN group default qlen 1000
  link/ether 02:42:a3:0c:9c:fb brd ff:ff:ff:ff:ff:ff
  inet 172.17.0.1/16 brd 172.17.255.255 scope global docker0
    valid_lft forever preferred_lft forever
  inet6 fa80::42:a3ff:fe0c:9cfb/64 scope link
    valid_lft forever preferred_lft forever
…
```

Éditer «**/etc/bind/named.conf.option**» pour ajouter l’interface de docker

```
options {
    directory "/var/cache/bind";

    // Pour des raisons de sécurité.
    // Cache la version du serveur DNS pour les clients.
    version "Pas pour les crackers";*

    listen-on { 127.0.0.1; 10.10.10.1; 172.17.0.1; };
    listen-on-v6 { ::1; fd00::; fe80::42:a3ff:fe0c:9cfb; };

    // Optionnel - Comportement par défaut de BIND en récursions.
    recursion yes;

    allow-query { 127.0.0.1; 10.10.10.1; ::1; fd00::; 172.17.0.0/16; fe80::42:a3ff:fe0c:9cfb; };

    // Récursions autorisées seulement pour les interfaces clients
    allow-recursion { 127.0.0.1; 10.10.10.0/24; ::1; fd00::/8; 172.17.0.0/16; fe80::42:a3ff:fe0c:9cfb; };

    dnssec-validation auto;

    // Activer la journalisation des requêtes DNS
    querylog yes;
};
```

```
utilisateur@MachineUbuntu:~$ sudo named-checkconf
```

Redémarrer votre Ubuntu pour valider les modifications

```
utilisateur@MachineUbuntu:~$ reboot
```

### Tester Docker

Après vous être reconnecter sous Ubuntu, vérifiez dans un terminal que docker fonctionne bien en exécutant la commande docker `docker run hello-world` ci-dessous.

```
utilisateur@MachineUbuntu:~$ docker run hello-world
Unable to find image 'hello-world:latest' locally
latest: Pulling from library/hello-world
0e03bdcc26d7: Pull complete
Digest: sha256:ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
Status: Downloaded newer image for hello-world:latest

Hello from Docker!
This message shows that your installation appears to be working correctly.

To generate this message, Docker took the following steps:
  1. The Docker client contacted the Docker daemon.
  2. The Docker daemon pulled the "hello-world" image from the Docker Hub.
    (amd64)
  3. The Docker daemon created a new container from that image which runs the executable that produces the output you are currently reading.
  4. The Docker daemon streamed that output to the Docker client, which sent it to your terminal.

To try something more ambitious, you can run an Ubuntu container with:
  $ docker run -it ubuntu bash

Share images, automate workflows, and more with a free Docker ID: https://hub.docker.com/

For more examples and ideas, visit: https://docs.docker.com/get-started/

utilisateur@MachineUbuntu:~$ docker ps -a
CONTAINER ID   IMAGE         COMMAND    CREATED          STATUS                      PORTS   NAMES
dcd0d025b44b   hello-world   "/hello"   19 seconds ago   Exited (0) 16 seconds ago           elegant_torvalds
```

Nous sommes maintenant prêts à installer GitLab.

### Installer GitLab

GitLab est un gestionnaire de référentiels open source basé sur Rails (langage Rubis) développé par la société GitLab. Il s’agit d’un gestionnaire de révisions de code WEB basé sur git qui permet à votre équipe de collaborer sur le codage, le test et le déploiement d’applications. GitLab fournit plusieurs fonctionnalités, notamment les wikis, le suivi des problèmes, les révisions de code et les flux d’activité.

#### Téléchargez le paquet d’installation GitLab pour Ubuntu et l’installer

Installation longue (prévoir une image VM ou USB ?)

[https://packages.gitlab.com/gitlab/gitlab-ce](https://packages.gitlab.com/gitlab/gitlab-ce) et choisissez la dernière version gitlab-ce pour ubuntu xenial

```
utilisateur@MachineUbuntu:~/gitlab$ wget https://packages.gitlab.com/gitlab/gitlab-ce/packages/ubuntu/focal/gitlab-ce_14.1.3-ce.0_amd64.deb/download.deb
utilisateur@MachineUbuntu:~/gitlab$ sudo apt update ; sudo EXTERNAL_URL="http://gitlab.domaine-perso.fr" dpkg -i download.deb
```

#### Paramétrer GitLab

```
utilisateur@MachineUbuntu:~/gitlab$ sudo gitlab-ctl show-config
utilisateur@MachineUbuntu:~/gitlab$ sudo chmod o+r /etc/gitlab/gitlab.rb
utilisateur@MachineUbuntu:~/gitlab$ sudo nano /etc/gitlab/gitlab.rb
```

```
external_url "http://gitlab.domaine-perso.fr"
# Pour activer les fonctions artifacts (tester la qualité du code, déployer sur un serveur distant en SSH, etc.)
gitlab_rails['artifacts_enabled'] = true
# pour générer la doc et l’afficher avec Gitlab
pages_external_url "http://documentation.domaine-perso.fr"
```

```
utilisateur@MachineUbuntu:~/gitlab$ sudo openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/gitlab/trusted-certs/MachineUbuntu.key -out /etc/gitlab/trusted-certs/MachineUbuntu.crt
utilisateur@MachineUbuntu:~/gitlab$ sudo gitlab-ctl reconfigure
```

### Configurer et tester GitLab

Saisissez dans un navigateur l’URL **gitlab.domaine-perso.fr**



![image](images/gitlab_1.png)

Si vous n’avez pas la fenêtre d’initialisation du mot de passe :

```
utilisateur@MachineUbuntu:~/gitlab$ sudo gitlab-rake "gitlab:password:reset"
```



![image](images/gitlab_2.png)



![image](images/gitlab_3.png)



![image](images/gitlab_4.png)



![image](images/gitlab_5.png)

Tapez la touche **F5** pour rafraîchir l’affichage de votre navigateur.



![image](images/gitlab_6.png)



![image](images/gitlab_7.png)



![image](images/gitlab_8.png)



![image](images/gitlab_9.png)



![image](images/gitlab_10.png)



![image](images/gitlab_11.png)

Intégrer le dépot git local dans Gitlab :

```
utilisateur@MachineUbuntu:~/$ cd repertoire_de_developpement
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git config credential.helper store
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git remote add origin http://gitlab.domaine-perso.fr/utilisateur/initiation_developpement_python_pour_administrateur.git
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git push -u origin --all
Username for 'http://gitlab.domaine-perso.fr': utilisateur
Password for 'http://gitlab.domaine-perso.fr': motdepasse
Énumération des objets: 51, fait.
Décompte des objets: 100% (43/43), fait.
Compression par delta en utilisant jusqu’à 4 fils d’exécution
Compression des objets: 100% (43/43), fait.
Écriture des objets: 100% (51/51), 180.78 Kio \| 4.89 Mio/s, fait.
Total 51 (delta 3), réutilisés 0 (delta 0), réutilisés du pack 0 To http://gitlab.domaine-perso.fr/utilisateur/initiation_developpement_python_pour_administrateur.git
* [new branch] master → master
La branche 'master' est paramétrée pour suivre la branche distante 'master' depuis 'origin'.
```



![image](images/gitlab_12.png)



![image](images/gitlab_13.png)



![image](images/gitlab_14.png)



![image](images/gitlab_15.png)



![image](images/gitlab_16.png)

Vous pouvez maintenant récupérer les nouveaux fichiers d’information Gitlab (**CHANGELOG**, **CONTRIBUTING.md**, **LICENSE** et **README.md**) dans votre projet local :

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git fetch
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git merge
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ ssh-keygen -t rsa -b 2048 -C "Ma clé de chiffrement"
Generating public/private rsa key pair.
Enter file in which to save the key(/home/utilisateur/.ssh/id_rsa):
Created directory '/home/utilisateur/.ssh'.
Enter passphrase (empty for no passphrase): motdepasse
Enter same passphrase again: motdepasse
Your identification has been saved in /home/utilisateur/.ssh/id_rsa
Your public key has been saved in /home/utilisateur/.ssh/id_rsa.pub
The key fingerprint is: SHA256:n60tA2JwGV0tptwB48YrPT6hQQWrxGYhEVegfnO9GXM Ma clé de chiffrement
The key's randomart image is:
+---[RSA 2048]----+
|   +o+ooo+o..    |
|    = ..=..+ .   |
|   . = o+++ o    |
|  . +.oo+o..     |
|   . +o+ S E     |
|    . oo=.X o    |
|      ...=.o .   |
|          .oo    |
|           .o.   |
+----[SHA256]-----+
```



![image](images/gitlab_17.png)



![image](images/gitlab_18.png)



![image](images/gitlab_19.png)



![image](images/gitlab_20.png)

Copier le contenu du fichier «**/home/utilisateur/.ssh/id-rsa.pub**»



![image](images/gitlab_21.png)

### Autorisations pour Docker et le Runner

Cette étape consiste à créer un certificat pour autoriser Docker à interagir avec le registre et le Runner.

Pour le registre :

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ sudo mkdir -p /etc/docker/certs.d/MachineUbuntu:5000
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ sudo ln -s /etc/gitlab/trusted-certs/MachineUbuntu.crt /etc/docker/certs.d/MachineUbuntu:5000/ca.crt
```

Pour le runner :

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ sudo mkdir -p /etc/gitlab-runner/certs
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ sudo ln -s /etc/gitlab/trusted-certs/MachineUbuntu.crt /etc/gitlab-runner/certs/ca.crt
```

### Configurer et tester le Runner

Activation du runner dans docker

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ docker run --rm -it -v /etc/gitlab-runner:/etc/gitlab-runner gitlab/gitlab-runner register
Unable to find image 'gitlab/gitlab-runner:latest' locally
latest: Pulling from gitlab/gitlab-runner
a31c7b29f4ad: Pull complete
d843a3e4344f: Pull complete
cf545e7bed9f: Pull complete
c863409f4294: Pull complete
ba06fc4b920b: Pull complete
Digest: sha256:79692bb4b239cb2c1a70d7726e633ec918a6af117b68da5eac55a00a85f38812
Status: Downloaded newer image for gitlab/gitlab-runner:latest

Runtime platform arch=amd64 os=linux pid=7 revision=8925d9a0 version=14.2.0
Running in system-mode.

Enter the Gitlab instance URL (for example, https://gitlab.com/):
```

Pour activer le runner :



![image](images/gitlab_22.png)



![image](images/gitlab_23.png)



![image](images/gitlab_24.png)

Choisir l’option «**Exécuteurs**» et click sur le bouton «**Étendre**».



![image](images/gitlab_25.png)

Aller dans «**Spécific runners**» dans l’option Exécuteurs.



![image](images/gitlab_26.png)

Informations pour déclarer le runner pour le projet.



![image](images/gitlab_27.png)

```
Enter the GitLab instance URL (for example, https://gitlab.com/): http://gitlab.domaine-perso.fr/
Enter the registration token: 9FfDsP_9Z2cXWi1Axwig
Enter a description for the runner: [75d626bde768]: Runner Developpement Python 3
Enter tags for the runner (comma-separated): runner
Registering runner... succeeded runner=Tzzfs5xc
Enter an executor: kubernetes, custom, docker-ssh, shell, docker+machine, docker-ssh+machine, docker, parallels, ssh, virtualbox: docker
Enter the default Docker image (for example, ruby:2.6): python:latest
Runner registered successfully. Feel free to start it, but if it's running already the config should be automatically reloaded!
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ sudo chmod o+r /etc/gitlab-runner/config.toml
```

Changez dans «**/etc/gitlab-runner/config.toml**» :

```
concurrent = 1
check_interval = 0

[session_server]
  session_timeout = 1800

[[runners]]
  name = "Runner Developpement Python 3"
  url = "http://gitlab.domaine-perso.fr/"
  token = "9FfDsP_9Z2cXWi1Axwig"
  executor = "docker"
  pull_policy = "if-not-present"
  [runners.custom_build_dir]
  [runners.cache]
    [runners.cache.s3]
    [runners.cache.gcs]
    [runners.cache.azure]
  [runners.docker]
    tls_verify = false
    image = "python:latest"
    privileged = false
    disable_entrypoint_overwrite = false
    oom_kill_disable = false
    disable_cache = false
    volumes = ["/var/run/docker.sock:/var/run/docker.sock", "/cache"]
    shm_size = 0
```

Vous pouvez démarrer le Runner

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ docker run -d --restart always --name gitlab-runner -v /etc/gitlab-runner:/etc/gitlab-runner -v /var/run/docker.sock:/var/run/docker.sock gitlab/gitlab-runner:latest
c9f30b11275ac803ebb17209441c7e0b6351c60d9f0ddadc17c8b0a7ae9cbb96
```

Autorisez le registre pour la machine ubuntu

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ sudo ln -s /etc/docker/certs.d/MachineUbuntu\:5000/ca.crt /usr/local/share/ca-certificates/MachineUbuntu.crt
utilisateur@MachineUbuntu:~/**\ **repertoire_de_developpement**\ $ sudo update-ca-certificates
```

Si tout se passe bien vous obtenez le message :

```
Updatting certificates in /etc/ssl/certs...
1 added, 0 removed; done.
Running hooks in /etc/ca-certificates/update.d...
done.
```

Dans «**Specific runners**»  de l’option «**Exécuteurs**» du sous menu «**Intégration et livraison**» du menu «**Paramètres**» du projet apparaît le runner en exécution



![image](images/gitlab_28.png)

Mettre en pause le runner avec le bouton «**Pause**».

Cliquez sur l’icone 

![image](images/gitlab_29.png)

 pour éditer les options du runner, et sélectionnez «**Indique si l’exécuteur peut choisir des tâches sans étiquettes (tags)**» :



![image](images/gitlab_30.png)

Modifier aussi le temps «**Durée maximale d’exécution de la tâche**» avec «**30m**»

Relancer l’exécution du runner pour valider les modifications.



![image](images/gitlab_31.png)

#### Tester le fonctionnement du runner

Éditer le fichier «**.gitlab-ci.yml**» dans **repertoire_de_developpement**.

```
travail-de-construction:
  stage: build
  script:
    - echo "Bonjour, $GITLAB_USER_LOGIN !"

travail-de-tests-1:
  stage: test
  script:
    - echo "Ce travail teste quelque chose"

travail-de-tests-2:
  stage: test
  script:
    - echo "Ce travail teste quelque chose, mais prend plus de temps que travail-de-test-1."
    - echo "Une fois les commandes echo terminées, il exécute la commande de veille pendant 20 secondes"
    - echo "qui simule un test qui dure 20 secondes de plus que travail-de-test-1."
    - sleep 20

deploiement-production:
  stage: deploy
  script:
    - echo "Ce travail déploie quelque chose de la branche $CI_COMMIT_BRANCH."
```

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git add .
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git commit -m "Test du runner dans Gitlab"
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git push
```



![image](images/gitlab_32.png)

On peut voir l’activité en cours du runner avec l’icône : 

![image](images/gitlab_33.png)


Dans le sous menu «**Pipelines**» du menu «**Intégration et livraison**» du projet on peut voir les taches d’exécution du runner :



![image](images/gitlab_34.png)

On voit ici la tache «**Travail-de-construction**» en cours dans la phase de «**Build**» de l’exécuteur.



![image](images/gitlab_35.png)

Si on clique sur cette icône on voit les opérations en cours de la tache :



![image](images/gitlab_36.png)

Une fois la tache réussi, l’exécuteur passe dans la phase d’exécution des tests.



![image](images/gitlab_37.png)

On peut voir le résultat en cliquant sur les icônes des taches de tests.



![image](images/gitlab_38.png)



![image](images/gitlab_39.png)

Puis après l’exécuteur passe dans la phase «**Deploy**».



![image](images/gitlab_40.png)



![image](images/gitlab_41.png)

Test du déploiement docker :

```
default:
  image: python:latest
```

Pour plus d’informations sur Gitlab et son utilisation [https://github.com/SocialGouv/tutoriel-gitlab](https://github.com/SocialGouv/tutoriel-gitlab), [https://makina-corpus.com/blog/metier/2019/gitlab-astuces-projets](https://makina-corpus.com/blog/metier/2019/gitlab-astuces-projets).

### Tester les Pages GitLab

#### Créer un projet de rendu de pages HTML

Créer un nouveau projet



![image](images/gitlab_42.png)

Création depuis un modèle



![image](images/gitlab_43.png)



![image](images/gitlab_44.png)

Choisir «**Pages/Plain HTML**» comme modèle



![image](images/gitlab_45.png)

Renseignez :


* le nom du projet «**HTML**»


* La description du projet «**Test des GitLab Pages**»


* Le niveau de sécurité «**Public**»



![image](images/gitlab_46.png)



![image](images/gitlab_47.png)

#### Créer le «runner» pour ce projet



![image](images/gitlab_48.png)



![image](images/gitlab_49.png)

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ docker run --rm -it -v /etc/gitlab-runner:/etc/gitlab-runner gitlab/gitlab-runner register
Runtime platform arch=amd64 os=linux pid=7 revision=8925d9a0 version=14.2.0
Running in system-mode.

Enter the GitLab instance URL (for example, https://gitlab.com/): http://gitlab.domaine-perso.fr/
Enter the registration token: 7YBLdSA9en4NMex5zyQy
Enter a description for the runner: [75d626bde768]: Runner Test Pages GitLab
Enter tags for the runner (comma-separated): runner
Registering runner... succeeded runner=Tzzfs5xc
Enter an executor: kubernetes, custom, docker-ssh, shell, docker+machine, docker-ssh+machine, docker, parallels, ssh, virtualbox: docker
Enter the default Docker image (for example, ruby:2.6): alpine:latest
Runner registered successfully. Feel free to start it, but if it's running already the config should be automatically reloaded!
```

Changez dans «**/etc/gitlab-runner/config.toml**» :

```
concurrent = 1
check_interval = 0

[session_server]
  session_timeout = 1800

[[runners]]
  name = "Runner Developpement Python 3"
  url = "http://gitlab.domaine-perso.fr/"
  token = "9FfDsP_9Z2cXWi1Axwig"
  executor = "docker"
  [runners.custom_build_dir]
  [runners.cache]
    [runners.cache.s3]
    [runners.cache.gcs]
    [runners.cache.azure]
  [runners.docker]
    tls_verify = false
    image = "python:latest"
    privileged = false
    disable_entrypoint_overwrite = false
    oom_kill_disable = false
    disable_cache = false
    volumes = ["/var/run/docker.sock:/var/run/docker.sock", "/cache"]
    shm_size = 0

[[runners]]
  name = "Runner Test Pages GitLab"
  url = "http://gitlab.domaine-perso.fr/"
  token = "7YBLdSA9en4NMex5zyQy"
  executor = "docker"
  pull_policy = "if-not-present"
  [runners.custom_build_dir]
  [runners.cache]
    [runners.cache.s3]
    [runners.cache.gcs]
    [runners.cache.azure]
  [runners.docker]
    tls_verify = false
    image = "alpine:latest"
    privileged = false
    disable_entrypoint_overwrite = false
    oom_kill_disable = false
    disable_cache = false
    volumes = ["/var/run/docker.sock:/var/run/docker.sock", "/cache"]
    shm_size = 0
```

Vous pouvez configurer et redémarrer le Runner



![image](images/gitlab_50.png)



![image](images/gitlab_51.png)



![image](images/gitlab_52.png)

Modifiez l’option «**Indique si l’exécuteur peut choisir des tâches sans étiquettes (tags)**» pour l’activer. Et préciser une durrée maximale d’exécution de «**30m**»



![image](images/gitlab_53.png)

Enregirtrer les modifications et relancer le runner



![image](images/gitlab_54.png)

##### Déployer et tester le HTML dans une Pages GitLab



![image](images/gitlab_55.png)

Éditer le fichier «**gitlab-ci.yml**» avec GitLab en cliquant sur le bouton 

![image](images/gitlab_56.png)




![image](images/gitlab_57.png)

Renseigner le Message de commit «**Mise à jour du fichier .gitlab-ci.yml pour le lancement du runner**». Puis cliquer sur le bouton «**Commit changes**»



![image](images/gitlab_58.png)



![image](images/gitlab_59.png)

Cliquer sur la tache «**Pages**» sans annuler la tache ( l’icône Cancel de l’image )



![image](images/gitlab_60.png)



![image](images/gitlab_61.png)

Dans le menu «**Dépôt**» avec le sous menu «**Commits**» on peut voir la réussite de la tâche suite au commit.



![image](images/gitlab_62.png)

Maintenant il ne manque plus qu’a récupérer le site web de la page html.

Pour cela allons dans le menu «**Paramètres**»,  le sous menu «**Pages**» du projet.



![image](images/gitlab_63.png)



![image](images/gitlab_64.png)

La présence du lien «[http://utilisateur.documentation.domaine-perso.fr/html](http://utilisateur.documentation.domaine-perso.fr/html)» nous confirme que GitLab fonctionne avec les Pages.

Un click sur ce lien et on vérifie l’accès au site web.



![image](images/gitlab_65.png)

Supprimez le projet («**Paramètres/Général/Advenced/Delete project**»), et nettoyez le runner de test «**Runner Test Pages GitLab**» du fichier «**/etc/gitlab-runner/config.toml**».

# Les principaux éléments de syntaxe

## Syntaxe et grammaire Python

Plusieurs choses sont nécessaires pour écrire un code lisible : **la syntaxe**, **l’organisation du code**, **le découpage en fonctions** (et possiblement en classes que nous verrons avec les objets), mais souvent, aussi, **le bon sens**.

Pour cela, les «**PEP**» pour **P**ython **E**nhancement **P**roposal (proposition d’amélioration de Python) peuvent nous aider.

Distribuer document pep8.pdf.

On va aborder dans ce chapitre sans doute la plus célèbre des PEP, à savoir la PEP 8 [https://www.python.org/dev/peps/pep-0008/](https://www.python.org/dev/peps/pep-0008/), qui est incontournable lorsque l’on veut écrire du code Python correctement.

La «**Style Guide for Python Code**» est une des plus anciennes PEP (les numéros sont croissants avec le temps). Elle consiste en un nombre important de recommandations sur la syntaxe de Python.

Il est vivement recommandé de lire la PEP 8 en entier au moins une fois pour avoir une bonne vue d’ensemble en complément de ce cours. On ne présentera ici qu’un rapide résumé de cette PEP 8.

### L’identation

Dans la plus part des langages de programmation, l’indentation du code (c’est-à-dire la manière d’écrire le code en laissant des espaces de décalage en début des lignes) est laissée au choix éclairé du développeur. Mais, force est de constater, parfois le développeur n’est pas des plus experts pour rendre lisible par les autres, et même lui même, son code…



![image](images/GestionExpaces_1.png)

Python oblige donc le développeur à structurer son code à l’aide des indentations : ce sont elles qui détermineront les blocs (séquences d’instructions liées) et non les accolades comme dans la majorité des langages.



![image](images/GestionExpaces_2.png)

Les blocs de code sont déterminés par :


* La présence du caractère «:» en fin de ligne ;


* Une indentation des lignes suivantes à l’aide de tabulations ou d’espaces.



![image](images/GestionExpaces_3.png)

Attention à ne pas mélanger les tabulations avec les espaces pour l’indentation, Python n’aime pas ça du tout. Votre code ne fonctionnera pas et vous n’obtiendrez pas de message d’erreur explicite. Je conseille l’utilisation de quatre caractères espace pour faire une indentation.

### Les commentaires

Commentaires sur une ligne :

```
>>> # Ceci est le premier commentaire
>>> bidon = 1 # et ceci est le second commentaire
>>>           # ... et là le troisième!
>>> "# Ceci n’est pas un commentaire parce qu’il est entre guillemets."
```

Commentaires sur plusieurs lignes :

```
>>> """
... Ceci est un commentaire
... en plusieurs lignes
... qui sera ignoré lors de l'exécution
... """
```

### Chaînes de caractères

**Les chaînes de caractères** (ou chaînes) sont des séquences de lettres et de nombres, ou, en d’autres termes, des morceaux de textes. Elles sont entourées par deux guillemets.

Par exemple :

```
>>> "Bonjour, Python!"
```

Comment faire si vous voulez insérer des guillemets «**"**» à l’intérieur d’une chaîne ?

Si vous essayez à l’interpréteur :

```
>>> "J'ai dit "Wow!" très fort"
  File "<stdin>", line 1
  "J'ai dit "Wow!" très fort"
             ^
  SyntaxError: invalid syntax
```

Cela génère une erreur.

Le problème est que Python voit une chaîne, `"J'ai dit "` suivie de quelque chose qui n’est pas une chaîne: **Wow!** . Ce n’est pas ce que nous voulions!

Python propose deux moyens simples d’insérer des guillemets à l’intérieur d’une chaîne.

Vous pouvez commencer et terminer une chaîne littérale avec des apostrophes «**'**» à la place des guillemets, par exemple :

```
>>> 'bla bla'
```

Les guillemets peuvent ainsi être placés à l’intérieur :

```
>>> 'Tu as dit "Wow!" très fort.'
```

Vous pouvez placer une barre oblique inversée suivie du guillemet ou de l’apostrophe (`"` ou `'` ). Cela s’appelle une séquence d’échappement.

Python va supprimer la barre oblique inversée et n’afficher que le guillemet ou l’apostrophe à l’intérieur de la chaîne. A cause des séquences d’échappement, **la barre oblique inversée (\\)** est un symbole spécial.

Pour l’inclure dans une chaîne, il faut l’échapper avec une deuxième barre oblique inversée, en d’autres termes, **il faut écrire** `\\` dans votre chaîne littérale.

Voici un exemple que vous pouvez tester avec l’interpréteur :

```
>>> 'L\'exemple avec un apostrophe.'
>>> "Voici un \"échappement\" de guillemets"
>>> "Un exemple d’échappement \
... pour écrire sur plusieurs lignes\
... un texte long"
```

Pourquoi le dernier exemple fonctionne ?

### Majuscules et Minuscules (Variables, instructions, fonctions, objets)

Nous abordons ici les règles de nommage.

Voir document pep8.pdf déjà distribué.

Les noms de **variables**, de **fonctions** et de **modules** doivent être de la forme :

```
ma_variable
fonction_test_27()
mon_module
```

C’est-à-dire en minuscules avec un caractère «**souligné**» `_` («**tiret du bas**» ou underscore en anglais) pour séparer les différents «**mots**» dans le nom.

Les **constantes** sont écrites en majuscules :

```
MA_CONSTANTE
VITESSE_LUMIÈRE
```

Les noms de **classes** et les **exceptions** sont de la forme :

```
MaClasse
MonException
```

Pensez à **donner à vos variables des noms qui ont du sens**.

Évitez autant que possible les a1, a2, i, truc, toto…

Les noms de variables à un caractère sont néanmoins autorisés pour les boucles et les indices :

```
>>> ma_liste = [1, 3, 5, 7, 9, 11]
>>> for i in range(len(ma_liste)):
...     ma_liste[i]
...
...
1
3
5
7
9
11
```

Enfin, des **noms de variable à une lettre peuvent être utilisés lorsque cela a un sens mathématique** (par exemple, les noms x, y et z évoquent des coordonnées cartésiennes).

### Gestion des espaces

La PEP 8 recommande **d’entourer les opérateurs +, -, /, \*, ==, !=, >=, not, in, and, or… d’un espace**, avant et après.

Par exemple :

```
# code recommandé
ma_variable = 3 + 7
mon_texte = "souris"
mon_texte == ma_variable
# code non recommandé :
ma_variable=3+7
mon_texte="souris"
mon_texte== ma_variable
```

Il n’y a, par contre, **pas d’espace** à **l’intérieur** des crochets **[]**, des accolades **{}** et des parenthèses **()** :

```
# code recommandé :
ma_liste[1]
mon_dico{"clé"}
ma_fonction(argument)
# code non recommandé :
ma_liste[ 1 ]
mon_dico{"clé" }
ma_fonction( argument )
```

Ni juste **avant** la parenthèse **(** ouvrante d’une fonction ou le crochet **{** ouvrant d’une liste ou d’un dictionnaire :

```
# code recommandé :
ma_liste[1]
mon_dico{"clé"}
ma_fonction(argument)
# code non recommandé :
ma_liste [1]
mon_dico {"clé"}
ma_fonction (argument)
```

On met **un espace après** les caractères **:** et **,** (mais pas avant) :

```
# code recommandé :
ma_liste = [1, 2, 3]
mon_dico = {"clé1": "valeur1", "clé2": "valeur2"}
ma_fonction(argument1, argument2)
# code non recommandé :
ma_liste = [1 , 2 ,3]
mon_dico = {"clé1":"valeur1", "clé2":"valeur2"}
ma_fonction (argument1 ,argument2)
```

Par contre, pour **les tranches de listes**, on ne met **pas d’espace** autour du **:** :

```
# code recommandé :
ma_liste = [1, 3, 5, 7, 9, 1]
ma_liste[1:3]
ma_liste[1:4:2]
ma_liste[::2]
# code non recommandé :
ma_liste[1 : 3]
ma_liste[1: 4:2 ]
ma_liste[ : :2]
```

Enfin, on n’ajoute **pas plusieurs espaces** autour du **=** ou des autres opérateurs pour faire joli :

```
# code recommandé :
x1 = 1
x2 = 3
x_old = 5
# code non recommandé :
x1    = 1
x2    = 3
x_old = 5
```

### Les règles de base d’écriture des fonctions/procédures

Maintenant que vous êtes prêt à écrire des programmes plus longs et plus complexes, il est temps de parler du style de codage. La plupart des langages peuvent être écrits (ou plutôt formatés) selon différents styles ; certains sont plus lisibles que d’autres. Rendre la lecture de votre code plus facile aux autres est toujours une bonne idée, et adopter un bon style de codage peut énormément vous y aider.


* **Utilisez des indentations de 4 espaces et pas de tabulations**. 4 espaces constituent un bon compromis entre une indentation courte (qui permet une profondeur d’imbrication plus importante) et une longue (qui rend le code plus facile à lire). Les tabulations introduisent de la confusion et doivent être proscrites autant que possible.


* Faites en sorte que **les lignes ne dépassent pas 79 caractères**, au besoin en insérant des retours à la ligne (**actuellement cela a évolué vers 127**). Vous facilitez ainsi la lecture pour les utilisateurs qui n’ont qu’un petit écran et, pour les autres, cela leur permet de visualiser plusieurs fichiers côte à côte.


* **Utilisez des lignes vides pour séparer les fonctions et les classes**, ou pour scinder de gros blocs de code à l’intérieur de fonctions.


* Lorsque c’est possible, **placez les commentaires sur leurs propres lignes**.


* **Utilisez les chaînes de documentation**.


* **Utilisez des espaces autour des opérateurs et après les virgules**, mais pas juste à l’intérieur des parenthèses : `a = f(1, 2) + g(3, 4)`.


* **Nommez toujours vos classes et fonctions de la même manière** ; la convention est d’utiliser une notation [UpperCamelCase](https://medium.com/@anthowelc/c-est-quoi-le-camelcase-7fa02dc7fcee) pour **les classes**, et **minuscules_avec_trait_bas** pour **les fonctions et méthodes**. Utilisez toujours `self` comme **nom du premier argument des méthodes** (voyez Une première approche des classes pour en savoir plus sur les classes et les méthodes).


* N’utilisez pas d’encodage exotique dès lors que votre code est censé être utilisé dans des environnements internationaux. Par défaut, Python travaille en [UTF-8](https://www.w3.org/International/questions/qa-what-is-encoding.fr). Préférez les caractères du simple [ASCII](https://fr.wikibooks.org/wiki/Les_ASCII_de_0_%C3%A0_127/La_table_ASCII) pour votre code. N’utilisez pas de caractères exotiques lorsque votre code est censé être utilisé dans des environnements internationaux.

## Utilisation de Python comme calculatrice

L’interpréteur agit comme une simple calculatrice. Vous pouvez lui entrer une expression et il vous affiche la valeur.

Distribuer document codage_nombres.pdf.

La syntaxe des expressions est simple, les opérateurs `+`, `-`, `\*` et `/` fonctionnent comme dans la plupart des langages (par exemple, Pascal ou C) ; les parenthèses peuvent être utilisées pour faire des regroupements. Par exemple :

```
>>> 2 + 2
4
>>> 50 - 5 \ 6
20
>>> (50 - 5 * 6) / 4
5.0
>>> 8 / 5  # la division retourne toujours un nombre à virgule flottant
1.6
```

Les nombres entiers (comme 2, 4, 20) sont de type **int**, alors que les décimaux (comme 5.0, 1.6) sont de type **float**.

Les **divisions** `/` donnent toujours des **float**.

Utilisez **l’opérateur** `//` pour effectuer des **divisions entières**, afin d’obtenir un résultat entier.

Pour obtenir **le reste** d’une division entière, utilisez **l’opérateur** `%` :

```
>>> 17 / 3 # la division classique renvoie un nombre à virgule flottante
5.666666666666667
>>> 17 // 3  # division entière, ne tient pas compte du reste
5
>>> 17 % 3  # l’opérateur % retourne le reste de la division
2
>>> 5 * 3 + 2  # le quotien * diviseur + reste
17
```

En Python, il est possible de calculer des puissances avec l’opérateur `\*\*` :

```
>>> 5 ** 2  # 5 au carré
25
>>> 2 ** 7  # 2 à la puissance 7
128
```

Les **nombres à virgule flottante** sont tout à fait admis (Python utilise le point «**.**» comme séparateur entre la partie entière et la partie décimale des nombres, c’est la convention anglo-saxonne), **les opérateurs avec des opérandes de types différents convertissent l’opérande de type entier en type virgule flottante** :

```
>>> 4 * 3.75 - 1
14.0
```

En plus des **int** et des **float**, il existe les **Décimal** et les **Fraction** avec l’utilisation d’une bibliothèque.

Python gère aussi **les nombres complexes**, en utilisant le suffixe «**j**» ou «**J**» pour indiquer la partie imaginaire (tel que `3+5j`).

```
>>> (3+5j) * (3-5j)
(34+0j)
>>> _.conjugate()
(34+0j)
>>> (34+0j).real
34.0
>>> 34 + 0j
(34+0j)
>>> _.imag
0.0
```

## Les variables

### L’affectation dans le code

Le signe égal `=` est utilisé pour affecter une valeur à une variable.

Dans ce cas, aucun résultat n’est affiché avant l’invite suivante :

```
>>> largeur = 20
>>> hauteur = 5 * 9
>>> largeur * hauteur
900
```

Si une variable n’est pas définie (si aucune valeur ne lui a été affectée), son utilisation produit une erreur :

```
>>> n # Essaye d'accéder à une variable non définie
Traceback (most recent call last):
File "<input>", line 1, in <module>
 n # Essaye d'accéder à une variable non définie
NameError: name 'n' is not defined
```

En mode interactif, la dernière expression affichée est affectée à la variable `_`. Ainsi, lorsque vous utilisez Python comme calculatrice, cela vous permet de continuer des calculs facilement, par exemple :

```
>>> taxe = 12.5 / 100
>>> prix = 100.50
>>> prix * taxe
12.5625
>>> prix + _
113.0625
>>> round(_, 2)
113.06
```

Cette variable doit être considérée comme une variable en lecture seule par l’utilisateur. N’affectez pas de valeur explicitement à `_`. Vous créeriez ainsi une variable locale indépendante, avec le même nom, qui masquerait la variable native et son fonctionnement magique.

### L’affectation au clavier

#### La fonction input

```
>>> nom = input('Saisissez votre nom : ')
Saisissez votre nom : PERSONNE
>>> 'Bonjour ' + nom
'Bonjour PERSONNE'
```

### L’affectation par variables passées à un script

#### Passage d’arguments en ligne de commande

Python supporte complètement la création de programmes qui peuvent être lancés en ligne de commande, à l’aide d’arguments et de drapeaux longs ou cours pour spécifier diverses options.

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ mkdir 4_Passage_paramètres ; cd 4_Passage_paramètres
utilisateur@MachineUbuntu:~/repertoire_de_developpement/4_Passage_paramètres$ nano litparams.py ; chmod u+x litparams.py
```

```
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys

for arg in sys.argv:
    print(arg)
```

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement/4_Passage_paramètres$ ./litparams.py -a --help bidon
```

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement/4_Passage_paramètres$ nano litargs.py ; chmod u+x litargs.py
```

```
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys

print('Nombre d\'arguments :', len(sys.argv), 'arguments.')
print('Liste des arguments :', str(sys.argv))
```

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement/4_Passage_paramètres$ ./litargs.py -a --help bidon ; cd ..
```

## L’affichage d’informations

Il existe bien des moyens de présenter les sorties d’un programmes ; les données peuvent être affichées sous une forme lisible par un être humain ou sauvegardées dans un fichier pour une utilisation future. Cette partie présente quelques possibilités.

### À l’écran du terminal

```
print()
```

#### Formatage de données

Souvent vous voudrez plus de contrôle sur le formatage de vos sorties chaîne de caractères et aller au delà d’un affichage de valeurs séparées par des espaces. **Il y a plusieurs moyens de formater ces chaînes de caractères pour l’affichage**

##### Les expressions formatées f' {} '

Commencez une chaîne de caractère avec **f** ou **F** avant d’ouvrir vos guillemets doubles ou simple. Dans ces chaînes de caractère, vous pouvez entrer des expressions Python entre les accolades `{}` qui peuvent contenir des variables ou des valeurs littérales.

```
>>> année = 2005
>>> évènement = 'Sky'
>>> f'En {année} : {évènement}'
'En 2005 : Sky'
```

##### La méthode str.format()

Les chaînes de caractères exige un plus grand effort manuel. Vous utiliserez toujours les accolades `{}` pour indiquer où une variable sera substituée et suivant des détails sur son formatage. Vous devrez également fournir les informations à formater.

```
>>> votes_oui = 42572654
>>> votes_non = 43132495
>>> pourcentage = votes_oui / (votes_oui + votes_non)
>>> '{:-9} votes OUI {:2.2%}'.format(votes_oui, pourcentage)
' 42572654 votes OUI 49.67%'
```

##### Concaténations de tranches de chaînes

Enfin, vous pouvez construire des concaténations de chaînes vous-même et modifier leur format texte, et ainsi créer n’importe quel agencement.

Le type des chaînes a des méthodes utiles pour aligner des chaînes, pour afficher suivant une taille fixe, suivant la casse, etc.

```
>>> s = 'coucou mon texte'
>>> dir(s)
['__add__', '__class__', '__contains__', '__delattr__', '__dir__',   '__doc__', '__eq__', '__format__', '__ge__', '__getattribute__', '__getitem__', '__getnewargs__', '__gt__', '__hash__', '__init__', '__init_subclass__', '__iter__', '__le__', '__len__', '__lt__', '__mod__', '__mul__', '__ne__', '__new__', '__reduce__', '__reduce_ex__', '__repr__', '__rmod__', '__rmul__', '__setattr__', '__sizeof__', '__str__', '__subclasshook__', 'capitalize', 'casefold', 'center', 'count', 'encode', 'endswith', 'expandtabs', 'find', 'format', 'format_map', 'index', 'isalnum', 'isalpha', 'isascii', 'isdecimal', 'isdigit', 'isidentifier', 'islower', 'isnumeric', 'isprintable', 'isspace', 'istitle', 'isupper', 'join', 'ljust', 'lower', 'lstrip', 'maketrans', 'partition', 'replace', 'rfind', 'rindex', 'rjust', 'rpartition', 'rsplit', 'rstrip', 'split', 'splitlines', 'startswith', 'strip', 'swapcase', 'title', 'translate', 'upper', 'zfill']
```

La bibliothèque de fonctions «**string**» contient une classe **Template** qui permet aussi de remplacer des valeurs au sein de chaînes de caractères, en utilisant des marqueurs comme `$x`, et en les remplaçant par les valeurs d’un dictionnaire, mais sa capacité à formater les chaînes est plus limitée.

#### La sortie en chaînes de caractères

Lorsqu’un affichage basique suffit, pour afficher simplement une variable pour en inspecter le contenu, vous pouvez convertir n’importe quelle valeur ou objet en chaîne de caractères en utilisant la fonction `repr()` ou la fonction `str()`.

**La fonction** `str()` est destinée à représenter les valeurs sous une forme lisible par un être humain.

**La fonction** `repr()` est destinée à générer des représentations qui puissent être lues par l’interpréteur (ou qui lèvera une **SyntaxError** s’il n’existe aucune syntaxe équivalente).

Pour les objets qui n’ont pas de représentation humaine spécifique, `str()` renvoie la même valeur que `repr()`.

Beaucoup de valeurs, comme les nombres ou les structures telles que les listes ou les dictionnaires, ont la même représentation en utilisant les deux fonctions. Les chaînes de caractères, en particulier, ont deux représentations distinctes.

Quelques exemples :

```
>>> s = 'Bonjour à tous :-)'
>>> str(s)
'Bonjour à tous :-)'
>>> repr(s)
"'Bonjour à tous :-)'"
>>> str(1/7)
'0.14285714285714285'
>>> x = 10 * 3.25
>>> y = 200 * 200
>>> s = 'La valeur de x est ' + repr(x) + ', et celle de y est ' + repr(y) + '...'
>>> print(s)
La valeur de x est 32.5, et celle de y est 40000...
>>> # repr() ajoute les guillemets et les barres obliques inverses d'une chaîne
>>> salut = 'Bonjour à tous :-)\n'
>>> salutations = repr(salut)
>>> print(salutations)
'Bonjour à tous :-)\n'
>>> # L'argument de repr() peut être n'importe quel objet Python
>>> repr((x, y, ('bidon', 'œufs')))
"(32.5, 40000, ('bidon', 'œufs'))"
```

#### Les *chaînes* de caractères formatées (f-strings)

**Les chaînes de caractères formatées** `f''` (aussi appelées **f-strings**) vous permettent d’inclure la valeur d’expressions Python dans des chaînes de caractères en les préfixant avec «**f**» ou «**F**» et écrire des expressions comme {expression}.

L’expression peut être suivie d’un spécificateur de format. Cela permet un plus grand contrôle sur la façon dont la valeur est rendue. L’exemple suivant arrondit pi à trois décimales après la virgule :

```
>>> import math
>>> print(f'La valeur de pi est approximativement {math.pi:.3f}.')
La valeur de pi est approximativement 3.142.
```

Donner un entier après les “:” `f'{variable:10}'` indique la largeur minimale de ce champ en nombre de caractères. C’est utile pour faire de jolis tableaux :

```
>>> table = {'Sylvie': 4127, 'Jacques': 4098, 'David': 7678}
>>> for nom, téléphone in table.items():
...     print(f'{nom:10} ==> {téléphone:10d}')
...
...
Sylvie     ==>       4127
Jacques    ==>       4098
David      ==>       7678
```

D’autres modificateurs peuvent être utilisés pour convertir la valeur avant son formatage. «**!a**» applique la fonction `ascii()`, «**!s**» applique la fonction :`str()`, et «**!r**» applique la fonction `repr()` :

```
>>> animaux = 'rats Womp'
>>> print(f'Mon aéroglisseur est plein de {animaux}.')
Mon aéroglisseur est plein de rats Womp.
>>> print(f'Mon aéroglisseur est plein de {animaux!r}.')
Mon aéroglisseur est plein de 'rats Womp'.
```

Pour plus d’informations sur ces spécifications de formats, voir dans le guide Python en ligne «[Mini-langage de spécification de format](https://docs.python.org/fr/3/library/string.html#formatspec)».

#### La méthode de chaîne de caractères format()

L’utilisation de base de la méthode str.format() ressemble à ceci :

```
>>> print('Le {} nous dit "{}!"'.format('Sith', 'utilise le coté obscur de la force'))
Le Sith nous dit "utilise le coté obscur de la force!"
```

Les accolades et les caractères à l’intérieur (appelés les champs de formatage) sont remplacés par les objets passés en paramètres à la méthode `str.format()`. Un nombre entre accolades se réfère à la position de l’objet passé à la méthode `str.format()`.

```
>>> print('{0} et {1}'.format('bidon', 'pub'))
bidon et pub
>>> print('{1} et {0}'.format('bidon', 'pub'))
pub et bidon
```

Si des arguments nommés sont utilisés dans la méthode str.format(), leurs valeurs sont utilisées en se basant sur le nom des arguments

```
>>> print('Cet aliment {nourriture} est {qualité}.'.format(nourriture = 'hamburger', qualité='chimique'))
Cet aliment hamburger est chimique.
```

Les arguments positionnés et nommés peuvent être combinés arbitrairement :

```
>>> print('L’histoire de {0}, {1}, et {autre}.'.format('Bernard', 'Martin', autre='George'))
L’histoire de Bernard, Martin, et George.
```

Si vous avez une chaîne de formatage vraiment longue que vous ne voulez pas découper, il est possible de référencer les variables à formater par leur nom plutôt que par leur position. Utilisez simplement un dictionnaire et la notation entre crochets «**[]**» pour accéder aux clés.

```
>>> table = {'Sylvie': 4127, 'Jacques': 4098, 'Daniel': 8637678}
>>> print('Jacques: {0[Jacques]:d}; Sylvie: {0[Sylvie]:d}; ' 'Daniel: {0[Daniel]:d}'.format(table))
Jacques: 4098; Sylvie: 4127; Daniel: 8637678
```

Vous pouvez obtenir le même résultat en passant le tableau comme des arguments nommés en utilisant la notation «**\*\***».

```
>>> table = {'Sylvie': 4127, 'Jacques': 4098, 'Daniel': 8637678}
>>> print('Jacques: {Jacques:d}; Sylvie: {Sylvie:d}; Daniel: {Daniel:d}'.format(**table))
Jacques: 4098; Sylvie: 4127; Daniel: 8637678
```

C’est particulièrement utile en combinaison avec la fonction native `vars()` qui renvoie un dictionnaire contenant toutes les variables locales.

A titre d’exemple, les lignes suivantes produisent un ensemble de colonnes alignées de façon ordonnée donnant les entiers, leurs carrés et leurs cubes :

```
>>> for x in range(1, 11):
...     print('{0:2d} {1:3d} {2:4d}'.format(x, x*x, x*x*x))
...
...
1   1    1
2   4    8
3   9   27
4  16   64
5  25  125
6  36  216
7  49  343
8  64  512
9  81  729
10 100 1000
```

Pour avoir une description complète du formatage des chaînes de caractères avec la méthode `str.format()`, lisez «Syntaxe de formatage de chaîne».

### Logs Système

#### Utiliser logging

Nous allons aborder ici en avance un module *logging* qui fournit un ensemble de fonctions pour une utilisation simple d’affichages de logs dans une application avec une possibilité de filtrage de la verbosité.
Ces fonctions sont `debug()`, `info()`, `warning()`, `error()` et `critical()`.

Pour déterminer quand employer la journalisation, voyez la table ci-dessous, qui vous indique, pour chaque tâche parmi les plus communes, l’outil approprié.

#### Choix du niveau d’information

| Tâche que vous souhaitez mener

                                                                                                                                | Le meilleur outil pour cette tâche

                                                                                                                                                                                                                                                                |
| ------------------------------------------------------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Affiche la sortie console d’un script en ligne de commande ou d’un programme lors de son utilisation ordinaire

                                                | [print()](https://docs.python.org/fr/3/library/functions.html#print)

                                                                                                                                                                                                                                                                                           |
| Rapporter des évènements qui ont lieu au cours du fonctionnement normal d’un programme (par exemple pour suivre un statut ou examiner des dysfonctionnements)

 | [logging.info()](https://docs.python.org/fr/3/library/logging.html#logging.info)

ou

[logging.debug()](https://docs.python.org/fr/3/library/logging.html#logging.debug)

pour une sortie très détaillée à visée diagnostique

                                                                                                                                                                                                          |
| Émettre un avertissement (*warning* en anglais) en relation avec un évènement particulier au cours du fonctionnement d’un programme

                             | [warnings.warn()](https://docs.python.org/fr/3/library/warnings.html#warnings.warn)

dans le code de la bibliothèque si le problème est évitable et l’application cliente doit être modifiée pour éliminer cet avertissement

[logging.warning()](https://docs.python.org/fr/3/library/logging.html#logging.warning)

si l’application cliente ne peut rien faire pour corriger la situation mais l’évènement devrait quand même être noté

 |
| Rapporter une erreur lors d’un évènement particulier en cours d’exécution

                                                                                     | Lever une exception

                                                                                                                                                                                                                                                                               |
| Rapporter la suppression d’une erreur sans lever d’exception (par exemple pour la gestion d’erreur d’un processus de long terme sur un serveur)

               | [logging.error()](https://docs.python.org/fr/3/library/logging.html#logging.error),

[logging.exception()](https://docs.python.org/fr/3/library/logging.html#logging.exception)

ou [logging.critical()](https://docs.python.org/fr/3/library/logging.html#logging.critical),

au mieux, selon l’erreur spécifique et le domaine d’application

                                                                                                                                                                    |
Les fonctions de journalisation sont nommées d’après le niveau ou la sévérité des évènements qu’elles suivent. Les niveaux standards et leurs applications sont décrits ci-dessous (par ordre croissant de sévérité) :

#### Typologie des journalisations

| Niveau

                                                                                                                                                        | Pourqoi c’est utilisé

                                                                                                                                                                                                                                                                             |
| ------------------------------------------------------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| **DEBUG**

                                                                                                                                                         | Information détaillée, intéressante seulement lorsqu’on diagnostique un problème.

                                                                                                                                                                                                                 |
| **INFO**

                                                                                                                                                          | Confirmation que tout fonctionne comme prévu.

                                                                                                                                                                                                                                                     |
| **WARNING**

                                                                                                                                                       | L’indication que quelque chose d’inattendu a eu lieu, ou de la possibilité d’un problème dans un futur proche (par exemple « espace disque faible »). Le logiciel fonctionne encore normalement.

                                                                                                  |
| **ERROR**

                                                                                                                                                         | Du fait d’un problème plus sérieux, le logiciel n’a pas été capable de réaliser une tâche.

                                                                                                                                                                                                        |
| **CRITICAL**

                                                                                                                                                      | Une erreur sérieuse, indiquant que le programme lui-même pourrait être incapable de continuer à fonctionner.

                                                                                                                                                                                      |
Le niveau par défaut est WARNING, ce qui signifie que seuls les évènements de ce niveau et au-dessus sont suivis, sauf si le paquet logging est configuré pour faire autrement.

Les évènements suivis peuvent être gérés de différentes façons. La manière la plus simple est de les afficher dans la console. Une autre méthode commune est de les écrire dans un fichier.

#### Un exemple simple

Un exemple très simple est :

```
>>> import logging
>>> logging.warning('Attention!') # affiche un message dans la console
WARNING:root:Attention!
>>> logging.info('C’est bon relâche la pression') # n’imprime rien
```

Si vous entrez ces lignes dans un script que vous exécutez, vous verrez `WARNING:root:Attention!` » affiché dans la console. Le message INFO n’apparaît pas parce que le niveau par défaut est WARNING. Le message affiché inclut l’indication du niveau et la description de l’évènement fournie dans l’appel à logging, ici «Attention!». Ne vous préoccupez pas de la partie «root» pour le moment : nous détaillerons ce point plus bas. La sortie elle-même peut être formatée de multiples manières si besoin. Les options de formatage seront aussi expliquées plus bas.

#### Enregistrer les évènements dans un fichier

Il est très commun d’enregistrer les évènements dans un fichier, c’est donc ce que nous allons regarder maintenant. Il faut essayer ce qui suit avec un interpréteur Python nouvellement démarré, ne poursuivez pas la session commencée ci-dessus :

```
>>> import logging
>>> logging.basicConfig(filename='./exemple.log', encoding='utf-8', level=logging.DEBUG)
>>> logging.debug('Ce message doit aller dans le fichier journal')
>>> logging.info('Celui là aussi')
>>> logging.warning('Et encore celui-ci')
>>> logging.error('Et des trucs non-ASCII aussi, comme Fêtes de Noël')
```

**Modifié dans la version 3.9**: L’argument d’encodage a été ajouté.

Dans les versions antérieures de Python ou lorsqu’il n’est pas spécifié, l’encodage utilisé est la valeur par défaut utilisée par `open()`.
Bien que cela ne soit pas montré dans l’exemple ci-dessus, un argument d’erreurs peut également maintenant être passé, qui détermine comment les erreurs de codage sont gérées. Pour les valeurs disponibles et les valeurs par défaut, consultez la documentation de `open()`.

Maintenant, si nous ouvrons le fichier «**exemple.log**» et lisons ce qui s’y trouve, on trouvera les messages de log :

```
DEBUG:root:Ce message doit aller dans le fichier journal
INFO:root:Celui là aussi
WARNING:root:Et encore celui-ci
ERROR:root:Et des trucs non-ASCII aussi, comme Fêtes de Noël
```

Cet exemple montre aussi comment on peut régler le niveau de journalisation qui sert de seuil pour le suivi. Dans ce cas, comme nous avons réglé le seuil à DEBUG, tous les messages ont été écrits.

#### Régler le niveau de journalisation d’un script

Si vous souhaitez régler le niveau de journalisation à partir d’une option de la ligne de commande comme :

```
--log=INFO
```

Créer avec votre éditeur de texte le fichier «**niveau_journalisation.py**».

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ mkdir 5_Niveau_journalisation ; cd 5_Niveau_journalisation
utilisateur@MachineUbuntu:~/repertoire_de_developpement/5_Niveau_journalisation$ nano niveau_journalisation.py ; chmod u+x niveau_journalisation.py
```

```
#! /usr/bin/env python3
# -*- coding: utf8 -*-

import argparse, logging

# Récupère l'argument de la ligne de commande du paramètre log et le met dans la variable loglevel
params = argparse.ArgumentParser()
params.add_argument('--log')
args = params.parse_args()
loglevel = args.log

# Défini le niveau de journalisation
if loglevel:
    numeric_level = getattr(logging, loglevel.upper())
else:
    numeric_level = logging.DEBUG

# Teste si le paramètre est valide
if not isinstance(numeric_level, int):
    raise ValueError('Niveau de journalisation invalide : %s' % loglevel)

# Configure le niveau de journalisation et le fichier où journaliser
logging.basicConfig(filename='niveau.log', filemode='w', level=numeric_level)

# Messages de tests
logging.error('Message erreur')
logging.warning('Message alerte')
logging.info('Message information')
logging.debug('Message debug')
```

Vous passez la valeur du paramètre donné à l’option `--log` dans une variable **loglevel**. L’appel à `basicConfig()` doit être fait avant un appel à `debug()`, `info()`, etc. Si vous exécutez le script plusieurs fois **sans l’option «filemode»**, les messages des exécutions successives sont ajoutés au fichier «**niveau.log**».

Si vous voulez que chaque exécution reprenne un fichier vierge, sans conserver les messages des exécutions précédentes, vous devez spécifier l’argument **filemode** à **'w'**. Le texte n’est plus ajouté au fichier de log, donc les messages des exécutions précédentes sont perdus.

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement/5_Niveau_journalisation$ ./niveau_journalisation.py
utilisateur@MachineUbuntu:~/repertoire_de_developpement/5_Niveau_journalisation$ cat niveau.log
ERROR:root:Message erreur
WARNING:root:Message alerte
INFO:root:Message information
DEBUG:root:Message debug
utilisateur@MachineUbuntu:~/repertoire_de_developpement/5_Niveau_journalisation$ python3 ./niveau_journalisation.py --log=DEBUG
utilisateur@MachineUbuntu:~/repertoire_de_developpement/5_Niveau_journalisation$ cat niveau.log
ERROR:root:Message erreur
WARNING:root:Message alerte
INFO:root:Message information
DEBUG:root:Message debug
utilisateur@MachineUbuntu:~/repertoire_de_developpement/5_Niveau_journalisation$ python3 ./niveau_journalisation.py --log=INFO
utilisateur@MachineUbuntu:~/repertoire_de_developpement/5_Niveau_journalisation$ cat niveau.log
ERROR:root:Message erreur
WARNING:root:Message alerte
INFO:root:Message information
utilisateur@MachineUbuntu:~/repertoire_de_developpement/5_Niveau_journalisation$ python3 ./niveau_journalisation.py --log=WARNING
utilisateur@MachineUbuntu:~/repertoire_de_developpement/5_Niveau_journalisation$ cat niveau.log
ERROR:root:Message erreur
WARNING:root:Message alerte
utilisateur@MachineUbuntu:~/repertoire_de_developpement/5_Niveau_journalisation$ python3 ./niveau_journalisation.py --log=ERROR
utilisateur@MachineUbuntu:~/repertoire_de_developpement/5_Niveau_journalisation$ cat niveau.log
ERROR:root:Message erreur
utilisateur@MachineUbuntu:~/repertoire_de_developpement/5_Niveau_journalisation$ python3 ./niveau_journalisation.py --log=BIDON ; cd ..
Traceback (most recent call last):
  File "./niveau_journalisation.py", line 14, in <module>
    numeric_level = getattr(logging, loglevel.upper())
AttributeError: module 'logging' has no attribute 'BIDON'
```

#### Modifier le format du message affiché

Pour changer le format utilisé pour afficher le message, vous devez préciser le format que vous souhaitez employer :

```
>>> import logging
>>> logging.basicConfig(format='%(levelname)s:%(message)s', level=logging.DEBUG)
>>> logging.debug('Message d’analyse de code')
DEBUG:Message d’analyse de code
>>> logging.basicConfig(format='Mon programme %(levelname)s:%(lineno)d:%(pathname)s:%(message)s', level=logging.DEBUG, force=True)
>>> logging.debug('Message d’analyse de code')
Mon programme DEBUG:1:<bpython-input-7>:Message d’analyse de code
>>> logging.info('Message d’information')
Mon programme INFO:1:<bpython-input-8>:Message d’information
>>> logging.warning('Attention!')
Mon programme WARNING:1:<bpython-input-9>:Attention!
```

### GUI

Gui intégré client lourd (Windows, gtk, qt, etc.).

Gui web (remi, django, etc.).

On verra cela plus loin dans le cours.

## Les types de variables

### Logique

#### Booléen

```
>>> a = True
>>> type(a)
<class 'bool'>
>>> b = False
>>> type(b)
<class 'bool'>
```

Les tests

```
>>> a = 10 > 9
>>> a
True
>>> a = 10 == 9
>>> a
False
```

Les valeurs vraie

```
>>> bool("abc")
True
>>> bool(123)
True
>>> bool(["apple", "cherry", "banana"])
True
```

Les valeurs Nules

```
bool(False)
bool(None)
bool(0)
bool("")
bool(())
bool([])
bool({})
```

### Chaînes de caractères

#### Chaîne de caractère ASCII

Python 3

```
byte()
```

Python 2

```
str()
```

#### Chaîne de caractère Unicode

Python 3

```
str()
```

Python 2

```
unicode()
```

#### Manipulation des chaînes de caractères

Python sait manipuler des chaînes de caractères, qui peuvent être exprimées de différentes manières. **Elles peuvent être écrites entre guillemets anglo-saxon simples ('…') ou entre guillemets anglo-saxon doubles ("…") sans distinction. \\ peut aussi être utilisé pour protéger un guillemet** :

```
>>> 'inutile bidon' # simples quotes
'inutile bidon'
>>> 'L\'apostrophe' # utilise \\' pour échapper le simple quote…
"L'apostrophe"
>>> "L'apostrophe" # …ou on utilise des doubles quotes
"L'apostrophe"
>>> 'Son nom est "Personne"'
'Son nom est "Personne"'
>>> "Son nom est \"Personne\""
'Son nom est "Personne"'
>>> 'Python c’est "l\'avenir"'
'Python c’est "l\'avenir"'
```

En mode interactif, l’interpréteur affiche les chaînes de caractères entre guillemets. Les guillemets et autres caractères spéciaux sont protégés avec des barres obliques inverses (backslash en anglais). Bien que cela puisse être affiché différemment de ce qui a été entré (les guillemets peuvent changer), les deux formats sont équivalents. La chaîne est affichée entre guillemets si elle contient un guillemet simple et aucun guillemet, sinon elle est affichée entre guillemets simples. **La fonction print() affiche les chaînes de manière plus lisible, en retirant les guillemets et en affichant les caractères
spéciaux qui étaient protégés par une barre oblique inverse** :

```
>>> print('Python c’est "l\'avenir"')
Python c’est "l'avenir"
>>> s = 'Première ligne.\nSeconde ligne.' # \n c’est nouvelle ligne
>>> s # sans print(), \n est incluse dans la sortie
'Première ligne.\nSeconde ligne.'
>>> print(s) # avec print(), \n est traduit comme une nouvelle ligne
Première ligne.
Seconde ligne.
```

**Si vous ne voulez pas que les caractères précédés d’un \\ soient interprétés** comme étant spéciaux, utilisez les chaînes brutes (raw strings en anglais) en préfixant la chaîne d’un **r** :

```
>>> print('C:\son\nom') # \n veut dire nouvelle ligne!
C:\son
om
>>> print(r'C:\son\nom') # avec r avant le quote
C:\son\nom
```

**Les chaînes de caractères peuvent s’étendre sur plusieurs lignes. Utilisez alors des triples guillemets, simples ou doubles : '''…''' ou """…"""**. Les retours à la ligne sont automatiquement inclus, mais on peut l’empêcher en ajoutant \\ à la fin de la ligne. L’exemple suivant :

```
>>> print("""\
... Utilisation: programme [OPTIONS]
...     -h            Affiche ce message d’utilisation
...     -H nomMachine Nom de la machine où se connecter
... """)
```

produit l’affichage suivant (notez que le premier retour à la ligne n’est pas inclus) :

```
Utilisation: programme [OPTIONS]
    -h            Affiche ce message d’utilisation
    -H nomMachine Nom de la machine où se connecter
```

**Les chaînes peuvent être concaténées** (collées ensemble) avec l’opérateur «**+**» et **répétées** avec l’opérateur «**\***» :

```
>>> # 2 fois 'an', suivit par 'as'
>>> 2 * 'an' + 'as'
'ananas'
```

Plusieurs **chaînes de caractères**, écrites littéralement (c’est-à-dire entre guillemets), **côte à côte, sont automatiquement concaténées**.

```
>>> 'Py' 'thon'
'Python'
```

Cette fonctionnalité est surtout intéressante pour couper des chaînes trop longues :

```
>>> texte = ('Mettez plusieurs chaînes entre les parenthèses '
... 'pour les avoir réunis.')
>>> texte
'Mettez plusieurs chaînes entre les parenthèses pour les avoir réunis.'
```

**Cela ne fonctionne cependant qu’avec les chaînes littérales, pas avec les variables ni les expressions** :

```
>>> prefixe = 'Py'
>>> prefixe 'thon' # impossible de concaténer une variable et une chaîne littérale
File "<input>", line 1
 prefixe 'thon' # impossible de concaténer une variable et une chaîne littérale
         ^
SyntaxError: invalid syntax
>>> ('un' * 3) 'ium'
File "<input>", line 1
 ('un' * 3) 'ium'
            ^
SyntaxError: invalid syntax
```

Pour **concaténer des variables**, ou des variables avec des chaînes littérales, utilisez l’opérateur «**+**» :

```
>>> prefixe + 'thon'
'Python'
```

**Les chaînes de caractères peuvent être indexées** (ou indicées, c’est-à-dire que l’on peut accéder aux caractères par leur position), le premier caractère d’une chaîne étant à la position 0. Il n’existe pas de type distinct pour les caractères, un caractère est simplement une chaîne de longueur 1 :

Pour visualiser la façon dont les indices fonctionnent

```
Position :   1   2   3   4   5   6
           +---+---+---+---+---+---+
           | P | y | t | h | o | n |
           +---+---+---+---+---+---+
Indice :     0   1   2   3   4   5
```

Exemples :

```
>>> mot = 'Python'
>>> mot[0] # caractère en position 1
'P'
>>> mot[5] # caractère en position 6
'n'
```

Les indices peuvent également être négatifs, on compte alors en partant de la droite. Par exemple :

Pour visualiser la façon dont les indices négatifs fonctionnent

```
Position :   1   2   3   4   5   6
           +---+---+---+---+---+---+
           | P | y | t | h | o | n |
           +---+---+---+---+---+---+
Indice :    -6  -5  -4  -3  -2  -1
```

Exemples :

```
>>> mot[-1] # dernier caractère
'n'
>>> mot[-2] # avant-dernier caractère
'o'
>>> mot[-6]
'P'
```

Notez que, comme -0 égale 0, les indices négatifs commencent par -1.

En plus d’accéder à un élément par son indice, il est aussi possible de « trancher » (slice en anglais) une chaîne. Accéder à une chaîne par un indice permet d’obtenir un caractère, trancher permet d’obtenir une sous-chaîne :

Pour mémoriser la façon dont les tranches fonctionnent, vous pouvez imaginer que les indices pointent entre les caractères, le côté gauche du premier caractère ayant la position 0. Le côté droit du dernier caractère d’une chaîne de n caractères a alors pour indice n.

```
Position :   1   2   3   4   5   6
           +---+---+---+---+---+---+
           | P | y | t | h | o | n |
           +---+---+---+---+---+---+
           0   1   2   3   4   5   6
          -6  -5  -4  -3  -2  -1
```

La première ligne de nombres donne la position des indices 0…6 dans la chaîne ; la deuxième ligne donne l’indice négatif correspondant. La tranche de i à j est constituée de tous les caractères situés entre les bords libellés i et j, respectivement.

Pour des indices non négatifs, la longueur d’une tranche est la différence entre ces indices, si les deux sont entre les bornes. Par exemple, la longueur de mot[1:3] est 2.

Exemples :

```
>>> mot[0:2] # caractères de la position 1 (inclus) à 3 (exclus)
'Py'
>>> mot[2:5] # caractères de la position 3 (inclus) à 6 (exclus)
'tho'
>>> mot[-6:-4] # caractères de la position 1 (inclus) à 3 (exclus)
'Py'
>>> mot[-4:-1] # caractères de la position 3 (inclus) à 6 (exclus)
'tho'
```

**Notez que le début est toujours inclus et la fin toujours exclue**. Cela assure que s[:i] + s[i:] est toujours égal à s :

```
>>> mot[:2] + mot[2:]
'Python'
>>> mot[:4] + mot[4:]
'Python'
```

Les valeurs par défaut des indices de tranches ont une utilité ; le premier indice vaut zéro par défaut (c.-à-d. lorsqu’il est omis), le deuxième correspond par défaut à la taille de la chaîne de caractères

```
>>> mot[:2] # caractère du début à la position 3 (exclu)
'Py'
>>> mot[4:] # caractères de la position 5 (inclus) à la fin
'on'
>>> mot[-2:] # caractères de l'avant-dernier (inclus) à la fin
'on'
```

Utiliser un indice trop grand produit une erreur :

```
>>> mot[42] # le mot n'a que 6 caractères
Traceback (most recent call last):
File "<input>", line 1, in <module>
 mot[42] # le mot n'a que 6 caractères
IndexError: string index out of range
```

Cependant, les indices hors bornes sont gérés silencieusement lorsqu’ils sont utilisés dans des tranches :

```
>>> mot[4:42]
'on'
>>> mot[42:]
''
```

Les chaînes de caractères, en Python, ne peuvent pas être modifiées. On dit qu’elles sont immuables. Affecter une nouvelle valeur à un indice dans une chaîne produit une erreur :

```
>>> mot[0] = 'J'
Traceback (most recent call last):
File "<input>", line 1, in <module>
 mot[0] = 'J'
TypeError: 'str' object does not support item assignment
>>> mot[2:] = 'py'
Traceback (most recent call last):
File "<input>", line 1, in <module>
 mot[2:] = 'py'
TypeError: 'str' object does not support item assignment
```

Si vous avez besoin d’une chaîne différente, vous devez en créer une nouvelle :

```
>>> 'J' + mot[1:]
'Jython'
>>> mot[:2] + 'py'
'Pypy'
```

La fonction native len() renvoie la longueur d’une chaîne :

```
>>> s = 'anticonstitutionnellement'
>>> len(s)
25
```

### Nombres

Vu avec la calculatrice python

#### Nombre entier optimisé (int)

Python 2

```
int()
```

#### Nombre entier de taille arbitraire (long int)

Python 3

```
int()
```

Python 2

```
long()
```

#### Nombre à virgule flottante

```
float()
```

#### Nombre complexe

```
complex()
```

### Données multiples

Python connaît différents types de données combinés, utilisés pour regrouper plusieurs valeurs.

#### Liste de longueur fixe

```
tuple()
```

le tuple (ou n-uplet, dénomination que nous utiliserons dans la suite de cette documentation).

Un n-uplet consiste en différentes valeurs séparées par des virgules, par exemple :

```
>>> t = 12345, 54321, 'hello!'
>>> t[0]
12345
>>> t
(12345, 54321, 'hello!')
>>> # Les tuples peuvent être imbriqués
>>> u = t, (1, 2, 3, 4, 5)
>>> u
((12345, 54321, 'hello!'), (1, 2, 3, 4, 5))
>>> # Les tuples sont immuables
>>> t[0] = 88888
Traceback (most recent call last):
File "<input>", line 1, in <module>
 t[0] = 88888
TypeError: 'tuple' object does not support item assignment
>>> # mais ils peuvent contenir des objets mutables
>>> v = ([1, 2, 3], [3, 2, 1])
>>> v
([1, 2, 3], [3, 2, 1])
```

Comme vous pouvez le voir, les n-uplets sont toujours affichés entre parenthèses, de façon à ce que des n-uplets imbriqués soient interprétés correctement ; ils peuvent être saisis avec ou sans parenthèses, même si celles-ci sont souvent nécessaires (notamment lorsqu’un n-uplet fait partie d’une expression plus longue). Il n’est pas possible d’affecter de valeur à un élément d’un n-uplet ; par contre, il est possible de créer des n-uplets contenant des objets muables, comme des listes.

Si les n-uplets peuvent sembler similaires aux listes, ils sont souvent utilisés dans des cas différents et pour des raisons différentes. Les n-uplets sont immuables et contiennent souvent des séquences hétérogènes d’éléments qui sont accédés par « dissociation » (unpacking en anglais, voir plus loin) ou par indice (ou même par attributs dans le cas des namedtuples). Les listes sont souvent muables et contiennent des éléments généralement homogènes qui sont accédés par itération sur la liste.

Un problème spécifique est la construction de n-uplets ne contenant aucun ou un seul élément : la syntaxe a quelques tournures spécifiques pour s’en accommoder. Les n-uplets vides sont construits par une paire de parenthèses vides ; un n-uplet avec un seul élément est construit en faisant suivre la valeur par une virgule (il n’est pas suffisant de placer cette valeur entre parenthèses). Pas très joli, mais efficace.

Par exemple :

```
>>> vide = ()
>>> singleton = 'bonjour', # <-- noter la virgule de fin
>>> len(vide)
0
>>> len(singleton)
1
>>> singleton
('bonjour',)
```

L’instruction `t = 12345, 54321, 'hello !'` est un exemple d’une agrégation de n-uplet (tuple packing en anglais) : les valeurs «12345», «54321» et «hello !» sont agrégées ensemble dans un n-uplet. L’opération inverse est aussi possible :

```
>>> x, y, z = t
```

Ceci est appelé, de façon plus ou moins appropriée, une distribution de séquence (sequence unpacking en anglais) et fonctionne pour toute séquence placée à droite de l’expression. Cette distribution requiert autant de variables dans la partie gauche qu’il y a d’éléments dans la séquence. Notez également que cette affectation multiple est juste une combinaison entre une agrégation de n-uplet et une distribution de séquence.

#### Les ensembles

```
set()
```

Python fournit également un type de donnée pour les ensembles. Un ensemble est une collection non ordonnée sans élément dupliqué. Des utilisations basiques concernent par exemple des tests d’appartenance ou des suppressions de doublons. Les ensembles savent également effectuer les opérations mathématiques telles que les unions, intersections, différences et différences symétriques.

Des accolades ou la fonction set() peuvent être utilisés pour créer des ensembles. **Notez que pour créer un ensemble vide, {} ne fonctionne pas**, cela crée un dictionnaire vide. **Utilisez plutôt set()**.

Voici une brève démonstration :

```
>>> panier = {'pomme', 'orange', 'pomme', 'poire', 'orange', 'banane'}
>>> print(panier) # montre que les doublons ont été supprimés
{'poire', 'pomme', 'banane', 'orange'}
>>> 'orange' in panier # test d'adhésion rapide
True
>>> 'digitaire' in panier # la digitaire est une plante
False
>>> # Démontrer les opérations d'ensemble sur des lettres uniques à partir de deux mots
>>> a = set('abracadabra')
>>> b = set('alacazam')
>>> a # lettres uniques dans a
{'b', 'a', 'c', 'r', 'd'}
>>> a - b # lettres en a mais pas en b
{'r', 'b', 'd'}
>>> a | b # lettres en a ou b ou les deux
{'b', 'a', 'c', 'r', 'm', 'l', 'z', 'd'}
>>> a & b # lettres en a et b
{'a', 'c'}
>>> a ^ b # lettres en a ou b mais pas les deux
{'r', 'b', 'm', 'l', 'z', 'd'}
```

Il est possible d’écrire des expressions dans des ensembles :

```
>>> a = {x for x in 'abracadabra' if x not in 'abc'}
>>> a
{'r', 'd'}
```

#### Liste de longueur variable

```
list()
```

Le plus souple est la liste, qui peut être écrit comme une suite, placée entre crochets, de valeurs (éléments) séparées par des virgules. Les listes et les chaînes de caractères ont beaucoup de propriétés en commun, comme l’indiçage et les opérations sur des tranches. Les éléments d’une liste ne sont pas obligatoirement tous du même type, bien qu’à l’usage ce soit souvent le cas.

```
>>> carrés = [1, 4, 9, 16, 25]
>>> carrés
[1, 4, 9, 16, 25]
```

Comme les chaînes de caractères (et toute autre type de séquence), les listes peuvent être indicées et découpées :

```
>>> carrés[0] # l'indexation renvoie l'élément
1
>>> carrés[-1]
25
>>> carrés[-3:] # slicing renvoie une nouvelle liste
[9, 16, 25]
```

Toutes les opérations par tranches renvoient une nouvelle liste contenant les éléments demandés. Cela signifie que l’opération suivante renvoie une copie distincte de la liste :

```
>>> carrés[:]
[1, 4, 9, 16, 25]
```

Les listes gèrent aussi les opérations comme les concaténations :

```
>>> carrés + [36, 49, 64, 81, 100]
[1, 4, 9, 16, 25, 36, 49, 64, 81, 100]
```

Mais à la différence des chaînes qui sont immuables, les listes sont muables : il est possible de modifier leur contenu :

```
>>> cubes = [1, 8, 27, 63, 125] # Quelque chose ne va pas ici
>>> 4 ** 3 # le cube de 4 est 64, pas 63!
64
>>> cubes[3] = 64 # remplacer la mauvaise valeur
>>> cubes
[1, 8, 27, 64, 125]
```

Il est aussi possible d’ajouter de nouveaux éléments à la fin d’une liste avec la méthode append() (les méthodes sont abordées plus tard) :

```
>>> cubes.append(216) # ajouter le cube de 6
>>> cubes.append(7 ** 3) # et le cube de 7
>>> cubes
[1, 8, 27, 64, 125, 216, 343]
```

Des affectations de tranches sont également possibles, ce qui peut même modifier la taille de la liste ou la vider complètement :

```
>>> lettres = ['a', 'b', 'c', 'd', 'e', 'f', 'g']
>>> lettres
['a', 'b', 'c', 'd', 'e', 'f', 'g']
>>> # remplacer certaines valeurs
>>> lettres[2:5] = ['C', 'D', 'E']
>>> lettres
['a', 'b', 'C', 'D', 'E', 'f', 'g']
>>> # maintenant supprimez-les
>>> lettres[2:5] = []
>>> lettres
['a', 'b', 'f', 'g']
>>> # effacer la liste en remplaçant tous les éléments par une liste vide
>>> lettres[:] = []
>>> lettres
[]
```

La primitive len() s’applique aussi aux listes :

```
>>> lettres = ['a', 'b', 'c', 'd']
>>> len(lettres)
4
```

Il est possible d’imbriquer des listes (c’est à dire de créer des listes contenant d’autres listes).

Par exemple :

```
>>> a = ['a', 'b', 'c']
>>> n = [1, 2, 3]
>>> x = [a, n]
>>> x
[['a', 'b', 'c'], [1, 2, 3]]
>>> x[0]
['a', 'b', 'c']
>>> x[0][1]
'b'
```

##### Premiers pas vers la programmation

Bien entendu, on peut utiliser Python pour des tâches plus compliquées que d’additionner deux et deux. Par exemple, on peut écrire le début de la suite de Fibonacci comme ceci :

```
>>> # Série de Fibonacci
>>> # la somme de deux éléments définit le suivant
>>> a, b = 0, 1
>>> while a < 10:
... print(a)
... a, b = b, a+b
...
...
0
1
1
2
3
5
8
```

Cet exemple introduit plusieurs nouvelles fonctionnalités.

**La première ligne contient une affectation multiple** : les variables a et b se voient affecter simultanément leurs nouvelles valeurs 0 et 1. Cette méthode est encore utilisée à la dernière ligne, pour démontrer que les expressions sur la partie droite de l’affectation sont toutes évaluées avant que les affectations ne soient effectuées. Ces expressions en partie droite sont toujours évaluées de la gauche vers la droite.

**La boucle while s’exécute tant que la condition** (ici : a < 10) reste vraie. En Python, comme en C, tout entier différent de zéro est vrai et zéro est faux. La condition peut aussi être une chaîne de caractères, une liste, ou en fait toute séquence ; une séquence avec une valeur non nulle est vraie, une séquence vide est fausse. Le test utilisé dans l’exemple est une simple comparaison. Les opérateurs de comparaison standards sont écrits comme en C : < (inférieur), > (supérieur), == (égal), <= (inférieur ou égal), >= (supérieur ou égal) et != (non égal).

**Le corps de la boucle est indenté** : l’indentation est la méthode utilisée par Python pour regrouper des instructions. En mode interactif, vous devez saisir une tabulation ou des espaces pour chaque ligne indentée. En pratique, vous aurez intérêt à utiliser un éditeur de texte pour les saisies plus compliquées ; tous les éditeurs de texte dignes de ce nom disposent d’une fonction d’auto-indentation. Lorsqu’une expression composée est saisie en mode interactif, elle doit être suivie d’une ligne vide pour indiquer qu’elle est terminée (car l’analyseur ne peut pas deviner que vous venez de saisir la dernière ligne). Notez bien que toutes les lignes à l’intérieur d’un bloc doivent être indentées au même niveau.

**La fonction print() écrit les valeurs des paramètres qui lui sont fournis**. Ce n’est pas la même chose que d’écrire l’expression que vous voulez afficher (comme nous l’avons fait dans l’exemple de la calculatrice), en raison de la manière qu’a **print()** de gérer les paramètres multiples, les nombres décimaux et les chaînes. Les chaînes sont affichées sans apostrophe et une espace est insérée entre les éléments de telle sorte que vous pouvez facilement formater les choses, comme ceci :

```
>>> i = 256*256
>>> print('La valeur de i est', i)
La valeur de i est 65536
```

Le paramètre nommé **end** peut servir pour enlever le retour à la ligne ou pour terminer la ligne par une autre chaîne :

```
>>> a, b = 0, 1
>>> while a < 1000:
... print(a, end=',')
... a, b = b, a+b
...
...
0,1,1,2,3,5,8,13,21,34,55,89,144,233,377,610,987,>>>
```

Lexique à distribuer :

Le type liste dispose de méthodes supplémentaires. Voici toutes les méthodes des objets de type liste :

```
list.append(x)
```

Ajoute un élément à la fin de la liste. Équivalent à a[len(a):] = [x].

```
list.extend(iterable)
```

Étend la liste en y ajoutant tous les éléments de l’itérable. Équivalent à a[len(a):] = iterable.

```
list.insert(i, x)
```

Insère un élément à la position indiquée. Le premier argument est la position de l’élément courant avant lequel l’insertion doit s’effectuer, donc a.insert(0, x) insère l’élément en tête de la liste et a.insert(len(a), x) est équivalent à a.append(x).

```
list.remove(x)
```

Supprime de la liste le premier élément dont la valeur est égale à x. Une exception ValueError est levée s’il n’existe aucun élément avec cette valeur.

```
list.pop([i])
```

Enlève de la liste l’élément situé à la position indiquée et le renvoie en valeur de retour. Si aucune position n’est spécifiée, a.pop() enlève et renvoie le dernier élément de la liste (les crochets autour du i dans la signature de la méthode indiquent que ce paramètre est facultatif et non que vous devez placer des crochets dans votre code ! Vous retrouverez cette notation fréquemment dans le Guide de Référence de la Bibliothèque Python).

```
list.clear()
```

Supprime tous les éléments de la liste. Équivalent à del a[:].

```
list.index(x[, start[, end]])
```

Renvoie la position du premier élément de la liste dont la valeur égale x (en commençant à compter les positions à partir de zéro). Une exception ValueError est levée si aucun élément n’est trouvé.

Les arguments optionnels start et end sont interprétés de la même manière que dans la notation des tranches et sont utilisés pour limiter la recherche à une sous-séquence particulière. L’indice renvoyé est calculé relativement au début de la séquence complète et non relativement à start.

```
list.count(x)
```

Renvoie le nombre d’éléments ayant la valeur x dans la liste.

```
list.sort(key=None, reverse=False)
```

Ordonne les éléments dans la liste (les arguments peuvent personnaliser l’ordonnancement, voir sorted() pour leur explication).

```
list.reverse()
```

Inverse l’ordre des éléments dans la liste.

```
list.copy()
```

Renvoie une copie superficielle de la liste. Équivalent à a[:].

Exemple suivant utilise la plupart des méthodes des listes :

```
>>> fruits = ['orange', 'pomme', 'poire', 'banane', 'kiwi', 'pomme', 'banane']
>>> fruits.count('pomme')
2
>>> fruits.count('mandarine')
0
>>> fruits.index('banane')
3
>>> fruits.index('banane', 4) # Trouver la prochaine banane à partir d'une position 4
6
>>> fruits.reverse()
>>> fruits
['banane', 'pomme', 'kiwi', 'banane', 'poire', 'pomme', 'orange']
>>> fruits.append('raisin')
>>> fruits
['banane', 'pomme', 'kiwi', 'banane', 'poire', 'pomme', 'orange', 'raisin']
>>> fruits.sort()
>>> fruits
['banane', 'banane', 'kiwi', 'orange', 'poire', 'pomme', 'pomme', 'raisin']
>>> fruits.pop()
'raisin'
>>> fruits
['banane', 'banane', 'kiwi', 'orange', 'poire', 'pomme', 'pomme']
```

Vous avez probablement remarqué que les méthodes telles que **insert**, **remove** ou **sort**, qui ne font que modifier la liste, n’affichent pas de valeur de retour (elles renvoient None) 1. C’est un principe respecté par toutes les structures de données variables en Python.

Une autre chose que vous remarquerez peut-être est que toutes les données ne peuvent pas être ordonnées ou comparées. Par exemple, [None, “hello”, 10] ne sera pas ordonné parce que les entiers ne peuvent pas être comparés aux chaînes de caractères et None ne peut pas être comparé à d’autres types. En outre, il existe certains types qui n’ont pas de relation d’ordre définie. Par exemple, 3+4j < 5+7j n’est pas une comparaison valide.

Approfondir chez soit ou au travail voir [https://docs.python.org/fr/3/tutorial/datastructures.html#using-lists-as-stacks](https://docs.python.org/fr/3/tutorial/datastructures.html#using-lists-as-stacks) et la suite

#### Dictionnaire

```
dict()
```

Un autre type de donnée très utile, natif dans Python, est le dictionnaire (voir Les types de correspondances — **dict**). Ces dictionnaires sont parfois présents dans d’autres langages sous le nom de « mémoires associatives » ou de « tableaux associatifs ». À la différence des séquences, qui sont indexées par des nombres, **les dictionnaires sont indexés par des clés**, qui peuvent être de n’importe quel type immuable ; les chaînes de caractères et les nombres peuvent toujours être des clés. Des n-uplets peuvent être utilisés comme clés s’ils ne contiennent que des chaînes, des nombres ou des n-uplets ; si un n-uplet contient un objet muable, de façon directe ou indirecte, il ne peut pas être utilisé comme une clé. Vous ne pouvez pas utiliser des
listes comme clés, car les listes peuvent être modifiées en place en utilisant des affectations par position, par tranches ou via des méthodes comme **append()** ou **extend()**.

Le plus simple est de considérer les dictionnaires comme des ensembles de paires clé: valeur, les clés devant être uniques (au sein d’un dictionnaire). Une paire d’accolades crée un dictionnaire vide : {}.
Placer une liste de paires clé:valeur séparées par des virgules à l’intérieur des accolades ajoute les valeurs correspondantes au dictionnaire ; c’est également de cette façon que les dictionnaires sont affichés.

Les opérations classiques sur un dictionnaire consistent à stocker une valeur pour une clé et à extraire la valeur correspondant à une clé. Il est également possible de supprimer une paire clé-valeur avec **del**.
Si vous stockez une valeur pour une clé qui est déjà utilisée, l’ancienne valeur associée à cette clé est perdue. Si vous tentez d’extraire une valeur associée à une clé qui n’existe pas, une exception est levée.

Exécuter **list(d)** sur un dictionnaire d renvoie une liste de toutes les clés utilisées dans le dictionnaire, dans l’ordre d’insertion (si vous voulez qu’elles soient ordonnées, utilisez **sorted(d))**. Pour tester si une clé est dans le dictionnaire, utilisez le mot-clé in.

Voici un petit exemple utilisant un dictionnaire :

```
>>> téléphone = {'daniel': 4098, 'paul': 4139}
>>> téléphone['luc'] = 4127
>>> téléphone
{'daniel': 4098, 'paul': 4139, 'luc': 4127}
>>> téléphone['daniel']
4098
>>> del téléphone['paul']
>>> téléphone['sami'] = 4127
>>> téléphone
{'daniel': 4098, 'luc': 4127, 'sami': 4127}
>>> list(téléphone)
['daniel', 'luc', 'sami']
>>> sorted(téléphone)
['daniel', 'luc', 'sami']
>>> 'luc' in téléphone
True
>>> 'daniel' not in téléphone
False
```

Le constructeur **dict()** fabrique un dictionnaire directement à partir d’une liste de paires clé-valeur stockées sous la forme de n-uplets :

```
>>> dict([('paul', 4139), ('luc', 4127), ('daniel', 4098)])
{'paul': 4139, 'luc': 4127, 'daniel': 4098}
```

De plus, il est possible de créer des dictionnaires par compréhension depuis un jeu de clef et valeurs :

```
>>> {x: x**2 for x in (2, 4, 6)}
{2: 4, 4: 16, 6: 36}
```

Lorsque les clés sont de simples chaînes de caractères, il est parfois plus facile de spécifier les paires en utilisant des paramètres nommés :

```
>>> dict(paul=4139, luc=4127, daniel=4098)
{'paul': 4139, 'luc': 4127, 'daniel': 4098}
```

### Autres

#### Fichier

```
File
```

#### Absence de type

```
NoneType
```

#### Absence d’implémentation

```
NotImplementedType
```

#### fonction

```
Function
```

#### module

```
module
```

## Les fonctions intégrées

Lexique pédagogique à fournir «Les fonctions de base» :

### Détermination du type d’une variable

```
type()
```

### Conversion de types

```
bool()
```

Convertit en booléen : `"0"`, `""` et `"None"` donnent `"False"` et le reste `"True"`.

```
int()
```

Permet de modifier une variable en entier. Provoque une erreur si cela n’est pas possible.

```
str()
```

Permet de transformer la plupart des variables d’un autre type en chaînes de caractère.

```
float()
```

Permet la transformation en flottant.

```
repr()
```

Similaire à « str ». Voir la partie sur les objets.

```
eval()
```

Évalue le contenu de son argument comme si c’était du code Python.

```
long() # Python 2
```

Transforme une valeur en long.

### Voir les propriétés des fonctions

#### Fonction d’aide sur les fonctions Python

```
help()
```

#### Fonction de visualisation des propriétés et méthodes des fonctions Python

```
dir()
```

La fonction interne dir() est utilisée pour trouver quels noms sont définis par un module. Elle donne une liste de chaînes classées par ordre lexicographique :

```
>>> import math, sys
>>> dir(math)
['__doc__', '__file__', '__loader__', '__name__', '__package__', '__spec__', 'acos', 'acosh', 'asin', 'asinh', 'atan', 'atan2', 'atanh', 'ceil', 'comb', 'copysign', 'cos', 'cosh', 'degrees', 'dist', 'e', 'erf', 'erfc', 'exp', 'expm1', 'fabs', 'factorial', 'floor', 'fmod', 'frexp', 'fsum', 'gamma', 'gcd', 'hypot', 'inf', 'isclose', 'isfinite', 'isinf', 'isnan', 'isqrt', 'lcm', 'ldexp', 'lgamma', 'log', 'log10', 'log1p', 'log2', 'modf', 'nan', 'nextafter', 'perm', 'pi', 'pow', 'prod', 'radians', 'remainder', 'sin', 'sinh', 'sqrt', 'tan', 'tanh', 'tau', 'trunc', 'ulp']
```

Sans paramètre, **dir()** liste les noms actuellement définis :

```
>>> a = [1, 2, 3, 4, 5]
>>> import math
>>> cos = math.cos
>>> dir()
['__builtins__', '__doc__', '__loader__', '__name__', '__package__', '__spec__', 'a', 'b', 'cos', 'cubes', 'fruits', 'help', 'i', 'lettres', 'math', 'n', 'sys', 'téléphone', 'x']
```

Notez qu’elle liste tous les types de noms : les variables, fonctions, modules, etc.

**dir()** ne liste ni les fonctions primitives, ni les variables internes. Si vous voulez les lister, elles sont définies dans le module **builtins** :

```
>>> import builtins
>>> dir(builtins)
['ArithmeticError', 'AssertionError', 'AttributeError', 'BaseException', 'BlockingIOError', 'BrokenPipeError', 'BufferError', 'BytesWarning', 'ChildProcessError', 'ConnectionAbortedError', 'ConnectionError', 'ConnectionRefusedError', 'ConnectionResetError', 'DeprecationWarning', 'EOFError', 'Ellipsis', 'EnvironmentError', 'Exception', 'False', 'FileExistsError', 'FileNotFoundError', 'FloatingPointError', 'FutureWarning', 'GeneratorExit',   'IOError', 'ImportError', 'ImportWarning', 'IndentationError', 'IndexError', 'InterruptedError', 'IsADirectoryError', 'KeyError', 'KeyboardInterrupt', 'LookupError', 'MemoryError', 'ModuleNotFoundError', 'NameError', 'None', 'NotADirectoryError', 'NotImplemented', 'NotImplementedError', 'OSError', 'OverflowError', 'PendingDeprecationWarning', 'PermissionError', 'ProcessLookupError', 'RecursionError', 'ReferenceError', 'ResourceWarning', 'RuntimeError', 'RuntimeWarning', 'StopAsyncIteration', 'StopIteration', 'SyntaxError', 'SyntaxWarning', 'SystemError', 'SystemExit', 'TabError', 'TimeoutError', 'True', 'TypeError', 'UnboundLocalError', 'UnicodeDecodeError', 'UnicodeEncodeError', 'UnicodeError', 'UnicodeTranslateError', 'UnicodeWarning', 'UserWarning', 'ValueError', 'Warning', 'ZeroDivisionError', '_', '__build_class__', '__debug__', '__doc__', '__import\__', '__loader__', '__name__', '__package__', '__spec__', 'abs', 'all', 'any', 'ascii', 'bin', 'bool', 'breakpoint', 'bytearray', 'bytes', 'callable', 'chr', 'classmethod', 'compile', 'complex', 'copyright', 'credits', 'delattr', 'dict', 'dir', 'divmod', 'enumerate', 'eval', 'exec', 'exit', 'filter', 'float', 'format', 'frozenset', 'getattr', 'globals', 'hasattr', 'hash', 'help', 'hex', 'id', 'input', 'int', 'isinstance', 'issubclass', 'iter', 'len', 'license', 'list', 'locals', 'map', 'max', 'memoryview', 'min', 'next', 'object', 'oct', 'open', 'ord', 'pow', 'print', 'property', 'quit', 'range', 'repr', 'reversed', 'round', 'set', 'setattr', 'slice', 'sorted', 'staticmethod', 'str', 'sum', 'super', 'tuple', 'type', 'vars', 'zip']
```

#### L’identification des objets

```
id()
```

```
>>> id(cos)
140293507063376
```

### Zoom sur fonctions et propriétés

#### Range

Si vous devez itérer sur une suite de nombres, la fonction native **range()** est faite pour cela. Elle génère des suites arithmétiques :

```
>>> for i in range(5):
... print(i)
...
...
0
1
2
3
4
```

Le dernier élément fourni en paramètre ne fait jamais partie de la liste générée ; **range(10)** génère une liste de 10 valeurs, dont les valeurs
vont de **0 à 9**. Il est possible de spécifier une valeur de début et une valeur d’incrément différentes (y compris négative pour cette dernière, que l’on appelle également parfois le “pas”) :

```
>>> tuple(range(5, 10))
(5, 6, 7, 8, 9)
>>> tuple(range(0, 10, 3))
(0, 3, 6, 9)
>>> tuple(range(-10, -100, -30))
(-10, -40, -70)
```

Une chose étrange se produit lorsqu’on affiche un range :

```
>>> print(range(10))
range(0, 10)
```

L’objet renvoyé par **range()** se comporte presque comme une liste, mais ce n’en est pas une. Cet objet génère les éléments de la séquence au fur et à mesure de l’itération, sans réellement produire la liste en tant que telle, économisant ainsi de l’espace.

On appelle de tels objets des **iterable**, c’est-à-dire des objets qui conviennent à des fonctions ou constructions qui s’attendent à quelque chose duquel ils peuvent tirer des éléments, successivement, jusqu’à épuisement. Nous avons vu que l’instruction **for** est une de ces constructions, et un exemple de fonction qui prend un itérable en paramètre est sum() :

```
>>> sum(range(4))  # 0 + 1 + 2 + 3
6
```

Plus loin nous voyons d’autres fonctions qui donnent des itérables ou en prennent en paramètre. Si vous vous demandez comment obtenir une liste à partir d’un range, voilà la solution :

```
>>> list(range(4))
[0, 1, 2, 3]
```

#### Chaînes de caractères

##### split() : sépare une chaîne en liste

Fractionne une chaîne de caractères Python suivant un délimiteur. Si le paramètre du nombre de divisions est spécifié split() retourne seulement dans la liste les premiers élément fractionnés suivant la quantité demandée.

**Syntaxe :**

```
str.split(str="", num=string.count(str))
```

**Paramètres :**


* str : séparateur, espaces par défaut.


* num : le nombre de divisions.

**Valeur de retour :**

> Renvoie une liste de chaînes après division.

Exemples

L’exemple suivant montre la distribution de **split()** :

```
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

chaine = "Ligne1-abcdef \\nLigne2-abc \\nLigne3-abcd"
print(chaine.split())
print(chaine.split(' ', 1))
```

Exemple du résultat de sortie ci-dessus :

```
['Ligne1-abcdef', 'Ligne2-abc', 'Ligne3-abcd']
['Ligne1-abcdef', '\nLigne2-abc \\nLigne3-abcd']
```

##### join() : Concatène une liste de caractères

Transforme une liste en chaîne avec le séparateur en préfixe (`"".join(MaListe)`).

**Syntaxe :**

```
string.join(iterable)
```

**Paramètres :**

La méthode join() prend un seul paramètre.


* iterable(Obligatoire) : Tout objet itérable où toutes les valeurs renvoyées sont des chaînes

**Valeur de retour :**

> La méthode join() renvoie une chaîne créée en joignant les éléments d’un itérable par un séparateur.

Exemple

Joindre tous les éléments d’un tuple dans une chaîne, en utilisant le caractère «|» comme séparateur :

```
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

tupl = ("Python", "Rust", "Julia")
résultat = "|".join(tupl)
print(résultat)
```

Sortie :

```
Python|Rust|Julia
```

Joignez tous les éléments d’un dictionnaire dans une chaîne, en utilisant le caractère «#» comme séparateur :

```
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

dict = {"nom": "Alexandre", "age": "25"}
résultat = "#".join(dict)
print(résultat)
résultat = "#".join(dict.values())
print(résultat)
```

Sortie :

```
nom#age
Alexandre#25
```

## Les modules

Lorsque vous quittez et entrez à nouveau dans l’interpréteur Python, tout ce que vous avez déclaré dans la session précédente est perdu. Afin de rédiger des programmes plus longs, vous devez utiliser un éditeur de texte, préparer votre code dans un fichier et exécuter Python avec ce fichier en paramètre. Cela s’appelle créer un script. Lorsque votre programme grandit, vous pouvez séparer votre code dans plusieurs fichiers. Ainsi, il vous est facile de réutiliser du code écrit pour un programme dans un autre sans avoir à les copier.

Pour gérer cela, Python vous permet de placer des définitions dans un fichier et de les utiliser dans un script ou une session interactive. Un tel fichier est appelé un module et les définitions d’un module peuvent être importées dans un autre module ou dans le module main (qui est le module qui contient vos variables et définitions lors de l’exécution d’un script au niveau le plus haut ou en mode interactif).

Un module est un fichier contenant des définitions et des instructions (des fonctions, des classes et des variables.). Son nom de fichier est le nom du module suffixé de «**.py**».

À l’intérieur d’un module, son propre nom est accessible par la variable `__name__`. Ce module, avec ses variables, fonctions ou classes, peut être chargé à partir d’un autre module ; c’est ce que l’on appelle l’importation.

### __name__ et __main__

Lorsque l’interpréteur exécute un module, la variable **__name__** sera définie comme **__main__** si le module en cours d’exécution est le programme principal.

```
>>> print(" __name__ est défini à {}".format(__name__))
__name__ est défini à __main__
```

Mais si le code importe le module depuis un autre module, la variable **__name__** sera définie sur le nom de ce module. Jetons un œil à un exemple.

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ mkdir 7_Modules ; cd 7_Modules
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ nano ./mon_module.py
```

Créez un module Python nommé **mon_module.py** et saisissez ce code à l’intérieur:

```
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Module de fichier Python
print("Mon module __name__ est défini à {}".format(__name__))
```

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ python3
```

```
Python 3.9.4 (default, Apr 4 2021, 19:38:44)
[GCC 10.2.1 20210401] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> import mon_module
Mon module __name__ est défini à mon_module
>>> quit()
```

### Gestion des imports

La façon habituelle d’utiliser **__name__** et **__main__** ressemble à ceci avec le script **mon_module_2.py**:

```
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Module de fichier Python
print("Mon module __name__ est défini à {}".format(__name__))
if __name__ == "__main__":
    print("Fichier exécuté directement")
else:
    print("Fichier exécuté comme importé")
```

Ce qui nous donne à l’exécution directe du module python :

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ chmod u+x ./mon_module_2.py
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./mon_module_2.py
Mon module __name__ est défini à __main__
Fichier exécuté directement
```

Et à l’exécution comme module :

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ python3
```

```
Python 3.9.4 (default, Apr 4 2021, 19:38:44)
[GCC 10.2.1 20210401] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> import mon_module_2
Mon module __name__ est défini à mon_module
Fichier exécuté comme importé
>>> quit()
```

## Les bibliothèques de fonctions ou d’objets

### Les modules PYTHON

Lexique pédagogique à fournir **Les bibliothèques de fonctions ou
d’objets.**

### Le dépôt de modules Python

#### Pip

Une des forces de **Python** est la multitude de bibliothèques disponibles (près de 6000 bibliothèques gravitent autour du projet **Django**).

Par exemple installer une bibliothèque peut vite devenir ennuyeux:


* trouver le bon site,


* la bonne version de la bibliothèque,


* l’installer,


* trouver ses dépendances,


* etc.

Il existe une solution qui vous permet de télécharger très simplement une bibliothèque **pip**.

##### PIP c’est quoi ?

**Pip** est un **système de gestion de paquets** utilisé pour installer et gérer des librairies écrites en Python. Vous pouvez trouver une grande partie de ces librairies dans le [Python Package Index](https://pypi.python.org/pypi) (ou PyPI). **Pip** empêche les installations partielles en annonçant toutes les exigences avant l’installation.

```
pip install librairie
```

Vous pouvez choisir la version qui vous intéresse :

```
pip install librairie==2.2
```

Supprimer une librairie :

```
pip uninstall librairie
```

Mettre à jour une librairie :

```
pip install librairie --upgrade
```

Revenir sur une version antérieure :

```
pip install librairie==2.1 --upgrade
```

Rechercher une nouvelle librairie :

```
pip search librairie
```

Vous indiquer quelles librairies ne sont plus à jour :

```
pip list --outdated
```

Afficher toutes les librairies installées et leur version :

```
pip freeze
```

Exporter la liste des librairies, vous pourrez la réimporter ailleurs :

```
pip freeze > lib.txt
```

Importer la liste de librairie comme ceci :

```
pip install -r lib.txt
```

Créer un gros zip qui contient toutes les dépendances :

```
pip bundle <nom_du_bundle>.pybundle -r lib.txt
```

Pour installer les librairies :

```
pip install <nom_du_bundle>.pybundle
```

Pour installer depuis un dépôt distant ([Voir la section du support VCS](https://pip.pypa.io/en/stable/reference/pip_install/#vcs-support)) :

```
pip install git+https://github.com/chemin/monmodule.git#egg=monmodule
```

Pour le lien ver le support VCS : [https://pip.pypa.io/en/stable/reference/pip_install/#vcs-support](https://pip.pypa.io/en/stable/reference/pip_install/#vcs-support)

Voir plus d’informations [https://docs.python.org/fr/3.6/installing/index.html](https://docs.python.org/fr/3.6/installing/index.html)

### Les modules de gestion des paramètres de la ligne de commande

#### Modules dépréciés

Python fourni un module **getopt**(déprécié depuis Python 3.7) ou **optparse** (déprécié depuis Python 3.2) qui vous aident à analyser les options et les arguments de la ligne de commande. Le module **getopt** fournit deux fonctions et une exception pour activer l’analyse des arguments de ligne de commande.

Supposons que nous voulions passer deux noms de fichiers via la ligne de commande et que nous voulions également donner une option pour vérifier l’utilisation du script. L’utilisation en ligne de commande du script est la suivante :

```
test.py -i <fichier_en_entrée> -o <fichier_de_sortie>
```

```
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys, getopt

def main(argv):
    fichierentre = ''
    fichiersortie = ''
    try:
        opts, args = getopt.getopt(argv,"hi:o:",["ifile=","ofile="])
    except getopt.GetoptError:
        print('utilisation : test.py -i <fichier_en_entrée> -o <fichier_de_sortie>')
        sys.exit(2)

    for opt, arg in opts:
        if opt == '-h':
            print('utilisation : test.py -i <fichier_en_entrée> -o <fichier_de_sortie>')
            sys.exit()
        elif opt in ("-i", "--ifile"):
            fichierentre = arg
        elif opt in ("-o", "--ofile"):
            fichiersortie = arg

    print('Le fichier en entré est', fichierentre)
    print('Le fichier en sortie est', fichiersortie)

if __name__ == "__main__":
    main(sys.argv[1:])
```

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ test.py -h
utilisation: test.py -i <fichier_en_entrée> -o <fichier_en_entrée>
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ test.py -i BMP -o
utilisation: test.py -i <fichier_en_entrée> -o <fichier_en_entrée>
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ test.py -i input.txt -o output.cvs
Le fichier en entré est input.txt
Le fichier en sortie est output.cvs
```

#### Argparse

Le [module argparse](https://docs.python.org/fr/3/library/argparse.html#module-argparse) remplace *getopt* et *optparse*. Il facilite l’écriture d’interfaces de ligne de commande conviviales. Le programme définit les arguments dont il a besoin et **argparse** trouvera comment les analyser à partir de **sys.argv**. Le module **argparse** génère également automatiquement des messages d’aide et d’utilisation, et émet des erreurs lorsque les utilisateurs donnent au programme des arguments non valides.

##### Utilisation du module


1. Construction du parseur.

> `parser = argparse.ArgumentParser(description = 'ma description')`

> On peut donner une description qui terminera dans l’aide d’usage.


1. Ajout d’un argument de la ligne de commande.

> `parser.add_argument('-foo')`

> `'-foo'` est un argument optionnel (non obligatoire). C’est parce que cela commence par «**-**» ou «**--**».

> Pour avoir un argument positionnel (obligatoire), saisir `'foo'`.


1. Parcourir les arguments.

> `args = parser.parse_args()`

> Agit automatiquement sur **sys.argv**

On peut alors accéder aux valeurs des arguments en faisant directement : `args.foo`

On peut aussi récupérer les valeurs sous forme de dictionnaire avec : `vars(args)`

On peut explicitement imprimer l’aide avec : `parser.print_help()`

On peut explicitement imprimer l’usage simplifié de la commande par : `parser.print_usage()`

##### Ajout d’options

`parser.add_argument('-f', '--foo')`

On peut utiliser l’arguments optionnel simplifié **-f** ou nommé **--foo** en ligne de commande.

`parser.add_argument('-foo', help='what -foo does', metavar='fValue')`

Nom de l’argument **-foo** dans les messages d’utilisation avec l’option `metavar=` ainsi que l’aide sur l’option avec `help=`.

`parser.add_argument('-foo', dest='fVal')`

La valeur pourra être accédée après parsing avec `args.fVal` plutôt qu’avec `args.foo`.

`parser.add_argument('-foo', required=True)`

L’argument est obligatoire.

`parser.add_argument('-foo', action='store_true')`

L’argument ne prend pas de valeur et renvoi **True** si présent (**False** sinon).

`parser.add_argument('-foo', action='append')`

L’argument renvoi une liste de valeurs avec autant d’éléments que le nombre de fois où l’argument est présent.

Par exemple : **2** valeurs si «**-foo a -foo b**».

`parser.add_argument('-foo', choices=['a', 'b', 'c'])`

L’argument doit prendre l’une des valeurs indiquée.

`parser.add_argument('-foo', default='test')`

Donne une valeur par défaut.

`parser.add_argument('-foo', type=int)`

Indique que l’argument doit être un entier plutôt qu’une chaîne de caractères (défaut).
On peut utiliser **int**, **float**, **str**, **complex**.

##### Exemples

Fichier **argparse1.py** :

Implémentation minimale.

```
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse

parser = argparse.ArgumentParser()
parser.parse_args()
```

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse1.py
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7\_Modules$ ./argparse1.py --help
usage: argparse1.py [-h]

optional arguments:
    -h, --help show this help message and exit
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse1.py foo
usage: argparse1.py [-h]
argparse1.py: error: unrecognized arguments: foo
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse1.py --verbose
usage: argparse1.py [-h]
argparse1.py: error: unrecognized arguments: --verbose
```

Voilà ce qu’il se passe :


1. Exécuter le script sans aucun paramètre a pour effet de ne rien afficher sur la sortie d’erreur. Ce n’est pas très utile.


2. La deuxième commande commence à montrer l’intérêt du module **argparse**. On n’a quasiment rien fait mais on a déjà un beau message d’aide . L’option **--help** (pas besoin de la préciser), que l’on peut aussi raccourcir en **-h**.


3. Préciser quoi que ce soit d’autre comme argument de la ligne de commande entraîne une erreur.


4. Même si on reçoit aussi un argument optionnel non défini.

Fichier **argparse2.py** :

Comment passer un argument de ligne de commande ?

```
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse

parser = argparse.ArgumentParser()
parser.add_argument("echo")
args = parser.parse_args()
print(args.echo)
```

On a ajouté la méthode `add_argument()` que l’on utilise pour préciser quels paramètres de lignes de commandes le programme peut accepter. Dans le cas présent, c’est **echo** pour que cela corresponde à sa fonction. Utiliser le programme nécessite maintenant que l’on précise un paramètre.

La méthode `parse_args()` renvoie les données des arguments de la ligne de commande, dans le cas présent : **echo**.

**argparse** affecte automatiquement la variable comme par «magie». C’est à dire que nous n’avons pas besoin de préciser dans quelle variable la valeur est stockée. Vous pouvez remarquer aussi que le nom de variable `args.echo` est le même que l’argument en chaîne de caractères donné à la méthode `add_argument("echo")`.

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse2.py
usage: argparse2.py [-h] echo
argparse2.py: error: the following arguments are required: echo
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse2.py --help
usage: argparse2.py [-h] echo

positional arguments:
 echo

optional arguments:
    -h, --help show this help message and exit
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse2.py monparamètre
monparamètre
```

Voilà ce qu’il se passe :


1. Sans arguments la commande renvoie l’aide simplifiée avec le message d’erreur.


2. Nous voyons l’aide du programme avec la commande «**--help**».


3. Avec le bon argument on affiche la valeur de l’argument saisie.

Notez cependant que, même si l’affichage d’aide paraît bien , il n’est pas aussi utile qu’il pourrait l’être. Par exemple, on peut lire que **echo** est un argument positionnel mais on ne peut pas savoir ce que cela fait autrement qu’en le devinant ou en lisant le code source.

Fichier **argparse3.py** :

Comment afficher une aide plus précise pour un argument de la ligne de commande ?

```
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse

parser = argparse.ArgumentParser()
parser.add_argument("echo", help="renvoi la valeur du paramètre que vous avez passé")
args = parser.parse_args()
print(args.echo)
```

Nous ajoutons simplement le paramètre `help=""` à la méthode `add_argument()`.

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse3.py -h
usage: argparse3.py [-h] echo

positional arguments:
    echo    echo renvoi la valeur du paramètre que vous avez passé

optional arguments:
    -h, --help show this help message and exit
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse3.py monparamètre
monparamètre
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse3.py monparamètre etunautre
usage: argparse3.py [-h] echo
argparse3.py: error: argument square: invalid int value: 'etunautre'
```


1. Nous observons bien que le message d’aide est plus précis.


2. Cela fonctionne avec une valeur atribuée au paramètre positioné «**echo**».


3. Cela ne prend qu’un paramètre.

Fichier **argparse4.py** :

Comment calculer le carré d’un nombre en ligne de commande ?

```
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse

parser = argparse.ArgumentParser()
parser.add_argument("carré", help="affiche le carré du nombre passé en argument")
args = parser.parse_args()
print(args.carré**2)
```

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse4.py 4
Traceback (most recent call last):
    File "./argparse4.py", line 8, in <module>
        print(args.carré**2)
    TypeError: unsupported operand type(s) for \*\* or pow(): 'str' and 'int'
```

Cela n’a pas très bien fonctionné. C’est parce que **argparse** traite les paramètres que l’on donne comme des chaînes de caractères, à moins qu’on ne lui indique de faire autrement.

Fichier **argparse5.py** :

Comment traiter le paramètre d’entrée comme un entier ?

```
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse

parser = argparse.ArgumentParser()
parser.add_argument("carré", help="affiche le carré du nombre passé en argument", type=int)
args = parser.parse_args()
print(args.carré**2)
```

Nous ajoutons simplement le paramètre `type=int` à la méthode `add_argument()`.

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse5.py 4
16
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7\_Modules$ ./argparse5.py quatre
usage: argparse5.py [-h] carré

argparse5.py: error: argument carré: invalid int value: 'quatre'
```

Cela a bien fonctionné. Maintenant le programme va même s’arrêter si l’entrée n’est pas un entier avant de procéder à l’exécution.

Fichier **argparse6.py** :

Comment ajouter un paramètre optionnel ?

```
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import** **argparse**

parser = argparse.ArgumentParser()
parser.add_argument("--verbosity", help="augmente la verbosité de sortie")
args = parser.parse_args()

if args.verbosity :
    print("verbosité activée")
```

On rajoute «**-**» ou «**--**» pour montrer que l’argument de ligne de commande est bien optionnel, il n’y aura alors pas d’erreur si on exécute le programme sans celui-ci.

Notez que par défaut, si une option n’est pas utilisée, la variable associée, dans le cas présent `args.verbosity`, prend la valeur `None`. C’est pour cela quelle échoue au test [if](https://docs.python.org/fr/3/reference/compound_stmts.html#if).

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse6.py --verbosity 1
verbosité activée
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse6.py
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse6.py --help
usage: argparse6.py [-h] [--verbosity VERBOSITY]

optional arguments:
    -h, --help            show this help message and exit
    --verbosity VERBOSITY
                          augmente la verbosité de sortie
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse6.py --verbosity
usage: argparse6.py [-h] [--verbosity VERBOSITY]
argparse6.py: error: argument --verbosity: expected one argument
```


1. Validation de la verbosité


2. La commande sans paramètre ne retourne rien et n’est pas en erreur.


3. Le message d’aide est un peu différent quand on utilise l’option **--verbosity** si on ne précise pas une valeur.


4. Le paramètre optionnel **--verbosity** demande impérativement une valeur d’attribution.

L’exemple ci-dessus accepte obligatoirement une valeur entière arbitraire pour **--verbosity**, mais seul l’état (vrai/faux) de présence du paramètre est réellement utile pour notre commande.

Fichier **argparse7.py** :

Comment prendre en compte l’état booléen de présence d’un argument ?

```
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse

parser = argparse.ArgumentParser()
parser.add_argument("--verbose", help="augmente la verbosité de sortie", action="store_true")
args = parser.parse_args()

if args.verbose:
    print("verbosité activée")
```

Notez que maintenant on précise avec le paramètre `action=` dans `add_argument()` un état booléen. Et on lui donne la valeur **"store_true"**. Cela signifie que si l’argument de ligne de commande est précisée, la valeur `True` est assignée à `args.verbose`. Ne rien préciser renvoie la valeur `False`.

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse7.py --verbose
verbosité activée
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse7.py
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse7.py --help
usage: argparse7.py [-h] [--verbose]

optional arguments:
    -h, --help    show this help message and exit
    --verbose     augmente la verbosité de sortie
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse7.py --verbose 1
usage: argparse7.py [-h] [--verbose]
argparse7.py: error: unrecognized arguments: 1
```


1. Maintenant le paramètre est plus une option qu’un paramètre qui nécessite une valeur. On a même changé le nom du paramètre pour qu’il corresponde à cette idée.


2. Pas d’aide retournée.


3. Notez que l’aide est différente avec l’option «**--verbose**».


4. Dans l’esprit de ce que sont vraiment les options de la ligne de commande, pas des paramètres, quand vous tentez de préciser une valeur de paramètre l’aide simplifiée d’usage et une erreur sont renvoyées.

Si vous êtes familier avec l’utilisation de la ligne de commande, vous avez dû remarquer que nous n’avons pas abordé les raccourcies des paramètres.

Fichier **argparse8.py** :

Comment ajouter un raccourcie de paramètre de la ligne de commande ?

```
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse

parser = argparse.ArgumentParser()
parser.add_argument("-v", "--verbose", help="augmente la verbosité de sortie", action="store_true")
args = parser.parse_args()

if args.verbose:
    print("verbosité activée")
```

Nous allons simplement en ajouter un au code avec `"-v"` en amont de `"--verbose"` dans les options de `add_argument()`. Sachez que le dernier paramètre saisi est la clé de paramètre, ici c’est `"--verbose"`, les autres en amont sont des raccourcies, ici `"-v"`.

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse8.py -v
verbosité activée
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse8.py --help
usage: argparse8.py [-h] [-v]

optional arguments:

    -h, --help    show this help message and exit
    -v, --verbose augmente la verbosité de sortie
```

Notez que la nouvelle option est aussi indiquée dans l’aide.

Fichier **argparse9.py** :

Comment maintenant ajouter un argument positionné (obligatoire) supplémentaire ?

```
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse

parser = argparse.ArgumentParser()
parser.add_argument("carré", type=int, help="affiche le carré du nombre passé en argument")
parser.add_argument("-v", "--verbose", action="store_true", help="augmente la verbosité de sortie")
args = parser.parse_args()
reponse = args.carré**2

if args.verbose:
    print("le carré de {} est égal à {}".format(args.carré, reponse))
else:
    print(reponse)
```

Nous avons ajouté un argument positionné de type entier «**carré**» dans le code en ajoutant un autre appel à la méthode `add_argument()`.

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse9.py
usage: argparse9.py [-h] [-v] carré
argparse9.py: error: the following arguments are required: carré
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse9.py 4
16
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse9.py 4 --verbose
le carré de 4 est égal à 16
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse9.py --verbose 4
le carré de 4 est égal à 16
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse9.py -v 4
le carré de 4 est égal à 16
```


1. L’option d’argument positionné apparaît dans l’aide, et on remarque que les argument optionnels sont entre crochets. L’argument positionné n’étant pas saisie l’aide renvoie un message d’erreur.


2. Le calcul de la valeur fonctionne bien


3. Notez que l’ordre importe peu avec les autres commandes passées.

Fichier **argparse10.py** :

Qu’en est-il si nous donnons à ce programme la possibilité d’avoir plusieurs niveaux de verbosité, et que celui-ci les prend en compte ?

```
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse

parser = argparse.ArgumentParser()
parser.add_argument("carré", type=int, help="affiche le carré du nombre passé en argument")
parser.add_argument("-v", "--verbosity", type=int, help="augmente la verbosité de sortie")
args = parser.parse_args()
reponse = args.carré**2

if args.verbosity == 2:
    print("le carré de {} est égal à {}".format(args.carré, reponse))
elif args.verbosity == 1:
    print("{}² = {}".format(args.carré, reponse))
else:
    print(reponse)
```

Ajout dans le code de tests de niveau de verbosité.

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$  ./argparse10.py 4
16
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse10.py 4 -v
usage: argparse10.py [-h] [-v VERBOSITY] carré
argparse10.py: error: argument -v/--verbosity: expected one argument
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse10.py 4 -v 1
4² = 16
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse10.py 4 -v 2
le carré de 4 est égal à 16
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse10.py 4 -v 3
16
```

Tout semble bon sauf pour le dernier cas. Notre programme contient un bogue.

Fichier **argparse11.py** :

Comment restreindre les valeurs que **--verbosity** accepte ?

```
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse

parser = argparse.ArgumentParser()
parser.add_argument("carré", type=int, help="affiche le carré du nombre passé en argument")
parser.add_argument("-v", "--verbosity", type=int, choices=[0, 1, 2], help="augmente la verbosité de sortie")
args = parser.parse_args()
reponse = args.carré**2

if args.verbosity == 2:
    print("le carré de {} est égal à {}".format(args.carré, reponse))
elif args.verbosity == 1:
    print("{}² = {}".format(args.carré, reponse))
else:
    print(reponse)
```

On ajoute l’option `choices=[]` à la méthode `add_argument()`.

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse11.py 4 -v 3
usage: argparse11.py [-h] [-v {0,1,2}] carré
argparse11.py: error: argument -v/--verbosity: invalid choice: 3 (choose from 0, 1, 2)
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse11.py 4 -h
usage: argparse11.py [-h] [-v {0,1,2}] carré
positional arguments:
    carré                              affiche le carré du nombre passé en argument
optional arguments:
    -h, --help                         show this help message and exit
    -v {0,1,2}, --verbosity {0,1,2}    augmente la verbosité de sortie
```

Notez que ce changement est pris en compte à la fois dans le message d’erreur et dans le texte d’aide.

Essayons maintenant une approche différente pour jouer sur la verbosité. Cela correspond également à comment le programme CPython gère ses propres paramètres de verbosité (jetez un œil sur la sortie de la commande **python --help**) :

Fichier **argparse12.py** :

Comment compter le nombre fois où un paramètre est saisi ?

```
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse

parser = argparse.ArgumentParser()
parser.add_argument("carré", type=int, help="affiche le carré du nombre passé en argument")
parser.add_argument("-v", "--verbosity", action="count", help="augmente la verbosité de sortie")
args = parser.parse_args()
reponse = args.carré**2
if args.verbosity == 2:
    print("le carré de {} est égal à {}".format(args.carré, reponse))
elif args.verbosity == 1:
    print("{}² = {}".format(args.carré, reponse))
else:
    print(reponse)
```

Nous avons introduit une autre action `"count"` à la méthode `add_argument()`, pour compter le nombre d’occurrences d’un argument optionnel en particulier :

Oui, c’est maintenant d’avantage une option (similaire à `action="store_true"`) de la version précédente de notre script. C’est plus logique pour comprendre le message d’erreur.

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse12.py 4
16
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7\_Modules$ ./argparse12.py 4 -v
4² == 16
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse12.py 4 -vv
le carré de 4 est égal à 16
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse12.py 4 --verbosity --verbosity
le carré de 4 est égal à 16
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse12.py 4 -v 1
usage: argparse12.py [-h] [-v] carré
argparse12.py: error: unrecognized arguments: 1
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse12.py 4 -h
usage: argparse12.py [-h] [-v] carré

positional arguments:
    carré               affiche le carré du nombre passé en argument

optional arguments:
    -h, --help show     this help message and exit
    -v, --verbosity     augmente la verbosité de sortie
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse12.py 4 -vvv
16
```


1. La commande calcule le carré


2. Cela se comporte de la même manière que l’action `"store_true"`.


3. Maintenant voici une démonstration de ce que l’action `"count"` fait. Vous avez sûrement vu ce genre d’utilisation auparavant. Et si vous ne spécifiez pas l’option **-v**, cette option prendra la valeur `None`.


4. Comme on s’y attend, en spécifiant l’option dans sa forme longue, on devrait obtenir la même sortie.


5. Une valeur passé en paramètre génère une erreur.


6. Affiche l’aide normalement


7. La dernière sortie du programme montre que celui-ci contient un bogue.

Malheureusement, notre sortie d’aide n’est pas très informative à propos des nouvelles possibilités de notre programme, mais cela peut toujours être corrigé en améliorant sa documentation (en utilisant l’argument **help**).

Fichier **argparse13.py** :

Comment améliorer la documentation de l’exercice précédent et corriger le bogue ?

```
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse

parser = argparse.ArgumentParser()
parser.add_argument("carré", type=int, help="affiche le carré du nombre passé en argument")
parser.add_argument("-v", "--verbosity", action="count", help="augmente la verbosité de sortie")
args = parser.parse_args()
reponse = args.carré**2

# corection: remplacer == avec >=
if args.verbosity >= 2:
    print("le carré de {} est égal à {}".format(args.carré , reponse))
elif args.verbosity >= 1:
    print("{}² = {}".format(args.carré , reponse))
else:
    print(reponse)
```

Il suffit de changer le test «**==**» par «**>=**».

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse13.py 4 -vvv
le carré de 4 est égal à 16
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse13.py 4 -vvvv
le carré de 4 est égal à 16
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse13.py 4
Traceback (most recent call last):
    File "argparse13.py", line 12, in <module>
        if args.verbosity >= 2:
TypeError: '>=' not supported between instances of 'NoneType' and 'int'
```

Les premières exécutions du programme sont correctes, et le bogue que nous avons eu précédemment est corrigé.

La troisième sortie du programme est un autre bogue introduit par la modification.

Nous voulons que pour n’importe quelle valeur `>= 2` le programme soit verbeux tout en calculant le carré sans ce paramètre.

Fichier **argparse14.py** :

Comment corriger le nouveau bogue du code de l’exemple précédent pour avoir la sortie du carré ?

```
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse

parser = argparse.ArgumentParser()
parser.add_argument("carré", type=int, help="affiche le carré du nombre passé en argument")
parser.add_argument("-v", "--verbosity", action="count", default=0, help="augmente la verbosité de sortie")
args = parser.parse_args()
reponse = args.carré**2

# corection: remplacer == avec >=
if args.verbosity >= 2:
    print("le carré de {} est égal à {}".format(args.carré , reponse))
elif args.verbosity >= 1:
    print("{}² = {}".format(args.carré , reponse))
else:
    print(reponse)
```

Nous introduisons une nouvelle option `default=` dans la méthode `add_argument()`. Nous la définisons à l’entier **0** pour la rendre compatible avec les autres valeurs entières de l’option `count=`. Rappelez-vous que par défaut, si un argument optionnel n’est pas spécifié, il sera définit à `None` une valeur booléenne, et ne pourra donc pas être comparé à une valeur de type entier. Une erreur [TypeError](https://docs.python.org/fr/3/library/exceptions.html#TypeError) sera alors levée.

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse14.py 4
16
```

Fichier **argparse15.py** :

Qu’en est-il si nous souhaitons étendre notre mini programme pour le rendre capable de calculer d’autres puissances, et pas seulement des carrés?

```
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse

parser = argparse.ArgumentParser()
parser.add_argument("x", type=int, help="la base")
parser.add_argument("y", type=int, help="l’exposant")
parser.add_argument("-v", "--verbosity", action="count", default=0)
args = parser.parse_args()
reponse = args.x**args.y

if args.verbosity >= 2:
    print("{} à la puissance {} est égal à {}".format(args.x, args.y, reponse))
elif args.verbosity >= 1:
    print("{}^{} = {}".format(args.x, args.y, reponse))
else:
    print(reponse)
```

Nous modifions les arguments de saisies et l’opération de calcul.

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse15.py
usage: argparse15.py [-h] [-v] x y
argparse15.py: error: the following arguments are required: x, y
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse15.py -h
usage: argparse15.py [-h] [-v] x y

positional arguments:
    x          la base
    y          l’exposant

optional arguments:
    -h, --help show this help message and exit
    -v, --verbosity
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse15.py 4 2 -v
4^2 = 16
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse15.py 4 2 -vv
4 à la puissance 2 est égal à 16
```

Il est à noter que jusqu’à présent nous avons utilisé le niveau de verbosité pour **changer** le texte qui est affiché.

Fichier **argparse16.py** :

Comment utiliser le principe du niveau de verbosité pour **changer** de sens de texte ?

```
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse

parser = argparse.ArgumentParser()
parser.add_argument("x", type=int, help="la base")
parser.add_argument("y", type=int, help="l’exposant")
parser.add_argument("-v", "--verbosity", action="count", default=0)
args = parser.parse_args()
reponse = args.x**args.y

if args.verbosity >= 2:
    print("Exécution de '{}'".format(__file__))
if args.verbosity >= 1:
    print("{}^{} = ".format(args.x, args.y), end="")
print(reponse)
```

Modifions le texte affiché par les options `args.verbosity` pour afficher la commande exécutée.

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse16.py 4 2
16
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse16.py 4 2 -v
4^2 = 16
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7\_Modules$ ./argparse16.py 4 2 -vv
Exécution de './argparse16.py'
4^2 = 16
```

Jusque là, nous avons travaillé avec deux méthodes `parse_args()` et `add_argument()` d’une instance de `argparse.ArgumentParser`.

Voyons maintenant l’utilisation de la méthode `add_mutually_exclusive_group()`. Cette méthode nous permet de spécifier des paramètres qui sont en conflit entre eux.

Fichier **argparse17.py** :

Comment utiliser `add_mutually_exclusive_group()` ?

```
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse

parser = argparse.ArgumentParser()
group = parser.add_mutually_exclusive_group()
group.add_argument("-v", "--verbose", action="store_true")
group.add_argument("-q", "--quiet", action="store_true")
parser.add_argument("x", type=int, help="la base")
parser.add_argument("y", type=int, help="l’exposant")
args = parser.parse_args()
reponse = args.x**args.y

if args.quiet:
    print(reponse)
elif args.verbose:
    print("{} à la puissance {} est égal à {}".format(args.x, args.y, reponse))
else:
    print("{}^{} = {}".format(args.x, args.y, reponse))
```

Changeons aussi le reste du programme de telle sorte que la nouvelle fonctionnalité fasse sens. Nous allons introduire l’option **--quiet**, qui va avoir l’effet opposé de l’option **--verbose** :

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse17.py 4 2
4^2 == 16
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse17.py 4 2 -q
16
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse17.py 4 2 -v
4 à la puissance 2 est égal à 16
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse17.py 4 2 -vq
usage: argparse17.py [-h] [-v | -q] x y
argparse17.py: error: argument -q/--quiet: not allowed with argument -v/--verbose
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse17.py 4 2 -v --quiet
usage: argparse17.py [-h] [-v | -q] x y
test.py: error: argument -q/--quiet: not allowed with argument -v/--verbose
```

Avant d’en finir, vous voudrez certainement dire à vos utilisateurs de votre outil de ligne de commande quel est le but principal du programme. Juste dans le cas ou ils ne le sauraient pas :-)

Fichier **argparse18.py** :

Comment modifier l’aide d’un programme en ligne de commande avec un titre général ?

```
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse

parser = argparse.ArgumentParser(description="calcule X à la puissance Y")
group = parser.add_mutually_exclusive_group()
group.add_argument("-v", "--verbose", action="store_true")
group.add_argument("-q", "--quiet", action="store_true")
parser.add_argument("x", type=int, help="la base")
parser.add_argument("y", type=int, help="l’exposant")
args = parser.parse_args()
reponse = args.x**args.y

if args.quiet:
    print(reponse)
elif args.verbose:
    print("{} à la puissance {} est égal à {}".format(args.x, args.y, reponse))
else:
    print("{}^{} = {}".format(args.x, args.y, reponse))
```

On ajoute l’option `description=` lors de la création de l’objet `argparse.ArgumentParser()`

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse18.py --help
usage: argparse18.py [-h] [-v \| -q] x y

calcule X à la puissance Y

positional arguments:
    x                 la base
    y                 l’exposant

optional arguments:
    -h, --help show   this help message and exit
    -v, --verbose
    -q, --quiet
```

C’est bien jolie tout cela mais on mélange de l’anglais avec du Français.

Fichier **argparse19.py** :

Comment faire pour traduire les messages d’aide en Français ?

```
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import gettext

__TRANSLATIONS = {
    'ambiguous option: %(option)s could match %(matches)s': 'option ambiguë: %(option)s parmi %(matches)s', 'argument "-" with mode %r': 'argument "-" en mode %r', 'cannot merge actions - two groups are named %r': 'cannot merge actions - two groups are named %r', "can't open '%(filename)s': %(error)s": "can't open '%(filename)s': %(error)s", 'dest= is required for options like %r': 'dest= is required for options like %r', 'expected at least one argument': 'au moins un argument est attendu', 'expected at most one argument': 'au plus un argument est attendu', 'expected one argument': 'un argument est nécessaire', 'ignored explicit argument %r': 'ignored explicit argument %r', 'invalid choice: %(value)r (choose from %(choices)s)': 'choix invalide: %(value)r (parmi %(choices)s)', 'invalid conflict_resolution value: %r': 'invalid conflict_resolution value: %r', 'invalid option string %(option)r: must start with a character %(prefix_chars)r': 'invalid option string %(option)r: must start with a character %(prefix_chars)r', 'invalid %(type)s value: %(value)r': 'valeur invalide de type %(type)s: %(value)r', 'mutually exclusive arguments must be optional': 'mutually exclusive arguments must be optional', 'not allowed with argument %s': "pas permis avec l'argument %s", 'one of the arguments %s is required': 'au moins un argument requis parmi %s', 'optional arguments': 'arguments optionnels', 'positional arguments': 'arguments positionnels', "'required' is an invalid argument for positionals": "'required' is an invalid argument for positionals", 'show this help message and exit': 'afficher ce message d\’aide', 'unrecognized arguments: %s': 'argument non reconnu: %s', 'unknown parser %(parser_name)r (choices: %(choices)s)': 'unknown parser %(parser_name)r (choices: %(choices)s)', 'usage: ': 'utilisation: ', '%(prog)s: error: %(message)s\n': '%(prog)s: erreur: %(message)s\n', '%r is not callable': '%r is not callable', }

gettext.gettext = lambda text: __TRANSLATIONS[text] or text

import argparse

parser = argparse.ArgumentParser(description="calcule X à la puissance Y")
group = parser.add_mutually_exclusive_group()
group.add_argument("-v", "--verbose", action="store_true")
group.add_argument("-q", "--quiet", action="store_true")
parser.add_argument("x", type=int, help="la base")
parser.add_argument("y", type=int, help="l’exposant")
args = parser.parse_args()
reponse = args.x**args.y

if args.quiet:
    print(reponse)
elif args.verbose:
    print("{} à la puissance {} est égal à {}".format(args.x, args.y, reponse))
else:
    print("{}^{} = {}".format(args.x, args.y, reponse))
```

La traduction se fait avec gettext.

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse19.py --help
utilisation: argparse19.py [-h] [-v | -q] x y

calcule X à la puissance Y

arguments positionnels:
    x             la base
    y             l’exposant

arguments optionnels:
    -h, --help    affiche ce message d’aide
    -v, --verbose
    -q, --quiet
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ cd ..
```

### Gestion des dates et du temps

#### Date et heure

**Datetime** est un module qui permet de manipuler des dates et des durées sous forme d’objets. L’idée est simple: vous manipulez l’objet pour faire tous vos calculs, et quand vous avez besoin de l’afficher, vous formatez l’objet en chaîne de caractères.

On peut créer artificiellement un objet **datetime** , ses paramètres sont:

```
datetime (année, mois, jour, heure, minute, seconde, microseconde, fuseau horaire)
```

Mais seuls «année», «mois» et «jour» sont obligatoires.

```
>>> from datetime import datetime
>>> datetime(2000, 1, 1)
datetime.datetime(2000, 1, 1, 0, 0)
```

Nous sommes ici le premier janvier 2000, à la seconde et la minute zéro, de l’heure zéro.

On peut bien entendu récupérer l’heure et la date du jour:

```
>>> actuellement = datetime.now()
>>> actuellement
datetime.datetime(2021, 7, 9, 10, 13, 1, 25073)
>>> actuellement.year
2021
>>> actuellement.month
7
>>> actuellement.day
9
>>> actuellement.hour
10
>>> actuellement.minute
13
>>> actuellement.second
1
>>> actuellement.microsecond
25073
>>> actuellement.isocalendar() # année, semaine, jour
datetime.IsoCalendarDate(year=2021, week=27, weekday=5)
>>> maintenant = datetime.now # obtenir l’heure avec une variable
>>> print(maintenant())
2021-07-09 10:14:51.359460
>>> print(maintenant())
2021-07-09 10:15:0.918195
```

Enfin, si vous souhaitez uniquement vous occuper de la date ou de l’heure:

```
>>> print(maintenant().strftime('%Hh %Mmin %Ss %d/%m/%Y')) # change une date en chaîne.
10h 16min 22s 09/07/2021
>>> from datetime import date, time, datetime
>>> maDate = datetime.strptime('2021-06-05 12:30:00', '%Y-%m-%d %H:%M:%S') # change une chaîne en date.
>>> print(maDate)
2021-06-05 12:30:00
>>> maDate
datetime.datetime(2021, 6, 5, 12, 30)
```

#### Durée

En plus de pouvoir récupérer la date du jour, on peut calculer la différence entre deux dates. Par exemple, combien de temps y a-t-il entre aujourd’hui et le premier jour de l’an 2000 ?

```
>>> duree = maintenant() - datetime(2000, 1, 1)
>>> duree
datetime.timedelta(days=7860, seconds=39227, microseconds=140524)
```

Et vous découvrez ici un autre objet, le **timedelta**. Cet objet représente une durée en jours, secondes et microsecondes.

```
>>> duree.days
7860
>>> duree.seconds
39227
>>> duree.microseconds
140524
>>> duree.total_seconds
<built-in method total_seconds of datetime.timedelta object at 0x7efc4f7655d0>
>>> duree.total_seconds()
679144227.140524
```

On peut créer son propre **timedelta** :

```
>>> from datetime import timedelta
>>> print(timedelta(days=3, seconds=100))
3 days, 0:01:40
```

Cela permet de répondre à la question : «Quelle date serons-nous dans 2 jours, 4 heures, 3 minutes, et 12 secondes ?»:

```
>>> print(maintenant() + timedelta(days=2, hours=4, minutes=3, seconds=12))
2021-07-11 15:12:00.371922
```

Les objets **datetime** et **timedelta** sont immutables. Ainsi si vous voulez utiliser une version légèrement différente d’un objet **datetime** , il faudra toujours en créer un nouveau. Par exemple:

```
>>> actuellement.replace(year=1995) # on créer un nouvel objet
datetime.datetime(1995, 7, 9, 10, 13, 1, 25073)
```

Vous noterez que je ne parles pas de fuseau horaire. Et bien c’est parce que l’implémentation Python est particulièrement ratée : l’API est compliquée et les données ne sont pas à jour. Il faut dire que la mesure du temps, contrairement à ce qu’on pourrait penser, n’est pas vraiment le truc le plus stable du monde, et des pays changent régulièrement leur manière de faire.

#### Calendrier

Le module **calendar**.

Il permet de manipuler un calendrier comme un objet, et de déterminer les jours d’un mois, les semaines, vérifier les caractéristiques d’un jour en particulier, etc. :

```
>>> import calendar
>>> calendar.mdays # combien de jour par mois ?
[0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
>>> calendar.isleap(2000) # est-ce une année bissextile ?
True
>>> calendar.weekday(2000, 1, 1) # quel jour était cette date ?
5
>>> calendar.MONDAY, calendar.TUESDAY, calendar.WEDNESDAY, calendar.THURSDAY, calendar.FRIDAY
(0, 1, 2, 3, 4)
```

On peut instancier un calendrier et itérer dessus:

```
>>> cal = calendar.Calendar()
>>> cal.getfirstweekday()
0
>>> list(cal.iterweekdays())
[0, 1, 2, 3, 4, 5, 6]
>>> list(cal.itermonthdays(2000, 1))
[0, 0, 0, 0, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 0, 0, 0, 0, 0, 0]
>>> list(cal.itermonthdates(2000, 1))
[datetime.date(1999, 12, 27), datetime.date(1999, 12, 28), datetime.date(1999, 12, 29), datetime.date(1999, 12, 30), datetime.date(1999, 12, 31), datetime.date(2000, 1, 1), datetime.date(2000, 1, 2), datetime.date(2000, 1, 3), datetime.date(2000, 1, 4), datetime.date(2000, 1, 5), datetime.date(2000, 1, 6), datetime.date(2000, 1, 7), datetime.date(2000, 1, 8), datetime.date(2000, 1, 9), datetime.date(2000, 1, 10), datetime.date(2000, 1, 16), datetime.date(2000, 1, 17), datetime.date(2000, 1, 18), datetime.date(2000, 1, 19), datetime.date(2000, 1, 20),     datetime.date(2000, 1, 21), datetime.date(2000, 1, 22), datetime.date(2000, 1, 23), datetime.date(2000, 1, 24), datetime.date(2000, 1, 25), datetime.date(2000, 1, 26), datetime.date(2000, 1, 27), datetime.date(2000, 1, 28), datetime.date(2000, 1, 29), datetime.date(2000, 1, 30), datetime.date(2000, 1, 31), datetime.date(2000, 2, 1), datetime.date(2000, 2, 2), datetime.date(2000, 2, 3), datetime.date(2000, 2, 4), datetime.date(2000, 2, 5), datetime.date(2000, 2, 6)]
>>> cal.monthdayscalendar(2000, 1)
[[0, 0, 0, 0, 0, 1, 2], [3, 4, 5, 6, 7, 8, 9], [10, 11, 12, 13, 14, 15, 16], [17, 18, 19, 20, 21, 22, 23], [24, 25, 26, 27, 28, 29, 30], [31, 0, 0, 0, 0, 0, 0]]
```

Comme souvent Python vient aussi avec de très bons modules tierces pour manipuler les dates :


* **dateutils** est un datetime boosté aux hormones qui permet notamment de donner des durées floues comme “+ 1 mois” et de gérer des événements qui se répètent.


* **babel** n’est pas spécialisé dans les dates mais dans la localisation. Le module possède des outils pour formater des dates selon le format de chaque pays, et aussi avec des formats naturels comme “il y a une minute”.


* **pytz** est une implémentation saine de gestion des fuseaux horaires en Python.

### Module Windows

```
pip install pywin32
```

Vous pouvez écrire des logs dans le gestionnaire d’évènements windows. Pour cela vous devez utiliser le module **win32evlog**

```
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import win32evtlog
import pprint
import sys

# S'abonne aux événements de l'application et les enregistre.
# Pour déclencher manuellement un nouvel événement, ouvrez une console d'administration et tapez: (remplacez 125 par tout autre ID qui vous convient)
#     eventcreate.exe /L "application" /t warning /id 125 /d "Ceci est un avertissement de test"
#
# event_context peut être `None` si ce n'est pas obligatoire, c'est juste pour montrer comment cela fonctionne
event_context = {"info": "cet objet est toujours passé à votre retour"}
# Event log source to listen to
event_source = 'application'

def new_logs_event_handler(raison, contexte, evnmt):
    """
    Appelé lorsque de nouveaux événements sont enregistrés.

    raison - raison pour laquelle l'événement a été enregistré?
    contexte- contexte dans lequel le gestionnaire d'événements a été enregistré
    evnmt - événement capturé
    """
    # Imprimez simplement quelques informations sur l'événement
    print('raison', raison, 'contexte', contexte, 'événement capturé', evnmt)

    # Rendre l'événement en XML, il y a peut-être un moyen d'obtenir un objet mais je ne l'ai pas trouvé
    print('Événement rendu :', win32evtlog.EvtRender(evt, win32evtlog.EvtRenderEventXml))

    # ligne vide pour séparer les journaux
    print(' - ')

    # Assurez-vous que tout le texte imprimé est réellement imprimé sur la console maintenant
    sys.stdout.flush()

    return 0

# Abonnez-vous aux futurs événements
subscription = win32evtlog.EvtSubscribe(event_source, win32evtlog.EvtSubscribeToFutureEvents, None, Callback=new_logs_event_handler, Context=event_context, Query=None)
```

Exemple plus complet

```
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import win32evtlog
import win32api
import win32con
import win32security # Pour traduire NT Sids en noms de compte.
import win32evtlogutil

def ReadLog(computer, logType="Application", dumpEachRecord = 0):
    # Lit l'intégralité du journal.
    h=win32evtlog.OpenEventLog(computer, logType)
    numRecords = win32evtlog.GetNumberOfEventLogRecords(h)
    print("Il y a% enregistrements" % numRecords)

    num=0
    while 1:
        objects = win32evtlog.ReadEventLog(h, win32evtlog.EVENTLOG_BACKWARDS_READ|win32evtlog.EVENTLOG_SEQUENTIAL_READ, 0)
        if not objects:
            break
        for object in objects:
            # À des fins de test, mais ne l'imprime pas.
            msg = win32evtlogutil.SafeFormatMessage(object, logType)
            if object.Sid is not None:
                try:
                    domain, user, typ = win32security.LookupAccountSid(computer, object.Sid)
                    sidDesc = "%s/%s" % (domain, user)
                except win32security.error:
                    sidDesc = str(object.Sid)
                    user_desc = "Événement associé à l'utilisateur %s" % (sidDesc,)
            else:
                user_desc = None
            if dumpEachRecord:
                print("Enregistrement d'événement de %r généré à %s"\ % (object.SourceName, object.TimeGenerated.Format()))
                if user_desc:
                    print user_desc
                    try:
                        print msg
                    except UnicodeError:
                        print("(message d'impression d'erreur unicode: repr() suit…)")
                        print(repr(msg))
        num = num + len(objects)

    if numRecords == num:
        print("Succès de la lecture complète", numRecords, "enregistrements")
    else:
        print("Impossible d'obtenir tous les enregistrements - signalé %d, mais trouvé %d" % (numRecords, num))
        print ("(Notez d'autres applications peuvent avoir écrit des enregistrements pendant l'exécution!)")
    win32evtlog.CloseEventLog(h)

def usage():
    print("Écrit un événement dans le journal des événements.")
    print("-w : N'écrire aucun enregistrement de test.")
    print("-r : Ne pas lire le journal des événements")
    print("-c : nomOrdinateur : Traiter le journal sur l'ordinateur spécifié")
    print("-v : Verbeux")
    print("-t : LogType - Utiliser le journal spécifié - défaut = 'Application'")

def test():
    # vérifier s'il fonctionne sous Windows NT, sinon, afficher un avis et terminer
    if win32api.GetVersion() & 0x80000000:
        print("Cet exemple ne fonctionne que sur NT")
        return

    import sys, getopt
    opts, args = getopt.getopt(sys.argv[1:], "rwh?c:t:v")
    computer = None
    do_read = do_write = 1
    logType = "Application"
    verbose = 0

    if len(args)>0:
        print("Arguments non valides")
        usage()
        return 1

    for opt, val in opts:
        if opt == '-t':
            logType = val
        if opt == '-c':
            computer = val
        if opt in ['-h', '-?']:
            usage()
            return
        if opt == '-r':
            do_read = 0
        if opt == '-w':
            do_write = 0
        if opt == '-v':
            verbose = verbose + 1

    if do_write:
        ph = win32api.GetCurrentProcess()
        th = win32security.OpenProcessToken(ph,win32con.TOKEN_READ)
        my_sid = win32security.GetTokenInformation(th,win32security.TokenUser)[0]

        win32evtlogutil.ReportEvent(logType, 2,
            strings=["Le texte du message pour l'événement 2", "Un autre insert"],
            data="Raw\0Data".encode("ascii"), sid=my_sid)
        win32evtlogutil.ReportEvent(logType, 1, eventType=win32evtlog.EVENTLOG_WARNING_TYPE,
            strings=["Un avertissement", "Un avertissement encore plus grave"],
            data="Raw\0Data".encode("ascii"), sid=my_sid)
        win32evtlogutil.ReportEvent(logType, 1, eventType=win32evtlog.EVENTLOG_INFORMATION_TYPE,
            strings=["Une info", "Trop d'informations"],
            data="Raw\0Data".encode("ascii"), sid=my_sid)
        print("Écriture réussie de 3 enregistrements dans le journal")

    if do_read:
        ReadLog(computer, logType, verbose > 0)

if __name__ == '__main__':
    test()
```

## L’organisation du code

### Structures de contrôles

#### Structures alternatives

##### If, else, elif

Les instructions [if, else, elif](https://courspython.com/tests.html#if) sont sans doute les plus connues.

Par exemple :

```
>>> import os
>>> x = int(input("SVP entrez un entier: "))
SVP entrez un entier: 42
>>> if x < 0:
...     x = 0
...     print('Nombre négatif remplacé par zéro')
... elif x == 0:
...     print('Zéro')
... elif x == 1:
...     print('Unité')
... else:
...     print('Plus grand')
...
Plus grand
```

Il peut y avoir un nombre quelconque de parties `elif` et la partie `else` est facultative. Le mot clé `elif` est un raccourci pour `else if`, mais permet de gagner un niveau d’indentation. Une séquence `if ... elif ... elif ...` est par ailleurs équivalente aux instructions «**switch**» ou «**case**» disponibles dans d’autres langages.

#### Structures itératives

##### For in

Les instructions [for in](https://courspython.com/boucles.html#for) que propose Python sont un peu différente de celle que l’on peut trouver en C ou en Pascal.

Au lieu de toujours itérer sur une suite arithmétique de nombres (comme en Pascal), ou de donner à l’utilisateur la possibilité de définir le pas d’itération et la condition de fin (comme en C), l’instruction `for` en Python itère sur les éléments d’une séquence (qui peut être une liste, une chaîne de caractères…), dans l’ordre dans lequel ils apparaissent dans la séquence.

Par exemple :

```
>>> # Mesure quelques chaînes de caractères
>>> mots = ['fenêtre', 'chat', 'quantique']
>>> for l in mots:
...     print(l, len(l))
...
fenêtre 7
chat 4
quantique 9
```

Le code qui modifie une collection tout en itérant sur cette même collection peut être délicat à mettre en place.

```
>>> # Strategie:  Itérer sur une copie
>>> utilisateurs = {'Vador': 'inactif', 'Luc': 'actif', 'Padawan': 'actif'}
>>> for utilisateur, statut in utilisateurs.copy().items():
...     if statut == 'inactif':
...         del utilisateurs[utilisateur]
...
...
>>> utilisateurs
{'Luc': 'actif', 'Padawan': 'actif'}
```

Au lieu de cela, il est généralement plus simple de boucler sur une copie de la collection ou de créer une nouvelle collection :

```
>>> # Strategie: Créer une nouvelle collection utilisateurs
>>> utilisateurs = {'Vador': 'inactif', 'Luc': 'actif', 'Padawan': 'actif'}
>>> utilisateurs_actifs = utilisateurs.copy()
>>> utilisateurs_vivants = []
>>> for utilisateur, statut in utilisateurs.items():
...     if statut == 'inactif':
...         del utilisateurs_actifs[utilisateur]
... else:
...          utilisateurs_vivants.append(utilisateur)
...
>>> utilisateurs_actifs
{'Luc': 'actif', 'Padawan': 'actif'}
>>> utilisateurs_vivants
['Luc', 'Padawan']
>>> utilisateurs
{'Vador': 'inactif', 'Luc': 'actif', 'padawan': 'actif'}
```

Pour itérer sur les indices d’une séquence, on peut combiner les fonctions `range()` et `len()` :

```
>>> phrase = ['Luc', 'a', 'un', 'petit', 'laser']
>>> for mot in range(len(phrase)):
...     print(mot, phrase[mot])
...
0 Luc
1 a
2 un
3 petit
4 laser
```

Cependant, dans la plupart des cas, il est plus pratique d’utiliser la fonction `enumerate()`.

```
>>> for mot in enumerate(phrase):
...     print(mot[0], mot[1])
...
0 Luc
1 a
2 un
3 petit
4 laser
```

##### While

L’instruction [while](https://courspython.com/boucles.html#while) permet de faire des boucle suivant une condition.

###### break, continue

L’instruction `break`, comme en C, interrompt la boucle `for` ou `while`.

Les boucles peuvent également disposer d’une instruction `else`. Celle-ci est exécutée lorsqu’une boucle se termine alors que tous ses éléments ont été traités (dans le cas d’un `for`) ou que la condition devient fausse (dans le cas d’un `while`), **mais pas lorsque la boucle est interrompue par une instruction** `break`.

L’exemple suivant, qui effectue une recherche de nombres premiers, en est une démonstration :

```
>>> for n in range(2, 10):
...     for x in range(2, n):
...         if n % x == 0:
...             print(n, 'égal', x, '*', n//x)
...             break
...     else:
...         # la boucle s’est terminée sans trouver de facteur
...         print(n, 'est un nombre premier')
...
...
2 est un nombre premier
3 est un nombre premier
4 égal 2 * 2
5 est un nombre premier
6 égal 2 * 3
7 est un nombre premier
8 égal 2 * 4
9 égal 3 * 3
```

Oui, ce code est correct. Regardez attentivement : l’instruction `else` est rattachée à la boucle `for`, et non à l’instruction `if`.

Lorsqu’elle est utilisée dans une boucle, la clause `else` est donc plus proche de celle associée à une instruction `try` que de celle associée à une instruction `if`: la clause `else` d’une instruction `try` s’exécute lorsqu’aucune exception n’est déclenchée, et celle d’une boucle lorsqu’aucun `break` n’intervient. Nous verrons plus ultérieurement dans ce cours l’instruction `try` et le traitement des exceptions.

L’instruction `continue`, également empruntée au C, fait passer la boucle à son itération suivante :

```
>>> for num in range(2, 10):
...     if num % 2 == 0:
...         print("Un nombre pair a été trouvé : ", num)
...         continue
...     print("Un nombre impair a été trouvé : ", num)
...
Un nombre pair a été trouvé :  2
Un nombre impair a été trouvé :  3
Un nombre pair a été trouvé :  4
Un nombre impair a été trouvé :  5
Un nombre pair a été trouvé :  6
Un nombre impair a été trouvé :  7
Un nombre pair a été trouvé :  8
Un nombre impair a été trouvé :  9
```

###### pass

L’instruction `pass` ne fait rien. Elle peut être utilisée lorsqu’une instruction est nécessaire pour fournir une syntaxe correcte, mais qu’aucune action ne doit être effectuée.

Par exemple :

```
>>> while True:
...     pass  # Attente occupée pour l'interruption du clavier (Ctrl + C)
...
^CTraceback (most recent call last):
 File "<stdin>", line 2, in <module>
KeyboardInterrupt
```

On utilise couramment cette instruction pour créer des classes minimales d’objets :

```
>>> class MaClasseVide:
...     pass
...
>>>
```

Un autre cas d’utilisation du `pass` est de réserver un espace en phase de développement pour une fonction ou un traitement conditionnel, vous permettant ainsi de construire votre code à un niveau plus abstrait.

L’instruction **pass** est alors ignorée silencieusement :

```
>>> def initlog(*args):
...     pass   # N'oubliez pas de mettre en œuvre cela!
...
```

##### alternative do while

```
>>> while True:
...     #… code
...     if cond :
...         break
```

##### Techniques de boucles des dictionnaires

Lorsque vous faites une boucle sur un dictionnaire, les clés et leurs valeurs peuvent être récupérées en même temps en utilisant la méthode `items()` :

```
>>> chevaliers_jedi = {'Luc': 'le padawan', 'Yoda': 'le grand maître', 'Obi-Wan Kenobi': 'le guerrier'}
>>> for k, v in chevaliers_jedi.items(): print(k, v)
...
Luc le padawan
Yoda le grand maître
Obi-Wan Kenobi le guerrier
```

Lorsque vous faites une boucle sur une séquence, la position et la valeur correspondante peuvent être récupérées en même temps en utilisant la fonction `enumerate()` :

```
>>> for i, v in enumerate(['tic', 'tac', 'toe']): print(i, v)
...
0 tic
1 tac
2 toe
```

Pour faire une boucle sur deux séquences, ou plus en même temps, les éléments peuvent être associés en utilisant la fonction `zip()` :

```
>>> questions = ['nom', 'côté de la force', 'couleur du sabre']
>>> réponses = ['luc', 'la lumière', 'le vert']
>>> for q, r in zip(questions, réponses):
...     print('Quel est votre {0} ? C\’est {1}.'.format(q, r))
...
Quel est votre nom ? C’est luc.
Quel est votre côté de la force ? C’est la lumière.
Quel est votre couleur du sabre ? C’est le vert.
```

Pour faire une boucle en sens inverse sur une séquence, commencez par spécifier la séquence dans son ordre normal, puis appliquez la fonction `reversed()` :

```
>>> for n in reversed(range(1, 10, 2)):  print(n)
...
9
7
5
3
1
```

Pour faire une boucle sur une séquence de manière ordonnée, utilisez la fonction `sorted()` qui renvoie une nouvelle liste ordonnée sans altérer la source :

```
>>> panier = ['pomme', 'orange', 'pomme', 'poire', 'orange', 'banane']
>>> for f in sorted(panier): print(f)
...
banane
orange
orange
poire
pomme
pomme
```

L’utilisation de la fonction `set()` sur une séquence élimine les doublons. L’utilisation de la fonction `sorted()` en combinaison avec `set()` sur une séquence est une façon idiomatique de boucler sur les éléments uniques d’une séquence dans l’ordre :

```
>>> for f in sorted(set(panier)): print(f)
...
banane
orange
poire
pomme
```

Il est parfois tentant de modifier une liste pendant son itération. Cependant, c’est souvent plus simple et plus sûr de créer une nouvelle liste à la place. :

```
>>> import math
>>> données_brutes = [56.2, float('NaN'), 51.7, 55.3, 52.5, float('NaN'), 47.8]
>>> données_filtrées = []
>>> for valeur in données_brutes:
...     if not math.isnan(valeur):
...         données_filtrées.append(valeur)
...
...
>>> données_filtrées
[56.2, 51.7, 55.3, 52.5, 47.8]
```

# La génération de la documentation du programme

Voir [https://www.codeflow.site/fr/article/documenting-python-code](https://www.codeflow.site/fr/article/documenting-python-code)

Sphinx est un outil très complet permettant de générer des documentations riches et bien structurées. Il a originellement été créé pour la documentation du langage Python, et a très vite été utilisé pour documenter de nombreux autres projets.

Pour ce qui est de la documentation de code, il est évidemment bien adapté au Python, mais peut aussi être utilisé avec d’autres langages.

Parmi les fonctionnalités que Sphinx propose :


* la génération automatique de la documentation à partir du code (avec le support de nombreux langages),


* la possibilité de faire des références entre les pages,


* le support de plusieurs formats de sortie (HTML, PDF, LaTeX, EPUB, pages de manuel, …),


* la gestion d’extensions permettant de l’adapter à toutes les situations et langages.

## Configurer Sphinx pour Python

Le répertoire racine Sphinx d’une collection de textes bruts permettant de générer la documentation est appelé le [répertoire source](https://ugaugtjjyr2ylvegfxect62ccm--www-sphinx-doc-org.translate.goog/en/master/glossary.html#term-source-directory).
C’est le répertoire que nous avons nommé lors de l’installation «**sources-documents**».

Ce répertoire contient également le fichier de configuration Sphinx **conf.py**, où vous pouvez configurer tous les aspects de la façon dont Sphinx lit vos sources et construit votre documentation.

Pour paramétrer tous ces aspects, il faut éditer le fichier **conf.py** qui se trouve dans le dossier **~/repertoire_de_developpement/docs/sources-documents**.

### Renseigner la racine des fichiers de CODE

Indiquez où se trouve vos fichiers de code python pour générer votre documentation :

```
# -*- coding: utf-8 -*-

import os
import sys

sys.path.insert(0, os.path.abspath('../..'))
sys.setrecursionlimit(1500)

# Liste des modèles, relatifs au répertoire source, qui correspondent aux
# fichiers et répertoires à ignorer lors de la recherche de fichiers source.
# Ce modèle affecte également html_static_path et html_extra_path.
exclude_patterns = ['.env']

# Une liste de chemins contenant des modèles supplémentaires
# (ou des modèles qui remplacent les modèles intégrés/spécifiques au thème).
# Les chemins relatifs sont considérés comme relatifs au répertoire de
# configuration.
templates_path = ['_templates']
```

### Configurer les paramètres de documentation

Langue, titre, copyright et auteur :

```
langage = 'fr'

# L’internationalisation de la documentation
locale_dirs = ['locales/']
gettext_compact = False

project = "Documentation sur l’initiation à la programmation Python pour l’administrateur systèmes"
copyright = '2021, Prénom NOM'
author = 'Prénom NOM'
```

Versions du document :

```
from Documentation.mon_module import __version__, __release_life_cycle__

version = __version__ # utilisation restructuredtext |version|
release = __release_life_cycle__ # utilisation restructuredtext |release|
# pre-alpha = faisabilité, alpha = développement, beta = test, rc = qualification, prod = production
```

Extensions des fichiers de la documentation :

```
source_suffix = {
    '.rst': 'restructuredtext',
    '.txt': 'restructuredtext',
    '.md': 'markdown',
}
# ou de la forme
# source_suffix = ['.rst', '.md']
# source_suffix = '.rst'
```

Autres paramètres :

```
# Le document maître toctree.
master_doc = 'index'

# Le thème de la coloration syntaxique
pygments_style = 'sphinx'
```

### Changer de thème pour votre documentation

Le thème par défaut, Alabaster, est très minimaliste, pour avoir une documentation plus sexy, il est préférable de changer le thème par défaut.



![image](images/documentation_1.png)

#### Le thème sphinx book



![image](images/documentation_2.png)

Il existe un certain nombre de thèmes intégrés à Sphinx, et de nombreux autres disponibles. Vous pouvez consulter les thèmes disponibles sur le site [https://sphinx-themes.org/](https://sphinx-themes.org/) .

On va donc voir comment installer **sphinx-book-theme** (mais libre à vous d’en choisir un autre, le principe reste le même).

Pour utiliser le thème «**Sphinx book**», il faut commencer par l’installer, ce qui peut être fait à l’aide de la commande suivante :

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ cd docs ; sudo pip install sphinx-book-theme
Collecting sphinx-book-theme
    Downloading sphinx_book_theme-0.1.0-py3-none-any.whl (87 kB)
        |████████████████████████████████| 87 kB 813 kB/s
Requirement already satisfied: sphinx<4,>=2 in /usr/local/lib/python3.9/dist-packages (from sphinx-book-theme) (3.5.4)
Requirement already satisfied: click in /usr/lib/python3/dist-packages (from sphinx-book-theme) (7.1.2)
Requirement already satisfied: docutils>=0.15 in /usr/local/lib/python3.9/dist-packages (from sphinx-book-theme) (0.16)
Collecting pydata-sphinx-theme~=0.6.0
    Downloading pydata_sphinx_theme-0.6.3-py3-none-any.whl (1.4 MB)
        |████████████████████████████████| 1.4 MB 3.9 MB/s
Collecting beautifulsoup4<5,>=4.6.1
    Downloading beautifulsoup4-4.9.3-py3-none-any.whl (115 kB)
        |████████████████████████████████| 115 kB 4.4 MB/s
Requirement already satisfied: pyyaml in /usr/lib/python3/dist-packages (from sphinx-book-theme) (5.3.1)
Collecting soupsieve>1.2
    Downloading soupsieve-2.2.1-py3-none-any.whl (33 kB)
Requirement already satisfied: packaging in /usr/local/lib/python3.9/dist-packages (from sphinx<4,>=2->sphinx-book-theme) (20.9)
Requirement already satisfied: babel>=1.3 in /usr/local/lib/python3.9/dist-packages (from sphinx<4,>=2->sphinx-book-theme) (2.9.0)
Requirement already satisfied: imagesize in /usr/local/lib/python3.9/dist-packages (from sphinx<4,>=2->sphinx-book-theme) (1.2.0)
Requirement already satisfied: sphinxcontrib-qthelp in /usr/local/lib/python3.9/dist-packages (from sphinx<4,>=2->sphinx-book-theme) (1.0.3)
Requirement already satisfied: Pygments>=2.0 in /usr/local/lib/python3.9/dist-packages (from sphinx<4,>=2->sphinx-book-theme) (2.8.1)
Requirement already satisfied: requests>=2.5.0 in /usr/lib/python3/dist-packages (from sphinx<4,>=2->sphinx-book-theme) (2.25.1)
Requirement already satisfied: setuptools in /usr/lib/python3/dist-packages (from sphinx<4,>=2->sphinx-book-theme) (52.0.0)
Requirement already satisfied: sphinxcontrib-devhelp in /usr/local/lib/python3.9/dist-packages (from sphinx<4,>=2->sphinx-book-theme) (1.0.2)
Requirement already satisfied: alabaster<0.8,>=0.7 in /usr/local/lib/python3.9/dist-packages (from sphinx<4,>=2->sphinx-book-theme) (0.7.12)
Requirement already satisfied: snowballstemmer>=1.1 in /usr/local/lib/python3.9/dist-packages (from sphinx<4,>=2->sphinx-book-theme) (2.1.0)
Requirement already satisfied: sphinxcontrib-serializinghtml in /usr/local/lib/python3.9/dist-packages (from sphinx<4,>=2->sphinx-book-theme) (1.1.4)
Requirement already satisfied: sphinxcontrib-htmlhelp in  /usr/local/lib/python3.9/dist-packages (from sphinx<4,>=2->sphinx-book-theme) (1.0.3)
Requirement already satisfied: sphinxcontrib-jsmath in /usr/local/lib/python3.9/dist-packages (from sphinx<4,>=2->sphinx-book-theme) (1.0.1)
Requirement already satisfied: Jinja2>=2.3 in /usr/local/lib/python3.9/dist-packages (from sphinx<4,>=2->sphinx-book-theme) (2.11.3)
Requirement already satisfied: sphinxcontrib-applehelp in /usr/local/lib/python3.9/dist-packages (from sphinx<4,>=2->sphinx-book-theme) (1.0.2)
Requirement already satisfied: pytz>=2015.7 in /usr/local/lib/python3.9/dist-packages (from babel>=1.3->sphinx<4,>=2->sphinx-book-theme) (2021.1)
Requirement already satisfied: MarkupSafe>=0.23 in /usr/local/lib/python3.9/dist-packages (from Jinja2>=2.3->sphinx<4,>=2->sphinx-book-theme) (1.1.1)
Requirement already satisfied: pyparsing>=2.0.2 in /usr/local/lib/python3.9/dist-packages (from packaging->sphinx<4,>=2->sphinx-book-theme) (2.4.7)
Installing collected packages: soupsieve, beautifulsoup4, pydata-sphinx-theme, sphinx-book-theme
Successfully installed beautifulsoup4-4.9.3 pydata-sphinx-theme-0.6.3 soupsieve-2.2.1 sphinx-book-theme-0.1.0
```

Ensuite, il faut indiquer à Sphinx d’utiliser ce thème.

Il faudra remplacer la variable «**html_theme**» dans «**conf.py**» et modifier dans la section HTML les paramètres pour ce thème :

```
# -- Options pour sortie HTML -------------------------------------------------
html_title = "Documentation développement python pour l'administrateur"
html_short_title = "Développement Python 3"
html_logo = "images/logo.png"
html_favicon = "images/favicon.png"

html_theme = 'sphinx_book_theme'

# -- Options du thème
html_theme_options = {
    # Ajout du renvoie vers Gitlab
    'repository_url': "http://gitlab.domaine-perso.fr/utilisateur/initiation_developpement_python_pour_administrateur",
    'use_repository_button': True,
    # Ajout de la possibilité de laisser des retours de dysfonctionnements
    'use_issues_button': True,
    # Ajout de la possibilité de laisser des propositions de corrections à la documentation
    'use_edit_page_button': True,
    # Ajout de la possibilité de travailler sur une branche définie
    #'repository_branch': 'master',
    # Ajout du chemin relatif vers les sources de la doc
    'path_to_docs': "docs/sources-documents",
    # Ajout d'un téléchargement Rest ou PDF de la page actuelle
    'use_download_button': True,
    # Ajout d'un bouton lecture plein écran
    'use_fullscreen_button': True,
    # Ajout de la table des matières dans le panneau latéral gauche
    'home_page_in_toc': False,
    # Titre du panneau latéral droite qui sera notre table des matières
    'toc_title': "Contenu",
    # Ajout au pied de page du panneau latéral gauche
    'extra_navbar': "<p>Version " + release + "</p>",
}

#html_sidebars = {
#    "**": ["sbt-sidebar-nav.html", "sbt-sidebar-footer.html"]
#}

# Une liste de chemins contenant des fichiers statiques personnalisés
# (tels que des feuilles de style ou des fichiers de script).
# Les chemins relatifs sont considérés comme relatifs au répertoire de
# configuration.
# Ils sont copiés dans le répertoire \_static de la sortie après les fichiers
# statiques du thème.
# Par conséquent, un fichier nommé default.css écrasera le fichier default.css
# du thème.
html_static_path = ['_static']
```

### Inclure les extensions utiles pour Python

Installation des extensions non incluses de base :

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement/docs$ sudo apt install sphinx-intl texinfo xindy graphviz latexmk texlive-lang-french texlive-xetex fonts-freefont-otf ; sudo pip install sphinxcontrib-inlinesyntaxhighlight sphinx-copybutton sphinx-markdown-builder sphinx-tabs pbr
utilisateur@MachineUbuntu:~/repertoire_de_developpement/docs$ sudo pip install --upgrade sphinx wget https://github.com/mans0954/odfbuilder/releases/download/0.0.1/sphinxcontrib-odfbuilder-0.0.1.tar.gz
utilisateur@MachineUbuntu:~/repertoire_de_developpement/docs$ sudo pip install sphinxcontrib-odfbuilder
```

Il existe de nombreuses extensions intégrés à Sphinx, je vous présente ici celles qui me paraissent les plus utiles :


* [sphinx.ext.intersphinx](https://www.sphinx-doc.org/fr/master/usage/extensions/intersphinx.html) : générer des liens automatiques dans la documentation suivant des mots clés.


* [sphinx.ext.extlinks](https://www.sphinx-doc.org/en/master/usage/extensions/extlinks.html) : Fournit des alias aux URL de base de votre documentation, de sorte que vous n’avez qu’à donner le nom d’alias pour la création du lien dans votre documentation.


* [sphinx.ext.autodoc](https://www.sphinx-doc.org/en/master/usage/extensions/autodoc.html) : Insère automatiquement les docstrings des fonctions, des classes ou des modules Python dans votre documentation. Permet de construire directement de la documentation à partir de votre code Python.


* [sphinxcontrib.inlinesyntaxhighlight](https://sphinxcontrib-inlinesyntaxhighlight.readthedocs.io/en/latest/) : Insère du code d’un langage de programmation dans une ligne de texte.


* [sphinxcontrib-bibtex](https://sphinxcontrib-bibtex.readthedocs.io/en/latest/) : Insère des références bibliographiques.


* [sphinx.ext.todo](https://www.sphinx-doc.org/en/master/usage/extensions/todo.html) : Insère des taches, ou liste de taches à faire dans votre documentation.


* [sphinx.ext.githubpages](https://www.sphinx-doc.org/en/master/usage/extensions/githubpages.html) : Cette extension créée un fichier **.nojekyll** dans le répertoire HTML généré pour publier le document sur les pages GitHub.


* [sphinx.ext.imgmath](https://bizdd.readthedocs.io/en/latest/ext/math.html) : Insère des formules mathématiques dans votre documentation.


* [sphinx.ext.graphviz](https://www.sphinx-doc.org/en/master/usage/extensions/graphviz.html) : Cette extension vous permet d’intégrer des [graphiques Graphviz](https://cyberzoide.developpez.com/graphviz/) dans vos documents.


* [sphinx.ext.inheritance_diagram](https://www.sphinx-doc.org/en/master/usage/extensions/inheritance.html) : Cette extension vous permet d’inclure des diagrammes d’héritage, rendus via l’extension Graphviz.


* [sphinx_copybutton](https://sphinx-copybutton.readthedocs.io/en/latest/) : insère dans votre documentation une icône pour copier dans le presse-papiers le contenu de la directive.


* [hieroglyph](https://pvbookmarks.readthedocs.io/en/latest/documentation/doc_generators/sphinx/contributed_extensions/hieroglyph.html) : Permet de générer des slides de présentations.


* [sphinx.ext.ifconfig](https://www.sphinx-doc.org/en/master/usage/extensions/ifconfig.html) : Inclure le contenu de la directive uniquement si l’expression Python donnée en argument est `True`.


* [sphinx.ext.doctest](https://www.sphinx-doc.org/en/master/usage/extensions/doctest.html) : Cette extension vous permet de tester un code Python dans la documentation. Le constructeur doctest collectera le résultat et pourra agir suivant les retours d’exécutions obtenus.


* [sphinx_markdown_builder](https://pypi.org/project/sphinx-markdown-builder/) : Pour construire de la documentation au format markdown pour gitlab et créer les formats .odt ou .docx.

#### Ajout des extensions utiles pour Python et Sphinx

```
extensions = [
    'sphinx.ext.intersphinx',
    'sphinx.ext.extlinks',
    'sphinx.ext.autodoc',
    'sphinxcontrib.inlinesyntaxhighlight',
    'sphinx.ext.githubpages',
    'sphinx.ext.graphviz',
    'sphinx.ext.inheritance_diagram',
    'sphinx_copybutton',
    'sphinx.ext.tabs',
    'sphinx.ext.todo',
    'sphinx.ext.ifconfig',
    'sphinx.ext.doctest',
    'sphinx_markdown_builder',
    'sphinxcontrib-odfbuilder',
]
```

### Configuration des extensions

```
# -- Configuration des extensions ---------------------------------------------
```

#### Intersphinx

```
# -- Options pour intersphinx -------------------------------------------------
# Alias du lien de la documentation de Python 3
intersphinx_mapping = {'python': ('https://docs.python.org/fr/3/', None)}
# Utilisation restructuredtext:
# index de page WEB
# :ref:`python:reference-index`
# :ref:`Référence langage Python <python:reference-index>`
# lien WEB
# :doc:`python:library/enum`
# :doc:`Énumérations <python:library/enum>`
```

#### Extlinks

```
# -- Options pour extlinks ----------------------------------------------------
# Liens vers la documentation de Python 3
extlinks = {
    'docpython3': ('https://docs.python.org/fr/3/%s', 'Python'),
    'manpython3': ('https://docs.python.org/fr/3/library/%s.html', 'Manuel Python de '),
}
# utilisation restructuredtext :docpython3:`tutorial` ou :manpython3:`enum`
```

#### Autodoc

```
# -- Options pour autodoc -----------------------------------------------------
# Simule l'existence du module classes de python pour ne pas être bloqué
autodoc_mock_imports = ['classes']
```

#### Inline Syntax highlight

```
# -- Options pour inline syntax highlight --------------------------------------
# Langage défini par la directive de surbrillance si aucun langage n'est défini par le rôle
inline_highlight_respect_highlight = False
# Langage défini par la directive de surbrillance si aucun rôle n'est défini
inline_highlight_literals = False
```

#### Graphviz

```
# -- Options pour Graphviz ----------------------------------------------------
graphviz_output_format = 'svg'
# utilisation restructuredtext :
# .. graphviz::
#
#     digraph frameworks web python {
#         python [label="python", href="https://www.python.org/", target="_top"];
#         flask [label="Flask", href="https://flask.palletsprojects.com/en/1.1.x/", target="_top"];
#         django [label="Bjango", href="https://www.djangoproject.com/", target="_top"];
#         bottle [label="Bottle", href="https://bottlepy.org/", target="_top"];
#         turbogears [label="TurboGears", href="https://turbogears.org/", target="_top"];
#         web2py [label="web2py", href="http://www.web2py.com/", target="_top"];
#         cherrypy [label="CherryPy", href="https://cherrypy.org/", target="_top"];
#         quixote [label="Quixote", href="http://quixote.ca/", target="_top"];
#         python -> {flask; django; bottle; turbogears; web2py; cherrypy; quixote;};
#     }
```

#### Copybutton

```
# -- Options pour copybutton ----------------------------------------------------
# copybutton_prompt_text = '>>> '
```

#### Tabs

```
# -- Options pour tabs ----------------------------------------------------
# utilisation restructuredtext :
# .. tabs::
#     .. tab:: Python
#         Ma documentation sur Python
#         .. tabs::
#             .. code-tab:: py
#                 Fichier Python main.py
#             .. code-tab:: java
#                 Fichier java
#         .. tabs::
#             .. code-tab:: py
#                 def main():
#                     return
#             .. code-tab:: java
#                 class Main {
#                     public static void main(String[] args) {
#                     }
#                 }
#     .. tab:: Frameworks Python
#         .. tabs::
#             .. group-tab:: Flask
#                 Ma documentation sur Flask
#             .. group-tab:: Django
#                 Ma documentation sur Django
#         .. tabs::
#             .. group-tab:: Flask
#                 Le code d'exemple pour Flask
#             .. group-tab:: Django
#                 Le code d'exemple pour Django*
```

#### Ifconfig

```
# -- Options pour ifconfig  ----------------------------------------------------
def setup(app):
    app.add_config_value('niveau_developpement', 'alpha', True)
# utilisation restructuredtext :
# .. ifconfig:: niveau_developpement not in ('alpha', 'beta', 'rc')
#     En production
# .. ifconfig:: 'alpha' == niveau_developpement
#     En developpement
# .. ifconfig:: 'beta' == niveau_developpement
#     En test
# .. ifconfig:: 'rc' == niveau_developpement
#     En qualification
```

#### LaTeX

```
# -- Options pour LaTeX -------------------------------------------------------
latex_engine = 'xelatex'
latex_elements = {
    # Le format du papier('letterpaper' ou 'a4paper').
    'papersize': 'a4paper',
    #
    # La taille de la police ('10pt', '11pt' or '12pt').
    'pointsize': '12pt',
    #
    # Trucs supplémentaires pour le préambule LaTeX.
    'preamble': '',
    #
    # Alignement de la figure en LaTeX (flotteur)
    'figure_align': 'htbp',
}
#latex_show_urls = 'footnote'

# Regroupement de l'arborescence de documents en fichiers LaTeX. Liste des tuples
# (fichier source, nom du fichier cible, titre, auteur, documentclass [howto, manuel ou votre classe]).
latex_documents = [
    (master_doc, 'InitiationProgrammationPythonPourAdministrateurSystèmes.tex', "Initiation à la programmation Python pour l'administrateur systèmes", author, 'manual'),
]
```

#### Pages de manuel

```
# -- Options pour les pages de manuel ----------------------------------------
# Une entrée par page de manuel. Liste des tuples
# (fichier source, nom, description, auteurs, section du manuel).
man_pages = [
    (master_doc, 'InitiationProgrammationPythonPourAdministrateurSystèmes', "Initiation à la programmation Python pour l'administrateur systèmes", [author], 1),
]
```

#### Texinfo

```
# -- Options pour Texinfo ---------------------------------------------------
# Regroupement de l'arborescence des documents dans des fichiers Texinfo.
# Liste des tuples (fichier source, nom de la cible, titre, auteur, répertoire, description, catégorie)
texinfo_documents = [
    (master_doc, 'InitiationProgrammationPythonPourAdministrateurSystèmes', "Initiation à la programmation Python pour l'administrateur systèmes", author, 'InitiationProgrammationPythonPourAdministrateurSystèmes', "Formation d'initiation à la programmation Python pour l'administrateur systèmes.", 'Miscellaneous'),
]
```

#### Epub

```
# -- Options pour les Epub ---------------------------------------------------
# Informations bibliographiques Dublin Core.
epub_title = project
# L'identifiant unique du texte. Cela peut être un numéro ISBN
# ou la page d'accueil du projet.
# epub_identifier = ''

# Une identification unique pour le texte.
# epub_uid = ''

# A list of files that should not be packed into the epub file.
epub_exclude_files = ['search.html']
```

### Générer la documentation

**Attention des accents dans les noms de fichiers .rst font planter LaTeX.**

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement/docs$ make html
utilisateur@MachineUbuntu:~/repertoire_de_developpement/docs$ make latex
utilisateur@MachineUbuntu:~/repertoire_de_developpement/docs$ make latexpdf
utilisateur@MachineUbuntu:~/repertoire_de_developpement/docs$ make epub
utilisateur@MachineUbuntu:~/repertoire_de_developpement/docs$ make text
utilisateur@MachineUbuntu:~/repertoire_de_developpement/docs$ make markdown
utilisateur@MachineUbuntu:~/repertoire_de_developpement/docs$ make xml
utilisateur@MachineUbuntu:~/repertoire_de_developpement/docs$ make man
utilisateur@MachineUbuntu:~/repertoire_de_developpement/docs$ make texinfo
utilisateur@MachineUbuntu:~/repertoire_de_developpement/docs$ make info
utilisateur@MachineUbuntu:~/repertoire_de_developpement/docs$ make odt
```

#### Générer les formats odt ou docx

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement/docs$ sudo apt install pandoc
utilisateur@MachineUbuntu:~/repertoire_de_developpement/docs$ cd documentation/html ; pandoc ./index.html -o ../../InitiationProgrammationPythonPourAdministrateurSystèmes.odt
utilisateur@MachineUbuntu:~/repertoire_de_developpement/docs/documentation/html$ pandoc ./index.html -o ../../InitiationProgrammationPythonPourAdministrateurSystèmes.docx
```

#### Générer l’internationalisation

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement/docs$ make gettext
utilisateur@MachineUbuntu:~/repertoire_de_developpement/docs$ sphinx-intl update -p documentation/gettext -l fr -l en
```

Traduire les fichiers dans **./locales/<lang>/LC_MESSAGES/**

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement/docs$ make -e SPHINXOPTS="-Dlanguage='en'" html
```

## Rédiger la documentation

Le fichier d’entrée de votre documentation **index.rst** (toctree) se trouve dans **~/repertoire_de_developpement/docs/sources-documents**.

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement/docs$ sudo apt install retext
```

Éditer le fichier index.rst et le modifier ainsi :

```
.. Documentation sur l'initiation à la programmation Python pour l'administrateur systèmes. Document maître créé par sphinx-quickstart le Mercredi 24 Avril 2021 à 17h 27min 50s.
   Vous pouvez adapter ce fichier complètement à votre goût.
   Il contient la racine `toctree` de votre documentation.

.. toctree::
   :caption: Contenu :
   :maxdepth: 2

Initiation à la programmation Python pour l'administrateur systèmes
===================================================================


Index
=====

* :ref:`genindex`

Index des modules
=================

* :ref:`modindex`

.. * :ref:`search\`
```

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement/docs$ make html
```



![image](images/documentation_3.png)

### Parties, chapitres, sections, paragraphes

Par convention pour Python :


* «**#**» : Parties


* «**\***» : Chapitres


* «**=**» : Sections


* «**-**» : Sous-sections


* «**^**» : Sous-sous-section


* «**"**» : Paragraphes

```
.. Documentation sur l'initiation à la programmation Python pour l'administrateur systèmes. Document maître créé par sphinx-quickstart le Mercredi 24 Avril 2021 à 17h 27min 50s.
   Vous pouvez adapter ce fichier complètement à votre goût.
   Il contient la racine `toctree` de votre documentation.

.. sectionauthor:: Prénom NOM <prenom.nom@fai.fr>

.. codeauthor:: Geek DEVELOPPEUR <geek.developpeur@fai.fr>

.. toctree::
   :caption: Contenu :
   :maxdepth: 5

Programmation Python 3
######################

Un texte d’introduction sur la partie Python 3.

Initiation à la programmation Python pour l'administrateur systèmes
*******************************************************************

Un texte d’introduction pour mon chapitre.

Section 1
=========

Un texte pour la section 1.

Sous-section 1
--------------

Du texte pour la sous-section 1

    Du texte pour une sous-partie de la sous-section 1

        Du texte pour une sous sous partie de la sous-section 1

Sous-section 2
--------------

Du texte pour la sous-section 2

Exemple de titrages
###################

Chapitre 1
**********

Section 1
=========

Sous-section 1
--------------

Sous-sous-section 1
^^^^^^^^^^^^^^^^^^^

Paragraphe 1
""""""""""""

Sous-paragraphe 1
+++++++++++++++++
```

```
```

**utilisateur@MachineUbuntu:~/repertoire_de_developpement/docs$** make
html



![image](images/documentation_4.png)



![image](images/documentation_5.png)

### Mettre en forme du texte

```
.. Documentation sur l'initiation à la programmation Python pour l'administrateur systèmes. Document maître créé par sphinx-quickstart le Mercredi 24 Avril 2021 à 17h 27min 50s.
   Vous pouvez adapter ce fichier complètement à votre goût.
   Il contient la racine `toctree` de votre documentation.

.. toctree::
   :caption: Contenu :
   :maxdepth: 2

Initiation à la programmation Python pour l'administrateur systèmes
###################################################################

**Tout en gras.**

Du texte \ **en gras**\ pour ma documentation.

*Tout en italique.*

Du texte \ *en italique*\ pour ma documentation.

.. only:: html

    .. raw:: html

        Une phrase avec un <font color="Red">mot</font> en rouge.

.. only:: latex

    .. raw:: latex

        Une phrase avec un \textcolor{red}{mot} en rouge.

.. only:: odt

    Une phrase avec un mot non en rouge.
```

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement/docs$ make html
utilisateur@MachineUbuntu:~/repertoire_de_developpement/docs$ make latexpdf
```



![image](images/documentation_6.png)

### Insertion de texte d’échappement

```
.. Documentation sur l'initiation à la programmation Python pour l'administrateur systèmes. Document maître créé par sphinx-quickstart le Mercredi 24 Avril 2021 à 17h 27min 50s.
   Vous pouvez adapter ce fichier complètement à votre goût.
   Il contient la racine `toctree` de votre documentation.

.. toctree::
   :caption: Contenu :
   :maxdepth: 2

Initiation à la programmation Python pour l'administrateur systèmes
###################################################################

Le caractère \\

Du texte \
sur une seule ligne.

Du texte sans qu’il soit interprété ``\n, \r, \t, \\``.

``**Une phrase non en gras**``

``*Une phrase non en italique*``
```

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement/docs$ make html
```



![image](images/documentation_7.png)

### Les listes

```
.. Documentation sur l'initiation à la programmation Python pour l'administrateur systèmes. Document maître créé par sphinx-quickstart le Mercredi 24 Avril 2021 à 17h 27min 50s.
   Vous pouvez adapter ce fichier complètement à votre goût.
   Il contient la racine `toctree` de votre documentation.

.. toctree::
   :caption: Contenu :
   :maxdepth: 2

Initiation à la programmation Python pour l'administrateur systèmes
###################################################################

Liste à puces

* élément 1
* élément 2
* élément 3

La liste numérotée

1. élément 1
2. élément 2
3. élément 3

La liste numérotée automatiquement

#. élément 1
#. élément 2
#. élément 3
```

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement/docs$ make html
```



![image](images/documentation_8.png)

### Les tableaux

```
.. Documentation sur l'initiation à la programmation Python pour l'administrateur systèmes. Document maître créé par sphinx-quickstart le Mercredi 24 Avril 2021 à 17h 27min 50s.
   Vous pouvez adapter ce fichier complètement à votre goût.
   Il contient la racine `toctree` de votre documentation.

.. toctree::
   :caption: Contenu :
   :maxdepth: 2

Initiation à la programmation Python pour l'administrateur systèmes
###################################################################

+-----+-----------+
|  A  |     B     |
+=====+=====+=====+
|  1  |  2  |  3  |
+-----+-----+-----+

==== ==== ====
    A    B
--------- ----
 A0   A1   B0
==== ==== ====
 01   02   03
 04   05   06
==== ==== ====

.. csv-table:: Personnel
  :header: "Prénom", "Nom"
  :widths: 40, 40

  "Franc", "GEEK"
  "Emmanuel", "DICTATOR"

.. list-table:: Lettres
  :widths: 10 10 20
  :header-rows: 1
  :stub-columns: 1

  * - Lettre
    - A
    - B
  * - Nombre
    - 25
    - 5
```

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement/docs$ make html
```



![image](images/documentation_9.png)

### Insertion de blocs, code, image, graphviz, liens, mathématiques, notes, citations

```
.. Documentation sur l'initiation à la programmation Python pour l'administrateur systèmes. Document maître créé par sphinx-quickstart le Mercredi 24 Avril 2021 à 17h 27min 50s.
   Vous pouvez adapter ce fichier complètement à votre goût.
   Il contient la racine `toctree` de votre documentation.

.. role:: python(code)
   :language: python

.. toctree::
   :caption: Contenu :
   :maxdepth: 2

Initiation à la programmation Python pour l'administrateur systèmes
###################################################################

Pour afficher un texte en Python on utilise la fonction :python:`print("Mon texte")`.

.. literalinclude:: ../../1_Mode_interprété/mon_1er_programme.py

.. code-block:: python

    print("Bonjour les zouzous")

.. image:: ../../../Images/tux.svg
   :alt: Sphinx c’est cool
   :align: center
   :width: 120px

.. graphviz::

    digraph "frameworks web python" {
        python [label="Python", href="https://www.python.org/", target="_top"];
        flask [label="Flask", href="https://flask.palletsprojects.com/en/1.1.x/", target="_top"];
        django [label="Django", href="https://www.djangoproject.com/", target="_top"];
        bottle [label="Bottle", href="https://bottlepy.org/", target="_top"];
        turbogears [label="TurboGears", href="https://turbogears.org/", target="_top"];
        web2py [label="web2py", href="http://www.web2py.com/", target="_top"];
        cherrypy [label="CherryPy", href="https://cherrypy.org/", target="_top"];
        quixote [label="Quixote", href="http://quixote.ca/", target="_top"];
        python -> {flask; django; bottle; turbogears; web2py; cherrypy; quixote;};
    }

`Python <https://www.python.org>`_

- :ref:`python:reference-index`
- :ref:`Référence langage Python <python:reference-index>`
- :doc:`python:library/enum`
- :doc:`Énumérasions <python:library/enum>`
- :docpython3:`tutorial`
- :manpython3:`enum`

:download: `Téléchargement <http://gitlab.domaine-perso.fr/utilisateur/initiation_developpement_python_pour_administrateur/-/raw/master/README.md?inline=false>`_

.. math::
   :nowrap:

    \begin{gather*}
    (a + b)² = a² + 2ab + b² \\
    \sqrt{\frac{n}{n-\sqrt[3]{2}} S} \\
    \int_a^b x \, \mathrm dx = [x^2]_a^b = [(b)^2 – (a)²] = b² – a² \\
    \mathrm{2~H_{2(g)}+O_{2(g)}=2~H_2O_{(1)}}
    \end{gather*}

L'équation d'Euler :eq:`euler` est utile en mathématiques.

Référence à la citation du formateur [citation]_.

Première note [#n1]_, deuxième note [#n2]_

Et encore une troisième note [#n3]_

.. [citation] «Python que oui, ou Python que non…».

.. math:: e^{i\pi} + 1 = 0
   :label: euler

.. rubric:: Notes de bas de page

.. [#n1] Note 1
.. [#n2] Note 2
.. [#n3] Note 3
```

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement/docs$ make html
```



![image](images/documentation_10.png)

### Boites et conditions d’affichages

Il faut d’abord modifier l’extension «**sphinx.ext.todo**» au fichier **conf.py**.

```
[extensions]
todo_include_todos = True
```

Puis modifier **index.rst** comme suit :

```
.. Documentation sur l'initiation à la programmation Python pour l'administrateur systèmes. Document maître créé par sphinx-quickstart le Mercredi 24 Avril 2021 à 17h 27min 50s.
   Vous pouvez adapter ce fichier complètement à votre goût.
   Il contient la racine `toctree` de votre documentation.

.. toctree::
   :maxdepth: 2
   :caption: Contenu :

Initiation à la programmation Python pour l'administrateur systèmes
###################################################################

.. ifconfig:: niveau_developpement == 'alpha'

    .. only :: format_html and not builder_epub

        .. raw:: html

            <font color="Red">Liste des TODOs à faire</font>

    .. only :: latex

        .. raw:: latex

            \textcolor{red}{Liste des TODOs à faire}

    .. todolist::

.. ifconfig:: niveau_developpement not in ( 'pre-alpha', 'alpha', 'beta', 'rc')

    En production
.. ifconfig:: niveau_developpement == 'pre-alpha'

    En faisabilité
.. ifconfig:: niveau_developpement == 'alpha'

    En développement
.. ifconfig:: niveau_developpement == 'beta'

    En test
.. ifconfig:: niveau_developpement == 'rc'

    En qualification

Les boîtes :

.. ifconfig:: niveau_developpement == 'alpha'

    .. todo:: Compléter la boîte voir aussi

.. seealso:: Ceci est une boîte

.. ifconfig:: niveau_developpement == 'alpha'

    .. todo:: Compléter la boîte note

.. note:: Ceci est une boîte

.. sidebar:: Ceci est une boîte

    Contenu de la barre de côté

.. warning:: Ceci est une boîte

.. important:: Ceci est une boîte

.. ifconfig:: niveau_developpement == 'alpha'

    .. todo:: Compléter la boîte Remarque

.. topic:: **Remarque**

    Ceci est le contenu du sujet

.. only:: format_html and not builder_epub

    Les onglets à n'utiliser que pour une documentation purement html

    .. tabs::

        .. tab:: Code

            Ma documentation sur le code

            .. tabs::

                .. code-tab:: py

                    python AfficheArguments.py Bonjour à tous

                .. code-tab:: java

                    java AfficheArguments Bonjour à tous

            .. tabs::

                .. code-tab:: py

                    import sys

                    for arg in sys.argv:
                        print(arg)

                .. code-tab:: java

                    public class AfficheArguments {
                        public static void main(String[] args) {
                            int i;
                            for (String s : args) System.out.println(s);
                        }
                    }

        .. tab:: Group

            Ma documentation sur le code

            .. tabs::

                .. group-tab:: Python

                    Exécuter le programme :

                    .. code-block:: shell

                        python AfficheArguments.py Bonjour à tous

                .. group-tab:: Java

                    Exécuter le programme :

                    .. code-block:: shell

                        java AfficheArguments Bonjour à tous

            .. tabs::

                .. group-tab:: Python

                    Le code Python :

                    .. code-block:: python

                        import sys

                        for arg in sys.argv:
                            print(arg)

                .. group-tab:: Java

                    Le code java :

                    .. code-block:: java

                        public class AfficheArguments {
                            public static void main(String[] args) {
                                int i;
                                for (String s : args) System.out.println(s);
                            }
                        }
```

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement/docs$ make html
utilisateur@MachineUbuntu:~/repertoire_de_developpement/docs$ make epub
utilisateur@MachineUbuntu:~/repertoire_de_developpement/docs$ make latexpdf
```



![image](images/documentation_11.png)

### Documenter le code Python

Modifier le fichier «**index.rst**» :

```
.. Documentation sur l'initiation à la programmation Python pour l'administrateur systèmes. Document maître créé par sphinx-quickstart le Mercredi 24 Avril 2021 à 17h 27min 50s.
   Vous pouvez adapter ce fichier complètement à votre goût.
   Il contient la racine `toctree` de votre documentation.

.. toctree::
   :maxdepth: 2
   :caption: Contenu :

Initiation à la programmation Python pour l'administrateur systèmes
###################################################################

Approche manuelle :

.. py:module:: module
   :platform: Linux
   :synopsis: Un court résumé du périmètre d’utilisation du module

.. py:function:: fonction(paramètres)

    .. py:class:: Classe(paramètres)

        .. py:method:: méthode(paramètres)

            .. py:attribute:: attribut

.. py:decorator:: décorateur(paramètres)

.. py:exception:: exception
```

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement/docs$ make html
```



![image](images/documentation_12.png)

Pour l’approche automatique, dont nous verrons l’utilisation un peu plus loin, il faut utiliser :

```
.. Documentation sur l'initiation à la programmation Python pour l'administrateur systèmes. Document maître créé par sphinx-quickstart le Mercredi 24 Avril 2021 à 17h 27min 50s.
   Vous pouvez adapter ce fichier complètement à votre goût.
   Il contient la racine `toctree` de votre documentation.

.. toctree::
   :maxdepth: 2
   :caption: Contenu :

Initiation à la programmation Python pour l'administrateur systèmes
###################################################################

Approche automatique :

.. automodule:: Documentation.mon_module
   :members:
```

Rôle des membres de **automodule** dans nos fichiers «**.rst**» :


* **:members:** affiche les éléments publics (qui ne débute pas par «**_**»).


* **:special-members:** Affiche aussi les constructeurs des éléments publics.


* **:undoc-members:** Affiche aussi les éléments sans docstring.


* **:private-members:** Affiche aussi les éléments privés (qui commencent par «**_**»).


* **:inherited-members:** Affiche les éléments non hérités.


* **:show-inheritance:** Affiche les classes mères dont hérite les classes Python.

Ces définitions vont se trouver dans la structure de la documentation «**.rst**».

Pour l’écriture de la documentation directement dans notre code il faudra renseigner nos docstring. Sphinx ajoute de nombreuses nouvelles directives, et rôles de texte interprétés, au balisage reST standard.

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement/docs$ cd .. ; mkdir Documentation ; cd Documentation ; touch mon_module.py
```

#### Balise de méta-information

##### sectionauthor

Lorsque vous commencez l’écriture de la documentation d’un code Python, il est très utile pour les autres rédacteurs, ou les développeurs, de savoir qui l’a saisi dans le code. La directive `.. sectionauthor:: Auteur Documentation <auteur.documentation@fai.fr>` identifie l’auteur de la documentation de la section actuelle. La partie d’adresse courriel doit être en minuscules.

Actuellement, ce balisage n’est pas interprété dans la sortie documentaire, mais elle permet de garder une trace des contributions à la documentation dans le code.

Exemple à saisir dans «**mon_module.py**» :

```
# -*- coding: utf-8 -*-

"""
.. sectionauthor:: Stagiaire ADMINISTRATEUR <stagiaire.administrateur@fai.fr>
"""
```

#### Balisages spécifiques au module

Lorque vous commencez l’écriture d’un module Python (fichier «**mon_module.py**»), la première des informations à fournir aux autres développeurs est sur le module lui même. Le balisage décrit dans cette section est utilisé pour fournir ces informations sur le module pour la documentation. **Chaque module doit être documenté dans son propre fichier**. Normalement, ce balisage apparaît après le titre du module dans le fichier ; un fichier typique peut commencer comme ceci :

```
# -*- coding: utf-8 -*-

"""
.. sectionauthor:: Stagiaire ADMINISTRATEUR <stagiaire.administrateur@fai.fr>
:mod:`mon_module` -- Module d'exemple
#####################################

.. module:: mon_paquet.mon_module
   :synopsis:: Ce module illustre comment écrire une docstring de module avec Python
   :platform: Linux
"""
```

**NOTE**: Il est important de donner un titre de module puisque cela sera inséré dans l’arborescence de la table des matières pour la documentation.

##### module

La directive `.. module:: nom_du_module` marque le début de la description d’un module, d’un package ou d’un sous-module. **Le nom doit être entièrement qualifié** (c’est-à-dire incluant le nom du package pour les sous-modules). Il est paramétrable avec :


* L’option `:synopsis: Résumé rapide` qui doit consister en une phrase décrivant l’objectif du module. Elle n’est actuellement utilisée que dans l’index global des modules.


* L’option `:platform: Unix, Mac, Windows`, si elle est présente, qui indique les plateformes compatibles avec le code Python. C’est une liste séparée par des virgules des plates-formes. Si le code est disponible pour toutes les plates-formes, l’option doit être omise. Les clés sont des identifiants courts ; les exemples utilisés incluent «**Linux**», «**Unix**», «**Mac**» et «**Windows**». Il est important d’utiliser une clé qui a déjà été utilisée le cas échéant.


* L’option `:deprecated:` (sans valeur) peut être donnée pour marquer un module comme obsolète ; il sera alors désigné comme tel à divers endroits.

##### moduleauthor

```
# -*- coding: utf-8 -*-

"""
.. sectionauthor:: Stagiaire ADMINISTRATEUR <stagiaire.administrateur@fai.fr>
:mod:`mon_module` -- Module d'exemple
#####################################

.. module:: mon_module
   :platform: Unix, Windows
   :synopsis: Ce module illustre comment écrire votre docstring dans Python.
.. moduleauthor:: Formateur PYTHON <formateur.python@fai.fr>
.. moduleauthor:: Stagiaire ADMINISTRATEUR <stagiaire.administrateur@fai.fr>

"""
```

La directive `.. moduleauthor::`, qui peut apparaître plusieurs fois, nomme les auteurs du code Python du module, tout comme `.. sectionauthor::` nomme le(s) auteur(s) d’une documentation. Elle suit les même règles de syntaxe que `.. sectionauthor::`.

#### Pour le code, nos fonctions et nos classes


* «**:var/ivar/cvar nom_variable: description**» : Pour nos variables.


* «**:param/parameter/arg/argument/key/keywords nom_paramètre: description**» : Pour décrire un paramètre d’une fonction ou d’un objet.


* «**:type element: type**» : Pour décrite le type d’une variable ou d’un paramètre (Callable, int, float, long, str, tuple, list, dict, None, True, False, boolean).


* «**:returns/return: description**» : Décrit ce qui est retourné par une fonction ou un objet.


* «**:rtype: type**» : Pour décrite le type de ce qui est retourné par une fonction ou un objet (Callable, int, float, long, str, tuple, list, dict, None,
True, False, boolean).


* «**:raises/raise/except/exception nom_exception: description**» : Décrit une exception dans votre code.

Tout ceci nous permet d’écrire un code de documentation minimal final (avec des variables Python et Sphinx utiles) :

```
# -*- coding: utf-8 -*-

"""
.. sectionauthor:: Stagiaire ADMINISTRATEUR <stagiaire.administrateur@fai.fr>
:mod:`mon_module` -- Module d'exemple
#####################################

.. module:: mon_module
   :platform: Unix, Windows
   :synopsis: Ce module illustre comment écrire votre docstring dans Python.
.. moduleauthor:: Formateur PYTHON <formateur.python@fai.fr>
.. moduleauthor:: Stagiaire ADMINISTRATEUR <stagiaire.administrateur@fai.fr>

"""

__title__ = "Module illustration écriture docstring Python"
__author__ = "Formateur PYTHON"
__version__ = '0.7.3'
__release_life_cycle__ = 'alpha'
# pre-alpha = faisabilité, alpha = développement, beta = test, rc = qualification, prod = production
__docformat__ = 'reStructuredText'
```

Fichier **mon_module.py** avec une classe Python d’exemple :

```
# -*- coding: utf-8 -*-

"""
.. sectionauthor:: Stagiaire ADMINISTRATEUR <stagiaire.administrateur@fai.fr>
:mod:`mon_module` -- Module d'exemple
#####################################

.. module:: mon_module
   :platform: Unix, Windows
   :synopsis: Ce module illustre comment écrire votre docstring dans Python.
.. moduleauthor:: Formateur PYTHON <formateur.python@fai.fr>
.. moduleauthor:: Stagiaire ADMINISTRATEUR <stagiaire.administrateur@fai.fr>

"""

__title__ = "Module illustration écriture docstring Python"
__author__ = "Formateur PYTHON"
__version__ = '0.7.3'
__release_life_cycle__ = 'alpha'
# pre-alpha = faisabilité, alpha = développement, beta = test, rc = qualification, prod = production
__docformat__ = 'reStructuredText'

class ClasseExemple(object):
    """Cette classe docstring montre comment utiliser sphinx et la syntaxe rst.
    La première ligne est une brève explication\ de la classe avec ses paramètres
    et ce que renvoi l’objet.
    Cela doit être complété par une description plus précise des méthodes et des
    attributs dans le code de la classe dans le code.
    La seule méthode ici est :func:`mafonction`.

    - **paramètres**, **types**, **retour** et **type de retours**::

        :param arg1: description
        :param arg2: description
        :type arg1: type arg1
        :type arg2: type arg2
        :return: description du retour
        :rtype: type du retour

    - Documentez des sections **Exemples** en utilisant la syntaxe des doubles points ``:``
        ::

            :Exemple:

                suivi d'une ligne vierge!

        qui apparaît comme suit :

        :Exemple:

            suivi d'une ligne vierge!

    - Des sections spéciales telles que **Voir aussi**, **Avertissements**, **Notes** avec la syntaxe sphinx (*directives de paragraphe*)::

        .. seealso:: blabla
        .. warnings:: blabla
        .. note:: blabla
        .. todo:: blabla

    .. warning::
       Il existe de nombreux autres champs Info mais ils peuvent être redondants:

           * param, parameter, arg, argument, key, keyword: Description d'un paramètre.
           * type: Type de paramètre.
           * raises, raise, except, exception: Quand une exception spécifique est levée.
           * var, ivar, cvar: Description d'une variable.
           * returns, return: Description de la valeur de retour.
           * rtype: Type de retour.

    .. note::
        Il existe de nombreuses autres directives telles que :
        versionadded, versionchanged, rubric, cent\ ered\ , ...
        Voir la documentation sphinx pour plus de détails.

    Voici ci-dessous les résultats pour :func:`mafonction` docstring.

    """

def maméthode(self, arg1, arg2, arg3):
    """Retourne (arg1 / arg2) + arg3

    Ceci est une explication plus précise, qui peut inclure des mathématiques
    avec la syntaxe latex :math:`\\alpha`.
    Ensuite, vous devez fournir une sous-section facultative (juste pour être
    cohérent et avoir une documentation uniforme. Rien ne vous empêche de
    changer l'ordre):

        - paramètres utilisés ``:param <nom>: <description>``
        - type des paramètres ``:type <nom>: <description>``
        - retours de la méthode ``:returns: <description>``
        - exemples (doctest)
        - utilisation de voir aussi ``.. seealso:: texte``
        - utilisation des notes ``.. note:: texte``
        - utilisation des alertes ``.. warning:: texte``
        - liste des restes à faire ``.. todo:: texte``

    **Avantages**:
        - Utilise les balises sphinx.
        - Belle sortie HTML avec les directives Seealso, Note, Warning.

    **Désavantages**:
        - En regardant simplement la docstring, les sections de paramètres, de
          types et de retours n'apparaissent pas bien dans le code.

    :param arg1: la première valeur
    :param arg2: la première valeur
    :param arg3: la première valeur
    :type arg1: int, float,...
    :type arg2: int, float,...
    :type arg3: int, float,...
    :returns: arg1/arg2 +arg3
    :rtype: int, float

    :Example:

    .. code-block:: pycon

        >>> import template
        >>> a = template.ClasseExemple()
        >>> a.mafonction(1,1,1)
        2

    .. note:: il peut être utile de souligner une caractéristique importante

    .. seealso:: :class:`AutreClasseExemple`
    .. warning:: arg2 doit être différent de zéro.
    .. todo:: vérifier que arg2 est non nul.
    """
    return arg1 / arg2 + arg3
```

Générer la documentation pour voir le rendu :

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement/Documentation$ cd ../docs
utilisateur@MachineUbuntu:~/repertoire_de_developpement/docs$ make html
utilisateur@MachineUbuntu:~/repertoire_de_developpement/docs$ cd ..
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git add .
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git status
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git commit -m "Configuration de la documentation du projet"
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git push
```



![image](images/documentation_13.png)

## Mise en œuvre de la documentation avec Gitlab

### Définition de la structure du document

Supposons que vous ayez exécuté **sphinx-quickstart**. Il a créé un répertoire source avec **conf.py** et un document maître, **index.rst**.

La fonction principale du [document maître](https://ugaugtjjyr2ylvegfxect62ccm--www-sphinx-doc-org.translate.goog/en/master/glossary.html#term-master-document) (ou *toctree*) est de servir de page d’accueil et de contenir la racine de «**l’arborescence de la documentation du projet**». C’est l’une des principales choses que Sphinx ajoute à reStructuredText, un moyen de connecter plusieurs fichiers à une seule hiérarchie de documents.

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ makedir docs/sources-documents/classes docs/sources-documents/cours
```

Modifiez **index.rst** :

```
.. |date| date::

:Date: |date|
:Revision: 1.0
:Author: Prénom NOM <prénom.nom@fai.fr>
:Description: Documentation sur l'initiation à la programmation Python pour l'administrateur systèmes
:Info: Voir <http://gitlab.domaine-perso.fr/utilisateur/initiation_developpement_python_pour_administrateur> pour la mise à jour de ce cours.

.. toctree::
   :maxdepth: 2
   :caption: Contenu

.. include:: cours/InitiationProgrammationPythonPourAdministrateurSystemes.rst

Modules
*******

.. automodule:: Documentation.mon_module
   :members:
```

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ touch docs/sources-documents/cours/InitiationProgrammationPythonPourAdministrateurSystemes.rst
```

Modifiez **InitiationProgrammationPythonPourAdministrateurSystemes.rst** :

```
.. Cours Initiation à la programmation Python pour l'administrateur systèmes.

Initiation à la programmation Python pour l'administrateur systèmes
###################################################################
```

### Générer la documentation avec un script

Créer un fichier **makedocs** pour générer la documentation, et **makediagrammes** pour générer ultérieurement les diagrammes de Classes Python.

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ touch makedocs makediagrammes
```

Modifier **makedocs** avec le contenu ci-dessous :

```
#!/bin/bash

titre=$(tput bold ; tput setaf 1 ; tput setab 53)
section=$(tput bold ; tput setaf 3 ; tput setab 240)
soussection=$(tput bold ; tput setaf 4 ; tput setab 0)
ordinaire=$(tput sgr0)

echo -e "$titre""Création de la documentation du projet""$ordinaire"

echo -e "$section""Création des diagrammes de classes""$ordinaire"
( exec "./makediagrammes" )

cd docs
echo -e "$section""Création des fichiers de documentations""$ordinaire"
echo -e "$soussection""Format html""$ordinaire"
make html
echo -e "$soussection""Format LaTeX""$ordinaire"
make latex >/dev/null
echo -e "$soussection""Format epub""$ordinaire"
make epub
echo -e "$soussection""Format pdf""$ordinaire"
make latexpdf >/dev/null
echo -e "$soussection""Format texte""$ordinaire"
make text
echo -e "$soussection""Format xml""$ordinaire"
make xml
echo -e "$soussection""Format markdown""$ordinaire"
make markdown
echo -e "$soussection""Création des pages de manuel""$ordinaire"
make man
echo -e "$soussection""Création des pages texinfo""$ordinaire"
make texinfo
echo -e "$soussection""Création des pages info""$ordinaire"
make info
echo -e "$soussection""Format ODT""$ordinaire"
make odt
pandoc ./documentation/html/index.html -o InitiationProgrammationPythonPourAdministrateurSystèmes.odt
echo -e "$soussection""Format DOCX""$ordinaire"
pandoc ./documentation/html/index.html -o InitiationProgrammationPythonPourAdministrateurSystèmes.docx

echo -e "$section""Création du README.md du projet""$ordinaire"
cp ./documentation/markdown/index.md ../README.md

echo -e "$section""Changement du chemin des images dans README.md""$ordinaire"
cd..
sed -i 's/classes\//docs\/source\/classes\//g'\ README.md
sed -i 's/images\//docs\/source\/images\//g'\ README.md
```

Modifier **makediagrammes** avec le contenu ci-dessous :

```
#!/bin/bash
```

Rendre exécutable.

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ chmod u+x makedocs makediagrammes
```

Créez les documents de la documentation :

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ ./makedocs
```

Sauvegarder les documents dans le dépôt git et dans Gitlab :

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git add .
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git status
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git commit -m "Génération par un script de la documentation du projet"
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git push
```



![image](images/documentation_14.png)

### Générer la documentation avec Gitlab

Créer un fichier vide **requirements.txt** pour préparer à l’installation des modules Python et **packages.txt** pour installer les applications utiles dans l’environnement de test Python. Créer aussi des fichiers vides **docs-requirements.txt** et **docs-packages.txt** pour la construction de la documentation.

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ touch requirements.txt packages.txt docs-requirements.txt docs-packages.txt
```

#### Génération de la documentation dans Gitlab

Modifier **.gitlab-ci.yml** :

Remarque : il faut impérativement saisir «**pages**» comme section

```
image: python:latest

stages:
    - deploy

pages:
    stage: deploy
    script:
        - echo "$GITLAB_USER_LOGIN déploiement de la documentation"
        - echo "** Mises à jour et installation des applications supplémentaires **"
        - echo "Mises à jour système"
        - apt -y update
        - apt -y upgrade
        - echo "Installation des applications supplémentaires"
        - cat docs-packages.txt | xargs apt -y install
        - echo "Mise à jour de PIP"
        - pip install --upgrade pip
        - echo "Installation des dépendances de modules python"
        - pip install -U -r docs-requirements.txt
        - echo "** Génération des diagrammes de classes **"
        - ./makediagrammes
        - echo "** Génération de la documentation HTML **"
        - sphinx-build -b html ./docs/sources-document public
    artifacts:
        paths:
            - public
    only:
        - master
```

Modifier **docs-packages.txt**:

```
dnsutils
sphinx-intl
```

Modifier **docs-requirements.txt**:

```
sphinx
sphinx-intl
sphinxcontrib-inlinesyntaxhighlight
sphinx-copybutton
sphinx-tabs
sphinx-markdown-builder
sphinx-book-theme
```

Modifier **conf.py** pour supprimer les extensions de sortie inutiles:

```
extensions = [
    'sphinx.ext.intersphinx',
    'sphinx.ext.extlinks',
    'sphinx.ext.autodoc',
    'sphinx.ext.githubpages',
    'sphinx.ext.graphviz',
    'sphinx.ext.inheritance_diagram',
    'sphinx_copybutton',
    'sphinx.ext.tabs',
    'sphinx.ext.todo',
    'sphinx.ext.ifconfig',
    'sphinx.ext.doctest',
    'sphinx_markdown_builder',
    'sphinxcontrib-odfbuilder',
]
```

Modifiez **cours/InitiationProgrammationPythonPourAdministrateurSystemes.rst** :

```
.. Cours Initiation à la programmation Python pour l'administrateur systèmes.

`Initiation à la programmation Python pour l'administrateur systèmes <http://utilisateur.documentation.domaine-perso.fr/initiation_developpement_python_pour_administrateur/>`_
###############################################################################################################################################################################
```



![image](images/documentation_15.png)



![image](images/documentation_16.png)



![image](images/documentation_17.png)



![image](images/documentation_18.png)



![image](images/documentation_19.png)



![image](images/documentation_20.png)

# La qualité du code

Vous avez écrit votre code de votre projet, mais avez vous :


* utilisé des modules odsolètes ?


* respecté les standards d’écriture Python définis dans les spécifications de la **PEP 8** ?

Pour cela, en plus de la commande `python3 -Wd` pour vérifier l’obsolescence du code, nous avons plusieurs outils **Pylint**, **pyflakes**, **pychecker**, **pep8** ou **flake8**, qui permettent de vérifier la conformité de votre code avec la **PEP 8**.

Dans ce cours nous allons utiliser **Pylint**.

Voyons comment fonctionne cet outil ?

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ sudo apt install pylint
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ pylint 1_Mode_interprété/mon_1er_programme.py
************ Module mon_1er_programme
1_Mode_interprété/mon_1er_programme.py:1:0: C0114: Missing module docstring (missing-module-docstring)
-----------------------------------
Your code has been rated at 0.00/10
```

Comment éviter un message d’erreur de documentation lorsque l’on ne veut pas de documentation dans son code ?

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ pylint --disable=missing-module-docstring
1_Mode_interprété/mon_1er_programme.py
---------------------
Your code has been rated at 10.00/10 (previous run: 0.00/10, +10.00)
```

Comment éviter des fichiers, ou répertoires, que l’on ne veut pas tester avec `pylint` ?

Nous allons d’abord tester dans un terminal la bonne remontée des fichiers à tester pour `pylint` avec un script shell.

Créer un fichier **choix-fichiers-a-tester**.

```
#! /usr/bin/env bash

find -type f -name "*.py" ! -path "*1_Mode_interprété*" ! -path "*2_Debug*" ! -path "*3_Interpreteur_alerts*" ! -path "*4_Passage_paramètres*" ! -path "*5_Niveau_journalisation*" ! -path "*6_Chaines_split_join*" ! -path "*7_Modules*" ! -path "*/.env/*" ! -path "*docs*"
```

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ chmod u+x choix-fichiers-a-tester ; ./choix-fichiers-a-tester
./Documentation/mon_module.py
```

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ pylint $(find -type f -name "*.py" ! -path "*1_Mode_interprété*" ! -path "*2_Debug*" ! -path "*3_Interpreteur_alerts*" ! -path "*4_Passage_paramètres*" ! -path "*5_Niveau_journalisation*" ! -path "*6_Chaines_split_join*" ! -path "*7_Modules*" ! -path "*/.env/*" ! -path "*docs*")
************ Module mon_module
Documentation/mon_module.py:72:59: C0303: Trailing whitespace (trailing-whitespace)
Documentation/mon_module.py:127:31: C0303: Trailing whitespace (trailing-whitespace)
Documentation/mon_module.py:19:0: R0205: Class 'ClasseExemple' inherits from object, can be safely removed from bases in python3 (useless-object-inheritance)
Documentation/mon_module.py:79:4: R0201: Method could be a function (no-self-use)
Documentation/mon_module.py:19:0: R0903: Too few public methods (1/2) (too-few-public-methods)

------------------------------------------------------------------
Your code has been rated at 5.00/10
```

Nous allons maintenant voir comment mettre ces tests d’obsolescence et de qualité du code dans **GitLab**.

## Test de l’environnement Python dans Gitlab

Ici nous allons tester le déploiement de l’environnement **Python 3** pour notre code, avec le déploiement d’une page de documentation.

Modifier **.gitlab-ci.yml** :

```
image: python:latest

stages:
  - build
  - deploy

construction-environnement:
  stage: build
  script:
    - echo "Bonjour $GITLAB_USER_LOGIN !"
    - echo "** Mises à jour et installation des applications supplémentaires **"
    - echo "Mises à jour système"
    - apt -y update
    - apt -y upgrade
    - echo "Installation des applications supplémentaires"
    - cat packages.txt | xargs apt -y install
    - echo "Mise à jour de PIP"
    - pip install --upgrade pip
    - echo "Installation des dépendances de modules python"
    - pip install -U -r requirements.txt
  only:
    - master

pages:
  stage: deploy
  script:
    - echo "$GITLAB_USER_LOGIN déploiement de la documentation"
    - echo "** Mises à jour et installation des applications supplémentaires **"
    - echo "Mises à jour système"
    - apt -y update
    - apt -y upgrade
    - echo "Installation des applications supplémentaires"
    - cat docs-packages.txt | xargs apt -y install
    - echo "Mise à jour de PIP"
    - pip install --upgrade pip
    - echo "Installation des dépendances de modules python"
    - pip install -U -r docs-requirements.txt
    - echo "** Génération des diagrammes de classes **"
    - ./makediagrammes
    - echo "** Génération de la documentation HTML **"
    - sphinx-build -b html ./docs/sources-document public
  artifacts:
    paths:
      - public
  only:
    - master
```



![image](images/QualitéCode_1.png)



![image](images/QualitéCode_2.png)

## Test de l’obsolescence du code Python dans Gitlab

Ici nous allons utiliser la commande `python3 -Wd` pour générer une image d’obsolescence du code. Nous allons le tester avec le fichier «**3_Interpreteur_alerts/monscript.py**».

Modifier le fichier **.gitlab-ci.yml**. Voici à quoi ressemble le fichier **.gitlab-ci.yml** pour ce projet :

```
image: python:latest

stages:
  - build
  - Static Analysis
  - deploy

construction-environnement:
  stage: build
  script:
    - echo "Bonjour $GITLAB_USER_LOGIN !"
    - echo "** Mises à jour et installation des applications supplémentaires **"
    - echo "Mises à jour système"
    - apt -y update
    - apt -y upgrade
    - echo "Installation des applications supplémentaires"
    - cat packages.txt | xargs apt -y install
    - echo "Mise à jour de PIP"
    - pip install --upgrade pip
    - echo "Installation des dépendances de modules python"
    - pip install -U -r requirements.txt
  only:
    - master

obsolescence-code:
  stage: Static Analysis
  allow_failure: true
  script:
    - echo "$GITLAB_USER_LOGIN test de l'obsolescence du code"
    - python3 -Wd Documentation/mon_module.py 2> /tmp/output.txt
    - sed -n '/DeprecationWarning:/p' /tmp/output.txt > /tmp/obsolescence.txt
    - \[ -s /tmp/obsolescence.txt \] && cat /tmp/obsolescence.txt || echo "Pas d'obsolescences"

pages:
  stage: deploy
  before_script:
    - echo "** Mises à jour et installation des applications supplémentaires **"
    - echo "Mises à jour système"
    - apt -y update
    - apt -y upgrade
    - echo "Installation des applications supplémentaires"
    - cat docs-packages.txt | xargs apt -y install
    - echo "Mise à jour de PIP"
    - pip install --upgrade pip
    - echo "Installation des dépendances de modules python"
    - pip install -U -r docs-requirements.txt
    - echo "Création de l’infrastructure pour l'obsolescence du code"
    - mkdir -p public/obsolescence public/badges
    - echo undefined > public/obsolescence/obsolescence.score
  script:
    - echo "** $GITLAB_USER_LOGIN déploiement de la documentation **"
    - python3 -Wd Documentation/mon_module.py 2> /tmp/output.txt
    - sed -n '/DeprecationWarning:/p' /tmp/output.txt > /tmp/obsolescence.txt
    - \[ -s /tmp/obsolescence.txt \] && cat /tmp/obsolescence.txt || echo "Pas d'obsolescences"
    - \[ -s /tmp/obsolescence.txt \] && echo oui > public/obsolescence/obsolescence.score || echo non > public/obsolescence/obsolescence.score
    - echo "Obsolescence $(cat public/obsolescence/obsolescence.score)"
    - echo "Génération des diagrammes de classes"
    - ./makediagrammes
    - echo "Création du logo SVG d'obsolescence de code"
    - anybadge --overwrite --label "Obsolescence du code" --value=$(cat public/obsolescence/obsolescence.score) --file=public/badges/obsolescence.svg oui=red non=green
    - echo "Génération de la documentation HTML"
    - sphinx-build -b html ./docs/sources-document public
  artifacts:
    paths:
      - public
  only:
    - master
```

Modifier le fichier **repertoire_de_developpement/docs-requirements.txt**.

Et ajouter à la fin du fichier :

```
anybadge
```

Modifier le fichier **repertoire_de_developpement/docs/sources-documents/index.rst**.

```
.. |date| date::

:Date: |date|
:Revision: 1.0
:Author: Prénom NOM <prénom.nom@fai.fr>
:Description: Documentation sur l'initiation à la programmation Python pour l'administrateur systèmes
:Info: Voir <http://gitlab.domaine-perso.fr/utilisateur/initiation_developpement_python_pour_administrateur> pour la mise à jour de ce cours.

.. toctree::
   :maxdepth: 2
   :caption: Contenu

.. include:: cours/InitiationProgrammationPythonPourAdministrateurSystemes.rst


.. only:: html

  .. image:: ./badges/obsolescence.svg
     :alt: Obsolescence du code Python
     :align: left
     :width: 200px

Modules
*******

.. automodule:: Documentation.mon_module
   :members:
```

Déployez les fichiers dans gitlab.



![image](images/QualitéCode_3.png)



![image](images/QualitéCode_4.png)



![image](images/QualitéCode_5.png)



![image](images/QualitéCode_6.png)



![image](images/QualitéCode_7.png)



![image](images/QualitéCode_8.png)



![image](images/QualitéCode_9.png)



![image](images/QualitéCode_10.png)



![image](images/QualitéCode_11.png)



![image](images/QualitéCode_12.png)

## Test de qualité du code dans Gitlab

Ici nous allons tester avec `pylint` la conformance du code de votre projet avec le standard **PEP 8**.

Voici à quoi ressemble le fichier **.gitlab-ci.yml** pour ce projet :

```
image: python:latest

stages:
  - build
  - Static Analysis
  - deploy

construction-environnement:
  stage: build
  script:
    - echo "Bonjour $GITLAB_USER_LOGIN !"
    - echo "** Mises à jour et installation des applications supplémentaires **"
    - echo "Mises à jour système"
    - apt -y update
    - apt -y upgrade
    - echo "Installation des applications supplémentaires"
    - cat packages.txt | xargs apt -y install
    - echo "Mise à jour de PIP"
    - pip install --upgrade pip
    - echo "Installation des dépendances de modules python"
    - pip install -U -r requirements.txt
  only:
    - master

obsolescence-code:
  stage: Static Analysis
  allow_failure: true
  script:
    - echo "$GITLAB_USER_LOGIN test de l'obsolescence du code"
    - python3 -Wd Documentation/mon_module.py 2> /tmp/output.txt
    - sed -n '/DeprecationWarning:/p' /tmp/output.txt > /tmp/obsolescence.txt
    - \[ -s /tmp/obsolescence.txt \] && cat /tmp/obsolescence.txt || echo "Pas d'obsolescences"

qualité-du-code:
  stage: Static Analysis
  allow_failure: true
  before_script:
    - echo "Installation de Pylint"
    - pip install -U pylint-gitlab
  script:
    - echo "$GITLAB_USER_LOGIN test de la qualité du code"
    - pylint --output-format=text $(bash choix-fichiers-a-tester) | tee /tmp/pylint.txt
  after_script:
    - sed -n 's/^Your code has been rated at \([-0-9.]*\)\/.*/\1/p' /tmp/pylint.txt > pylint.score
    - echo "Votre score de qualité de code Pylint est de $(cat pylint.score)"

pages:
  stage: deploy
  before_script:
    - echo "** Mises à jour et installation des applications supplémentaires **"
    - echo "Mises à jour système"
    - apt -y update
    - apt -y upgrade
    - echo "Installation des applications supplémentaires"
    - cat docs-packages.txt | xargs apt -y install
    - echo "Mise à jour de PIP"
    - pip install --upgrade pip
    - echo "Installation des dépendances de modules python"
    - pip install -U -r docs-requirements.txt
    - echo "Création de l’infrastructure pour l'obsolescence et la qualité de code"
    - mkdir -p public/obsolescence public/quality public/badges public/pylint
    - echo undefined > public/obsolescence/obsolescence.score
    - echo undefined > public/quality/pylint.score
    - pip install -U pylint-gitlab
  script:
    - echo "** $GITLAB_USER_LOGIN déploiement de la documentation **"
    - python3 -Wd Documentation/mon_module.py 2> /tmp/output.txt
    - sed -n '/DeprecationWarning:/p' /tmp/output.txt > /tmp/obsolescence.txt
    - \[ -s /tmp/obsolescence.txt \] && cat /tmp/obsolescence.txt || echo "Pas d'obsolescences"
    - \[ -s /tmp/obsolescence.txt \] && echo oui > public/obsolescence/obsolescence.score || echo non > public/obsolescence/obsolescence.score
    - echo "Obsolescence $(cat public/obsolescence/obsolescence.score)"
    - pylint --exit-zero --output-format=text $(bash choix-fichiers-a-tester) | tee /tmp/pylint.txt
    - sed -n 's/^Your code has been rated at \([-0-9.]*\)\/.*/\1/p' /tmp/pylint.txt > public/quality/pylint.score
    - echo "Votre score de qualité de code Pylint est de $(cat public/quality/pylint.score)"
    - echo "Création du rapport HTML de qualité de code"
    - pylint --exit-zero --output-format=pylint_gitlab.GitlabPagesHtmlReporter $(bash choix-fichiers-a-tester) > public/pylint/index.html
    - echo "Génération des diagrammes de classes"
    - ./makediagrammes
    - echo "Création du logo SVG d'obsolescence de code"
    - anybadge --overwrite --label "Obsolescence du code" --value=$(cat public/obsolescence/obsolescence.score) --file=public/badges/obsolescence.svg oui=red non=green
    - echo "Création du logo SVG de qualité de code"
    - anybadge --overwrite --label "Qualité du code avec Pylint" --value=$(cat public/quality/pylint.score) --file=public/badges/pylint.svg 4=red 6=orange 8=yellow 10=green
    - echo "Génération de la documentation HTML"
    - sphinx-build -b html ./docs/sources-document public
  artifacts:
    paths:
      - public
  only:
    - master
```

Modifier le fichier **repertoire_de_developpement/docs/sources-documents/index.rst**.

```
.. |date| date::

:Date: |date|
:Revision: 1.0
:Author: Prénom NOM <prénom.nom@fai.fr>
:Description: Documentation sur l'initiation à la programmation Python pour l'administrateur systèmes
:Info: Voir <http://gitlab.domaine-perso.fr/utilisateur/initiation_developpement_python_pour_administrateur> pour la mise à jour de ce cours.

.. toctree::
   :maxdepth: 2
   :caption: Contenu

.. include:: cours/InitiationProgrammationPythonPourAdministrateurSystemes.rst


.. only:: html

  .. image:: ./badges/obsolescence.svg
     :alt: Obsolescence du code Python
     :align: left
     :width: 200px

  .. image:: ./badges/pylint.svg
     :alt: Cliquez pour voir le rapport
     :align: left
     :width: 200px
     :target: ./pylint/index.html


----


Modules
*******

.. automodule:: Documentation.mon_module
   :members:
```

Déployez les fichiers dans gitlab.



![image](images/QualitéCode_13.png)



![image](images/QualitéCode_14.png)



![image](images/QualitéCode_15.png)



![image](images/QualitéCode_16.png)



![image](images/QualitéCode_17.png)



![image](images/QualitéCode_18.png)



![image](images/QualitéCode_19.png)



![image](images/QualitéCode_20.png)



![image](images/QualitéCode_21.png)



![image](images/QualitéCode_22.png)

# Tests unitaire Python

Après avoir testé la qualité de rédaction du code Python suivant les standards, nous allons maintenant tester le comportement du programme tel qu’attendu par le programmeur.

## Le module Unittest

Les tests unitaires permettent de vérifier le comportement logiciel des éléments spécifiques d’un programme.

Par exemple ils permettent de vérifier le fonctionnement de méthodes, d’objets, de fonctions. La mise en place des tests unitaires est aussi utile pour s’assurer que la correction de dysfonctionnements logiciels n’entraînera pas de régressions dans la code ailleurs.

**Unittest** est disponible nativement dans Python, et il est basé sur le modèle de framework Xunit imaginé par Kent Beck et Erich Gamma.

Créer le répertoire «**repertoire_de_developpement/Unittest**».

Puis dans ce répertoire créer le fichier «**Calculatrice.py**» :

```
class Calculatrice:
    """ Fait des opérations entre deux valeurs """
    def __init__(self):
        """ Initialisation de la classe """
        self.efface()

    def valeur1(self, première_valeur):
        """ Affecte la première valeur """
        self.__a = première_valeur

    def valeur2(self, deuxième_valeur):
        """ Affecte la deuxième valeur """
        self.__b = deuxième_valeur

    def efface(self):
        """\ Efface les valeurs\ """
        self.__a = None
        self.__b = None

    def ajoute(self):
        """\ Ajoute les valeurs\ """
        return self.__a + self.__b

    def divise(self):
        """\ Divise les valeurs\ """
        return self.__a / self.__b

if __name__ == '__main__':
    calcule = Calculatrice()
    calcule.valeur1(4)
    calcule.valeur2(2)
    print("Valeur des opérandes : 4 et 2")
    print("Addition : ", calcule.ajoute())
    print('Division : ', calcule.divise())
```

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement/Unittest$ python3 Calculatrice.py
Valeur des opérandes : 4 et 2
Addition : 6
Division : 2.0
```

Exemple de test unitaires :

Créer le fichier **Calculatrice_test.py** :

```
# -*- coding: utf-8 -*-
import unittest
from Calculatrice import Calculatrice

class Calculatricetest(unittest.TestCase):
    """ Tests de la classe Calculatrice """
    def test_simple_ajoute(self):
        """ Tests sur la somme """
        print("\n Début du test de la somme")
        self.objet = Calculatrice()
        self.objet.valeur1(2)
        self.objet.valeur2(3)
        self.assertAlmostEqual(self.objet.ajoute(), 5)

    def test_simple_divise(self):
        """ Tests sur la division """
        print("\n Début du test de la division")
        self.objet = Calculatrice()
        self.objet.valeur1(10)
        self.objet.valeur2(2)
        self.assertAlmostEqual(self.objet.divise(), 5)

if __name__ == '__main__':
    unittest.main()
```

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement//Unittest$ python3 Calculatrice_test.py
Début du test de la somme
.
Début du test de la division
.
----------------------------------------------------------------------
Ran 2 tests in 0.000s

OK
```

### Les valeurs de retour des tests

Il y a trois valeurs de retour pour un test :


* **OK** : le test s’est déroulé correctement.


* **F** : (Fail) le test a échoué.


* **E** : (Error) une erreur est présente dans le code.

#### Tests réussits

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement/Unittest$ python3 Calculatrice_test.py
Début du test de la somme
.
Début du test de la division
.
----------------------------------------------------------------------
Ran 2 tests in 0.000s

OK
```

#### Modification engendrant un échec

Modifiez dans Calculatrice.py

```
def ajoute(self):
    """ Ajoute les valeurs """
    return self.__a + self.__b + 1
```

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement/Unittest$ python3 Calculatrice_test.py
Début du test de la somme
F
Début du test de la division
.
======================================================================
FAIL: test_simple_ajoute (__main__.Calculatricetest)
----------------------------------------------------------------------
Traceback (most recent call last):
File "/home/utilisateur/Calculatrice_test.py", line 7, in test_simple_ajoute
 self.assertAlmostEqual(self.objet.ajoute(), 5)
AssertionError: 6 != 5 within 7 places (1 difference)

----------------------------------------------------------------------
Ran 2 tests in 0.000s

FAILED (failures=1)
```

#### Modification engendrant une erreur

Modifiez dans Calculatrice_test.py

```
class Calculatricetest(unittest.TestCase):
    """ Tests de la classe Calculatrice """
    def test_simple_ajoute(self):
        """ Tests sur la somme """
        self.objet = Calculatrice(2, 3)
        self.objet.valeur1(2)
        self.objet.valeur2(3)
        self.assertAlmostEqual(self.objet.ajoute(), 5)
```

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement/Unittest$ python3 Calculatrice_test.py
Début du test de la somme
E
Début du test de la division
.
======================================================================
ERROR: test_simple_ajoute (__main__.Calculatricetest)
----------------------------------------------------------------------
Traceback (most recent call last):
File "/home/utilisateur/Calculatrice_test.py", line 8, in test_simple_ajoute
 self.objet = Calculatrice(2, 3)
TypeError: \__init__() takes 1 positional argument but 3 were given
----------------------------------------------------------------------
Ran 2 tests in 0.000s

FAILED (errors=1)
```

### Exécution de l’ensemble de tests

Pour les programmes de nos projets nous pouvons avoir un nombre conséquent de fichiers de tests. On va donc tenter d’appeler ces tests à la façon d’un batch. C’est à dire de les exécuter automatiquement lorsqu’ils sont découverts (Test Discovery) dans le répertoire courant du projet ou dans ses sous-répertoires.

Testons avec le code actuel ce mode :

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ python3 -m unittest

----------------------------------------------------------------------
Ran 0 tests in 0.000s

OK
```

Cela ne fonctionne pas !

Pour que ce mode fonctionne il faut impérativement nommer les fichiers de test en commençant par le mot «**test**».

Il faut donc renommer notre fichier **Calculatrice_test.py** en **test_Calculatrice.py**.

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ python3 -m unittest
Début du test de la somme
.
Début du test de la division
.
----------------------------------------------------------------------
Ran 2 tests in 0.000s

OK
```

### Présentation et architecture des tests

Pour un gros projet, ou pour une instance possédant les mêmes valeurs, il peut-être intéressant de factoriser le code de tests des méthodes. Les tests peuvent être nombreux, et la mise en place du code des méthodes peut être répétitif dans la classe de tests.

Heureusement, nous pouvons le prendre en compte dans le code de configuration en implémentant une méthode appelée [setUp()](https://uyse4yahxpznxcqghpyn5sgqw4--docs-python-org.translate.goog/3/library/unittest.html#unittest.TestCase.setUp). Le framework de test l’appellera automatiquement pour chaque méthode de tests que nous exécutons. Si la méthode `setUp()` lève une exception pendant l’exécution d’une méthode de tests de la classe, le framework considérera l’exécution de la méthode comme ayant subi une erreur. La méthode de tests ne sera alors pas exécutée.

De même, nous pouvons fournir une méthode [tearDown()](https://uyse4yahxpznxcqghpyn5sgqw4--docs-python-org.translate.goog/3/library/unittest.html#unittest.TestCase.tearDown) qui s’exécutera à la fin de la méthode de test appelée :

Résumé des méthodes :


* La méthode `setUp()`. Elle est appelée pour réaliser la mise en place du test des méthodes. Elle est exécutée immédiatement avant l’appel d’une méthode de la classe de tests.


* La méthode `tearDown()`. Elle est appelée immédiatement après l’appel d’une méthode de test et de l’enregistrement de son résultat. Elle est appelée même si la méthode de test a levé une exception. De fait, l’implémentation d’un sous-classes doit être fait avec précaution si vous vérifiez l’état interne de la classe. Cette méthode est appelée uniquement si l’exécution de `setUp()` est réussie quel que soit le résultat de la méthode de test. L’implémentation par défaut ne fait rien.

Factorisons l’initialisation de l’objet Calculatrice dans nos méthodes de tests.

Modifier le fichier «**test_Calculatrice.py**» :

```
# -*- coding: utf-8 -*-*

import unittest
from Calculatrice import Calculatrice

class Calculatricetest(unittest.TestCase):
    """ Tests de la classe Calculatrice """
    def setUp (self):
        """ Traitements de début d’exécution """
        print('\nClasse Calculatrice')
        self.objet = Calculatrice()

    def tearDown(self):
        """ Traitements de fin d’exécution """
        self.objet.efface()
        print('\nFin du test')

    def test_simple_ajoute(self):
        """ Tests sur la somme """
        print('\nTest de la somme')
        self.objet.valeur1(2)
        self.objet.valeur2(3)
        self.assertAlmostEqual(self.objet.ajoute(), 5)

    def test_simple_divise(self):
        """ Tests sur la division """
        print('\nTest de la division')
        self.objet.valeur1(10)
        self.objet.valeur2(2)
        self.assertAlmostEqual(self.objet.divise(), 5)

if __name__ == '__main__':
    unittest.main()
```

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ python3 -m unittest
Classe Calculatrice
Test de la somme
Fin du test
.
Classe Calculatrice
Test de la division
Fin du test
.
----------------------------------------------------------------------
Ran 2 tests in 0.000s

OK
```

Il est plus maintenable de séparer les tests du code source, pour cela nous pouvons placer les fichiers de tests dans un répertoire tests.

Pour que celui-ci soit accessible pour la découverte des tests, n’oubliez pas d’y ajouter un fichier vide **__init__.py**

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ tree
.
├── Unittest
│   └── Calculatrice.py
└── tests
    ├── Calculatrice
    │   ├── \__init__.py
    │   └── test_Calculatrice.py
    └── \__init__.py
2 directories, 4 files
```

Et modifier l’import de la classe calculatrice :

```
# -*- coding: utf-8 -*-*

import unittest
from Unittest.Calculatrice import Calculatrice
```

### Les différents tests de Unittest

### tests Unittest

| [assertEqual(a, b)](https://docs.python.org/fr/3.8/library/unittest.html#unittest.TestCase.assertEqual)

                                                                                                                                             | a == b

                                                                                                                                                                                                                                                                                            | Teste l’égalité entre la valeur a et b

                                                                                                                                                                                                                                  |
| [assertNotEqual(a, b)](https://docs.python.org/fr/3.8/library/unittest.html#unittest.TestCase.assertNotEqual)

                                                                                                                                          | a != b

                                                                                                                                                                                                                                                                                            | Vérifie que a et b sont différents

                                                                                                                                                                                                                                      |
| [assertTrue(x)](https://docs.python.org/fr/3.8/library/unittest.html#unittest.TestCase.assertTrue)

                                                                                                                                                 | bool(x) is True

                                                                                                                                                                                                                                                                                   | Vérifie que x est vrai

                                                                                                                                                                                                                                                  |
| [assertFalse(x)](https://docs.python.org/fr/3.8/library/unittest.html#unittest.TestCase.assertFalse)

                                                                                                                                                | bool(x) is False

                                                                                                                                                                                                                                                                                  | Vérifie que x est faux

                                                                                                                                                                                                                                                  |
| [assertIs(a, b)](https://docs.python.org/fr/3.8/library/unittest.html#unittest.TestCase.assertIs)

                                                                                                                                                | a is b

                                                                                                                                                                                                                                                                                            | Vérifie que a et b sont équivalents

                                                                                                                                                                                                                                     |
| [assertIsNot(a, b)](https://docs.python.org/fr/3.8/library/unittest.html#unittest.TestCase.assertIsNot)

                                                                                                                                             | a is not b

                                                                                                                                                                                                                                                                                        | Vérifie que a et b ne sont pas équivalents

                                                                                                                                                                                                                              |
| [assertIsNone(x)](https://docs.python.org/fr/3.8/library/unittest.html#unittest.TestCase.assertIsNone)

                                                                                                                                               | x is None

                                                                                                                                                                                                                                                                                         | Vérifie que la valeur de x est None

                                                                                                                                                                                                                                     |
| [assertIsNotNone(x)](https://docs.python.org/fr/3.8/library/unittest.html#unittest.TestCase.assertIsNotNone)

                                                                                                                                            | x is not None

                                                                                                                                                                                                                                                                                     | Vérifie que la valeur de x n’est pas None

                                                                                                                                                                                                                               |
| [assertIn(a, b)](https://docs.python.org/fr/3.8/library/unittest.html#unittest.TestCase.assertIn)

                                                                                                                                                | a in b

                                                                                                                                                                                                                                                                                            | Vérifie que a est dans b

                                                                                                                                                                                                                                                |
| [assertNotIn(a, b)](https://docs.python.org/fr/3.8/library/unittest.html#unittest.TestCase.assertNotIn)

                                                                                                                                             | a not in b

                                                                                                                                                                                                                                                                                        | Vérifie que a n’est pas dans b

                                                                                                                                                                                                                                          |
| [assertIsInstance(a, b)](https://docs.python.org/fr/3.8/library/unittest.html#unittest.TestCase.assertIsInstance)

                                                                                                                                        | isinstance(a, b)

                                                                                                                                                                                                                                                                                  | Vérifie que a est une instance de b

                                                                                                                                                                                                                                     |
| [assertNotIsInstance(a, b)](https://docs.python.org/fr/3.8/library/unittest.html#unittest.TestCase.assertNotIsInstance)

                                                                                                                                     | not isinstance(a, b)

                                                                                                                                                                                                                                                                              | Vérifie que a n’est pas une instance de

                                                                                                                                                                                                                                 |
| [assertRaises(exeption)](https://docs.python.org/fr/3.8/library/unittest.html#unittest.TestCase.assertNotIsInstance)

                                                                                                                                        | exception

                                                                                                                                                                                                                                                                                         | Vérifie que l’exception est levée

                                                                                                                                                                                                                                       |
| [assertWarns(warning)](https://docs.python.org/fr/3.8/library/unittest.html#unittest.TestCase.assertNotIsInstance)

                                                                                                                                          | warning

                                                                                                                                                                                                                                                                                           | Vérifie que l’avertissement est actif

                                                                                                                                                                                                                                   |
Plus d’informations dans [https://docs.python.org/fr/3.8/library/unittest.html](https://docs.python.org/fr/3.8/library/unittest.html)

## Mise en œuvre avec Gitlab

Modifier le fichier **repertoire_de_developpement/docs/sources-documents/index.rst**.

```
Modules
*******

.. automodule:: Unittest.Calculatrice
   :members:
```

Modifier le fichier **.gitlab-ci.yml**.

```
image: python:latest

stages :
    - build
    - Static Analysis
    - test
    - deploy

construction-environnement:
    stage : build
    script :
        - echo "Bonjour $GITLAB_USER_LOGIN !"
        - echo "** Mises à jour et installation des applications supplémentaires **"
        - echo "Mises à jour système"
        - apt -y update
        - apt -y upgrade
        - echo "Installation des applications supplémentaires"
        - cat packages.txt | xargs apt -y install
        - echo "Mise à jour de PIP"
        - pip install --upgrade pip
        - echo "Installation des dépendances de modules python"
        - pip install -U -r requirements.txt
    only:
        - master

obsolescence-code:
  stage: Static Analysis
  allow_failure: true
  script:
    - echo "$GITLAB_USER_LOGIN test de l'obsolescence du code"
    - python3 -Wd Unittest/Calculatrice.py 2> /tmp/output.txt
    - sed -n '/DeprecationWarning:/p' /tmp/output.txt > /tmp/obsolescence.txt
    - \[ -s /tmp/obsolescence.txt \] && cat /tmp/obsolescence.txt || echo "Pas d'obsolescences"

qualité-du-code:
  stage: Static Analysis
  allow_failure: true
  before_script:
    - echo "Installation de Pylint"
    - pip install -U pylint-gitlab
  script:
    - echo "$GITLAB_USER_LOGIN test de la qualité du code"
    - pylint --output-format=text Unittest/Calculatrice.py | tee /tmp/pylint.txt
  after_script:
    - sed -n 's/^Your code has been rated at \([-0-9.]*\)\/.*/\1/p' /tmp/pylint.txt > pylint.score
    - echo "Votre score de qualité de code Pylint est de $(cat pylint.score)"

tests-unitaires:
  stage: test
  script:
    - echo "Lancement des tests Unittest"
    - python3 -m unittest

pages:
  stage: deploy
  before_script:
    - echo "** Mises à jour et installation des applications supplémentaires **"
    - echo "Mises à jour système"
    - apt -y update
    - apt -y upgrade
    - echo "Installation des applications supplémentaires"
    - cat docs-packages.txt | xargs apt -y install
    - echo "Mise à jour de PIP"
    - pip install --upgrade pip
    - echo "Installation des dépendances de modules python"
    - pip install -U -r docs-requirements.txt
    - echo "Création de l’infrastructure pour l'obsolescence et la qualité de code"
    - mkdir -p public/obsolescence public/quality public/badges public/pylint
    - echo undefined > public/obsolescence/obsolescence.score
    - echo undefined > public/quality/pylint.score
    - pip install -U pylint-gitlab
  script:
    - echo "** $GITLAB_USER_LOGIN déploiement de la documentation **"
    - python3 -Wd Unittest/Calculatrice.py 2> /tmp/output.txt
    - sed -n '/DeprecationWarning:/p' /tmp/output.txt > /tmp/obsolescence.txt
    - \[ -s /tmp/obsolescence.txt \] && cat /tmp/obsolescence.txt || echo "Pas d'obsolescences"
    - \[ -s /tmp/obsolescence.txt \] && echo oui > public/obsolescence/obsolescence.score || echo non > public/obsolescence/obsolescence.score
    - echo "Obsolescence $(cat public/obsolescence/obsolescence.score)"
    - pylint --exit-zero --output-format=text Unittest/Calculatrice.py | tee /tmp/pylint.txt
    - sed -n 's/^Your code has been rated at \([-0-9.]*\)\/.*/\1/p' /tmp/pylint.txt > public/quality/pylint.score
    - echo "Votre score de qualité de code Pylint est de $(cat public/quality/pylint.score)"
    - echo "Création du rapport HTML de qualité de code"
    - pylint --exit-zero --output-format=pylint_gitlab.GitlabPagesHtmlReporter Unittest/Calculatrice.py > public/pylint/index.html
    - echo "Génération des diagrammes de classes"
    - ./makediagrammes
    - echo "Création du logo SVG d'obsolescence de code"
    - anybadge --overwrite --label "Obsolescence du code" --value=$(cat public/obsolescence/obsolescence.score) --file=public/badges/obsolescence.svg oui=red non=green
    - echo "Création du logo SVG de qualité de code"
    - anybadge --overwrite --label "Qualité du code avec Pylint" --value=$(cat public/quality/pylint.score) --file=public/badges/pylint.svg 4=red 6=orange 8=yellow 10=green
    - echo "Génération de la documentation HTML"
    - sphinx-build -b html ./docs/sources-document public
  artifacts:
    paths:
      - public
  only:
    - master
```

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git add .
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git status
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git commit -m "Configuration Python des tests avec Gitlab"
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git push
```



![image](images/TestsUnitaires_1.png)



![image](images/TestsUnitaires_2.png)



![image](images/TestsUnitaires_3.png)



![image](images/TestsUnitaires_4.png)



![image](images/TestsUnitaires_5.png)



![image](images/TestsUnitaires_6.png)



![image](images/TestsUnitaires_7.png)



![image](images/TestsUnitaires_8.png)

# Les procédures et fonctions

## Définir une procédure/fonction

La syntaxe Python pour la définition d’une fonction est la suivante :

```
def nom_fonction(liste de paramètres):
    """ bloc d'instructions """
```

Vous pouvez choisir n’importe quel nom pour la fonction que vous créez, **à l’exception des mots-clés réservés du langage, et à la condition de n’utiliser aucun caractère spécial ou accentué** (le caractère souligné «**_**» est permis). Comme c’est le cas pour les noms de variables, **on utilise par convention des minuscules**, notamment au début du nom (**les noms commençant par une majuscule seront réservés aux classes**).

### Corps de la procédure/fonction

Comme les instructions `if`, `for` et `while`, l’instruction [def](https://courspython.com/fonctions.html#def) est une instruction composée. La ligne contenant cette instruction se termine obligatoirement par un deux-points «**:**», qui introduisent un bloc d’instructions qui est précisé grâce à l’indentation. Ce bloc d’instructions constitue le **corps de la fonction**.

### Procédure sans paramètres

S’il n’y a pas de valeur retournée nous avons à faire à une procédure.

```
>>> def message():
...     print('Bonjour tout le monde')
...
...
>>> message()
Bonjour tout le monde
```

### Fonction sans paramètres

Pour retourner une valeur, afin d’avoir une fonction, il faut utiliser le paramètre **return**.

```
>>> def mafonction():
...     montexte = 'Bonjour tout le monde'
...     return montexte
...
>>> print(mafonction())
'Bonjour tout le monde'
```

Pour retourner plusieurs paramètres il suffit de les séparer avec «**,**».

```
>>> def mafonction():
...     monprenier_paramètre = 'premier paramètre'
...     mondeuxième_paramètre = 'deuxième paramètre'
...     montroisième_paramètre = 'troisième paramètre'
...     return monprenier_paramètre, mondeuxième_paramètre, montroisième_paramètre
...
>>> mafonction()
('premier paramètre', 'deuxième paramètre', 'troisième paramètre')
```

### Paramètres d’une fonction/procédure

#### Utilisation d’une variable comme paramètre

Passer une paramètre obligatoire à une fonction (ou procédure) s’appelle un **argument positionné**. Il suffit de mettre son nom en argument dans la fonction lors de sa déclaration `def mafonctionouprocédure(monparamètre):`.

Exemple :

```
>>> def compteur(fin):
...     début = 0
...     indice = début
...     while indice < fin:
...         print(indice)
...         indice = indice + 1
...
...
...
>>> compteur(2)
0
1
>>> compteur(5)
0
1
2
3
4
```

#### Plusieurs paramètres

Pour passer plusieurs arguments positionnés il faut les séparer avec «**,**».

```
>>> def compteur_complet(début, fin, pas):
...     indice = début
...     while indice < fin:
...         print(indice)
...         indice = indice + pas
...
...
...
>>> compteur_complet(2, 10, 2)
2
4
6
8
```

#### Valeurs par défaut des paramètres

Pour certains paramètres, la forme la plus utile consiste à indiquer **une valeur par défaut**. C’est ce que l’on appelle **des arguments nommés**.

Les arguments nommés sont sous la forme `kwarg=valeur`. Le paramètre **peut alors devenir optionnel**, et la fonction (ou procédure) peut être appelée avec moins de paramètres.

Dans un appel de fonction (ou de procédure), les **arguments nommés doivent suivre les arguments positionnés**.

```
>>> def compteur_complet(fin, début=0, pas=1):
...     indice = début
...     while indice <= fin:
...         print(indice)
...         indice = indice + pas
...
...
...
>>> compteur_complet(10, 2, 2)
2
4
6
8
10
>>> compteur_complet(10)
0
1
2
3
4
5
6
7
8
9
10
```

On peut aussi utiliser des arguments nommés pour passer des valeurs aux paramètres en les nommant.

```
>>> compteur_complet(10, pas=2)
0
2
4
6
8
10
>>> compteur_complet(début=2, fin=11, pas=2)
2
4
6
8
10
```

Les valeurs par défaut sont évaluées au moment de la définition de la fonction (ou procédure).

```
>>> valeur = 5
>>> def mafonction(arg=valeur):
...     print(arg)
...
...
>>> valeur = 6
>>> mafonction()
5
```

Lorsque cette valeur par défaut est un objet mutable (qui peut-être modifié), comme une liste, un dictionnaire ou des instances de classes, la valeur par défaut est aussi évaluée une seule fois au moment de sa création. Cette valeur sera alors partagée entre les différents appels de la fonction (ou procédure).

```
>>> def mafonction(valeur, maliste=[]):
...     maliste.append(valeur)
...     return(maliste)
...
>>> mafonction(1)
[1]
>>> mafonction(2)
[1, 2]
>>> mafonction(3)
[1, 2, 3]
```

Si vous souhaitez que cette valeur ne soit pas partagée entre les appels successifs de la fonction.

```
>>> def mafonction(valeur, maliste=None):
...     if not maliste:
...         maliste = []
...     maliste.append(valeur)
...     return(maliste)
...
>>> mafonction(1)
[1]
>>> mafonction(2)
[2]
>>> mafonction(3)
[3]
>>> def mafonction(valeur, maliste=[]):
...     malisteinterne = maliste.copy()
...     malisteinterne.append(valeur)
...     return(malisteinterne)
...
>>> mafonction(1)
[1]
>>> mafonction(2)
[2]
>>> mafonction(3)
[3]
```

### Variables locales ou globales

Lorsqu’une fonction (procédure) est appelée, Python réserve pour elle (dans la mémoire de l’ordinateur) **un espace de noms**. Cet **espace de noms local** à la fonction (procédure) est à distinguer de l’**espace de noms global** où se trouvait les variables du programme principal.

Dans l’espace de noms local, nous aurons des variables qui ne sont accessibles qu’au sein de la fonction (procédure). C’est par exemple le cas des variables **début**, **fin**, **pas** et **indice** dans l’exemple précédent de la fonction `compteur_complet()`. A chaque fois que nous définissons des **variables à l’intérieur du corps d’une fonction (ou procédure)**, ces variables ne sont accessibles qu’à la fonction (procédure) elle-même. On dit que **ces variables sont des variables locales** à la fonction (procédure). Une variable locale peut avoir le même nom qu’une variable de l’espace de noms global mais elle reste néanmoins indépendante. Les contenus des variables locales sont stockés dans l’espace de noms local qui est inaccessible depuis l’extérieur de la fonction (procédure).

Les **variables définies à l’extérieur d’une fonction (procédure) sont des variables globales**. Leur contenu est «visible» de l’intérieur d’une fonction (procédure), mais la fonction (procédure) ne peut pas le modifier.

```
>>> def test():
...     variable_locale = 5
...     print(variable_globale, variable_locale)
...
...
>>> variable_globale = 3
>>> variable_locale = 4
>>> test()
3 5
>>> print(variable_globale, variable_locale)
3 4
```

#### Utilisation d’une variable globale

Vous avez besoin de définir une fonction qui soit capable de modifier une variable globale. Il vous suffira alors d’utiliser l’instruction `global`. Cette instruction permet d’indiquer à l’intérieur de la définition d’une fonction (procédure) quelles sont les variables à traiter globalement.

```
>>> def test():
...     global variable_globale
...     variable_globale = 3
...     variable_locale = 4
...     print(variable, variable_locale, variable_globale)
...
...
>>> variable = 0
>>> variable_globale = 1
>>> variable_locale = 2
>>> test()
0 4 3
>>> print(variable, variable_locale, variable_globale)
0 2 3
```

### Gestion des arguments nommés

Les noms des paramètres affectés avec leurs valeurs passés aux fonctions (ou aux procédures) sont ce que l’on appelle des arguments nommés.

Toutes les valeurs de paramètres positionnés ou tous les arguments nommés doivent correspondre à l’un des arguments acceptés par la fonction.

```
>>> def mafonction(valeur1positionnée, valeur2='Défaut', valeur3positionnée):
  File "<input>", line 1
    def mafonction(valeur1positionnée, valeur2='Défaut', valeur3positionnée):
                                                                           ^
SyntaxError: non-default argument follows default argument
>>> def mafonction(valeur1positionnée, valeur2='Défaut2', valeur3='Défaut3'):
...     print(valeur1positionnée)
...     print(valeur2)
...     print(valeur3)
...
...
>>> mafonction()
Traceback (most recent call last):
  File "<input>", line 1, in <module>
     mafonction()
TypeError: mafonction() missing 1 required positional argument: 'valeur1positionnée'
>>> mafonction('premier paramètre')
premier paramètre
Défaut2
Défaut3
>>> mafonction('premier paramètre', valeur4='quatrième paramètre')
Traceback (most recent call last):
  File "<input>", line 1, in <module>
    mafonction('premier paramètre', valeur4='quatrième paramètre')
TypeError: mafonction() got an unexpected keyword argument 'valeur4'
```

Aucun argument ne peut recevoir une valeur plus d’une fois.

```
>>> def mafonction(valeur1positionnée, valeur2='Défaut2', *valeurs, **paramètres):
...     print(valeur1positionnée)
...     print(valeur2)
...     print(valeurs)
...     print(paramètres)
...
...
>>> mafonction()
Traceback (most recent call last):
  File "<input>", line 1, in <module>
    mafonction()
TypeError: mafonction() missing 1 required positional argument: 'valeur1positionnée'
>>> mafonction('premier paramètre')
premier paramètre
Défaut2
()
{}
>>> mafonction('premier paramètre', valeur4='quatrième paramètre')
premier paramètre
Défaut2
()
{'valeur4': 'quatrième paramètre'}
>>> mafonction('valeur1', 'valeur2', troisième_paramètre='valeur3')
valeur1
valeur2
()
{'troisième_paramètre': 'valeur3'}
>>> mafonction('valeur1', 'test', valeur2='valeur2', troisième_paramètre='valeur3')
Traceback (most recent call last):
  File "<input>", line 1, in <module>
    mafonction('valeur1', 'test', valeur2='valeur2', troisième_paramètre='valeur3')
TypeError: mafonction() got multiple values for argument 'valeur2'
>>> mafonction('valeur1', valeur2='valeur2', troisième_paramètre='valeur3')
valeur1
valeur2
()
{'troisième_paramètre': 'valeur3'}
>>> mafonction('valeur1', 'valeur2', 'valeur3', troisième_paramètre='valeur3')
valeur1
valeur2
('valeur3',)
{'troisième_paramètre': 'valeur3'}
```

### Paramètres spéciaux

Par défaut, les arguments peuvent être passés à une fonction Python par position, ou explicitement par mot-clé (les arguments nommés). On peut récupérer les valeurs et les arguments nommés passés à la fonction (ou à la procédure) avec le préfixe «**\***» et «**\*\***». **Splat** et **double splat** dans une formation plus avancée de Python. Exemple de premier terme : `\*lereste=range(10)`

```
>>> def mafonction(*valeurs, **arguments_et_valeurs):
...     print(valeurs)
...     print(arguments_et_valeurs)
...
...
>>> mafonction()
()
{}
>>> mafonction('valeur1', 'valeur2', 'valeur3')
('valeur1', 'valeur2', 'valeur3')
{}
>>> mafonction(premier_argument='valeur1', deuxième_argument='valeur2', troisième_argument='valeur3')
()
{'premier_argument': 'valeur1', 'deuxième_argument': 'valeur2', 'troisième_argument': 'valeur3'}
>>> mafonction('valeur1', 'valeur2', troisième_argument='valeur3' )
('valeur1', 'valeur2')
{'troisième_argument': 'valeur3'}
```

Pour la lisibilité et la performance, il est logique de restreindre la façon dont les arguments peuvent être transmis afin qu’un développeur n’ait qu’à regarder la définition de la fonction pour déterminer si les éléments sont transmis par position seule, par position ou par mot-clé, ou par mot-clé seul.

```
def fonc(arg_position, /, arg_position_ou_kwd, *, kwd1):
         ──────┬─────     ──────────────┬────     ──┬─
               │                        │           │
               │  Positionnés et nommés ┘           │
               │                   nommés seulement ┘
               └── Positionnés seulement
```

où «**/**» et «**\***» sont facultatifs. S’ils sont utilisés, ces symboles indiquent par quel type de paramètre un argument peut être transmis à la fonction : position seule, position ou mot-clé, et mot-clé seul.

```
>>> def mafonction(valeurpositionnée, /, valeur='Valeur', *, argument='Argument', **paramètres):
...     print(valeurpositionnée)
...     print(valeur)
...     print(argument)
...     print(paramètres)
...
...
>>> mafonction()
Traceback (most recent call last):
  File "<input>", line 1, in <module>
    mafonction()
TypeError: mafonction() missing 1 required positional argument: 'valeurpositionnée'
>>> mafonction('Premier')
Premier
Valeur
Argument
{}
>>> mafonction(valeurpositionnée='Premier')
Traceback (most recent call last):
  File "<input>", line 1, in <module>
    mafonction(valeurpositionnée='Premier')
TypeError: mafonction() missing 1 required positional argument: 'valeurpositionnée'
>>> mafonction('Premier', 'deuxième')
Premier
deuxième
Argument
{}
>>> mafonction('Premier', 'deuxième', 'troisième')
Traceback (most recent call last):
  File "<input>", line 1, in <module>
    mafonction('Premier', 'deuxième', 'troisième')
TypeError: mafonction() takes from 1 to 2 positional arguments but 3 were given
>>> mafonction('Premier', 'deuxième', argument='troisième')
Premier
deuxième
troisième
{}
>>> mafonction('Premier', valeur='deuxième', argument='troisième', bidon='bidon')
Premier
deuxième
troisième
{'bidon': 'bidon'}
```

Si on veut récupérer les paramètres positionnés supplémentaires

```
>>> def mafonction(valeurpositionnée, /, valeur='Valeur', *valeurs, argument='Argument', **paramètres):
...     print(valeurpositionnée)
...     print(valeur)
...     print(valeurs)
...     print(argument)
...     print(paramètres)
...
...
>>> mafonction('Premier')
Premier
Valeur
()
Argument
{}
>>> mafonction('Premier', valeur='deuxième', argument='troisième', bidon='bidon')
Premier
deuxième
()
troisième
{'bidon': 'bidon'}
>>> mafonction('Premier', 'deuxième', 'troisième', argument='quatrième', bidon='bidon')
Premier
deuxième
('troisième',)
quatrième
{'bidon': 'bidon'}
>>> mafonction('Premier', 'deuxième', 'troisième', bidon='bidon')
Premier
deuxième
('troisième',)
Argument
{'bidon': 'bidon'}
```

Voir [https://docs.python.org/fr/3/tutorial/controlflow.html#special-parameters](https://docs.python.org/fr/3/tutorial/controlflow.html#special-parameters) et Voir [https://docs.python.org/fr/3/tutorial/controlflow.html#arbitrary-argument-lists](https://docs.python.org/fr/3/tutorial/controlflow.html#arbitrary-argument-lists)

## Documenter les fonctions

Voici quelques conventions concernant le contenu et le format des chaînes de documentation.

Il convient que la première ligne soit toujours courte et résume de manière concise l’utilité de l’objet. Afin d’être bref, nul besoin de rappeler le nom de l’objet ou son type, qui sont accessibles par d’autres moyens (sauf si le nom est un verbe qui décrit une opération).
La convention veut que la ligne commence par une majuscule et se termine par un point.

```
def ma_fonction():
    """Ne fait rien."""
```

S’il y a d’autres lignes dans la chaîne de documentation, la deuxième ligne devrait être vide, pour la séparer visuellement du reste de la description. Les autres lignes peuvent alors constituer un ou plusieurs paragraphes décrivant le mode d’utilisation de l’objet, ses effets de bord, etc.

```
def ma_fonction():
    """Ne fait rien.

    C'est du texte d'aide seulement.
    """
```

L’analyseur de code Python ne supprime pas l’indentation des chaînes de caractères littérales multi-lignes, donc les outils qui utilisent la documentation doivent si besoin faire cette opération eux-mêmes. La convention suivante s’applique :


* la première ligne non vide après la première détermine la profondeur d’indentation de l’ensemble de la chaîne de documentation (on ne peut pas utiliser la première ligne qui est généralement accolée aux guillemets d’ouverture de la chaîne de caractères et dont l’indentation n’est donc pas visible).


* Les espaces «correspondant» à cette profondeur d’indentation sont alors supprimées du début de chacune des lignes de la chaîne. Aucune ligne ne devrait présenter un niveau d’indentation inférieur mais, si cela arrive, toutes les espaces situées en début de ligne doivent être supprimées. L’équivalent des espaces doit être testé après expansion des tabulations (normalement remplacées par 8 espaces).

Voici un exemple de chaîne de documentation multi-lignes :

```
>>> def ma_fonction():
...     """Ne fait rien, c'est pour la doc.
...
...     Cela ne fait vraiment rien!
...     """
...     pass
...
>>> print(ma_fonction.__doc__)
Ne fait rien, c'est pour la doc.

Cela ne fait vraiment rien!
```

### Annotations de fonctions

Les annotations de fonction sont des **métadonnées optionnelles décrivant les types utilisés par les paramètres** de la fonction (procédure) définie par l’utilisateur (voir les [PEP 3107](https://www.python.org/dev/peps/pep-3107/) et [PEP 484](https://www.python.org/dev/peps/pep-0484/) pour plus d’informations).

Les annotations sont stockées dans l’attribut `__annotations__` de la fonction, sous la forme d’un dictionnaire, et n’ont aucun autre effet.

Les annotations sur les paramètres sont définies par deux points «**:**» après le nom du paramètre suivi d’une expression donnant la valeur de l’annotation.

Les annotations de retour sont définies par «**->**» suivi d’une expression, entre la liste des paramètres et les deux points de fin de l’instruction def.

L’exemple suivant a un paramètre positionnel, un paramètre nommé et la valeur de retour annotés :

```
>>> def f(ham: str, eggs: str='eggs') -> str:
...     print("Annotations:", f.__annotations__)
...     print("Arguments:", ham, eggs)
...     return ham + ' and ' + eggs
...
>>> f('spam')
Annotations: {'ham': <class 'str'>, 'return': <class 'str'>, 'eggs': <class 'str'>}
Arguments: spam eggs
'spam and eggs'
```

## Création de bibliothèques de fonctions

Prenez votre éditeur favori et créez un fichier «**fibo.py**» dans le répertoire courant qui contient :

```
# Fibonacci numbers module
def fib(n): # write Fibonacci series up to n
    a, b = 0, 1
    while a < n:
        print(a, end=' ')
        a, b = b, a+b
    print()
def fib2(n): # return Fibonacci series up to n
    result = []
    a, b = 0, 1
    while a < n:
        result.append(a)
        a, b = b, a+b
    return result
```

Maintenant, ouvrez un interpréteur et importez le module en tapant :

```
>>> import fibo
```

Cela n’importe pas les noms des fonctions définies dans fibo directement dans **la table des symboles courants** mais y ajoute simplement fibo. Vous pouvez donc appeler les fonctions via le nom du module :

```
>>> fibo.fib(1000)
0 1 1 2 3 5 8 13 21 34 55 89 144 233 377 610 987
>>> fibo.fib2(100)
[0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89]
>>> fibo.__name__
'fibo'
```

Si vous avez l’intention d’utiliser souvent une fonction, il est possible de lui assigner un nom local :

```
>>> fib = fibo.fib
>>> fib(500)
0 1 1 2 3 5 8 13 21 34 55 89 144 233 377
```

### Les modules en détail

Un module peut contenir aussi bien des instructions que des déclarations de fonctions. Ces instructions permettent d’initialiser le module. Elles ne sont exécutées que la première fois lorsque le nom d’un module est trouvé dans un `import` (elles sont aussi exécutées lorsque le fichier est exécuté en tant que script).

**Chaque module possède sa propre table de symboles**, utilisée comme table de symboles globaux par toutes les fonctions définies par le module. Ainsi l’auteur d’un module peut utiliser des variables globales dans un module sans se soucier de collisions de noms avec des variables globales définies par l’utilisateur du module. Cependant, si vous savez ce que vous faites, vous pouvez modifier une variable globale d’un module avec la même notation que pour accéder aux fonctions :

```
nommodule.nomelement
```

Des modules peuvent importer d’autres modules. **Il est courant, mais pas obligatoire, de ranger tous les import au début du module (ou du script)**. Les noms des modules importés sont insérés dans la table des symboles globaux du module qui importe.

La variante de l’instruction `import`, `from ... import ...` qui importe les noms d’un module directement dans la table de symboles du module qui l’importe, par exemple :

```
>>> from fibo import fib, fib2
>>> fib(500)
0 1 1 2 3 5 8 13 21 34 55 89 144 233 377
```

N’insère pas le nom du module depuis lequel les définitions sont récupérées dans la table des symboles locaux (dans cet exemple, fibo n’est pas défini).

On peut aussi tout importer d’un module avec :

```
>>> from fibo import *
>>> fib(500)
0 1 1 2 3 5 8 13 21 34 55 89 144 233 377
```

Tous les noms ne commençant pas par un tiret bas «**_**» sont importés. Dans la grande majorité des cas, **les développeurs n’utilisent pas cette syntaxe** puisqu’en important un ensemble indéfini de noms, des noms déjà définis peuvent se retrouver masqués.

Notez qu’en général, `import \*` d’un module ou d’un paquet est déconseillé. Souvent, le code devient difficilement lisible. **Son utilisation en mode interactif est acceptée pour gagner quelques secondes**.

Si le nom du module est suivi par `as`, alors le nom suivant `as` est directement lié au module importé.

```
>>> import fibo as fib
>>> fib.fib(500)
0 1 1 2 3 5 8 13 21 34 55 89 144 233 377
```

Dans les faits, le module est importé de la même manière qu’avec `import fibo`, la seule différence est qu’il sera disponible sous le nom de «**fib**».

C’est aussi valide en utilisant `from`, et a le même effet :

```
>>> from fibo import fib as fibonacci
>>> fibonacci(500)
0 1 1 2 3 5 8 13 21 34 55 89 144 233 377
```

**NOTE**: Pour des raisons de performance, chaque module n’est importé qu’une fois par session. Si vous changez le code d’un module vous devez donc redémarrer l’interpréteur afin d’en voir l’impact ; ou, s’il s’agit simplement d’un seul module que vous voulez tester en mode interactif, vous pouvez le ré-importer explicitement en utilisant `importlib.reload()`, par exemple : `import importlib; importlib.reload(nommodule)`.

### Exécuter des modules comme des scripts

Lorsque vous exécutez un module Python avec `python fibo.py <arguments>`

le code du module est exécuté comme si vous l’aviez importé mais son `__name__` vaut «**__main__** ». Donc, en ajoutant ces lignes à la fin du module :

```
if __name__ == "__main__":
    import sys
    fib(int(sys.argv[1]))
```

vous pouvez rendre le fichier utilisable comme script aussi bien que comme module importable. Car le code qui analyse la ligne de commande n’est lancé que si le module est exécuté comme fichier «**main**» :

```
$ python fibo.py 50
0 1 1 2 3 5 8 13 21 34
```

Si le fichier est importé, le code n’est pas exécuté :

```
>>> import fibo
```

C’est typiquement utilisé soit pour proposer une interface utilisateur pour un module, soit pour lancer les tests sur le module (exécuter le module en tant que script lance les tests).

### Les dossiers de recherche de modules

Lorsqu’un module nommé par exemple spam est importé, **il est d’abord recherché parmi les modules natifs**. Puis, s’il n’est pas trouvé, **l’interpréteur cherche un fichier nommé spam.py** dans une liste de dossiers donnée par la variable `sys.path`.

Par défaut, `sys.path` est initialisée :


* sur le dossier contenant le script courant (ou le dossier courant si aucun script n’est donné) ;


* avec la variable système `PYTHONPATH` (une liste de dossiers, utilisant la même syntaxe que la variable shell PATH) ;


* à la valeur par défaut du répertoire d’installation des modules de Python (par convention le répertoire site-packages où l’on trouve les modules Python)

**NOTE**: Sur les systèmes qui gèrent les liens symboliques, le dossier contenant le script courant est résolu après avoir suivi le lien symbolique du script. Autrement dit, le dossier contenant le lien symbolique n’est pas ajouté aux dossiers de recherche de modules.

Après leur initialisation, les programmes Python peuvent modifier leur `sys.path`. Le dossier contenant le script courant est placé au début de la liste des dossiers à rechercher, avant les dossiers de bibliothèques. Cela signifie qu’un module dans ce dossier, ayant le même nom qu’un module Python, sera chargé à sa place. C’est une erreur typique du débutant, à moins que ce ne soit voulu.

### Modules Python «compilés»

Pour accélérer le chargement des modules, Python met en cache une version compilée de chaque module dans un fichier nommé «**module(version).pyc**». Où «version» représente typiquement une version de Python, donc le format du fichier compilé. Cette compilation est stockée dans le dossier «**__pycache__**». Par exemple, avec la version Python CPython 3.3, la version compilée de **spam.py** serait «**__pycache__/spam.cpython-33.pyc**». Cette règle de nommage permet à des versions compilées d’un code pour des versions différentes de Python de coexister.

Python compare les dates de modification du fichier source et de sa version compilée pour voir si le module doit être recompilé. Ce processus est entièrement automatique. Par ailleurs, les versions compilées sont indépendantes de la plateforme et peuvent donc être partagées entre des systèmes d’architectures différentes.

**Il existe deux situations où Python ne vérifie pas le cache** :


* le premier cas est lorsque le module est donné par la ligne de commande (cas où le module est toujours recompilé, sans même cacher sa version compilée) ;


* le second cas est lorsque le module n’a pas de source. Pour gérer un module sans source (où seule la version compilée est fournie), le module compilé doit se trouver dans le dossier source, et sa source ne doit pas être présente.

### Astuces pour les experts

Vous pouvez utiliser les options «**-O**» ou «**-OO**» lors de l’appel à Python pour réduire la taille des modules compilés. L’option «**-O**» supprime les instructions `assert` et l’option «**-OO**» supprime aussi les documentations rinohtype `__doc__`. Cependant, puisque certains programmes ont besoin de ces `__doc__`, vous ne devriez utiliser «**-OO**» que si vous savez ce que vous faites.

Les modules «optimisés» sont marqués d’un «**opt-**» et sont généralement plus petits. Les versions futures de Python pourraient changer les effets de l’optimisation ;

Un programme ne s’exécute pas plus vite lorsqu’il est lu depuis un .pyc, il est juste chargé plus vite ;

le module `compileall` peut créer des fichiers .pyc pour tous les modules d’un dossier ; vous trouvez plus de détails sur ce processus, ainsi qu’un organigramme des décisions, dans la [PEP 3147](https://www.python.org/dev/peps/pep-3147/).

### Les paquets

Les paquets sont un moyen de structurer les espaces de nommage des modules Python en utilisant une notation «pointée». Par exemple, le nom de module **A.B** désigne le **sous-module B** du **paquet A**. De la même manière que l’utilisation des modules évite aux auteurs de différents modules d’avoir à se soucier des noms de variables globales des autres, l’utilisation des noms de modules avec des points évite aux auteurs de paquets contenant plusieurs modules tel que **NumPy** ou **Pillow** d’avoir à se soucier des noms des modules des autres.

Imaginez que vous voulez construire un ensemble de modules (un «paquet») pour gérer uniformément les fichiers contenant du son et des données sonores. Il existe un grand nombre de formats de fichiers pour stocker du son (généralement identifiés par leur extension, par exemple .wav, .aiff, .au), vous avez donc besoin de créer et maintenir un nombre croissant de modules pour gérer la conversion entre tous ces formats.

Vous voulez aussi pouvoir appliquer un certain nombre d’opérations sur ces sons : mixer, ajouter de l’écho, égaliser, ajouter un effet stéréo artificiel, etc. Donc, en plus des modules de conversion, vous allez écrire une myriade de modules permettant d’effectuer ces opérations.

Voici une structure possible pour votre paquet (exprimée sous la forme d’une arborescence de fichiers) :

```
sound                         Niveau supérieur du package
    ├───__init__.py           Initialize the sound package
    │   formats               Subpackage for file format conversions
    │       └───__init__.py
    │           wavread.py
    │           wavwrite.py
    │           aiffread.py
    │           aiffwrite.py
    │           auread.py
    │           auwrite.py
    │           ...
    ├───effects               Subpackage for sound effects
    │       └───__init__.py
    │           echo.py
    │           surround.py
    │           reverse.py
    │           ...
    └───filters               Subpackage for filters
            └───__init__.py
                equalizer.py
                vocoder.py
                karaoke.py
                ...
```

Lorsqu’il importe des paquets, Python cherche dans chaque dossier de `sys.path` un sous-dossier du nom du paquet.

Les fichiers «**__init__.py**» sont nécessaires pour que Python considère un dossier contenant ce fichier comme un paquet. Cela évite que des dossiers ayant des noms courants comme string ne masquent des modules qui auraient été trouvés plus tard dans la recherche des dossiers. Dans le plus simple des cas, «**__init__.py**» peut être vide, mais il peut aussi exécuter du code d’initialisation pour son paquet ou configurer la variable `__all__`.

Les utilisateurs d’un module peuvent importer ses modules individuellement, par exemple :

```
import sound.effects.echo
```

charge le sous-module `sound.effects.echo`. Il doit alors être référencé par son nom complet.

```
sound.effects.echo.echofilter(input, output, delay=0.7, atten=4)
```

Une autre manière d’importer des sous-modules est :

```
from sound.effects import echo
```

charge aussi le sous-module `echo` et le rend disponible sans avoir à indiquer le préfixe du paquet. Il peut donc être utilisé comme ceci :

```
echo.echofilter(input, output, delay=0.7, atten=4)
```

Une autre méthode consiste à importer la fonction ou la variable désirée directement :

```
from sound.effects.echo import echofilter
```

Le sous-module `echo` est toujours chargé mais ici la fonction `echofilter()` est disponible directement :

```
echofilter(input, output, delay=0.7, atten=4)
```

Notez que lorsque vous utilisez `from package import element`, «element» peut aussi bien être un sous-module, un sous-paquet ou simplement un nom déclaré dans le paquet (une variable, une fonction ou une classe).
L’instruction `import` cherche en premier si «element» est défini dans le paquet ; s’il ne l’est pas, elle cherche à charger un module et, si elle n’en trouve pas, une exception `ImportError` est levée.

Au contraire, en utilisant la syntaxe `import element.souselement.soussouselement`, chaque element sauf le dernier doit être un paquet. Le dernier element peut être un module ou un paquet, **mais ne peut être ni une fonction, ni une classe, ni une variable** définie dans l’élément précédent.

#### Importer un paquet

Qu’arrive-t-il lorsqu’un utilisateur écrit `from sound.effects import \*` ?

Idéalement, on pourrait espérer que Python aille chercher tous les sous-modules du paquet sur le système de fichiers et qu’ils seraient tous importés. Cela pourrait être long, et importer certains sous-modules pourrait avoir des effets secondaires indésirables ou, du moins, désirés seulement lorsque le sous-module est importé explicitement.

La seule solution, pour l’auteur du paquet, est de fournir un index explicite du contenu du paquet. L’instruction `import` utilise la convention suivante : si le fichier «**__init__.py**» du paquet définit une liste nommée `__all__`, cette liste est utilisée comme liste des noms de modules devant être importés lorsque `from package import \*` est utilisé. Il est de la responsabilité de l’auteur du paquet de maintenir cette liste à jour lorsque de nouvelles versions du paquet sont publiées.

Un auteur de paquet peut aussi décider de ne pas autoriser d’importer «**\***» pour son paquet. Par exemple, le fichier «**sound/effects/__init__.py**» peut contenir le code suivant :

```
__all__ = ["echo", "surround", "reverse"]
```

Cela signifie que `from sound.effects import \*` importe les trois sous-modules explicitement désignés du paquet `sound`.

Si `__all__` n’est pas définie, l’instruction `from sound.effects import \*` n’importe pas tous les sous-modules du paquet `sound.effects` dans l’espace de nommage courant, mais s’assure seulement que le paquet `sound.effects` a été importé (c.-à-d. que tout le code du fichier «**__init__.py**» a été exécuté), et importe ensuite les noms définis dans le paquet. Cela inclut tous les noms définis (et sous-modules chargés explicitement) par «**__init__.py**». Sont aussi inclus tous les sous-modules du paquet ayant été chargés explicitement par une instruction `import`.

Typiquement :

```
import sound.effects.echo
import sound.effects.surround
from sound.effects import *
```

Dans cet exemple, les modules `echo` et `surround` sont importés dans l’espace de nommage courant lorsque `from ... import` est exécuté, parce qu’ils sont définis dans le paquet `sound.effects` (cela fonctionne aussi lorsque `__all__` est définie).

Bien que certains modules ont été pensés pour n’exporter que les noms respectant une certaine structure lorsque `import \*` est utilisé, `import \*` reste considéré comme **une mauvaise pratique dans du code** à destination d’un environnement de production.

Rappelez-vous que rien ne vous empêche d’utiliser `from paquet import sous_module_specifique` ! C’est d’ailleurs la manière recommandée, à moins que le module qui fait les importations ait besoin de sous-modules ayant le même nom mais provenant de paquets différents.

#### Références internes dans un paquet

Lorsque les paquets sont organisés en sous-paquets (comme le paquet `sound` par exemple), vous pouvez utiliser des importations absolues pour cibler des paquets voisins. Par exemple, si le module `sound.filters.vocoder` a besoin du module `echo` du paquet `sound.effects`, il peut utiliser `from sound.effects import echo`.

Il est aussi possible d’écrire des importations relatives de la forme `from .module import name`.

Ces importations relatives sont préfixées par des points pour indiquer leur origine (paquet courant ou parent). Depuis le module `surround`, par exemple vous pouvez écrire :

```
from . import echo
from .. import formats
from ..filters import equalizer
```

Notez que les importations relatives se fient au nom du module actuel. Puisque le nom du module principal est toujours `__main__`, les modules utilisés par le module principal d’une application ne peuvent être importés que par des importations absolues.

#### Paquets dans plusieurs dossiers

Les paquets possèdent un attribut supplémentaire, `__path__`, qui est une liste initialisée avant l’exécution du fichier «**__init__.py**», contenant le nom de son dossier dans le système de fichiers. Cette liste peut être modifiée, altérant ainsi les futures recherches de modules et sous-paquets contenus dans le paquet.

Bien que cette fonctionnalité ne soit que rarement utile, elle peut servir à élargir la liste des modules trouvés dans un paquet.

# Les objets

## Objet et caractéristiques

Plus qu’un simple langage de script, Python est aussi un langage orienté objet.

Ce langage moderne et puissant est né au début des années 1990 sous l’impulsion de Guido van Rossum.

Apparue dans les années 60 quant à elle, la programmation orientée objet (POO) est un paradigme de programmation ; c’est-à-dire une façon de concevoir un programme informatique, reposant sur l’idée qu’un programme est composé d’objets interagissant les uns avec les autres.

En définitive, **un objet est une donnée**. Une donnée constituée de diverses propriétés, et pouvant être manipulée par différentes opérations.

La programmation orientée objet est le paradigme qui nous permet de définir nos propres types d’objets, avec leurs propriétés et opérations.
Ce paradigme vient avec de nombreux concepts qui seront explicités le long de ce cour.

À travers ce cour, nous allons nous intéresser à cette façon de penser et à le programmer avec le langage Python.

### Type

Ainsi, **tout objet est associé à un type**. Un type définit la sémantique d’un objet. On sait par exemple que les objets de type `int` sont des nombres entiers, que l’on peut les additionner, les soustraire, etc.

Pour la suite de ce cours, nous utiliserons un type `User` représentant un utilisateur sur un quelconque logiciel. Nous pouvons créer ce nouveau type à l’aide du code suivant :

```
class User:
    pass
```

Nous reviendrons sur ce code par la suite, retenez simplement que nous avons maintenant à notre disposition un type `User`.

Pour créer un objet de type `User`, il nous suffit de procéder ainsi :

```
john = User()
```

On dit alors que `john` est **une instance** de `User`.

### Les attributs

nous avons dit qu’un objet était constitué d’attributs. **Ces derniers représentent des valeurs propres à l’objet**.

Nos objets de type `User` pourraient par exemple contenir un identifiant (`id`), un nom (`name`) et un mot de passe (`password`).

En Python, nous pouvons facilement associer des valeurs à nos objets :

```
class User:
    pass

# Instanciation d'un objet de type User
john = User()

# Définition d'attributs pour cet objet
john.id = 1
john.name = 'john'
john.password = '12345'

print('Bonjour, je suis {}.'.format(john.name))
print('Mon id est le {}.'.format(john.id))
print('Mon mot de passe est {}.'.format(john.password))
```

Le code ci-dessus affiche :

```
Bonjour, je suis john.
Mon id est le 1.
Mon mot de passe est 12345.
```

Nous avons instancié un objet nommé `john`, de type `User`, auquel nous avons attribué trois attributs. Puis nous avons affiché les valeurs de ces attributs.

Notez que l’on peut redéfinir la valeur d’un attribut, et qu’un attribut peut aussi être supprimé à l’aide de l’opérateur `del`.

```
>>> john.password = 'mot de passe plus sécurisé !'
>>> john.password
'mot de passe plus sécurisé !'
>>> del john.password
>>> john.password
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
AttributeError: 'User' object has no attribute 'password'
```

Il est généralement déconseillé de nommer une valeur de la même manière qu’une fonction python:built-in. On évitera par exemple d’avoir une variable `id`, `type` ou `list`.

Dans le cas d’un attribut, cela n’est pas gênant car cela ne fait pas partie du même espace de noms. En effet, `john.id` n’entre pas en conflit avec `id`.

### Les méthodes

Les **méthodes sont les opérations applicables sur les objets**. Ce sont en fait **des fonctions qui recoivent notre objet en premier paramètre**.

Nos objets `User` ne contiennent pas encore de méthode, nous découvrirons comment en ajouter dans le chapitre suivant. Mais nous pouvons déjà imaginer une méthode `check_pwd` (check password) pour vérifier qu’un mot de passe entré correspond bien au mot de passe de notre utilisateur.

```
def user_check_pwd(user, password):
    return user.password == password
```

```
>>> user_check_pwd(john, 'toto')
False
>>> user_check_pwd(john, '12345')
True
```

Les méthodes recevant l’objet en paramètre, elles peuvent en lire et modifier les attributs. Souvenez-vous par exemple de la méthode `append()` des listes, qui permet d’insérer un nouvel élément, elle modifie bien la liste en question.

À travers cette partie nous avons défini et exploré la notion d’objet.

Un terme a pourtant été omis, le terme «**classe**». Il s’agit en Python d’un synonyme de «**type**». Un objet étant le fruit d’une classe, il est temps de nous intéresser à cette dernière et à sa construction.

## Classes

On définit une classe à l’aide du mot-clef `class` survolé plus tôt :

```
class User:
    pass
```

(l’instruction `pass` sert ici à indiquer à Python que le corps de notre classe est vide)

Il est conseillé en Python de nommer sa classe suivant `MonNomDeClasse`, c’est à dire qu’un nom est composé d’une suite de mots dont la première lettre est une capitale. On préférera par exemple une classe `MonNomDeClasse` que `mon_nom_de_classe`. Exception faite des types `builtins` qui sont couramment écrits en lettres minuscules.

On **instancie** une classe de la même manière qu’on appelle une fonction, **en suffixant son nom d’une paire de parenthèses**. Cela est valable pour notre classe `User`, mais aussi pour les autres classes évoquées plus haut.

```
>>> User()
<__main__.User object at 0x7fc28e538198>
>>> int()
0
>>> str()
''
>>> list()
[]
```

La classe `User` est identique, elle ne comporte aucune méthode. Pour définir une méthode dans une classe, il suffit de procéder comme pour une définition de fonction, mais dans le corps de la classe en question.

```
class User:
    def check_pwd(self, password):
        return self.password == password
```

Notre nouvelle classe `User` possède maintenant une méthode `check_pwd` applicable sur tous ses objets.

```
>>> john = User()
>>> john.id = 1
>>> john.name = 'john'
>>> john.password = '12345'
>>> john.check_pwd('toto')
False
>>> john.check_pwd('12345')
True
```

Quel est ce `self` reçu en premier paramètre par `check_pwd` ? Il s’agit simplement de l’objet sur lequel on applique la méthode, comme expliqué dans le chapitre précédent. Les autres paramètres de la méthode arrivent après.

La méthode étant définie au niveau de la classe, elle n’a que ce moyen pour savoir quel objet est utilisé. C’est un comportement particulier de Python, mais retenez simplement qu’appeler `john.check_pwd('12345')` équivaut à l’appel `User.check_pwd(john, '12345')`. C’est pourquoi `john` correspondra ici au paramère `self` de notre méthode.

`self` n’est pas un mot-clef du langage Python, le paramètre pourrait donc prendre n’importe quel autre nom. Mais il conservera toujours ce nom par convention.

Notez aussi, dans le corps de la méthode `check_pwd`, que `password` et `self.password` sont bien deux valeurs distinctes : la première est le paramètre reçu par la méthode, tandis que la seconde est l’attribut de notre objet.

### Portées et espaces de nommage en Python

Les définitions de classes font d’habiles manipulations avec les espaces de nommage, vous devez donc savoir comment les portées et les espaces de nommage fonctionnent.

Commençons par quelques définitions.

**Un espace de nommage est une table de correspondance entre des noms et des objets**. La plupart des espaces de nommage sont actuellement implémentés sous forme de dictionnaires Python, mais ceci n’est normalement pas visible (sauf pour les performances) et peut changer dans le futur. Comme exemples d’espaces de nommage, nous pouvons citer les primitives (fonctions comme `abs()` et les noms des exceptions de base) ; les noms globaux dans un module ; et les noms locaux lors d’un appel de fonction. D’une certaine manière, l’ensemble des attributs d’un objet forme lui-même un espace de nommage. L’important à retenir concernant les espaces de nommage est qu’il n’y a absolument aucun lien entre les noms de différents espaces de nommage ; par exemple, deux modules différents peuvent définir une fonction `maximize` sans qu’il n’y ait de confusion. Les utilisateurs des modules doivent préfixer le nom de la fonction avec celui du module.

À ce propos, nous utilisons le mot «**attribut**» pour tout nom suivant un point. Par exemple, dans l’expression `z.real`, `real` est un attribut de l’objet `z`. Rigoureusement parlant, les références à des noms dans des modules sont des références d’attributs : dans l’expression `nommodule.nomfonction`, `nommodule` est un objet module et `nomfonction` est un attribut de cet objet. Dans ces conditions, il existe une correspondance directe entre les attributs du module et les noms globaux définis dans le module : ils partagent le même espace de nommage!

Les attributs peuvent être en lecture seule ou modifiables. S’ils sont modifiables, l’affectation à un attribut est possible. Les attributs de modules sont modifiables : vous pouvez écrire `nommodule.la_reponse = 42`.
Les attributs modifiables peuvent aussi être effacés avec l’instruction `del`. Par exemple, `del nommodule.la_reponse` supprime l’attribut
`la_reponse` de l’objet nommé `nommodule`.

Les espaces de nommage sont créés à différents moments et ont différentes durées de vie. L’espace de nommage contenant les primitives est créé au démarrage de l’interpréteur Python et n’est jamais effacé. L’espace de nommage globaux pour un module est créé lorsque la définition du module est lue. Habituellement, les espaces de nommage des modules durent aussi jusqu’à l’arrêt de l’interpréteur. Les instructions exécutées par la première invocation de l’interpréteur, qu’elles soient lues depuis un fichier de script ou de manière interactive, sont considérées comme faisant partie d’un module appelé `__main__`, de façon qu’elles possèdent leur propre espace de nommage (les primitives vivent elles-mêmes dans un module, appelé `builtins`).

L’espace des noms locaux d’une fonction est créé lors de son appel, puis effacé lorsqu’elle renvoie un résultat ou lève une exception non prise en charge (en fait, «oublié» serait une meilleure façon de décrire ce qui se passe réellement). Bien sûr, des invocations récursives ont chacune leur propre espace de nommage.

**La portée** est la zone textuelle d’un programme Python où un espace de nommage est directement accessible. «Directement accessible» signifie ici qu’une référence non qualifiée à un nom est cherchée dans l’espace de nommage. Bien que les portées soient déterminées de manière statique, elles sont utilisées de manière dynamique. À n’importe quel moment de l’exécution, il y a au minimum trois ou quatre portées imbriquées dont les espaces de nommage sont directement accessibles :


* la portée la plus au centre, celle qui est consultée en premier, contient les noms locaux ;


* les portées des fonctions englobantes, qui sont consultées en commençant avec la portée englobante la plus proche, contiennent des noms non-locaux mais aussi non-globaux ;


* l’avant-dernière portée contient les noms globaux du module courant ;


* la portée englobante, consultée en dernier, est l’espace de nommage contenant les primitives.

Si un nom est déclaré comme `global`, alors toutes les références et affectations vont directement dans la portée intermédiaire contenant les noms globaux du module. Pour pointer une variable qui se trouve en dehors de la portée la plus locale, vous pouvez utiliser l’instruction `nonlocal`. Si une telle variable n’est pas déclarée `nonlocal`, elle est en lecture seule (toute tentative de la modifier crée simplement une nouvelle variable dans la portée la plus locale, en laissant inchangée la variable du même nom dans sa portée d’origine).

Habituellement, la portée locale référence les noms locaux de la fonction courante. En dehors des fonctions, la portée locale référence le même espace de nommage que la portée globale : l’espace de nommage du module. Les définitions de classes créent un nouvel espace de nommage dans la portée locale.

Il est important de réaliser que les portées sont déterminées de manière textuelle : la portée globale d’une fonction définie dans un module est l’espace de nommage de ce module, quelle que soit la provenance de l’appel à la fonction. En revanche, la recherche réelle des noms est faite dynamiquement au moment de l’exécution. Cependant la définition du langage est en train d’évoluer vers une résolution statique des noms au moment de la «compilation», donc ne vous basez pas sur une résolution dynamique (en réalité, les variables locales sont déjà déterminées de manière statique)!

Une particularité de Python est que, si aucune instruction `global` ou `nonlocal` n’est active, les affectations de noms vont toujours dans la
portée la plus proche. Les affectations ne copient aucune donnée : elles se contentent de lier des noms à des objets. Ceci est également vrai pour l’effacement : l’instruction del x supprime la liaison de x dans l’espace de nommage référencé par la portée locale. En réalité, toutes les opérations qui impliquent des nouveaux noms utilisent la portée locale : en particulier, les instructions import et les définitions de fonctions effectuent une liaison du module ou du nom de fonction dans la portée locale.

L’instruction `global` peut être utilisée pour indiquer que certaines variables existent dans la portée globale et doivent être reliées en local ; l’instruction `nonlocal` indique que certaines variables existent dans une portée supérieure et doivent être reliées en local.

#### Exemple de portées et d’espaces de nommage

Ceci est un exemple montrant comment utiliser les différentes portées et espaces de nommage, et comment `global` et `nonlocal` modifient l’affectation de variable :

```
def scope_test():
    def do_local():
        spam = "local spam"

    def do_nonlocal():
        nonlocal spam
        spam = "nonlocal spam"

    def do_global():
        global spam
        spam = "global spam"

    spam = "test spam"
    do_local()
    print("Après affectation locale:", spam)
    do_nonlocal()
    print("Après affectation non locale:", spam)
    do_global()
    print("Après affectation générale:", spam)

scope_test()
print("A portée générale:", spam)
```

Ce code donne le résultat suivant :

```
Après affectation locale: test spam
Après affectation non locale: nonlocal spam
Après affectation générale: nonlocal spam
A portée générale: global spam
```

Vous pouvez constater que l’affectation locale (qui est effectuée par défaut) n’a pas modifié la liaison de `spam` dans `scope_test`. L’affectation `nonlocal` a changé la liaison de `spam` dans `scope_test` et l’affectation `global` a changé la liaison au niveau du module.

Vous pouvez également voir qu’aucune liaison pour spam n’a été faite avant l’affectation `global`.

### Passages d’arguments

Nous avons vu qu’instancier une classe était semblable à un appel de fonction. Dans ce cas, comment passer des arguments à une classe, comme on le ferait pour une fonction ?

Il faut pour cela comprendre les bases du mécanisme d’instanciation de Python. Quand on appelle une classe, un nouvel objet de ce type est construit en mémoire, puis initialisé. Cette initialisation permet d’assigner des valeurs à ses attributs.

L’objet est initialisé à l’aide d’une méthode spéciale de sa classe, la méthode `__init__`. Cette dernière recevra les arguments passés lors de l’instanciation.

```
class User:
    def __init__(self, id, name, password):
        self.id = id
        self.name = name
        self.password = password

    def check_pwd(self, password):
        return self.password == password
```

Nous retrouvons dans cette méthode le paramètre `self`, qui est donc utilisé pour modifier les attributs de l’objet.

```
>>> john = User(1, 'john', '12345')
>>> john.check_pwd('toto')
False
>>> john.check_pwd('12345')
True
```

### Méthodes spéciales __repr__ et __str__

Nous avons vu précédemment la méthode `__init__`, permettant d’initialiser les attributs d’un objet. On appelle cette méthode une méthode spéciale, il y en a encore beaucoup d’autres en Python. Elles sont reconnaissables par leur nom débutant et finissant par deux underscores.

Vous vous êtes peut-être déjà demandé d’où provenait le résultat affiché sur la console quand on entre simplement le nom d’un objet.

```
>>> import datetime
>>> aujourdhui = datetime.datetime.now()
>>> str(aujourdhui)
'2020-10-06 12:19:45.099479'
>>> repr(aujourdhui)
'datetime.datetime(2020, 10, 6, 12, 19, 45, 99479)'
>>> resultat = eval(repr(aujourdhui))
>>> print(resultat)
2020-10-06 12:19:45.099479
```

```
>>> john = User(1, 'john', '12345')
>>> john
<__main__.User object at 0x7fefd77fae10>
```

Il s’agit en fait de la représentation d’un objet, calculée à partir de sa méthode spéciale `__repr__`.

```
>>> john.__repr__()
'<__main__.User object at 0x7fefd77fae10>'
```

À noter qu’une méthode spéciale n’est presque jamais directement appelée en Python, on lui préférera dans le cas présent la fonction builtin `repr`.

```
>>> repr(john)
'<__main__.User object at 0x7fefd77fae10>'
```

Il nous suffit alors de redéfinir cette méthode `__repr__` pour bénéficier de notre propre représentation.

```
>>> class User:
...     def __repr__(self):
...         return '<User: {}, {}>'.format(self.id, self.name)
>>> User(1, 'john', '12345')
<User: 1, john>
```

Une autre opération courante est la conversion de notre objet en chaîne de caractères afin d’être affiché via print par exemple. Par défaut, la conversion en chaîne correspond à la représentation de l’objet, mais elle peut être surchargée par la méthode `__str__`.

```
>>> class User:
...
... def __repr__(self):
...     return '<User: {}, {}>'.format(self.id, self.name)
...
... def __str__(self):
...     return '{}-{}'.format(self.id, self.name)
>>> john = User(1, 'john', 12345)
>>> john
<User: 1, john>
>>> repr(john)
'<User: 1, john>'
>>> str(john)
'1-john'
>>> print(john)
1-john
```

Exemple :

```
>>> class Fraction:
...     def __init__(self, num, den):
...         self.__num = num
...         self.__den = den
...
...     def resultat(self):
...         return 1/2
...
...     def __str__(self):
...         return str(self.resultat())
...
...     def __repr__(self):
...         return str(self.__num) + '/' + str(self.__den)
>>> f = Fraction(1,2)
>>> f.resultat()
0.5
>>> print(f)
>>> print('Valeur numérique de ma fraction : ' + str(f))
Valeur numérique de ma fraction : 0.5
>>> print('Représentation de ma fraction : ', repr(f))
Représentation de ma fraction :  1/2
```

Exercice :

```
>>> from math import *
>>> class Racine:
...     def __init__(self, valeur):
...         self.__valeur = valeur
...
...     def resultat(self):
...         return sqrt(self.__valeur)
...
...     def __str__(self):
...         return str(self.resultat())
...
...     def __repr__(self):
...         return '√' + str(self.__valeur)
...
>>> nombre = Racine(2)
>>> nombre.resultat()
1.4142135623730951
>>> print(nombre)
1.4142135623730951
>>> repr(nombre)
'√2'
```

### Variables privées, l’encapsulation

Au commencement étaient les invariants

Les différents attributs de notre objet forment un état de cet objet, normalement stable. Ils sont en effet liés les uns aux autres, la modification d’un attribut pouvant avoir des conséquences sur un autre. Les invariants correspondent aux relations qui lient ces différents attributs.

Imaginons que nos objets `User` soient dotés d’un attribut contenant une évaluation du mot de passe (savoir si ce mot de passe est assez sécurisé ou non), il doit alors être mis à jour chaque fois que nous modifions l’attribut `password` d’un objet `User`.

Dans le cas contraire, le mot de passe et l’évaluation ne seraient plus corrélés, et notre objet `User` ne serait alors plus dans un état stable. Il est donc important de veiller à ces invariants pour assurer la stabilité de nos objets.

Protège-moi

Au sein d’un objet, les attributs peuvent avoir des sémantiques différentes. Certains attributs vont représenter des propriétés de l’objet et faire partie de son interface (tels que le prénom et le nom de nos objets `User`). Ils pourront alors être lus et modifiés depuis l’extérieur de l’objet, on parle dans ce cas d’attributs publics.

D’autres vont contenir des données internes à l’objet, n’ayant pas vocation à être accessibles depuis l’extérieur. Nous allons sécuriser notre stockage du mot de passe en ajoutant une méthode pour le hasher (à l’aide du module `crypt`), afin de ne pas stocker d’informations sensibles dans l’objet. Ce condensat du mot de passe ne devrait pas être accessible de l’extérieur, et encore moins modifié (ce qui en altérerait la sécurité).

De la même manière que pour les attributs, certaines méthodes vont avoir une portée publique et d’autres privée (on peut imaginer une méthode interne de la classe pour générer notre identifiant unique). On nomme **encapsulation** cette notion de protection des attributs et méthodes d’un objet, dans le respect de ses invariants.

Certains langages implémentent dans leur syntaxe des outils pour gérer la visibilité des attributs et méthodes, mais il n’y a rien de tel en Python. Il existe à la place des conventions, qui indiquent aux développeurs quels attributs/méthodes sont publics ou privés. Quand vous voyez un nom d’attribut ou méthode débuter par un «**_**» au sein d’un objet, il indique quelque chose d’interne à l’objet (privé), dont la modification peut avoir des conséquences graves sur la stabilité.

```
>>> import crypt
>>> class User:
...     def __init__(self, id, name, password):
...         self.id = id
...         self.name = name
...         self._salt = crypt.mksalt() # sel utilisé pour le hash du mot de passe
...         self._password = self._crypt_pwd(password)
...
...     def _crypt_pwd(self, password):
...         return crypt.crypt(password, self._salt)
...
...     def check_pwd(self, password):
...         return self._password == self._crypt_pwd(password)
...
>>> john = User(1, 'john', '12345')
>>> john.check_pwd('12345')
True
```

On note toutefois qu’il ne s’agit que d’une convention, l’attribut `_password` étant parfaitement visible depuis l’extérieur.

```
>>> john._password
'$6$DwdvE5H8sT71Huf/$9a.H/VIK4fdwIFdLJYL34yml/QC3KZ7'
```

Il reste possible de masquer un peu plus l’attribut à l’aide du préfixe `__`. Ce préfixe a pour effet de renommer l’attribut en y insérant le nom de la classe courante.

```
>>> class User:
...     def __init__(self, id, name, password):
...         self.id = id
...         self.name = name
...         self.__salt = crypt.mksalt()
...         self.__password = self.__crypt_pwd(password)
...
...     def __crypt_pwd(self, password):
...         return crypt.crypt(password, self.__salt)
...
...     def check_pwd(self, password):
...         return self.__password == self.__crypt_pwd(password)
>>> john = User(1, 'john', '12345')
>>> john.__password
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
AttributeError: 'User' object has no attribute '__password'
>>> john._User__password
'$6$kjwoqPPHRQAamRHT$591frrNfNNb3.RdLXYiB/bgdCC4Z0p.B'
```

Ce comportement pourra surtout être utile pour éviter des conflits de noms entre attributs internes de plusieurs classes sur un même objet, que nous verrons lors de l’héritage.

Le hashage d’un mot de passe correspond à une opération non-réversible qui permet de calculer un condensat (hash) du mot de passe. Ce condensat peut-être utilisé pour vérifier la validité d’un mot de passe, mais ne permet pas de retrouver le mot de passe d’origine.

C++, Java, Ruby, etc.

### Duck-typing

Un objet en Python est défini par sa structure (les attributs qu’il contient et les méthodes qui lui sont applicables) plutôt que par son type.

Ainsi, pour faire simple, un fichier sera un objet possédant des méthodes `read`, `write` et `close`. Tout objet respectant cette définition sera considéré par Python comme un fichier.

```
class FakeFile:
    def read(self, size=0):
        return ''

    def write(self, s):
        return 0

    def close(self):
        pass

f = FakeFile()
print('foo', file=f)
```

Python est entièrement construit autour de cette idée, appelée **duck-typing** : «**Si je vois un animal qui vole comme un canard, cancane comme un canard, et nage comme un canard, alors j’appelle cet oiseau un canard**» (James Whitcomb Riley)

Exercice :

Pour ce premier exercice, nous allons nous intéresser aux classes d’un forum. Forts de notre type `User` pour représenter un utilisateur, nous souhaitons ajouter une classe `Post`, correspondant à un quelconque message.

Cette classe sera inititalisée avec un auteur (un objet `User`) et un contenu textuel (le corps du message). Une date sera de plus générée lors de la création.

Un Post possèdera une méthode format pour retourner le message formaté, correspondant au HTML suivant :

```
<div>
    <span>Par NOM_DE_L_AUTEUR le DATE_AU_FORMAT_JJ_MM_YYYY à HEURE_AU_FORMAT_HH_MM_SS</span>
    <p>
        CORPS_DU_MESSAGE
    </p>
</div>
```

De plus, nous ajouterons une méthode post à notre classe User, recevant un corps de message en paramètre et retournant un nouvel objet Post.

```
import crypt
import datetime

class User:
    def __init__(self, id, name, password):
        self.id = id
        self.name = name
        self._salt = crypt.mksalt()
        self._password = self._crypt_pwd(password)

    def _crypt_pwd(self, password):
        return crypt.crypt(password, self._salt)

    def check_pwd(self, password):
        return self._password == self._crypt_pwd(password)

    def post(self, message):
        return Post(self, message)

class Post:
    def __init__(self, author, message):
        self.author = author
        self.message = message
        self.date = datetime.datetime.now()

    def format(self):
        date = self.date.strftime('le %d/%m/%Y à %H:%M:%S')
        return '<div><span>Par {} {}</span><p>{}</p></div>'.format(self.author.name, date, self.message)

if __name__ == '__main__':
    user = User(1, 'john', '12345')
    p = user.post('Salut à tous')
    print(p.format())
```

Nous savons maintenant définir une classe et ses méthodes, initialiser nos objets, et protéger les noms d’attributs/méthodes.

Mais jusqu’ici, quand nous voulons étendre le comportement d’une classe, nous la redéfinissons entièrement en ajoutant de nouveaux attributs/méthodes. Le chapitre suivant présente l’héritage, un concept qui permet d’étendre une ou plusieurs classes sans toucher au code initial.

## Documenter les classes d’objets

Maintenant qu’on sait documenter une fonction, documentons une classe d’objets.

Exemple de documentation d’une classe `MaClasse` dans un module `mon_module` d’un paquet `mon_paquet`  :

```
# -*- coding: utf-8 -*-

"""
.. sectionauthor:: Stagiaire ADMINISTRATEUR <stagiaire.administrateur@fai.fr>
:mod:`mon_module` -- Module d'exemple de documentation d'une classe
###################################################################

.. module:: mon_paquet.mon_module
   :platform: Linux
   :synopsis: Ce module illustre comment écrire votre docstring pour une classe dans Python.
.. moduleauthor:: Formateur PYTHON <formateur.python@fai.fr>
.. moduleauthor:: Stagiaire ADMINISTRATEUR <stagiaire.administrateur@fai.fr>

"""

__title__ = "Module illustration écriture docstring d'une classe Python"
__author__ = "Formateur PYTHON"
__version__ = '0.7.3'
__release_life_cycle__ = 'alpha'
# pre-alpha = faisabilité, alpha = développement, beta = test, rc = qualification, prod = production
__docformat__ = 'reStructuredText'

class MaClasse():
    """
    Exemple de classe de mon_paquet.mon_module

    :param arg: argument du constructeur MaClasse
    :type arg: int
    """

    def __init__(self, arg):
        """
        Constructeur de MaClasse

        Les propriétés de la classe sont :
        :param param: p1
        :type param: int
        """
        self.p1 = None

    def bonjour(self, nom):
        """
        Permet d'afficher le message « Bonjour à toi <nom> »

        :param nom: Nom de la personne
        :type nom: str
        :return: Message de bonjour
        :rtype: str:
        """
        print("Bonjour " + nom)
        return "Bonjour à toi " + nom

if __name__ == "__main__":
    mon_objet = MaClasse()
    print mon_objet.bonjour("padawan")
```

Finalement, il n’y a rien de bien nouveau. On a documenté les méthodes de la classe `MaClasse` comme on l’a fait avec les fonctions.

**La seule différence c’est avec la méthode constructeur** `__init__` de la classe `MaClasse` où la docstring des paramètres de classe est directement dans la déclaration de la classe (`:param arg: argument du constructeur MaClasse` et `:type arg: int`).

C’est **le fonctionnement par défaut avec autodoc**.
Cette disposition permet de séparer la déclaration des propriétés de la classe, qui sont définies dans la méthode `__init__`, avec les paramètres de création d’objets de la classe `MaClasse` qui sont définis dans la déclaration des paramètres de la méthode `__init__`.

Ce comportement est réglable via une option `autoclass_content = 'configuration'` dans le fichier «**conf.py**».
Si vous préférez documenter avec le constructeur `__init__` les paramètres de création d’objet avec les propriétés de la classe, ce paramètre vous permet de définir comment seront insérés les paramètres de la classe avec «**autoclass**». Exemple pour que cela soit avec la déclaration de propriétés dans `__init__` :

```
autoclass_content = 'init'
```

Les valeurs possibles sont :


* **« class »** : Seule la docstring de la classe est insérée. C’est la valeur par défaut. Vous pouvez toujours documenter `__init__` en tant que méthode distincte en utilisant «**automethod**» ou l’option «**members**» pour générer automatique la documentation de vos classes.


* **« both »** : La docstring de la classe et de la méthode `__init__` sont concaténées et insérées.


* **« init »** : Seule la docstring de la méthode `__init__` est insérée.

**WARNING**: Si la classe n’a pas de méthode `__init__`, ou si la docstring de la méthode `__init__` est vide, et que la classe a une docstring avec la méthode `__new__` celle-ci sera utilisée à la place.

Nous avons maintenant abordé l’essentiel pour commencer une rédaction complète de sa documentation du code Python. Vous pouvez encore approfondir avec plein de directives Sphinx utiles [avec ce lien](https://devguide.python.org/documenting/).

## Notions avancées en objet

### Extension de classes

Nous allons nous intéresser à l’extension de classes.

Imaginons que nous voulions définir une classe `Admin`, pour gérer des administrateurs, qui réutiliserait le même code que la classe `User`. Tout ce que nous savons faire actuellement c’est copier/coller le code de la classe `User` en changeant son nom pour `Admin`.

Nous allons maintenant voir comment faire ça de manière plus élégante, grâce à l’héritage. Nous étudierons de plus les relations entre classes ansi créées.

Nous utiliserons donc la classe `User` suivante pour la suite de ce chapitre.

```
class User:
    """ Défini des propriétés d'un utilisateur """
    def __init__(self, id, name, password):
        """ Initialisation de l'utilisateur """
        self.id = id
        self.name = name
        self._salt = crypt.mksalt()
        self._password = self._crypt_pwd(password)

    def _crypt_pwd(self, password):
        """ Calcule un mot de passe crypté """
        return crypt.crypt(password, self._salt)

    def check_pwd(self, password):
        """ Vérifie le mot de passe """
        return self._password == self._crypt_pwd(password)
```

#### Hériter

L’héritage simple est le mécanisme permettant d’étendre une unique classe. Il consiste à créer une nouvelle classe (fille) qui bénéficiera des mêmes méthodes et attributs que sa classe mère. Il sera aisé d’en définir de nouveaux dans la classe fille, et cela n’altèrera pas le fonctionnement de la mère.

Par exemple, nous voudrions étendre notre classe `User` pour ajouter la possibilité d’avoir des administrateurs. Les administrateurs (`Admin`) possèderaient une nouvelle méthode, `manage`, pour administrer le système.

```
class Admin(User):
    """ Défini des propriétés d'un administrateur """
    def manage(self):
        """ Ajoute des fonctions d'administrateur """
        print('Je suis un Jedi!')
```

En plus des méthodes de la classe `User` (`__init__`, `_crypt_pwd` et `check_pwd`), Admin possède aussi une méthode `manage`.

```
>>> root = Admin(1, 'root', 'toor')
>>> root.check_password('toor')
True
>>> root.manage()
Je suis un Jedi!
>>> john = User(2, 'john', '12345')
>>> john.manage()
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
AttributeError: 'User' object has no attribute 'manage'
```

Nous pouvons avoir deux classes différentes héritant d’une même mère

```
class Guest(User):
    pass
```

`Admin` et `Guest` sont alors deux classes filles de `User`.

L’héritage simple permet aussi d’hériter d’une classe qui hérite elle-même d’une autre classe.

```
class SuperAdmin(Admin):
    pass
```

`SuperAdmin` est alors la fille de `Admin`, elle-même la fille de `User`. On dit alors que `User` **est une ancêtre** de `SuperAdmin`.

On peut constater quels sont les parents d’une classe à l’aide de l’attribut spécial `__bases__` des classes :

```
>>> Admin.__bases__
(<class '__main__.User'>,)
>>> Guest.__bases__
(<class '__main__.User'>,)
>>> SuperAdmin.__bases__
(<class '__main__.Admin'>,)
```

Que vaudrait alors `User.__bases__`, sachant que la classe `User` est définie sans héritage ?

```
>>> User.__bases__
(<class 'object'>,)
```

On remarque que, sans que nous n’ayons rien demandé, `User` hérite de `object`. En fait, `object` est l’ancêtre de toute classe Python. Ainsi, quand aucune classe parente n’est définie, c’est `object` qui est choisi.

##### Sous-typage

Nous avons vu que l’héritage permettait d’étendre le comportement d’une classe, mais ce n’est pas tout. L’héritage a aussi du sens au niveau des types, en créant un nouveau type compatible avec le parent.

En Python, la fonction `isinstance` permet de tester si un objet est l’instance d’une certaine classe.

```
>>> isinstance(root, Admin)
True
>>> isinstance(root, User)
True
>>> isinstance(root, Guest)
False
>>> isinstance(root, object)
True
```

Mais gardez toujours à l’esprit qu’en Python, on préfère se référer à la structure d’un objet qu’à son type (duck-typing), les tests à base de `isinstance` sont donc à utiliser pour des cas particuliers uniquement, où il serait difficile de procéder autrement.

#### Redéfinition de méthodes, la surcharge

Nous savons hériter d’une classe pour y insérer de nouvelles méthodes, mais nous ne savons pas étendre les méthodes déjà présentes dans la classe mère. La redéfinition est un concept qui permet de remplacer une méthode du parent.

Nous voudrions que la classe `Guest` ne possède plus aucun mot de passe. Celle-ci devra modifier la méthode `check_pwd` pour accepter tout mot de passe, et simplifier la méthode `__init__`.

On ne peut pas à proprement parler étendre le contenu d’une méthode, mais on peut la redéfinir :

```
class Guest(User):
def __init__(self, id, name):
    self.id = id
    self.name = name
    self._salt = ''
    self._password = ''

def check_pwd(self, password):
    return True
```

Cela fonctionne comme souhaité, mais vient avec un petit problème, le code de la méthode `__init__` est répété. En l’occurrence il ne s’agit que de 2 lignes de code, mais lorsque nous voudrons apporter des modifications à la méthode de la classe `User`, il faudra les répercuter sur `Guest`, ce qui donne vite quelque chose de difficile à maintenir.

Heureusement, Python nous offre un moyen de remédier à ce mécanisme, `super`! Oui, `super`, littéralement, une fonction un peu spéciale en Python, qui nous permet d’utiliser la classe parente (superclass).

`super` est une fonction qui prend initialement en paramètre une classe et une instance de cette classe. Elle retourne un objet proxy (Un proxy est un intermédiaire transparent entre deux entités) qui s’utilise comme une instance de la classe parente.

```
>>> guest = Guest(3, 'Guest')
>>> guest.check_pwd('password')
True
>>> super(Guest, guest).check_pwd('password')
False
```

Au sein de la classe en question, les arguments de `super` peuvent être omis (ils correspondront à la classe et à l’instance courantes), ce qui nous permet de simplifier notre méthode `__init__` et d’éviter les répétitions.

```
class Guest(User):
def __init__(self, id, name):
    super().__init__(id, name, '')

def check_pwd(self, password):
    return True
```

On notera tout de même que contrairement aux versions précédentes, l’initialisateur de `User` est appelé en plus de celui de `Guest`, et donc qu’un `self` et un hash du mot de passe sont générés alors qu’ils ne serviront pas.

Ça n’est pas très grave dans le cas présent, mais pensez-y dans vos développements futurs, afin de ne pas exécuter d’opérations coûteuses inutilement.

#### Héritage Conditionnel

Il nous arrive souvent en Python d’être bloqué, lors de la création d’une classe, parce que l’héritage est conditionné à une variable passée en paramètre de la classe.

Se présente alors deux cas :


* Le premier est un comportement de classe complètement différent avec ses méthodes et propriétés, c’est un proxy (filtre) de classes qu’il nous faudra utiliser.


* Le deuxième est un ajout de propriétés et de méthodes à la classe avec un vrai héritage conditionnel.

Nous allons voir comment résoudre cela avec la déclaration de classe `def __new__()`

##### Proxy de classes

Dans cet exercice, nous allons créer deux classes distinctes (`A` et `B`) qui suivant un paramètre passé à une métaclasse `Proxy` va retourner la bonne classe.

```
class A:
    def __init__(self, mon_paramètreA):
        self.ma_propriétéA = mon_paramètreA

    def maMéthodeA(self):
        pass

class B:
    def __init__(self, mon_paramètreB):
        self.ma_propriétéB = mon_paramètreB

    def maMéthodeB(self):
        pass

class Proxy:
    def __new__(cls, mon_paramètre):
        if mon_paramètre == 'valeur1':
            return A(mon_paramètre)
        if mon_paramètre == 'valeur2':
            return B(mon_paramètre)
```

Nous voyons ici que la déclaration `def __new__()` nous permet de récupérer les paramètres passés à la classe `Proxy`, ce qui nous permet avec un test de renvoyer un objet créé avec la bonne classe. C’est pour cela que l’on parle de Proxy de classe. Le paramètre `cls` représente la classe qui a besoin d’être instanciée.

On peut bien sur changer les conditions de filtrage suivant le type de test que l’on veut, ou augmenter le nombre de classes en option. Mais ici dans cette section nous parlons d’héritage de classe, nous allons voir avec ce procédé comment créer un héritage conditionnel.

##### Héritage conditionnel

Nous introduisons ici un nouveau concept pour les héritages, l’héritage conditionnel de classes `ClasseAHeriter if mon_paramètre == 'Valeur' else object`. La difficulté de la solution vient du fait que la variable passée à la classe ne peut être récupérée avant l’héritage de classe, `mon_paramètre` doit donc être `global` pour que la condition d’héritage fonctionne. Avec le proxy nous outrepassons cette limite de façon élégante…

Ici dans cet exercice, nous allons créer un héritage conditionnel de la classe `A` dans la classe `B` suivant un paramètre passé au travers de la classe proxy `ProxyHeritage`.

```
class A:
    def __init__(self, mon_paramètreA):
        self.ma_propriétéA = mon_paramètreA

    def maMéthodeA(self):
        pass

class ProxyHeritage:
    def __new__(cls, mon_paramètre):
        class B(A if mon_paramètre == 'Valeur' else object):
            def __init__(self, mon_paramètreB):
                self.ma_propriétéB = mon_paramètreB

            def maMéthodeB(self):
                pass
        return B(mon_paramètre)
```

Cette solution n’est pas satisfaisante. En effet si on veut hériter la classe `ProxyHeritage` le comportement de gestion des héritages de classes normal de Python n’est plus possible. Nous verrons plus loin dans ce cour comment faire.

Tout ceci nous permet maintenant d’introduire la section suivante.

#### Héritages Multiples

Avec l’héritage simple, nous pouvions étendre le comportement d’une classe. L’héritage multiple va nous permettre de le faire pour plusieurs classes à la fois. Il nous suffit de préciser plusieurs classes séparées par des virgules lors de la création de notre classe fille.

```
class A:
    def foo(self):
        return '!'

class B:
    def bar(self):
        return '?'

class C(A, B):
    pass
```

Notre classe `C` a donc deux mères : `A` et `B`. Cela veut aussi dire que les objets de type `C` possèdent à la fois les méthodes `foo` et `bar`.

```
>>> c = C()
>>> c.foo()
'!'
>>> c.bar()
'?'
```

##### Ordre d’héritage

L’ordre dans lequel on hérite des parents est important, il détermine dans quel ordre les méthodes seront recherchées dans les classes mères.
Ainsi, dans le cas où la méthode existe dans plusieurs parents, celle de la première classe sera conservée.

```
class A:
    def foo(self):
        return '!'

class B:
    def foo(self):
        return '?'

class C(A, B):
    pass

class D(B, A):
    pass
```

```
>>> C().foo()
'!'
>>> D().foo()
'?'
```

Cet ordre dans lequel les classes parentes sont explorées pour la recherche des méthodes est appelé **Method Resolution Order (MRO)**. On peut le connaître à l’aide de la méthode `mro` des classes.

```
>>> A.mro()
[<class '__main__.A'>, <class 'object'>]
>>> B.mro()
[<class '__main__.B'>, <class 'object'>]
>>> C.mro()
[<class '__main__.C'>, <class '__main__.A'>, <class '__main__.B'>, <class 'object'>]
>>> D.mro()
[<class '__main__.D'>, <class '__main__.B'>, <class '__main__.A'>, <class 'object'>]
```

C’est aussi ce MRO qui est utilisé par super pour trouver à quelle classe faire appel. super se charge d’explorer le MRO de la classe de l’instance qui lui est donnée en second paramètre, et de retourner un proxy sur la classe juste à droite de celle donnée en premier paramètre.

Ainsi, avec `c` une instance de `C`, :python\`super(C, c)\` retournera un objet se comportant comme une instance de `A`, `super(A, c)` comme une instance de `B`, et `super(B, c)` comme une instance de `object`.

```
>>> c = C()
>>> c.foo() # C.foo == A.foo
'!'
>>> super(C, c).foo() # A.foo
'!'
>>> super(A, c).foo() # B.foo
'?'
>>> super(B, c).foo() # object.foo -> méthode introuvable
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
AttributeError: 'super' object has no attribute 'foo'
```

Les classes parentes n’ont alors pas besoin de se connaître les unes les autres pour se référencer.

```
class A:
    def __init__(self):
        print("Début initialisation d'un objet de type A")
        super().__init__()
        print("Fin initialisation d'un objet de type A")

class B:
    def __init__(self):
        print("Début initialisation d'un objet de type B")
        super().__init__()
        print("Fin initialisation d'un objet de type B")

class C(A, B):
    def __init__(self):
        print("Début initialisation d'un objet de type C")
        super().__init__()
        print("Fin initialisation d'un objet de type C")

class D(B, A):
    def __init__(self):
        print("Début initialisation d'un objet de type D")
        super().__init__()
        print("Fin initialisation d'un objet de type D")
```

```
>>> C()
Début initialisation d'un objet de type C
Début initialisation d'un objet de type A
Début initialisation d'un objet de type B
Fin initialisation d'un objet de type B
Fin initialisation d'un objet de type A
Fin initialisation d'un objet de type C
<__main__.C object at 0x7f0ccaa970b8>
>>> D()
Début initialisation d'un objet de type D
Début initialisation d'un objet de type B
Début initialisation d'un objet de type A
Fin initialisation d'un objet de type A
Fin initialisation d'un objet de type B
Fin initialisation d'un objet de type D
<__main__.D object at 0x7f0ccaa971d0>
```

La méthode `__init__` des classes parentes n’est pas appelée automatiquement, et l’appel doit donc être réalisé explicitement.

C’est ainsi le `super().__init__()` présent dans la classe `C` qui appelle l’initialiseur de la classe `A`, qui appelle lui-même celui de la classe `B`. Inversement, pour la classe `D`, `super().__init__()` appelle l’initialiseur de `B` qui appelle celui de `A`.

On notera que les exemple donnés n’utilisent jamais plus de deux classes mères, mais il est possible d’en avoir autant que vous le souhaitez.

```
class A:
    pass

class B:
    pass

class C:
    pass

class D:
    pass

class E(A, B, C, D):
    pass
```

##### Mixins

Les **mixins** sont des classes dédiées à une fonctionnalité particulière, utilisable en héritant d’une classe de base et de ce mixin.

Par exemple, plusieurs types que l’on connaît sont appelés séquences (`str`, `list`, `tuple`). Ils ont en commun le fait d’implémenter l’opérateur `[]` et de gérer le slicing. On peut ainsi obtenir l’objet en ordre inverse à l’aide de `obj[::-1]`.

Un mixin qui pourrait nous être utile serait une classe avec une méthode `reverse` pour nous retourner l’objet inversé.

```
class Reversable:
    def reverse(self):
        return self[::-1]

class ReversableStr(Reversable, str):
    pass

class ReversableTuple(Reversable, tuple):
    pass
```

```
>>> s = ReversableStr('abc')
>>> s
'abc'
>>> s.reverse()
'cba'
>>> ReversableTuple((1, 2, 3)).reverse()
(3, 2, 1)
```

Ou encore nous pourrions vouloir ajouter la gestion d’une photo de profil à nos classes `User` et dérivées.

```
class ProfilePicture:
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.picture = '{}-{}.png'.format(self.id, self.name)

class UserPicture(ProfilePicture, User):
    pass

class AdminPicture(ProfilePicture, Admin):
    pass

class GuestPicture(ProfilePicture, Guest):
    pass
```

```
>>> john = UserPicture(1, 'john', '12345')
>>> john.picture
'1-john.png'
```

Exercice : Fils de discussion

Vous vous souvenez de la classe `Post` pour représenter un message ? Nous aimerions maintenant pouvoir instancier des fils de discussion (`Thread`) sur notre forum.

Qu’est-ce qu’un fil de discussion ?

Un message associé à un auteur et à une date ;

Mais qui comporte aussi un titre ;

Et une liste de posts (les réponses).

Le premier point indique clairement que nous allons réutiliser le code de la classe `Post`, donc en hériter.

Notre nouvelle classe sera initialisée avec un titre, un auteur et un message. `Thread` sera dotée d’une méthode `answer` recevant un auteur et un texte, et s’occupant de créer le post correspondant et de l’ajouter au fil. Nous changerons aussi la méthode `format` du `Thread` afin qu’elle concatène au fil l’ensemble de ses réponses.

La classe `Post` restera inchangée. Enfin, nous supprimerons la méthode `post` de la classe `User`, pour lui en ajouter deux nouvelles :


* `new_thread(title, message)` pour créer un nouveau fil de discussion associé à cet utilisateur ;


* `answer_thread(thread, message)` pour répondre à un fil existant.

```
import crypt
import datetime

class User:
    def __init__(self, id, name, password):
        self.id = id
        self.name = name
        self._salt = crypt.mksalt()
        self._password = self._crypt_pwd(password)

    def _crypt_pwd(self, password):
        return crypt.crypt(password, self._salt)

    def check_pwd(self, password):
        return self._password == self._crypt_pwd(password)

    def new_thread(self, title, message):
        return Thread(title, self, message)

    def answer_thread(self, thread, message):
        thread.answer(self, message)

class Post:
    def __init__(self, author, message):
        self.author = author
        self.message = message
        self.date = datetime.datetime.now()

    def format(self):
        date = self.date.strftime('le %d/%m/%Y à %H:%M:%S')
        return '<div><span>Par {} {}</span><p>{}</p></div>'.format(self.author.name, date, self.message)

class Thread(Post):
    def __init__(self, title, author, message):
        super().__init__(author, message)
        self.title = title
        self.posts = []

    def answer(self, author, message):
        self.posts.append(Post(author, message))

    def format(self):
        posts = [super().format()]
        posts += [p.format() for p in self.posts]
        return '\n'.join(posts)

if __name__ == '__main__':
    john = User(1, 'john', '12345')
    peter = User(2, 'peter', 'toto')
    thread = john.new_thread('Bienvenue', 'Bienvenue à tous')
    peter.answer_thread(thread, 'Merci')
    print(thread.format())
```

### L’héritage conditionnel

Nous revenons ici sur le traitement des héritages conditionnels déjà abordés. Pour optimiser notre code, et la réutilisation de classes, nous voulons pouvoir utiliser l’héritage conditionnel dans nos classes avec la méthode `__init__`. Nous voulons aussi avoir la possibilité de filtrer les méthodes et les propriétés de la classe à hériter.

Voici comment faire avec le fichier «**heritageconditionnel.py**» :

```
class Pere(object):
    def __init__(self, paramètrepère=False):
        self.mapropriété_père = None
        if paramètrepère:
            self.mapropriété_conditionnellepère = None
            def ma_Methode_Conditionnelle_père(self):
                pass
            self.ma_Methode_Conditionnelle_père = ma_Methode_Conditionnelle_père

    def ma_Methode_Père(self):
        pass


class Enfant(Pere):
    def __init__(self, paramètreEnfant=False, banieméthodes=[]):
        Pere.__init__(Pere, paramètreEnfant)
        self.mapropriété_enfant = None
        parent = Pere
        class new_parent(parent.__base__):
            pass
        parent_list = dir(parent)
        new_parent_list = dir(new_parent)
        candidates = set(parent_list) - set(new_parent_list)
        if paramètreEnfant:
            self.mapropriété_conditionnelleenfant = None
            def ma_Methode_Conditionnelle_Enfant(self):
                pass
            self.ma_Methode_Conditionnelle_Enfant = ma_Methode_Conditionnelle_Enfant
            for candidate in candidates:
                if candidate not in banieméthodes:
                    setattr(new_parent, candidate, parent.__getattribute__(parent, candidate))

        Enfant.__bases__ = (new_parent, )

    def ma_Methode_Enfant(self):
        pass


class PetitEnfant(Enfant):
    def __init__(self, paramètreEnfant=False, options=[]):
        self.mapropriétépetitenfant = None
        if paramètreEnfant:
            self.mapropriété_conditionnellepetitenfant = None
            def ma_Methode_Conditionnelle_PetitEnfant():
                pass
            self.ma_Methode_Conditionnelle_PetitEnfant = ma_Methode_Conditionnelle_PetitEnfant
            if options == []:
                super().__init__(paramètreEnfant)
            else:
                super().__init__(paramètreEnfant, options)
        elif options != []:
            super().__init__(True, banieméthodes=options)
        else:
            super().__init__()
    def ma_Methode_PetitEnfant(self):
        pass

if __name__ == '__main__':
    print('PetitEnfant()')
    a = PetitEnfant()
    print(dir(a))
    print('PetitEnfant(True)')
    a = PetitEnfant(True)
    print(dir(a))
    print('PetitEnfant(options=[\'ma_Methode_Père\']')
    a = PetitEnfant(options=['ma_Methode_Père'])
    print(dir(a))
    print("Enfant()")
    b = Enfant()
    print(dir(b))
    print("Enfant('True')")
    b = Enfant('True')
    print(dir(b))
    print("Père()")
    c = Pere()
    print(dir(c))
```

Ce qui nous donne en sortie d’exécution

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement/9_objets$ python3 ./heritageconditionnel.py
PetitEnfant()
['__class__', '__delattr__', '__dict__', '__dir__', '__doc__', '__eq__', '__format__', '__ge__', '__getattribute__', '__gt__', '__hash__', '__init__', '__init_subclass__', '__le__', '__lt__', '__module__', '__ne__', '__new__', '__reduce__', '__reduce_ex__', '__repr__', '__setattr__', '__sizeof__', '__str__', '__subclasshook__', '__weakref__', 'ma_Methode_Enfant', 'ma_Methode_PetitEnfant', 'mapropriété_enfant', 'mapropriétépetitenfant']
PetitEnfant(True)
['__class__', '__delattr__', '__dict__', '__dir__', '__doc__', '__eq__', '__format__', '__ge__', '__getattribute__', '__gt__', '__hash__', '__init__', '__init_subclass__', '__le__', '__lt__', '__module__', '__ne__', '__new__', '__reduce__', '__reduce_ex__', '__repr__', '__setattr__', '__sizeof__', '__str__', '__subclasshook__', '__weakref__', 'ma_Methode_Conditionnelle_Enfant', 'ma_Methode_Conditionnelle_PetitEnfant', 'ma_Methode_Conditionnelle_père', 'ma_Methode_Enfant', 'ma_Methode_PetitEnfant', 'ma_Methode_Père', 'mapropriété_conditionnelleenfant', 'mapropriété_conditionnellepetitenfant', 'mapropriété_conditionnellepère', 'mapropriété_enfant', 'mapropriété_père', 'mapropriétépetitenfant']
PetitEnfant(options=['ma_Methode_Père']
['__class__', '__delattr__', '__dict__', '__dir__', '__doc__', '__eq__', '__format__', '__ge__', '__getattribute__', '__gt__', '__hash__', '__init__', '__init_subclass__', '__le__', '__lt__', '__module__', '__ne__', '__new__', '__reduce__', '__reduce_ex__', '__repr__', '__setattr__', '__sizeof__', '__str__', '__subclasshook__', '__weakref__', 'ma_Methode_Conditionnelle_Enfant', 'ma_Methode_Conditionnelle_père', 'ma_Methode_Enfant', 'ma_Methode_PetitEnfant', 'mapropriété_conditionnelleenfant', 'mapropriété_conditionnellepère', 'mapropriété_enfant', 'mapropriété_père', 'mapropriétépetitenfant']
Enfant()
['__class__', '__delattr__', '__dict__', '__dir__', '__doc__', '__eq__', '__format__', '__ge__', '__getattribute__', '__gt__', '__hash__', '__init__', '__init_subclass__', '__le__', '__lt__', '__module__', '__ne__', '__new__', '__reduce__', '__reduce_ex__', '__repr__', '__setattr__', '__sizeof__', '__str__', '__subclasshook__', '__weakref__', 'ma_Methode_Enfant', 'mapropriété_enfant']
Enfant('True')
['__class__', '__delattr__', '__dict__', '__dir__', '__doc__', '__eq__', '__format__', '__ge__', '__getattribute__', '__gt__', '__hash__', '__init__', '__init_subclass__', '__le__', '__lt__', '__module__', '__ne__', '__new__', '__reduce__', '__reduce_ex__', '__repr__', '__setattr__', '__sizeof__', '__str__', '__subclasshook__', '__weakref__', 'ma_Methode_Conditionnelle_Enfant', 'ma_Methode_Conditionnelle_père', 'ma_Methode_Enfant', 'ma_Methode_Père', 'mapropriété_conditionnelleenfant', 'mapropriété_conditionnellepère', 'mapropriété_enfant', 'mapropriété_père']
Père()
['__class__', '__delattr__', '__dict__', '__dir__', '__doc__', '__eq__', '__format__', '__ge__', '__getattribute__', '__gt__', '__hash__', '__init__', '__init_subclass__', '__le__', '__lt__', '__module__', '__ne__', '__new__', '__reduce__', '__reduce_ex__', '__repr__', '__setattr__', '__sizeof__', '__str__', '__subclasshook__', '__weakref__', 'ma_Methode_Conditionnelle_père', 'ma_Methode_Père', 'mapropriété_conditionnellepère', 'mapropriété_père']
```

### Opérateurs

Il est maintenant temps de nous intéresser aux opérateurs du langage Python (`+`, `-`, `\*`, etc.). En effet, un code respectant la philosophie du langage se doit de les utiliser à bon escient.

Ils sont une manière claire de représenter des opérations élémentaires (addition, concaténation, …) entre deux objets. `a + b` est en effet plus lisible qu’un `add(a, b)` ou encore `a.add(b)`.

Ce chapitre a pour but de vous présenter les mécanismes mis en jeu par ces différents opérateurs, et la manière de les implémenter. Les opérateurs sont un autre type de méthodes spéciales que nous découvrirons dans cette section.

En effet, les opérateurs ne sont rien d’autres en Python que des fonctions, qui s’appliquent sur leurs opérandes. On peut s’en rendre compte à l’aide du module operator, qui répertorie les fonctions associées à chaque opérateur.

```
>>> import operator
>>> operator.add(5, 6)
11
>>> operator.mul(2, 3)
6
```

Ainsi, chacun des opérateurs correspondra à une méthode de l’opérande de gauche, qui recevra en paramètre l’opérande de droite.

#### Opérateurs arithmétiques

L’addition, par exemple, est définie par la méthode `__add__`.

```
>>> class A:
... def \__add__(self, other):
... return other # on considère self comme 0
...
>>> A() + 5
5
```

Assez simple, n’est-il pas ? Mais nous n’avons pas tout à fait terminé. Si la méthode est appelée sur l’opérande de gauche, que se passe-t-il quand notre objet se trouve à droite ?

```
>>> 5 + A()
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
TypeError: unsupported operand type(s) for +: 'int' and 'A'
```

Nous ne supportons pas cette opération. En effet, l’expression fait appel à la méthode `int.__add__` qui ne connaît pas les objets de type `A`.
Heureusement, ce cas a été prévu et il existe une fonction inverse, `__radd__`, appelée si la première opération n’était pas supportée.

```
>>> class A:
... def __add__(self, other):
... return other
... def __radd__(self, other):
... return other
...
>>> A() + 5
5
>>> 5 + A()
5
```

Il faut bien noter que `A.__radd__` ne sera appelée que si `int.__add__` a échoué.

Les autres opérateurs arithmétques binaires auront un comportement similaire, voici une liste des méthodes à implémenter pour chacun d’eux :


* Addition/Concaténation **(a + b)** — `__add__`, `__radd__`


* Soustraction/Différence **(a - b)** — `__sub__`, `__rsub__`


* Multiplication **(a \* b)** — `__mul__`, `__rmul__`


* Division **(a / b)** — `__truediv__`, `__rtruediv__`


* Division entière **(a // b)** — `__floordiv__`, `__rfloordiv__`


* Modulo/Formattage **(a % b)** — `__mod__`, `__rmod__`


* Exponentiation **(a \*\* b)** — `__pow__`, `__rpow__`

On remarque aussi que chacun de ces opérateurs arithmétiques possède une version simplifiée pour l’assignation **(a += b)** qui correspond à la méthode `__iadd__`. Par défaut, les méthodes `__add__` `__radd__` sont appelées, mais définir `__iadd__` permet d’avoir un comportement différent dans le cas d’un opérateur d’assignation, par exemple sur les listes :

```
>>> l = [1, 2, 3]
>>> l2 = l
>>> l2 = l2 + [4]
>>> l2
[1, 2, 3, 4]
>>> l
[1, 2, 3]
>>> l2 = l
>>> l2 += [4]
>>> l2
[1, 2, 3, 4]
>>> l
[1, 2, 3, 4]
```

#### Opérateurs arithmétiques unaires

Voyons maintenant les **opérateurs unaires**, qui ne prennent donc pas d’autre paramètre que `self`.


* Opposé **(-a)** — `__neg__`


* Positif **(+a)** - `__pos__`


* Valeur abosule **(abs(a))** — `__abs__`

#### Opérateurs de comparaison

De la même manière que pour les opérateurs arithmétiques et unaires, nous avons une méthode spéciale par opérateur de comparaison. Ces opérateurs s’appliqueront sur l’opérande gauche en recevant le droite en paramètre. Ils devront retourner un booléen.

Contrairement aux opérateurs arithmétiques, il n’est pas nécessaire d’avoir deux versions pour chaque opérateur puisque Python saura directement quelle opération inverse tester si la première a échoué (`a == b` est équivalent à `b == a`, `a < b` à `b > a`, etc.).


* Égalité **(a == b)** — `__eq__`


* Différence **(a != b)** — `__neq__`


* Stricte infériorité **(a < b)** — `__lt__`


* Infériorité **(a <= b)** — `__le__`


* Stricte supériorité **(a > b)** — `__gt__`


* Supériorité **(a >= b)** — `__ge__`

On notera aussi que beaucoup de ces opérateurs peuvent s’inférer les uns les autres. Par exemple, il suffit de savoir calculer `a == b` et `a < b` pour définir toutes les autres opérations. Ainsi, Python dispose d’un décorateur, `total_ordering` du module `functools`, pour automatiquement générer les opérations manquantes.

```
>>> from functools import total_ordering
>>> @total_ordering
... class Inferior:
...     def __eq__(self, other):
...         return False
...     def __lt__(self, other):
...         return True
...
>>> i = Inferior()
>>> i == 5
False
>>> i > 5
False
>>> i < 5
True
>>> i <= 5
True
>>> i != 5
True
```

#### Autres opérateurs

Nous avons ici étudié les principaux opérateurs du langage. Ces listes ne sont pas exhaustives et présentent juste la méthodologie à suivre.

Pour une liste complète, je vous invite à consulter la documentation du module operator : [https://docs.python.org/3/library/operator.html](https://docs.python.org/3/library/operator.html).

#### Exercice Arithmétique simple

Oublions temporairement nos utilisateurs et notre forum, et intéressons-nous à l’évaluation mathématique.

Imaginons que nous voulions représenter une expression mathématique, qui pourrait contenir des termes variables (par exemple, `2 \* (-x + 1)`).

Il va nous falloir utiliser un type pour représenter cette variable `x`, appelé `Var`, et un second pour l’expression non évaluée, `Expr`. Les `Var` étant un type particulier d’expressions.

Nous aurons deux autres types d’expressions : les opérations arithmétiques unaires **(+, -)** et binaires **(+, -, \*, /, //, %, \*\*)**. Vous pouvez vous appuyer un même type pour ces deux types d’opérations.

L’expression précédente s’évaluerait par exemple à :

```
BinOp(operator.mul, 2, BinOp(operator.add, UnOp(operator.neg, Var('x')), 1))
```

Nous ajouterons à notre type `Expr` une méthode `compute(\*\*values)`, qui permettra de calculer l’expression suivant une valeur donnée, de façon à ce que `Var('x').compute(x=5)` retourne **5**.

Enfin, nous pourrons ajouter une méthode `__repr__` pour obtenir une représentation lisible de notre expression.

```
import operator

def compute(expr, **values):
    if not isinstance(expr, Expr):
        return expr
    return expr.compute(**values)

class Expr:
    def compute(self, **values):
        raise NotImplementedError

    def __pos__(self):
        return UnOp(operator.pos, self, '+')

    def __neg__(self):
        return UnOp(operator.neg, self, '-')

    def __add__(self, rhs):
        return BinOp(operator.add, self, rhs, '+')

    def __radd__(self, lhs):
        return BinOp(operator.add, lhs, self, '+')

    def __sub__(self, rhs):
        return BinOp(operator.sub, self, rhs, '-')

    def __rsub__(self, lhs):
        return BinOp(operator.sub, lhs, self, '-')

    def __mul__(self, rhs):
        return BinOp(operator.mul, self, rhs, '*')

    def __rmul__(self, lhs):
        return BinOp(operator.mul, lhs, self, '*')

    def __truediv__(self, rhs):
        return BinOp(operator.truediv, self, rhs, '/')

    def __rtruediv__(self, lhs):
        return BinOp(operator.truediv, lhs, self, '/')

    def __floordiv__(self, rhs):
        return BinOp(operator.floordiv, self, rhs, '//')

    def __rfloordiv__(self, lhs):
        return BinOp(operator.floordiv, lhs, self, '//')

    def __mod__(self, rhs):
        return BinOp(operator.mod, self, rhs, '*')

    def __rmod__(self, lhs):
        return BinOp(operator.mod, lhs, self, '*')

class Var(Expr):
    def __init__(self, name):
        self.name = name

    def compute(self, **values):
        if self.name in values:
            return values[self.name]
        return self

    def __repr__(self):
        return self.name

class Op(Expr):
    def __init__(self, op, *args):
        self.op = op
        self.args = args

    def compute(self, **values):
        args = [compute(arg, **values) for arg in self.args]
        return self.op(*args)

class UnOp(Op):
    def __init__(self, op, expr, symbol=None):
        super().__init__(op, expr)
        self.symbol = symbol

    def __repr__(self):
        if self.symbol is None:
            return super().__repr__()
        return '{}{!r}'.format(self.symbol, self.args[0])

class BinOp(Op):
    def __init__(self, op, expr1, expr2, symbol=None):
        super().__init__(op, expr1, expr2)
        self.symbol = symbol

    def __repr__(self):
        if self.symbol is None:
            return super().__repr__()
        return '({!r} {} {!r})'.format(self.args[0], self.symbol, self.args[1])

if __name__ == '__main__':
    x = Var('x')
    expr = 2 * (-x + 1)
    print(expr)
    print(compute(expr, x=1))

    y = Var('y')
    expr += y
    print(compute(expr, x=0, y=10))
```

Les opérateurs sont une notion importante en Python, mais ils sont loin d’être la seule. Le chapitre suivant vous présentera d’autres concepts avancés du Python, qu’il est important de connaître, pour être en mesure de les utiliser quand cela s’avère nécessaire.

### Les attributs de classe

Nous avons déjà rencontré un attribut de classe, quand nous nous intéressions aux parents d’une classe. Souvenez-vous de `__bases__`, nous ne l’utilisions pas sur des instances mais sur notre classe directement.

En Python, les classes sont des objets comme les autres, et peuvent donc posséder leurs propres attributs.

```
>>> class User:
...     pass
...
>>> User.type = 'simple_user'
>>> User.type
'simple_user'
```

Les attributs de classe peuvent aussi se définir dans le corps de la classe, de la même manière que les méthodes.

```
class User:
    type = 'simple_user'
```

On notera à l’inverse qu’il est aussi possible de définir une méthode de la classe depuis l’extérieur :

```
>>> def User_repr(self):
...     return '<User>'
...
>>> User.__repr_\_ = User_repr
>>> User()
<User>
```

L’avantage des attributs de classe, c’est qu’ils sont aussi disponibles pour les instances de cette classe. Ils sont partagés par toutes les instances.

```
>>> john = User()
>>> john.type
'simple_user'
>>> User.type = 'admin'
>>> john.type
'admin'
```

C’est le fonctionnement du MRO de Python, il cherche d’abord si l’attribut existe dans l’objet, puis si ce n’est pas le cas, le cherche dans les classes parentes.

Attention donc, quand l’attribut est redéfini dans l’objet, il sera trouvé en premier, et n’affectera pas la classe.

```
>>> john = User()
>>> john.type
'admin'
>>> john.type = 'superadmin'
>>> john.type
'superadmin'
>>> User.type
'admin'
>>> joe = User()
>>> joe.type
'admin'
```

Attention aussi, quand l’attribut de classe est un objet mutable { Un objet mutable est un objet que l’on peut modifier (liste, dictionnaire) par opposition à un objet immutable (nombre, chaîne de caractères, tuple) }, il peut être modifié par n’importe quelle instance de la classe.

```
>>> class User:
...     users = []
...
>>> john, joe = User(), User()
>>> john.users.append(john)
>>> joe.users.append(joe)
>>> john.users
[<__main__.User object at 0x7f3b7acf8b70>, <__main__.User object at 0x7f3b7acf8ba8>]
```

L’attribut de classe est aussi conservé lors de l’héritage, et partagé avec les classes filles (sauf lorsque les classes filles redéfinissent l’attribut, de la même manière que pour les instances).

```
>>> class Guest(User):
...     pass
...
>>> Guest.users
[<__main__.User object at 0x7f3b7acf8b70>, <__main__.User object at 0x7f3b7acf8ba8>]
>>> class Admin(User):
...     users = []
...
>>> Admin.users
[]
```

### Méthodes de Classes

Comme pour les attributs, des méthodes peuvent être définies au niveau de la classe. C’est par exemple le cas de la méthode `mro`.

```
int.mro()
```

**Les méthodes de classe constituent des opérations relatives à la classe mais à aucune instance**. Elles recevront la classe courante en premier paramètre (nommé `cls`, correspondant au `self` des méthodes d’instance), et auront donc accès aux autres attributs et méthodes de classe.

Reprenons notre classe `User`, à laquelle nous voudrions ajouter le stockage de tous les utilisateurs, et la génération automatique de l’id.
Il nous suffirait d’une même méthode de classe pour stocker l’utilisateur dans un attribut de classe `users`, et qui lui attribuerait un `id` en fonction du nombre d’utilisateurs déjà enregistrés.

```
>>> root = Admin('root', 'toor')
>>> root
<User: 1, root>
>>> User('john', '12345')
<User: 2, john>
>>> guest = Guest('guest')
<User: 3, guest>
```

Les méthodes de classe se définissent comme les méthodes habituelles, à la différence près qu’elles sont précédées du décorateur `classmethod`.

```
import crypt

class User:
    users = []

    def __init__(self, name, password):
        self.name = name
        self._salt = crypt.mksalt()
        self._password = self._crypt_pwd(password)
        self.register(self)

    @classmethod
    def register(cls, user):
        cls.users.append(user)
        user.id = len(cls.users)

    def _crypt_pwd(self, password):
        return crypt.crypt(password, self._salt)

    def check_pwd(self, password):
        return self._password == self._crypt_pwd(password)

    def __repr__(self):
        return '<User: {}, {}>'.format(self.id, self.name)

class Guest(User):
    def __init__(self, name):
        super().__init__(name, '')

    def check_pwd(self, password):
        return True

class Admin(User):
    def manage(self):
        print('Je suis un Jedi!')
```

Vous pouvez constater le résultat en réessayant le code donné plus haut.

### Méthodes statiques

Les méthodes statiques sont très proches des méthodes de classe, mais sont plus **à considérer comme des fonctions au sein d’une classe**.

Contrairement aux méthodes de classe, elles ne recevront pas le paramètre `cls`, et n’auront donc pas accès aux attributs de classe,
méthodes de classe ou méthodes statiques.

Les méthodes statiques sont plutôt dédiées à des comportements annexes en rapport avec la classe, par exemple on pourrait remplacer notre attribut `id` par un `uuid` aléatoire, dont la génération ne dépendrait de rien d’autre dans la classe.

Elles se définissent avec le décorateur `staticmethod`.

```
import uuid

class User:
    def __init__(self, name, password):
        self.id = self._gen_uuid()
        self.name = name
        self._salt = crypt.mksalt()
        self._password = self._crypt_pwd(password)

    @staticmethod
    def _gen_uuid():
        return str(uuid.uuid4())

    def _crypt_pwd(self, password):
        return crypt.crypt(password, self._salt)

    def check_pwd(self, password):
        return self._password == self._crypt_pwd(password)
```

```
>>> john = User('john', '12345')
>>> john.id
'69ef1327-3d96-42a9-94e6-622619fbf666'
```

### Recherche d’attributs

Nous savons récupérer et assigner un attribut dont le nom est fixé, cela se fait facilement à l’aide des instructions `obj.foo` et `obj.foo = value`.

Mais nous est-il possible d’accéder à des attributs dont le nom est variable ?

Prenons une instance `john` de notre classe `User`, et le nom d’un attribut que nous voudrions extraire :

```
>>> john = User('john', '12345')
>>> attr = 'name'
```

La fonction `getattr` nous permet alors de récupérer cet attribut.

```
>>> getattr(john, attr)
'john'
```

Ainsi, `getattr(obj, 'foo')` est équivalent à `obj.foo`.

On trouve aussi une fonction `hasattr` pour tester la présence d’un attribut dans un objet. Elle est construite comme `getattr` mais retourne un booléen pour savoir si l’attribut est présent ou non.

```
>>> hasattr(john, 'name')
True
>>> hasattr(john, 'last_name')
False
>>> hasattr(john, 'id')
True
```

De la même manière, les fonctions `setattr` et `delattr` servent respectivement à modifier et supprimer un attribut.

```
>>> setattr(john, 'name', 'peter') # équivalent à `john.name = 'peter'`
>>> john.name
'peter'
>>> delattr(john, 'name') # équivalent à \`del john.name\`
>>> john.name
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
AttributeError: 'User' object has no attribute 'name'
```

### Les propriétés

Les propriétés sont une manière en Python de «dynamiser» les attributs d’un objet. Ils permettent de générer des attributs à la volée à partir de méthodes de l’objet.

Un exemple vaut mieux qu’un long discours :

```
class ProfilePicture:
    @property
    def picture(self):
        return '{}-{}.png'.format(self.id, self.name)

class UserPicture(ProfilePicture, User):
    pass
```

On définit donc une propriété `picture` avec `@property`, qui s’utilise comme un attribut. Chaque fois qu’on appelle `picture`, la méthode correspondante est appelée et le résultat est calculé.

```
>>> john = UserPicture('john', '12345')
>>> john.picture
'1-john.png'
>>> john.name = 'John'
>>> john.picture
'1-John.png'
```

Il s’agit là d’une propriété en lecture seule, il nous est en effet impossible de modifier la valeur de l’attribut `picture`.

```
>>> john.picture = 'toto.png'
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
AttributeError: can't set attribute
```

Pour le rendre modifiable, il faut ajouter à notre classe la méthode permettant de gérer la modification, à l’aide du décorateur `@picture.setter` (le décorateur setter de notre propriété picture, donc).

On utilisera ici un attribut `_picture`, qui pourra contenir l’adresse de l’image si elle a été définie, et `None` le cas échéant.

```
class ProfilePicture:
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._picture = None

    @property
    def picture(self):
        if self._picture is not None:
            return self._picture
        return '{}-{}.png'.format(self.id, self.name)

    @picture.setter
    def picture(self, value):
        self._picture = value

class UserPicture(ProfilePicture, User):
    pass
```

```
>>> john = UserPicture('john', '12345')
>>> john.picture
'1-john.png'
>>> john.picture = 'toto.png'
>>> john.picture
'toto.png'
```

Enfin, on peut aussi coder la suppression de l’attribut à l’aide de `@picture.deleter`, ce qui revient à réaffecter `None` à l’attribut `_picture`.

```
class ProfilePicture:
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._picture = None

    @property
    def picture(self):
        if self._picture is not None:
            return self._picture
        return '{}-{}.png'.format(self.id, self.name)

    @picture.setter
    def picture(self, value):
        self._picture = value

    @picture.deleter
    def picture(self):
        self._picture = None

class UserPicture(ProfilePicture, User):
    pass
```

```
>>> john = UserPicture('john', '12345')
>>> john.picture
'1-john.png'
>>> john.picture = 'toto.png'
>>> john.picture
'toto.png'
>>> del john.picture
>>> john.picture
'1-john.png'
```

### Classes abstraites

La notion de classes abstraites est utilisée lors de l’héritage pour forcer les classes filles à implémenter certaines méthodes (dites méthodes abstraites) et donc respecter une interface.

Les classes abstraites ne font pas partie du cœur même de Python, mais sont disponibles via un module de la bibliothèque standard, `abc` (Abstract Pere Classes). Ce module contient notamment la classe `ABC` et le décorateur `@abstractmethod`, pour définir respectivement une classe abstraite et une méthode abstraite de cette classe.

Une classe abstraite doit donc hériter d’`ABC`, et utiliser le décorateur cité pour définir ses méthodes abstraites.

```
import abc

class MyABC(abc.ABC):
    @abc.abstractmethod
    def foo(self):
        pass
```

Il nous est impossible d’instancier des objets de type `MyABC`, puisqu’une méthode abstraite n’est pas implémentée :

```
>>> MyABC()
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
TypeError: Can't instantiate abstract class MyABC with abstract methods foo
```

Il en est de même pour une classe héritant de `MyABC` sans redéfinir la méthode.

```
>>> class A(MyABC):
... pass
...
>>> A()
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
TypeError: Can't instantiate abstract class A with abstract methods foo
```

Aucun problème par contre avec une autre classe qui redéfinit bien la méthode.

```
>>> class B(MyABC):
... def foo(self):
...     return 7
...
>>> B()
<__main__.B object at 0x7f33065316a0>
>>> B().foo()
7
```

#### Exercice : Base de données

nous aborderons les méthodes de classe et les propriétés.

Reprenons notre forum, auquel nous souhaiterions ajouter la gestion d’une base de données.

Notre base de données sera une classe avec deux méthodes, `insert` et `select`. Son implémentation est libre, elle doit juste respecter l’interface suivante :

```
>>> class A: pass
...
>>> class B: pass
...
>>>
>>> db = Database()
>>> obj = A()
>>> obj.value = 42
>>> db.insert(obj)
>>> obj = A()
>>> obj.value = 5
>>> db.insert(obj)
>>> obj = B()
>>> obj.value = 42
>>> obj.name = 'foo'
>>> db.insert(obj)
>>>
>>> db.select(A)
<__main__.A object at 0x7f033697f358>
>>> db.select(A, value=5)
<__main__.A object at 0x7f033697f3c8>
>>> db.select(B, value=42)
<__main__.B object at 0x7f033697f438>
>>> db.select(B, value=42, name='foo')
<__main__.B object at 0x7f033697f438>
>>> db.select(B, value=5)
ValueError: item not found
```

Nous ajouterons ensuite une classe `Model`, qui se chargera de stocker dans la base toutes les instances créées. `Model` comprendra une méthode de classe `get(\*\*kwargs)` chargée de réaliser une requête select sur la base de données et de retourner l’objet correspondant. Les objets de type `Model` disposeront aussi d’une propriété `id`, retournant un identifiant unique de l’objet.

On pourra alors faire hériter nos classes `User` et `Post` de `Model`, afin que les utilisateurs et messages soient stockés en base de données. Dans un second temps, on pourra faire de `Model` une classe abstraite, par exemple en rendant abstraite la méthode `__init__`.

```
import abc
import datetime

class Database:
    data = []

    def insert(self, obj):
        self.data.append(obj)

    def select(self, cls, **kwargs):
        items = (item for item in self.data
            if isinstance(item, cls)
            and all(hasattr(item, k) and getattr(item, k) == v
            for (k, v) in kwargs.items()))
        try:
            return next(items)
        except StopIteration:
            raise ValueError('item not found')

class Model(abc.ABC):
    db = Database()
    @abc.abstractmethod
    def __init__(self):
        self.db.insert(self)
    @classmethod
    def get(cls, \**kwargs):
        return cls.db.select(cls, \**kwargs)
    @property
    def id(self):
        return id(self)

class User(Model):
    def __init__(self, name):
        super().__init__()
        self.name = name

class Post(Model):
    def __init__(self, author, message):
        super().__init__()
        self.author = author
        self.message = message
        self.date = datetime.datetime.now()

    def format(self):
        date = self.date.strftime('le %d/%m/%Y à %H:%M:%S')
        return '<div><span>Par {} {}</span><p>{}</p></div>'.format(self.author.name, date, self.message)

if __name__ == '__main__':
    john = User('john')
    peter = User('peter')
    Post(john, 'salut')
    Post(peter, 'coucou')

    print(Post.get(author=User.get(name='peter')).format())
    print(Post.get(author=User.get(id=john.id)).format())
```

Ces dernières notions ont dû compléter vos connaissances du modèle objet de Python, et vous devriez maintenant être prêts à vous lancer dans un projet exploitant ces concepts.

## La visualisation de l’architecture objets

Le programme `pylint`, que nous avons installé pour tester le code Python, est livré avec un outil de ligne de commande fort pratique `pyreverse`. Celui-ci permet d’imager les classes Python et de créer des diagrammes [UML](https://www.ibm.com/docs/fr/rational-soft-arch/9.5?topic=diagrams-uml-models) des classes Python.

### Pyreverse

**Pyreverse** permet de générer des diagrammes avec :


* des attributs de classes et si possible avec leurs types,


* des méthodes de classes avec leurs paramètres,


* la représentation des exceptions,


* les liens d’héritages de classes,


* et les liens d’association de classes.

#### Les options de la ligne de commande

#### Options de la ligne de commande de Pyreverse

| Option courte

                                                                                                                                                 | Option verbeuse

                                                                                                                                                                                                                                                                                   | Description

                                                                                                                                                                                                                                                             |
| ------------------------------------------------------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `-p <nom du projet>`

                                                                                                                                            | `--project=<nom du projet>`

                                                                                                                                                                                                                                                                         | Nom du fichier en sortie

                                                                                                                                                                                                                                                |
| `-o <format>`

                                                                                                                                                   | `--output=<format>`

                                                                                                                                                                                                                                                                                 | Format de sortie de fichier (**svg**, **svgz**, **png**, **jpeg**/**jpg**/**jpe**, **gif**, **ps**/**ps2**/**eps**, **pdf**, pic, pcl, hpgl, gd/gd2, fig, dia, **dot**/**xdot**, **plain**/**plain-ext**, vrml/vml/vmlz, tk, wbmp, xlib, etc.)

                                                                                      |
| `-c <classe>`

                                                                                                                                                   | `--class <classe>`

                                                                                                                                                                                                                                                                                  | Afficher la classe passée en paramètre

                                                                                                                                                                                                                                  |
| `-k`

                                                                                                                                                            | `--only-classnames`

                                                                                                                                                                                                                                                                                 | Afficher seulement les noms des classes

                                                                                                                                                                                                                                 |
| `-m[y/n]`

                                                                                                                                                       | `--module-names=[y/n]`

                                                                                                                                                                                                                                                                              | Inclure le nom des modules pour la représentation des classes.

                                                                                                                                                                                                          |
| `-b`

                                                                                                                                                            | `--show-builtin`

                                                                                                                                                                                                                                                                                    | Afficher les classes des objets natifs Python

                                                                                                                                                                                                                           |
| `-a <niveau>`

                                                                                                                                                   | `--show-ancestors=<niveau>`

                                                                                                                                                                                                                                                                         | Aficher le niveaux d’héritage passé en paramètre

                                                                                                                                                                                                                        |
| `-A`

                                                                                                                                                            | `--all-ancestors`

                                                                                                                                                                                                                                                                                   | Afficher tout l’arbre d’héritage

                                                                                                                                                                                                                                        |
| `-s <niveau>`

                                                                                                                                                   | `--show-associated=<niveau>`

                                                                                                                                                                                                                                                                        | Liens des imports suivant le niveau d’imports

                                                                                                                                                                                                                           |
| `-S`

                                                                                                                                                            | `--all-associated`

                                                                                                                                                                                                                                                                                  | Toutes les liaisons d’imports

                                                                                                                                                                                                                                           |
| `-f <mode>`

                                                                                                                                                     | `--filter-mode=<mode>`

                                                                                                                                                                                                                                                                              | Filtre ce qu’il faut faire apparaître (par défaut PUB_ONLY) :


* **PUB_ONLY** : Affiche les méthodes et les propriétés publiques.


* **SPECIAL** : Affiche les attributs privés ou protégés en plus.


* **OTHER** : Affiche le méthodes privées ou protégés en plus.


* **ALL** : Affiche tout.

 |
```
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ cd 9_objets
utilisateur@MachineUbuntu:~/repertoire_de_developpement/9_objets$ pyreverse -p test -o png scene.py
```

L’option `-p test` ajout le suffixe **test** au nom du fichier qui à défaut est «**classes.ext**».
L’option `-o png` choisit le format de sortie, ici un fichier image [PNG](https://fr.wikipedia.org/wiki/Portable_Network_Graphics).

Cela génère donc le fichier «**classes_test.png**».

Qui nous donne à afficher par défaut :



![image](images/pyreverse_test1.png)

Cela nous affiche le nom de la classe `Decor` avec sa propriété `fabriqueBatiments` affectée à l’objet `FabriqueBatiments`. Ceci nous permet de comprendre que cela est en fait une méthode pour l’objet.

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement/9_objets$ pyreverse -c Decor -o png scene.py
```

L’option `-c Decour` choisit la classe à générer et va produire une sortie de nom de fichier avec le nom de la classe «**Decor.png**».

Ce qui va nous donner comme diagramme plus intéressant :



![image](images/pyreverse_test2.png)

Là nous voyons avec les **flêches** l’héritage des classes `Vegetation`, `Urbanisation`, `Hydrotopologie`, `Geomorphologie` et `Atmosphere` qui elle même hérite de `Nuages`.

Nous voyon aussi les propriétés et méthodes de `FabriqueBatiments` qui est bien affectée à la propriété `fabriqueBatiments` (en vert) de la classe `Decor`

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement/9_objets$ pyreverse -k -c Decor -o png scene.py
```

L’option `-k` permet de n’afficher que le nom des classes :



![image](images/pyreverse_test3.png)

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement/9_objets$ pyreverse -f ALL -S -c Decor -o png scene.py
```

L’option `-f ALL` permet d’afficher les propriétés et les méthodes cachées :



![image](images/pyreverse_test4.png)

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement/9_objets$ pyreverse -a 1 -c Decor -o png scene.py
```

L’option `-a 1` permet de limiter le niveau de liens en héritage par rapport à la classe :



![image](images/pyreverse_test5.png)

En limitant au premier niveau `-a 1` la classe `Nuages` n’est plus affichée.

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement/9_objets$ pyreverse -b -c Decor -o png scene.py
```

L’option `-b` permet d’afficher l’héritage des classes natives Python :



![image](images/pyreverse_test6.png)

On voit alors bien apparaître la classe `object` comme on l’attendait, mais aussi les classes `builtin.list` et `builtin.str`.

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement/9_objets$ pyreverse -mn -c Decor -o png scene.py
```

L’option `-mn` permet de supprimer le chemin d’importation des classes dans les intitulés de classes :



![image](images/pyreverse_test7.png)

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement/9_objets$ pyreverse -s 0 -c Decor -o png scene.py
```

L’option `-s 0` permet de ne visualiser que les classes importées directement :



![image](images/pyreverse_test8.png)

On voit bien que la classe `FabriqueBatiments` ne s’affiche plus avec un niveau supplémentaire d’importation.

Vous pouvez maintenant générer tous les diagrammes de classes Python que vous voulez pour agrémenter les documentations de vos programmes.

### Mise en œuvre dans GitLab

<!-- .. include:: 09_La gestion des erreurs.rst -->
<!-- .. include:: 10_Les expressions régulières.rst -->
<!-- .. include:: 11_Stocker des données.rst -->
<!-- .. include:: 12_Le réseau.rst -->
<!-- .. include:: 13_Le WEB.rst -->
<!-- .. include:: 14_Générer des documents.rst -->
<!-- .. include:: 15_Approfondir ce cours.rst -->
<!-- .. include:: 16_Fin du cours.rst -->
