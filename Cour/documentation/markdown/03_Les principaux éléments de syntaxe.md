# Les principaux éléments de syntaxe

## Syntaxe et grammaire Python

Plusieurs choses sont nécessaires pour écrire un code lisible : **la syntaxe**, **l’organisation du code**, **le découpage en fonctions** (et possiblement en classes que nous verrons avec les objets), mais souvent, aussi, **le bon sens**.

Pour cela, les «**PEP**» pour **P**ython **E**nhancement **P**roposal (proposition d’amélioration de Python) peuvent nous aider.

Distribuer document pep8.pdf.

On va aborder dans ce chapitre sans doute la plus célèbre des PEP, à savoir la PEP 8 [https://www.python.org/dev/peps/pep-0008/](https://www.python.org/dev/peps/pep-0008/), qui est incontournable lorsque l’on veut écrire du code Python correctement.

La «**Style Guide for Python Code**» est une des plus anciennes PEP (les numéros sont croissants avec le temps). Elle consiste en un nombre important de recommandations sur la syntaxe de Python.

Il est vivement recommandé de lire la PEP 8 en entier au moins une fois pour avoir une bonne vue d’ensemble en complément de ce cours. On ne présentera ici qu’un rapide résumé de cette PEP 8.

### L’identation

Dans la plus part des langages de programmation, l’indentation du code (c’est-à-dire la manière d’écrire le code en laissant des espaces de décalage en début des lignes) est laissée au choix éclairé du développeur. Mais, force est de constater, parfois le développeur n’est pas des plus experts pour rendre lisible par les autres, et même lui même, son code…



![image](images/GestionExpaces_1.png)

Python oblige donc le développeur à structurer son code à l’aide des indentations : ce sont elles qui détermineront les blocs (séquences d’instructions liées) et non les accolades comme dans la majorité des langages.



![image](images/GestionExpaces_2.png)

Les blocs de code sont déterminés par :


* La présence du caractère «:» en fin de ligne ;


* Une indentation des lignes suivantes à l’aide de tabulations ou d’espaces.



![image](images/GestionExpaces_3.png)

Attention à ne pas mélanger les tabulations avec les espaces pour l’indentation, Python n’aime pas ça du tout. Votre code ne fonctionnera pas et vous n’obtiendrez pas de message d’erreur explicite. Je conseille l’utilisation de quatre caractères espace pour faire une indentation.

### Les commentaires

Commentaires sur une ligne :

```
>>> # Ceci est le premier commentaire
>>> bidon = 1 # et ceci est le second commentaire
>>>           # ... et là le troisième!
>>> "# Ceci n’est pas un commentaire parce qu’il est entre guillemets."
```

Commentaires sur plusieurs lignes :

```
>>> """
... Ceci est un commentaire
... en plusieurs lignes
... qui sera ignoré lors de l'exécution
... """
```

### Chaînes de caractères

**Les chaînes de caractères** (ou chaînes) sont des séquences de lettres et de nombres, ou, en d’autres termes, des morceaux de textes. Elles sont entourées par deux guillemets.

Par exemple :

```
>>> "Bonjour, Python!"
```

Comment faire si vous voulez insérer des guillemets «**"**» à l’intérieur d’une chaîne ?

Si vous essayez à l’interpréteur :

```
>>> "J'ai dit "Wow!" très fort"
  File "<stdin>", line 1
  "J'ai dit "Wow!" très fort"
             ^
  SyntaxError: invalid syntax
```

Cela génère une erreur.

Le problème est que Python voit une chaîne, `"J'ai dit "` suivie de quelque chose qui n’est pas une chaîne: **Wow!** . Ce n’est pas ce que nous voulions!

Python propose deux moyens simples d’insérer des guillemets à l’intérieur d’une chaîne.

Vous pouvez commencer et terminer une chaîne littérale avec des apostrophes «**'**» à la place des guillemets, par exemple :

```
>>> 'bla bla'
```

Les guillemets peuvent ainsi être placés à l’intérieur :

```
>>> 'Tu as dit "Wow!" très fort.'
```

Vous pouvez placer une barre oblique inversée suivie du guillemet ou de l’apostrophe (`"` ou `'` ). Cela s’appelle une séquence d’échappement.

Python va supprimer la barre oblique inversée et n’afficher que le guillemet ou l’apostrophe à l’intérieur de la chaîne. A cause des séquences d’échappement, **la barre oblique inversée (\\)** est un symbole spécial.

Pour l’inclure dans une chaîne, il faut l’échapper avec une deuxième barre oblique inversée, en d’autres termes, **il faut écrire** `\\` dans votre chaîne littérale.

Voici un exemple que vous pouvez tester avec l’interpréteur :

```
>>> 'L\'exemple avec un apostrophe.'
>>> "Voici un \"échappement\" de guillemets"
>>> "Un exemple d’échappement \
... pour écrire sur plusieurs lignes\
... un texte long"
```

Pourquoi le dernier exemple fonctionne ?

### Majuscules et Minuscules (Variables, instructions, fonctions, objets)

Nous abordons ici les règles de nommage.

Voir document pep8.pdf déjà distribué.

Les noms de **variables**, de **fonctions** et de **modules** doivent être de la forme :

```
ma_variable
fonction_test_27()
mon_module
```

C’est-à-dire en minuscules avec un caractère «**souligné**» `_` («**tiret du bas**» ou underscore en anglais) pour séparer les différents «**mots**» dans le nom.

Les **constantes** sont écrites en majuscules :

```
MA_CONSTANTE
VITESSE_LUMIÈRE
```

Les noms de **classes** et les **exceptions** sont de la forme :

```
MaClasse
MonException
```

Pensez à **donner à vos variables des noms qui ont du sens**.

Évitez autant que possible les a1, a2, i, truc, toto…

Les noms de variables à un caractère sont néanmoins autorisés pour les boucles et les indices :

```
>>> ma_liste = [1, 3, 5, 7, 9, 11]
>>> for i in range(len(ma_liste)):
...     ma_liste[i]
...
...
1
3
5
7
9
11
```

Enfin, des **noms de variable à une lettre peuvent être utilisés lorsque cela a un sens mathématique** (par exemple, les noms x, y et z évoquent des coordonnées cartésiennes).

### Gestion des espaces

La PEP 8 recommande **d’entourer les opérateurs +, -, /, \*, ==, !=, >=, not, in, and, or… d’un espace**, avant et après.

Par exemple :

```
# code recommandé
ma_variable = 3 + 7
mon_texte = "souris"
mon_texte == ma_variable
# code non recommandé :
ma_variable=3+7
mon_texte="souris"
mon_texte== ma_variable
```

Il n’y a, par contre, **pas d’espace** à **l’intérieur** des crochets **[]**, des accolades **{}** et des parenthèses **()** :

```
# code recommandé :
ma_liste[1]
mon_dico{"clé"}
ma_fonction(argument)
# code non recommandé :
ma_liste[ 1 ]
mon_dico{"clé" }
ma_fonction( argument )
```

Ni juste **avant** la parenthèse **(** ouvrante d’une fonction ou le crochet **{** ouvrant d’une liste ou d’un dictionnaire :

```
# code recommandé :
ma_liste[1]
mon_dico{"clé"}
ma_fonction(argument)
# code non recommandé :
ma_liste [1]
mon_dico {"clé"}
ma_fonction (argument)
```

On met **un espace après** les caractères **:** et **,** (mais pas avant) :

```
# code recommandé :
ma_liste = [1, 2, 3]
mon_dico = {"clé1": "valeur1", "clé2": "valeur2"}
ma_fonction(argument1, argument2)
# code non recommandé :
ma_liste = [1 , 2 ,3]
mon_dico = {"clé1":"valeur1", "clé2":"valeur2"}
ma_fonction (argument1 ,argument2)
```

Par contre, pour **les tranches de listes**, on ne met **pas d’espace** autour du **:** :

```
# code recommandé :
ma_liste = [1, 3, 5, 7, 9, 1]
ma_liste[1:3]
ma_liste[1:4:2]
ma_liste[::2]
# code non recommandé :
ma_liste[1 : 3]
ma_liste[1: 4:2 ]
ma_liste[ : :2]
```

Enfin, on n’ajoute **pas plusieurs espaces** autour du **=** ou des autres opérateurs pour faire joli :

```
# code recommandé :
x1 = 1
x2 = 3
x_old = 5
# code non recommandé :
x1    = 1
x2    = 3
x_old = 5
```

### Les règles de base d’écriture des fonctions/procédures

Maintenant que vous êtes prêt à écrire des programmes plus longs et plus complexes, il est temps de parler du style de codage. La plupart des langages peuvent être écrits (ou plutôt formatés) selon différents styles ; certains sont plus lisibles que d’autres. Rendre la lecture de votre code plus facile aux autres est toujours une bonne idée, et adopter un bon style de codage peut énormément vous y aider.


* **Utilisez des indentations de 4 espaces et pas de tabulations**. 4 espaces constituent un bon compromis entre une indentation courte (qui permet une profondeur d’imbrication plus importante) et une longue (qui rend le code plus facile à lire). Les tabulations introduisent de la confusion et doivent être proscrites autant que possible.


* Faites en sorte que **les lignes ne dépassent pas 79 caractères**, au besoin en insérant des retours à la ligne (**actuellement cela a évolué vers 127**). Vous facilitez ainsi la lecture pour les utilisateurs qui n’ont qu’un petit écran et, pour les autres, cela leur permet de visualiser plusieurs fichiers côte à côte.


* **Utilisez des lignes vides pour séparer les fonctions et les classes**, ou pour scinder de gros blocs de code à l’intérieur de fonctions.


* Lorsque c’est possible, **placez les commentaires sur leurs propres lignes**.


* **Utilisez les chaînes de documentation**.


* **Utilisez des espaces autour des opérateurs et après les virgules**, mais pas juste à l’intérieur des parenthèses : `a = f(1, 2) + g(3, 4)`.


* **Nommez toujours vos classes et fonctions de la même manière** ; la convention est d’utiliser une notation [UpperCamelCase](https://medium.com/@anthowelc/c-est-quoi-le-camelcase-7fa02dc7fcee) pour **les classes**, et **minuscules_avec_trait_bas** pour **les fonctions et méthodes**. Utilisez toujours `self` comme **nom du premier argument des méthodes** (voyez Une première approche des classes pour en savoir plus sur les classes et les méthodes).


* N’utilisez pas d’encodage exotique dès lors que votre code est censé être utilisé dans des environnements internationaux. Par défaut, Python travaille en [UTF-8](https://www.w3.org/International/questions/qa-what-is-encoding.fr). Préférez les caractères du simple [ASCII](https://fr.wikibooks.org/wiki/Les_ASCII_de_0_%C3%A0_127/La_table_ASCII) pour votre code. N’utilisez pas de caractères exotiques lorsque votre code est censé être utilisé dans des environnements internationaux.

## Utilisation de Python comme calculatrice

L’interpréteur agit comme une simple calculatrice. Vous pouvez lui entrer une expression et il vous affiche la valeur.

Distribuer document codage_nombres.pdf.

La syntaxe des expressions est simple, les opérateurs `+`, `-`, `\*` et `/` fonctionnent comme dans la plupart des langages (par exemple, Pascal ou C) ; les parenthèses peuvent être utilisées pour faire des regroupements. Par exemple :

```
>>> 2 + 2
4
>>> 50 - 5 \ 6
20
>>> (50 - 5 * 6) / 4
5.0
>>> 8 / 5  # la division retourne toujours un nombre à virgule flottant
1.6
```

Les nombres entiers (comme 2, 4, 20) sont de type **int**, alors que les décimaux (comme 5.0, 1.6) sont de type **float**.

Les **divisions** `/` donnent toujours des **float**.

Utilisez **l’opérateur** `//` pour effectuer des **divisions entières**, afin d’obtenir un résultat entier.

Pour obtenir **le reste** d’une division entière, utilisez **l’opérateur** `%` :

```
>>> 17 / 3 # la division classique renvoie un nombre à virgule flottante
5.666666666666667
>>> 17 // 3  # division entière, ne tient pas compte du reste
5
>>> 17 % 3  # l’opérateur % retourne le reste de la division
2
>>> 5 * 3 + 2  # le quotien * diviseur + reste
17
```

En Python, il est possible de calculer des puissances avec l’opérateur `\*\*` :

```
>>> 5 ** 2  # 5 au carré
25
>>> 2 ** 7  # 2 à la puissance 7
128
```

Les **nombres à virgule flottante** sont tout à fait admis (Python utilise le point «**.**» comme séparateur entre la partie entière et la partie décimale des nombres, c’est la convention anglo-saxonne), **les opérateurs avec des opérandes de types différents convertissent l’opérande de type entier en type virgule flottante** :

```
>>> 4 * 3.75 - 1
14.0
```

En plus des **int** et des **float**, il existe les **Décimal** et les **Fraction** avec l’utilisation d’une bibliothèque.

Python gère aussi **les nombres complexes**, en utilisant le suffixe «**j**» ou «**J**» pour indiquer la partie imaginaire (tel que `3+5j`).

```
>>> (3+5j) * (3-5j)
(34+0j)
>>> _.conjugate()
(34+0j)
>>> (34+0j).real
34.0
>>> 34 + 0j
(34+0j)
>>> _.imag
0.0
```

## Les variables

### L’affectation dans le code

Le signe égal `=` est utilisé pour affecter une valeur à une variable.

Dans ce cas, aucun résultat n’est affiché avant l’invite suivante :

```
>>> largeur = 20
>>> hauteur = 5 * 9
>>> largeur * hauteur
900
```

Si une variable n’est pas définie (si aucune valeur ne lui a été affectée), son utilisation produit une erreur :

```
>>> n # Essaye d'accéder à une variable non définie
Traceback (most recent call last):
File "<input>", line 1, in <module>
 n # Essaye d'accéder à une variable non définie
NameError: name 'n' is not defined
```

En mode interactif, la dernière expression affichée est affectée à la variable `_`. Ainsi, lorsque vous utilisez Python comme calculatrice, cela vous permet de continuer des calculs facilement, par exemple :

```
>>> taxe = 12.5 / 100
>>> prix = 100.50
>>> prix * taxe
12.5625
>>> prix + _
113.0625
>>> round(_, 2)
113.06
```

Cette variable doit être considérée comme une variable en lecture seule par l’utilisateur. N’affectez pas de valeur explicitement à `_`. Vous créeriez ainsi une variable locale indépendante, avec le même nom, qui masquerait la variable native et son fonctionnement magique.

### L’affectation au clavier

#### La fonction input

```
>>> nom = input('Saisissez votre nom : ')
Saisissez votre nom : PERSONNE
>>> 'Bonjour ' + nom
'Bonjour PERSONNE'
```

### L’affectation par variables passées à un script

#### Passage d’arguments en ligne de commande

Python supporte complètement la création de programmes qui peuvent être lancés en ligne de commande, à l’aide d’arguments et de drapeaux longs ou cours pour spécifier diverses options.

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ mkdir 4_Passage_paramètres ; cd 4_Passage_paramètres
utilisateur@MachineUbuntu:~/repertoire_de_developpement/4_Passage_paramètres$ nano litparams.py ; chmod u+x litparams.py
```

```
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys

for arg in sys.argv:
    print(arg)
```

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement/4_Passage_paramètres$ ./litparams.py -a --help bidon
```

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement/4_Passage_paramètres$ nano litargs.py ; chmod u+x litargs.py
```

```
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys

print('Nombre d\'arguments :', len(sys.argv), 'arguments.')
print('Liste des arguments :', str(sys.argv))
```

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement/4_Passage_paramètres$ ./litargs.py -a --help bidon ; cd ..
```

## L’affichage d’informations

Il existe bien des moyens de présenter les sorties d’un programmes ; les données peuvent être affichées sous une forme lisible par un être humain ou sauvegardées dans un fichier pour une utilisation future. Cette partie présente quelques possibilités.

### À l’écran du terminal

```
print()
```

#### Formatage de données

Souvent vous voudrez plus de contrôle sur le formatage de vos sorties chaîne de caractères et aller au delà d’un affichage de valeurs séparées par des espaces. **Il y a plusieurs moyens de formater ces chaînes de caractères pour l’affichage**

##### Les expressions formatées f' {} '

Commencez une chaîne de caractère avec **f** ou **F** avant d’ouvrir vos guillemets doubles ou simple. Dans ces chaînes de caractère, vous pouvez entrer des expressions Python entre les accolades `{}` qui peuvent contenir des variables ou des valeurs littérales.

```
>>> année = 2005
>>> évènement = 'Sky'
>>> f'En {année} : {évènement}'
'En 2005 : Sky'
```

##### La méthode str.format()

Les chaînes de caractères exige un plus grand effort manuel. Vous utiliserez toujours les accolades `{}` pour indiquer où une variable sera substituée et suivant des détails sur son formatage. Vous devrez également fournir les informations à formater.

```
>>> votes_oui = 42572654
>>> votes_non = 43132495
>>> pourcentage = votes_oui / (votes_oui + votes_non)
>>> '{:-9} votes OUI {:2.2%}'.format(votes_oui, pourcentage)
' 42572654 votes OUI 49.67%'
```

##### Concaténations de tranches de chaînes

Enfin, vous pouvez construire des concaténations de chaînes vous-même et modifier leur format texte, et ainsi créer n’importe quel agencement.

Le type des chaînes a des méthodes utiles pour aligner des chaînes, pour afficher suivant une taille fixe, suivant la casse, etc.

```
>>> s = 'coucou mon texte'
>>> dir(s)
['__add__', '__class__', '__contains__', '__delattr__', '__dir__',   '__doc__', '__eq__', '__format__', '__ge__', '__getattribute__', '__getitem__', '__getnewargs__', '__gt__', '__hash__', '__init__', '__init_subclass__', '__iter__', '__le__', '__len__', '__lt__', '__mod__', '__mul__', '__ne__', '__new__', '__reduce__', '__reduce_ex__', '__repr__', '__rmod__', '__rmul__', '__setattr__', '__sizeof__', '__str__', '__subclasshook__', 'capitalize', 'casefold', 'center', 'count', 'encode', 'endswith', 'expandtabs', 'find', 'format', 'format_map', 'index', 'isalnum', 'isalpha', 'isascii', 'isdecimal', 'isdigit', 'isidentifier', 'islower', 'isnumeric', 'isprintable', 'isspace', 'istitle', 'isupper', 'join', 'ljust', 'lower', 'lstrip', 'maketrans', 'partition', 'replace', 'rfind', 'rindex', 'rjust', 'rpartition', 'rsplit', 'rstrip', 'split', 'splitlines', 'startswith', 'strip', 'swapcase', 'title', 'translate', 'upper', 'zfill']
```

La bibliothèque de fonctions «**string**» contient une classe **Template** qui permet aussi de remplacer des valeurs au sein de chaînes de caractères, en utilisant des marqueurs comme `$x`, et en les remplaçant par les valeurs d’un dictionnaire, mais sa capacité à formater les chaînes est plus limitée.

#### La sortie en chaînes de caractères

Lorsqu’un affichage basique suffit, pour afficher simplement une variable pour en inspecter le contenu, vous pouvez convertir n’importe quelle valeur ou objet en chaîne de caractères en utilisant la fonction `repr()` ou la fonction `str()`.

**La fonction** `str()` est destinée à représenter les valeurs sous une forme lisible par un être humain.

**La fonction** `repr()` est destinée à générer des représentations qui puissent être lues par l’interpréteur (ou qui lèvera une **SyntaxError** s’il n’existe aucune syntaxe équivalente).

Pour les objets qui n’ont pas de représentation humaine spécifique, `str()` renvoie la même valeur que `repr()`.

Beaucoup de valeurs, comme les nombres ou les structures telles que les listes ou les dictionnaires, ont la même représentation en utilisant les deux fonctions. Les chaînes de caractères, en particulier, ont deux représentations distinctes.

Quelques exemples :

```
>>> s = 'Bonjour à tous :-)'
>>> str(s)
'Bonjour à tous :-)'
>>> repr(s)
"'Bonjour à tous :-)'"
>>> str(1/7)
'0.14285714285714285'
>>> x = 10 * 3.25
>>> y = 200 * 200
>>> s = 'La valeur de x est ' + repr(x) + ', et celle de y est ' + repr(y) + '...'
>>> print(s)
La valeur de x est 32.5, et celle de y est 40000...
>>> # repr() ajoute les guillemets et les barres obliques inverses d'une chaîne
>>> salut = 'Bonjour à tous :-)\n'
>>> salutations = repr(salut)
>>> print(salutations)
'Bonjour à tous :-)\n'
>>> # L'argument de repr() peut être n'importe quel objet Python
>>> repr((x, y, ('bidon', 'œufs')))
"(32.5, 40000, ('bidon', 'œufs'))"
```

#### Les *chaînes* de caractères formatées (f-strings)

**Les chaînes de caractères formatées** `f''` (aussi appelées **f-strings**) vous permettent d’inclure la valeur d’expressions Python dans des chaînes de caractères en les préfixant avec «**f**» ou «**F**» et écrire des expressions comme {expression}.

L’expression peut être suivie d’un spécificateur de format. Cela permet un plus grand contrôle sur la façon dont la valeur est rendue. L’exemple suivant arrondit pi à trois décimales après la virgule :

```
>>> import math
>>> print(f'La valeur de pi est approximativement {math.pi:.3f}.')
La valeur de pi est approximativement 3.142.
```

Donner un entier après les “:” `f'{variable:10}'` indique la largeur minimale de ce champ en nombre de caractères. C’est utile pour faire de jolis tableaux :

```
>>> table = {'Sylvie': 4127, 'Jacques': 4098, 'David': 7678}
>>> for nom, téléphone in table.items():
...     print(f'{nom:10} ==> {téléphone:10d}')
...
...
Sylvie     ==>       4127
Jacques    ==>       4098
David      ==>       7678
```

D’autres modificateurs peuvent être utilisés pour convertir la valeur avant son formatage. «**!a**» applique la fonction `ascii()`, «**!s**» applique la fonction :`str()`, et «**!r**» applique la fonction `repr()` :

```
>>> animaux = 'rats Womp'
>>> print(f'Mon aéroglisseur est plein de {animaux}.')
Mon aéroglisseur est plein de rats Womp.
>>> print(f'Mon aéroglisseur est plein de {animaux!r}.')
Mon aéroglisseur est plein de 'rats Womp'.
```

Pour plus d’informations sur ces spécifications de formats, voir dans le guide Python en ligne «[Mini-langage de spécification de format](https://docs.python.org/fr/3/library/string.html#formatspec)».

#### La méthode de chaîne de caractères format()

L’utilisation de base de la méthode str.format() ressemble à ceci :

```
>>> print('Le {} nous dit "{}!"'.format('Sith', 'utilise le coté obscur de la force'))
Le Sith nous dit "utilise le coté obscur de la force!"
```

Les accolades et les caractères à l’intérieur (appelés les champs de formatage) sont remplacés par les objets passés en paramètres à la méthode `str.format()`. Un nombre entre accolades se réfère à la position de l’objet passé à la méthode `str.format()`.

```
>>> print('{0} et {1}'.format('bidon', 'pub'))
bidon et pub
>>> print('{1} et {0}'.format('bidon', 'pub'))
pub et bidon
```

Si des arguments nommés sont utilisés dans la méthode str.format(), leurs valeurs sont utilisées en se basant sur le nom des arguments

```
>>> print('Cet aliment {nourriture} est {qualité}.'.format(nourriture = 'hamburger', qualité='chimique'))
Cet aliment hamburger est chimique.
```

Les arguments positionnés et nommés peuvent être combinés arbitrairement :

```
>>> print('L’histoire de {0}, {1}, et {autre}.'.format('Bernard', 'Martin', autre='George'))
L’histoire de Bernard, Martin, et George.
```

Si vous avez une chaîne de formatage vraiment longue que vous ne voulez pas découper, il est possible de référencer les variables à formater par leur nom plutôt que par leur position. Utilisez simplement un dictionnaire et la notation entre crochets «**[]**» pour accéder aux clés.

```
>>> table = {'Sylvie': 4127, 'Jacques': 4098, 'Daniel': 8637678}
>>> print('Jacques: {0[Jacques]:d}; Sylvie: {0[Sylvie]:d}; ' 'Daniel: {0[Daniel]:d}'.format(table))
Jacques: 4098; Sylvie: 4127; Daniel: 8637678
```

Vous pouvez obtenir le même résultat en passant le tableau comme des arguments nommés en utilisant la notation «**\*\***».

```
>>> table = {'Sylvie': 4127, 'Jacques': 4098, 'Daniel': 8637678}
>>> print('Jacques: {Jacques:d}; Sylvie: {Sylvie:d}; Daniel: {Daniel:d}'.format(**table))
Jacques: 4098; Sylvie: 4127; Daniel: 8637678
```

C’est particulièrement utile en combinaison avec la fonction native `vars()` qui renvoie un dictionnaire contenant toutes les variables locales.

A titre d’exemple, les lignes suivantes produisent un ensemble de colonnes alignées de façon ordonnée donnant les entiers, leurs carrés et leurs cubes :

```
>>> for x in range(1, 11):
...     print('{0:2d} {1:3d} {2:4d}'.format(x, x*x, x*x*x))
...
...
1   1    1
2   4    8
3   9   27
4  16   64
5  25  125
6  36  216
7  49  343
8  64  512
9  81  729
10 100 1000
```

Pour avoir une description complète du formatage des chaînes de caractères avec la méthode `str.format()`, lisez «Syntaxe de formatage de chaîne».

### Logs Système

#### Utiliser logging

Nous allons aborder ici en avance un module *logging* qui fournit un ensemble de fonctions pour une utilisation simple d’affichages de logs dans une application avec une possibilité de filtrage de la verbosité.
Ces fonctions sont `debug()`, `info()`, `warning()`, `error()` et `critical()`.

Pour déterminer quand employer la journalisation, voyez la table ci-dessous, qui vous indique, pour chaque tâche parmi les plus communes, l’outil approprié.

#### Choix du niveau d’information

| Tâche que vous souhaitez mener

 | Le meilleur outil pour cette tâche

 |
| ------------------------------------------------------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |  |  |  |  |  |  |
| Affiche la sortie console d’un script en ligne de commande ou d’un programme lors de son utilisation ordinaire

                                                | [print()](https://docs.python.org/fr/3/library/functions.html#print)

                                                                                                                                                                                                                                                                                           |
| Rapporter des évènements qui ont lieu au cours du fonctionnement normal d’un programme (par exemple pour suivre un statut ou examiner des dysfonctionnements)

 | [logging.info()](https://docs.python.org/fr/3/library/logging.html#logging.info)

ou

[logging.debug()](https://docs.python.org/fr/3/library/logging.html#logging.debug)

pour une sortie très détaillée à visée diagnostique

                                                                                                                                                                                                          |
| Émettre un avertissement (*warning* en anglais) en relation avec un évènement particulier au cours du fonctionnement d’un programme

                             | [warnings.warn()](https://docs.python.org/fr/3/library/warnings.html#warnings.warn)

dans le code de la bibliothèque si le problème est évitable et l’application cliente doit être modifiée pour éliminer cet avertissement

[logging.warning()](https://docs.python.org/fr/3/library/logging.html#logging.warning)

si l’application cliente ne peut rien faire pour corriger la situation mais l’évènement devrait quand même être noté

 |
| Rapporter une erreur lors d’un évènement particulier en cours d’exécution

                                                                                     | Lever une exception

                                                                                                                                                                                                                                                                               |
| Rapporter la suppression d’une erreur sans lever d’exception (par exemple pour la gestion d’erreur d’un processus de long terme sur un serveur)

               | [logging.error()](https://docs.python.org/fr/3/library/logging.html#logging.error),

[logging.exception()](https://docs.python.org/fr/3/library/logging.html#logging.exception)

ou [logging.critical()](https://docs.python.org/fr/3/library/logging.html#logging.critical),

au mieux, selon l’erreur spécifique et le domaine d’application

                                                                                                                                                                    |
Les fonctions de journalisation sont nommées d’après le niveau ou la sévérité des évènements qu’elles suivent. Les niveaux standards et leurs applications sont décrits ci-dessous (par ordre croissant de sévérité) :

#### Typologie des journalisations

| Niveau

                                                                                                                                                        | Pourqoi c’est utilisé

                                                                                                                                                                                                                                                                             |
| ------------------------------------------------------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| **DEBUG**

                                                                                                                                                         | Information détaillée, intéressante seulement lorsqu’on diagnostique un problème.

                                                                                                                                                                                                                 |
| **INFO**

                                                                                                                                                          | Confirmation que tout fonctionne comme prévu.

                                                                                                                                                                                                                                                     |
| **WARNING**

                                                                                                                                                       | L’indication que quelque chose d’inattendu a eu lieu, ou de la possibilité d’un problème dans un futur proche (par exemple « espace disque faible »). Le logiciel fonctionne encore normalement.

                                                                                                  |
| **ERROR**

                                                                                                                                                         | Du fait d’un problème plus sérieux, le logiciel n’a pas été capable de réaliser une tâche.

                                                                                                                                                                                                        |
| **CRITICAL**

                                                                                                                                                      | Une erreur sérieuse, indiquant que le programme lui-même pourrait être incapable de continuer à fonctionner.

                                                                                                                                                                                      |
Le niveau par défaut est WARNING, ce qui signifie que seuls les évènements de ce niveau et au-dessus sont suivis, sauf si le paquet logging est configuré pour faire autrement.

Les évènements suivis peuvent être gérés de différentes façons. La manière la plus simple est de les afficher dans la console. Une autre méthode commune est de les écrire dans un fichier.

#### Un exemple simple

Un exemple très simple est :

```
>>> import logging
>>> logging.warning('Attention!') # affiche un message dans la console
WARNING:root:Attention!
>>> logging.info('C’est bon relâche la pression') # n’imprime rien
```

Si vous entrez ces lignes dans un script que vous exécutez, vous verrez `WARNING:root:Attention!` » affiché dans la console. Le message INFO n’apparaît pas parce que le niveau par défaut est WARNING. Le message affiché inclut l’indication du niveau et la description de l’évènement fournie dans l’appel à logging, ici «Attention!». Ne vous préoccupez pas de la partie «root» pour le moment : nous détaillerons ce point plus bas. La sortie elle-même peut être formatée de multiples manières si besoin. Les options de formatage seront aussi expliquées plus bas.

#### Enregistrer les évènements dans un fichier

Il est très commun d’enregistrer les évènements dans un fichier, c’est donc ce que nous allons regarder maintenant. Il faut essayer ce qui suit avec un interpréteur Python nouvellement démarré, ne poursuivez pas la session commencée ci-dessus :

```
>>> import logging
>>> logging.basicConfig(filename='./exemple.log', encoding='utf-8', level=logging.DEBUG)
>>> logging.debug('Ce message doit aller dans le fichier journal')
>>> logging.info('Celui là aussi')
>>> logging.warning('Et encore celui-ci')
>>> logging.error('Et des trucs non-ASCII aussi, comme Fêtes de Noël')
```

**Modifié dans la version 3.9**: L’argument d’encodage a été ajouté.

Dans les versions antérieures de Python ou lorsqu’il n’est pas spécifié, l’encodage utilisé est la valeur par défaut utilisée par `open()`.
Bien que cela ne soit pas montré dans l’exemple ci-dessus, un argument d’erreurs peut également maintenant être passé, qui détermine comment les erreurs de codage sont gérées. Pour les valeurs disponibles et les valeurs par défaut, consultez la documentation de `open()`.

Maintenant, si nous ouvrons le fichier «**exemple.log**» et lisons ce qui s’y trouve, on trouvera les messages de log :

```
DEBUG:root:Ce message doit aller dans le fichier journal
INFO:root:Celui là aussi
WARNING:root:Et encore celui-ci
ERROR:root:Et des trucs non-ASCII aussi, comme Fêtes de Noël
```

Cet exemple montre aussi comment on peut régler le niveau de journalisation qui sert de seuil pour le suivi. Dans ce cas, comme nous avons réglé le seuil à DEBUG, tous les messages ont été écrits.

#### Régler le niveau de journalisation d’un script

Si vous souhaitez régler le niveau de journalisation à partir d’une option de la ligne de commande comme :

```
--log=INFO
```

Créer avec votre éditeur de texte le fichier «**niveau_journalisation.py**».

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ mkdir 5_Niveau_journalisation ; cd 5_Niveau_journalisation
utilisateur@MachineUbuntu:~/repertoire_de_developpement/5_Niveau_journalisation$ nano niveau_journalisation.py ; chmod u+x niveau_journalisation.py
```

```
#! /usr/bin/env python3
# -*- coding: utf8 -*-

import argparse, logging

# Récupère l'argument de la ligne de commande du paramètre log et le met dans la variable loglevel
params = argparse.ArgumentParser()
params.add_argument('--log')
args = params.parse_args()
loglevel = args.log

# Défini le niveau de journalisation
if loglevel:
    numeric_level = getattr(logging, loglevel.upper())
else:
    numeric_level = logging.DEBUG

# Teste si le paramètre est valide
if not isinstance(numeric_level, int):
    raise ValueError('Niveau de journalisation invalide : %s' % loglevel)

# Configure le niveau de journalisation et le fichier où journaliser
logging.basicConfig(filename='niveau.log', filemode='w', level=numeric_level)

# Messages de tests
logging.error('Message erreur')
logging.warning('Message alerte')
logging.info('Message information')
logging.debug('Message debug')
```

Vous passez la valeur du paramètre donné à l’option `--log` dans une variable **loglevel**. L’appel à `basicConfig()` doit être fait avant un appel à `debug()`, `info()`, etc. Si vous exécutez le script plusieurs fois **sans l’option «filemode»**, les messages des exécutions successives sont ajoutés au fichier «**niveau.log**».

Si vous voulez que chaque exécution reprenne un fichier vierge, sans conserver les messages des exécutions précédentes, vous devez spécifier l’argument **filemode** à **'w'**. Le texte n’est plus ajouté au fichier de log, donc les messages des exécutions précédentes sont perdus.

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement/5_Niveau_journalisation$ ./niveau_journalisation.py
utilisateur@MachineUbuntu:~/repertoire_de_developpement/5_Niveau_journalisation$ cat niveau.log
ERROR:root:Message erreur
WARNING:root:Message alerte
INFO:root:Message information
DEBUG:root:Message debug
utilisateur@MachineUbuntu:~/repertoire_de_developpement/5_Niveau_journalisation$ python3 ./niveau_journalisation.py --log=DEBUG
utilisateur@MachineUbuntu:~/repertoire_de_developpement/5_Niveau_journalisation$ cat niveau.log
ERROR:root:Message erreur
WARNING:root:Message alerte
INFO:root:Message information
DEBUG:root:Message debug
utilisateur@MachineUbuntu:~/repertoire_de_developpement/5_Niveau_journalisation$ python3 ./niveau_journalisation.py --log=INFO
utilisateur@MachineUbuntu:~/repertoire_de_developpement/5_Niveau_journalisation$ cat niveau.log
ERROR:root:Message erreur
WARNING:root:Message alerte
INFO:root:Message information
utilisateur@MachineUbuntu:~/repertoire_de_developpement/5_Niveau_journalisation$ python3 ./niveau_journalisation.py --log=WARNING
utilisateur@MachineUbuntu:~/repertoire_de_developpement/5_Niveau_journalisation$ cat niveau.log
ERROR:root:Message erreur
WARNING:root:Message alerte
utilisateur@MachineUbuntu:~/repertoire_de_developpement/5_Niveau_journalisation$ python3 ./niveau_journalisation.py --log=ERROR
utilisateur@MachineUbuntu:~/repertoire_de_developpement/5_Niveau_journalisation$ cat niveau.log
ERROR:root:Message erreur
utilisateur@MachineUbuntu:~/repertoire_de_developpement/5_Niveau_journalisation$ python3 ./niveau_journalisation.py --log=BIDON ; cd ..
Traceback (most recent call last):
  File "./niveau_journalisation.py", line 14, in <module>
    numeric_level = getattr(logging, loglevel.upper())
AttributeError: module 'logging' has no attribute 'BIDON'
```

#### Modifier le format du message affiché

Pour changer le format utilisé pour afficher le message, vous devez préciser le format que vous souhaitez employer :

```
>>> import logging
>>> logging.basicConfig(format='%(levelname)s:%(message)s', level=logging.DEBUG)
>>> logging.debug('Message d’analyse de code')
DEBUG:Message d’analyse de code
>>> logging.basicConfig(format='Mon programme %(levelname)s:%(lineno)d:%(pathname)s:%(message)s', level=logging.DEBUG, force=True)
>>> logging.debug('Message d’analyse de code')
Mon programme DEBUG:1:<bpython-input-7>:Message d’analyse de code
>>> logging.info('Message d’information')
Mon programme INFO:1:<bpython-input-8>:Message d’information
>>> logging.warning('Attention!')
Mon programme WARNING:1:<bpython-input-9>:Attention!
```

### GUI

Gui intégré client lourd (Windows, gtk, qt, etc.).

Gui web (remi, django, etc.).

On verra cela plus loin dans le cours.

## Les types de variables

### Logique

#### Booléen

```
>>> a = True
>>> type(a)
<class 'bool'>
>>> b = False
>>> type(b)
<class 'bool'>
```

Les tests

```
>>> a = 10 > 9
>>> a
True
>>> a = 10 == 9
>>> a
False
```

Les valeurs vraie

```
>>> bool("abc")
True
>>> bool(123)
True
>>> bool(["apple", "cherry", "banana"])
True
```

Les valeurs Nules

```
bool(False)
bool(None)
bool(0)
bool("")
bool(())
bool([])
bool({})
```

### Chaînes de caractères

#### Chaîne de caractère ASCII

Python 3

```
byte()
```

Python 2

```
str()
```

#### Chaîne de caractère Unicode

Python 3

```
str()
```

Python 2

```
unicode()
```

#### Manipulation des chaînes de caractères

Python sait manipuler des chaînes de caractères, qui peuvent être exprimées de différentes manières. **Elles peuvent être écrites entre guillemets anglo-saxon simples ('…') ou entre guillemets anglo-saxon doubles ("…") sans distinction. \\ peut aussi être utilisé pour protéger un guillemet** :

```
>>> 'inutile bidon' # simples quotes
'inutile bidon'
>>> 'L\'apostrophe' # utilise \\' pour échapper le simple quote…
"L'apostrophe"
>>> "L'apostrophe" # …ou on utilise des doubles quotes
"L'apostrophe"
>>> 'Son nom est "Personne"'
'Son nom est "Personne"'
>>> "Son nom est \"Personne\""
'Son nom est "Personne"'
>>> 'Python c’est "l\'avenir"'
'Python c’est "l\'avenir"'
```

En mode interactif, l’interpréteur affiche les chaînes de caractères entre guillemets. Les guillemets et autres caractères spéciaux sont protégés avec des barres obliques inverses (backslash en anglais). Bien que cela puisse être affiché différemment de ce qui a été entré (les guillemets peuvent changer), les deux formats sont équivalents. La chaîne est affichée entre guillemets si elle contient un guillemet simple et aucun guillemet, sinon elle est affichée entre guillemets simples. **La fonction print() affiche les chaînes de manière plus lisible, en retirant les guillemets et en affichant les caractères
spéciaux qui étaient protégés par une barre oblique inverse** :

```
>>> print('Python c’est "l\'avenir"')
Python c’est "l'avenir"
>>> s = 'Première ligne.\nSeconde ligne.' # \n c’est nouvelle ligne
>>> s # sans print(), \n est incluse dans la sortie
'Première ligne.\nSeconde ligne.'
>>> print(s) # avec print(), \n est traduit comme une nouvelle ligne
Première ligne.
Seconde ligne.
```

**Si vous ne voulez pas que les caractères précédés d’un \\ soient interprétés** comme étant spéciaux, utilisez les chaînes brutes (raw strings en anglais) en préfixant la chaîne d’un **r** :

```
>>> print('C:\son\nom') # \n veut dire nouvelle ligne!
C:\son
om
>>> print(r'C:\son\nom') # avec r avant le quote
C:\son\nom
```

**Les chaînes de caractères peuvent s’étendre sur plusieurs lignes. Utilisez alors des triples guillemets, simples ou doubles : '''…''' ou """…"""**. Les retours à la ligne sont automatiquement inclus, mais on peut l’empêcher en ajoutant \\ à la fin de la ligne. L’exemple suivant :

```
>>> print("""\
... Utilisation: programme [OPTIONS]
...     -h            Affiche ce message d’utilisation
...     -H nomMachine Nom de la machine où se connecter
... """)
```

produit l’affichage suivant (notez que le premier retour à la ligne n’est pas inclus) :

```
Utilisation: programme [OPTIONS]
    -h            Affiche ce message d’utilisation
    -H nomMachine Nom de la machine où se connecter
```

**Les chaînes peuvent être concaténées** (collées ensemble) avec l’opérateur «**+**» et **répétées** avec l’opérateur «**\***» :

```
>>> # 2 fois 'an', suivit par 'as'
>>> 2 * 'an' + 'as'
'ananas'
```

Plusieurs **chaînes de caractères**, écrites littéralement (c’est-à-dire entre guillemets), **côte à côte, sont automatiquement concaténées**.

```
>>> 'Py' 'thon'
'Python'
```

Cette fonctionnalité est surtout intéressante pour couper des chaînes trop longues :

```
>>> texte = ('Mettez plusieurs chaînes entre les parenthèses '
... 'pour les avoir réunis.')
>>> texte
'Mettez plusieurs chaînes entre les parenthèses pour les avoir réunis.'
```

**Cela ne fonctionne cependant qu’avec les chaînes littérales, pas avec les variables ni les expressions** :

```
>>> prefixe = 'Py'
>>> prefixe 'thon' # impossible de concaténer une variable et une chaîne littérale
File "<input>", line 1
 prefixe 'thon' # impossible de concaténer une variable et une chaîne littérale
         ^
SyntaxError: invalid syntax
>>> ('un' * 3) 'ium'
File "<input>", line 1
 ('un' * 3) 'ium'
            ^
SyntaxError: invalid syntax
```

Pour **concaténer des variables**, ou des variables avec des chaînes littérales, utilisez l’opérateur «**+**» :

```
>>> prefixe + 'thon'
'Python'
```

**Les chaînes de caractères peuvent être indexées** (ou indicées, c’est-à-dire que l’on peut accéder aux caractères par leur position), le premier caractère d’une chaîne étant à la position 0. Il n’existe pas de type distinct pour les caractères, un caractère est simplement une chaîne de longueur 1 :

Pour visualiser la façon dont les indices fonctionnent

```
Position :   1   2   3   4   5   6
           +---+---+---+---+---+---+
           | P | y | t | h | o | n |
           +---+---+---+---+---+---+
Indice :     0   1   2   3   4   5
```

Exemples :

```
>>> mot = 'Python'
>>> mot[0] # caractère en position 1
'P'
>>> mot[5] # caractère en position 6
'n'
```

Les indices peuvent également être négatifs, on compte alors en partant de la droite. Par exemple :

Pour visualiser la façon dont les indices négatifs fonctionnent

```
Position :   1   2   3   4   5   6
           +---+---+---+---+---+---+
           | P | y | t | h | o | n |
           +---+---+---+---+---+---+
Indice :    -6  -5  -4  -3  -2  -1
```

Exemples :

```
>>> mot[-1] # dernier caractère
'n'
>>> mot[-2] # avant-dernier caractère
'o'
>>> mot[-6]
'P'
```

Notez que, comme -0 égale 0, les indices négatifs commencent par -1.

En plus d’accéder à un élément par son indice, il est aussi possible de « trancher » (slice en anglais) une chaîne. Accéder à une chaîne par un indice permet d’obtenir un caractère, trancher permet d’obtenir une sous-chaîne :

Pour mémoriser la façon dont les tranches fonctionnent, vous pouvez imaginer que les indices pointent entre les caractères, le côté gauche du premier caractère ayant la position 0. Le côté droit du dernier caractère d’une chaîne de n caractères a alors pour indice n.

```
Position :   1   2   3   4   5   6
           +---+---+---+---+---+---+
           | P | y | t | h | o | n |
           +---+---+---+---+---+---+
           0   1   2   3   4   5   6
          -6  -5  -4  -3  -2  -1
```

La première ligne de nombres donne la position des indices 0…6 dans la chaîne ; la deuxième ligne donne l’indice négatif correspondant. La tranche de i à j est constituée de tous les caractères situés entre les bords libellés i et j, respectivement.

Pour des indices non négatifs, la longueur d’une tranche est la différence entre ces indices, si les deux sont entre les bornes. Par exemple, la longueur de mot[1:3] est 2.

Exemples :

```
>>> mot[0:2] # caractères de la position 1 (inclus) à 3 (exclus)
'Py'
>>> mot[2:5] # caractères de la position 3 (inclus) à 6 (exclus)
'tho'
>>> mot[-6:-4] # caractères de la position 1 (inclus) à 3 (exclus)
'Py'
>>> mot[-4:-1] # caractères de la position 3 (inclus) à 6 (exclus)
'tho'
```

**Notez que le début est toujours inclus et la fin toujours exclue**. Cela assure que s[:i] + s[i:] est toujours égal à s :

```
>>> mot[:2] + mot[2:]
'Python'
>>> mot[:4] + mot[4:]
'Python'
```

Les valeurs par défaut des indices de tranches ont une utilité ; le premier indice vaut zéro par défaut (c.-à-d. lorsqu’il est omis), le deuxième correspond par défaut à la taille de la chaîne de caractères

```
>>> mot[:2] # caractère du début à la position 3 (exclu)
'Py'
>>> mot[4:] # caractères de la position 5 (inclus) à la fin
'on'
>>> mot[-2:] # caractères de l'avant-dernier (inclus) à la fin
'on'
```

Utiliser un indice trop grand produit une erreur :

```
>>> mot[42] # le mot n'a que 6 caractères
Traceback (most recent call last):
File "<input>", line 1, in <module>
 mot[42] # le mot n'a que 6 caractères
IndexError: string index out of range
```

Cependant, les indices hors bornes sont gérés silencieusement lorsqu’ils sont utilisés dans des tranches :

```
>>> mot[4:42]
'on'
>>> mot[42:]
''
```

Les chaînes de caractères, en Python, ne peuvent pas être modifiées. On dit qu’elles sont immuables. Affecter une nouvelle valeur à un indice dans une chaîne produit une erreur :

```
>>> mot[0] = 'J'
Traceback (most recent call last):
File "<input>", line 1, in <module>
 mot[0] = 'J'
TypeError: 'str' object does not support item assignment
>>> mot[2:] = 'py'
Traceback (most recent call last):
File "<input>", line 1, in <module>
 mot[2:] = 'py'
TypeError: 'str' object does not support item assignment
```

Si vous avez besoin d’une chaîne différente, vous devez en créer une nouvelle :

```
>>> 'J' + mot[1:]
'Jython'
>>> mot[:2] + 'py'
'Pypy'
```

La fonction native len() renvoie la longueur d’une chaîne :

```
>>> s = 'anticonstitutionnellement'
>>> len(s)
25
```

### Nombres

Vu avec la calculatrice python

#### Nombre entier optimisé (int)

Python 2

```
int()
```

#### Nombre entier de taille arbitraire (long int)

Python 3

```
int()
```

Python 2

```
long()
```

#### Nombre à virgule flottante

```
float()
```

#### Nombre complexe

```
complex()
```

### Données multiples

Python connaît différents types de données combinés, utilisés pour regrouper plusieurs valeurs.

#### Liste de longueur fixe

```
tuple()
```

le tuple (ou n-uplet, dénomination que nous utiliserons dans la suite de cette documentation).

Un n-uplet consiste en différentes valeurs séparées par des virgules, par exemple :

```
>>> t = 12345, 54321, 'hello!'
>>> t[0]
12345
>>> t
(12345, 54321, 'hello!')
>>> # Les tuples peuvent être imbriqués
>>> u = t, (1, 2, 3, 4, 5)
>>> u
((12345, 54321, 'hello!'), (1, 2, 3, 4, 5))
>>> # Les tuples sont immuables
>>> t[0] = 88888
Traceback (most recent call last):
File "<input>", line 1, in <module>
 t[0] = 88888
TypeError: 'tuple' object does not support item assignment
>>> # mais ils peuvent contenir des objets mutables
>>> v = ([1, 2, 3], [3, 2, 1])
>>> v
([1, 2, 3], [3, 2, 1])
```

Comme vous pouvez le voir, les n-uplets sont toujours affichés entre parenthèses, de façon à ce que des n-uplets imbriqués soient interprétés correctement ; ils peuvent être saisis avec ou sans parenthèses, même si celles-ci sont souvent nécessaires (notamment lorsqu’un n-uplet fait partie d’une expression plus longue). Il n’est pas possible d’affecter de valeur à un élément d’un n-uplet ; par contre, il est possible de créer des n-uplets contenant des objets muables, comme des listes.

Si les n-uplets peuvent sembler similaires aux listes, ils sont souvent utilisés dans des cas différents et pour des raisons différentes. Les n-uplets sont immuables et contiennent souvent des séquences hétérogènes d’éléments qui sont accédés par « dissociation » (unpacking en anglais, voir plus loin) ou par indice (ou même par attributs dans le cas des namedtuples). Les listes sont souvent muables et contiennent des éléments généralement homogènes qui sont accédés par itération sur la liste.

Un problème spécifique est la construction de n-uplets ne contenant aucun ou un seul élément : la syntaxe a quelques tournures spécifiques pour s’en accommoder. Les n-uplets vides sont construits par une paire de parenthèses vides ; un n-uplet avec un seul élément est construit en faisant suivre la valeur par une virgule (il n’est pas suffisant de placer cette valeur entre parenthèses). Pas très joli, mais efficace.

Par exemple :

```
>>> vide = ()
>>> singleton = 'bonjour', # <-- noter la virgule de fin
>>> len(vide)
0
>>> len(singleton)
1
>>> singleton
('bonjour',)
```

L’instruction `t = 12345, 54321, 'hello !'` est un exemple d’une agrégation de n-uplet (tuple packing en anglais) : les valeurs «12345», «54321» et «hello !» sont agrégées ensemble dans un n-uplet. L’opération inverse est aussi possible :

```
>>> x, y, z = t
```

Ceci est appelé, de façon plus ou moins appropriée, une distribution de séquence (sequence unpacking en anglais) et fonctionne pour toute séquence placée à droite de l’expression. Cette distribution requiert autant de variables dans la partie gauche qu’il y a d’éléments dans la séquence. Notez également que cette affectation multiple est juste une combinaison entre une agrégation de n-uplet et une distribution de séquence.

#### Les ensembles

```
set()
```

Python fournit également un type de donnée pour les ensembles. Un ensemble est une collection non ordonnée sans élément dupliqué. Des utilisations basiques concernent par exemple des tests d’appartenance ou des suppressions de doublons. Les ensembles savent également effectuer les opérations mathématiques telles que les unions, intersections, différences et différences symétriques.

Des accolades ou la fonction set() peuvent être utilisés pour créer des ensembles. **Notez que pour créer un ensemble vide, {} ne fonctionne pas**, cela crée un dictionnaire vide. **Utilisez plutôt set()**.

Voici une brève démonstration :

```
>>> panier = {'pomme', 'orange', 'pomme', 'poire', 'orange', 'banane'}
>>> print(panier) # montre que les doublons ont été supprimés
{'poire', 'pomme', 'banane', 'orange'}
>>> 'orange' in panier # test d'adhésion rapide
True
>>> 'digitaire' in panier # la digitaire est une plante
False
>>> # Démontrer les opérations d'ensemble sur des lettres uniques à partir de deux mots
>>> a = set('abracadabra')
>>> b = set('alacazam')
>>> a # lettres uniques dans a
{'b', 'a', 'c', 'r', 'd'}
>>> a - b # lettres en a mais pas en b
{'r', 'b', 'd'}
>>> a | b # lettres en a ou b ou les deux
{'b', 'a', 'c', 'r', 'm', 'l', 'z', 'd'}
>>> a & b # lettres en a et b
{'a', 'c'}
>>> a ^ b # lettres en a ou b mais pas les deux
{'r', 'b', 'm', 'l', 'z', 'd'}
```

Il est possible d’écrire des expressions dans des ensembles :

```
>>> a = {x for x in 'abracadabra' if x not in 'abc'}
>>> a
{'r', 'd'}
```

#### Liste de longueur variable

```
list()
```

Le plus souple est la liste, qui peut être écrit comme une suite, placée entre crochets, de valeurs (éléments) séparées par des virgules. Les listes et les chaînes de caractères ont beaucoup de propriétés en commun, comme l’indiçage et les opérations sur des tranches. Les éléments d’une liste ne sont pas obligatoirement tous du même type, bien qu’à l’usage ce soit souvent le cas.

```
>>> carrés = [1, 4, 9, 16, 25]
>>> carrés
[1, 4, 9, 16, 25]
```

Comme les chaînes de caractères (et toute autre type de séquence), les listes peuvent être indicées et découpées :

```
>>> carrés[0] # l'indexation renvoie l'élément
1
>>> carrés[-1]
25
>>> carrés[-3:] # slicing renvoie une nouvelle liste
[9, 16, 25]
```

Toutes les opérations par tranches renvoient une nouvelle liste contenant les éléments demandés. Cela signifie que l’opération suivante renvoie une copie distincte de la liste :

```
>>> carrés[:]
[1, 4, 9, 16, 25]
```

Les listes gèrent aussi les opérations comme les concaténations :

```
>>> carrés + [36, 49, 64, 81, 100]
[1, 4, 9, 16, 25, 36, 49, 64, 81, 100]
```

Mais à la différence des chaînes qui sont immuables, les listes sont muables : il est possible de modifier leur contenu :

```
>>> cubes = [1, 8, 27, 63, 125] # Quelque chose ne va pas ici
>>> 4 ** 3 # le cube de 4 est 64, pas 63!
64
>>> cubes[3] = 64 # remplacer la mauvaise valeur
>>> cubes
[1, 8, 27, 64, 125]
```

Il est aussi possible d’ajouter de nouveaux éléments à la fin d’une liste avec la méthode append() (les méthodes sont abordées plus tard) :

```
>>> cubes.append(216) # ajouter le cube de 6
>>> cubes.append(7 ** 3) # et le cube de 7
>>> cubes
[1, 8, 27, 64, 125, 216, 343]
```

Des affectations de tranches sont également possibles, ce qui peut même modifier la taille de la liste ou la vider complètement :

```
>>> lettres = ['a', 'b', 'c', 'd', 'e', 'f', 'g']
>>> lettres
['a', 'b', 'c', 'd', 'e', 'f', 'g']
>>> # remplacer certaines valeurs
>>> lettres[2:5] = ['C', 'D', 'E']
>>> lettres
['a', 'b', 'C', 'D', 'E', 'f', 'g']
>>> # maintenant supprimez-les
>>> lettres[2:5] = []
>>> lettres
['a', 'b', 'f', 'g']
>>> # effacer la liste en remplaçant tous les éléments par une liste vide
>>> lettres[:] = []
>>> lettres
[]
```

La primitive len() s’applique aussi aux listes :

```
>>> lettres = ['a', 'b', 'c', 'd']
>>> len(lettres)
4
```

Il est possible d’imbriquer des listes (c’est à dire de créer des listes contenant d’autres listes).

Par exemple :

```
>>> a = ['a', 'b', 'c']
>>> n = [1, 2, 3]
>>> x = [a, n]
>>> x
[['a', 'b', 'c'], [1, 2, 3]]
>>> x[0]
['a', 'b', 'c']
>>> x[0][1]
'b'
```

##### Premiers pas vers la programmation

Bien entendu, on peut utiliser Python pour des tâches plus compliquées que d’additionner deux et deux. Par exemple, on peut écrire le début de la suite de Fibonacci comme ceci :

```
>>> # Série de Fibonacci
>>> # la somme de deux éléments définit le suivant
>>> a, b = 0, 1
>>> while a < 10:
... print(a)
... a, b = b, a+b
...
...
0
1
1
2
3
5
8
```

Cet exemple introduit plusieurs nouvelles fonctionnalités.

**La première ligne contient une affectation multiple** : les variables a et b se voient affecter simultanément leurs nouvelles valeurs 0 et 1. Cette méthode est encore utilisée à la dernière ligne, pour démontrer que les expressions sur la partie droite de l’affectation sont toutes évaluées avant que les affectations ne soient effectuées. Ces expressions en partie droite sont toujours évaluées de la gauche vers la droite.

**La boucle while s’exécute tant que la condition** (ici : a < 10) reste vraie. En Python, comme en C, tout entier différent de zéro est vrai et zéro est faux. La condition peut aussi être une chaîne de caractères, une liste, ou en fait toute séquence ; une séquence avec une valeur non nulle est vraie, une séquence vide est fausse. Le test utilisé dans l’exemple est une simple comparaison. Les opérateurs de comparaison standards sont écrits comme en C : < (inférieur), > (supérieur), == (égal), <= (inférieur ou égal), >= (supérieur ou égal) et != (non égal).

**Le corps de la boucle est indenté** : l’indentation est la méthode utilisée par Python pour regrouper des instructions. En mode interactif, vous devez saisir une tabulation ou des espaces pour chaque ligne indentée. En pratique, vous aurez intérêt à utiliser un éditeur de texte pour les saisies plus compliquées ; tous les éditeurs de texte dignes de ce nom disposent d’une fonction d’auto-indentation. Lorsqu’une expression composée est saisie en mode interactif, elle doit être suivie d’une ligne vide pour indiquer qu’elle est terminée (car l’analyseur ne peut pas deviner que vous venez de saisir la dernière ligne). Notez bien que toutes les lignes à l’intérieur d’un bloc doivent être indentées au même niveau.

**La fonction print() écrit les valeurs des paramètres qui lui sont fournis**. Ce n’est pas la même chose que d’écrire l’expression que vous voulez afficher (comme nous l’avons fait dans l’exemple de la calculatrice), en raison de la manière qu’a **print()** de gérer les paramètres multiples, les nombres décimaux et les chaînes. Les chaînes sont affichées sans apostrophe et une espace est insérée entre les éléments de telle sorte que vous pouvez facilement formater les choses, comme ceci :

```
>>> i = 256*256
>>> print('La valeur de i est', i)
La valeur de i est 65536
```

Le paramètre nommé **end** peut servir pour enlever le retour à la ligne ou pour terminer la ligne par une autre chaîne :

```
>>> a, b = 0, 1
>>> while a < 1000:
... print(a, end=',')
... a, b = b, a+b
...
...
0,1,1,2,3,5,8,13,21,34,55,89,144,233,377,610,987,>>>
```

Lexique à distribuer :

Le type liste dispose de méthodes supplémentaires. Voici toutes les méthodes des objets de type liste :

```
list.append(x)
```

Ajoute un élément à la fin de la liste. Équivalent à a[len(a):] = [x].

```
list.extend(iterable)
```

Étend la liste en y ajoutant tous les éléments de l’itérable. Équivalent à a[len(a):] = iterable.

```
list.insert(i, x)
```

Insère un élément à la position indiquée. Le premier argument est la position de l’élément courant avant lequel l’insertion doit s’effectuer, donc a.insert(0, x) insère l’élément en tête de la liste et a.insert(len(a), x) est équivalent à a.append(x).

```
list.remove(x)
```

Supprime de la liste le premier élément dont la valeur est égale à x. Une exception ValueError est levée s’il n’existe aucun élément avec cette valeur.

```
list.pop([i])
```

Enlève de la liste l’élément situé à la position indiquée et le renvoie en valeur de retour. Si aucune position n’est spécifiée, a.pop() enlève et renvoie le dernier élément de la liste (les crochets autour du i dans la signature de la méthode indiquent que ce paramètre est facultatif et non que vous devez placer des crochets dans votre code ! Vous retrouverez cette notation fréquemment dans le Guide de Référence de la Bibliothèque Python).

```
list.clear()
```

Supprime tous les éléments de la liste. Équivalent à del a[:].

```
list.index(x[, start[, end]])
```

Renvoie la position du premier élément de la liste dont la valeur égale x (en commençant à compter les positions à partir de zéro). Une exception ValueError est levée si aucun élément n’est trouvé.

Les arguments optionnels start et end sont interprétés de la même manière que dans la notation des tranches et sont utilisés pour limiter la recherche à une sous-séquence particulière. L’indice renvoyé est calculé relativement au début de la séquence complète et non relativement à start.

```
list.count(x)
```

Renvoie le nombre d’éléments ayant la valeur x dans la liste.

```
list.sort(key=None, reverse=False)
```

Ordonne les éléments dans la liste (les arguments peuvent personnaliser l’ordonnancement, voir sorted() pour leur explication).

```
list.reverse()
```

Inverse l’ordre des éléments dans la liste.

```
list.copy()
```

Renvoie une copie superficielle de la liste. Équivalent à a[:].

Exemple suivant utilise la plupart des méthodes des listes :

```
>>> fruits = ['orange', 'pomme', 'poire', 'banane', 'kiwi', 'pomme', 'banane']
>>> fruits.count('pomme')
2
>>> fruits.count('mandarine')
0
>>> fruits.index('banane')
3
>>> fruits.index('banane', 4) # Trouver la prochaine banane à partir d'une position 4
6
>>> fruits.reverse()
>>> fruits
['banane', 'pomme', 'kiwi', 'banane', 'poire', 'pomme', 'orange']
>>> fruits.append('raisin')
>>> fruits
['banane', 'pomme', 'kiwi', 'banane', 'poire', 'pomme', 'orange', 'raisin']
>>> fruits.sort()
>>> fruits
['banane', 'banane', 'kiwi', 'orange', 'poire', 'pomme', 'pomme', 'raisin']
>>> fruits.pop()
'raisin'
>>> fruits
['banane', 'banane', 'kiwi', 'orange', 'poire', 'pomme', 'pomme']
```

Vous avez probablement remarqué que les méthodes telles que **insert**, **remove** ou **sort**, qui ne font que modifier la liste, n’affichent pas de valeur de retour (elles renvoient None) 1. C’est un principe respecté par toutes les structures de données variables en Python.

Une autre chose que vous remarquerez peut-être est que toutes les données ne peuvent pas être ordonnées ou comparées. Par exemple, [None, “hello”, 10] ne sera pas ordonné parce que les entiers ne peuvent pas être comparés aux chaînes de caractères et None ne peut pas être comparé à d’autres types. En outre, il existe certains types qui n’ont pas de relation d’ordre définie. Par exemple, 3+4j < 5+7j n’est pas une comparaison valide.

Approfondir chez soit ou au travail voir [https://docs.python.org/fr/3/tutorial/datastructures.html#using-lists-as-stacks](https://docs.python.org/fr/3/tutorial/datastructures.html#using-lists-as-stacks) et la suite

#### Dictionnaire

```
dict()
```

Un autre type de donnée très utile, natif dans Python, est le dictionnaire (voir Les types de correspondances — **dict**). Ces dictionnaires sont parfois présents dans d’autres langages sous le nom de « mémoires associatives » ou de « tableaux associatifs ». À la différence des séquences, qui sont indexées par des nombres, **les dictionnaires sont indexés par des clés**, qui peuvent être de n’importe quel type immuable ; les chaînes de caractères et les nombres peuvent toujours être des clés. Des n-uplets peuvent être utilisés comme clés s’ils ne contiennent que des chaînes, des nombres ou des n-uplets ; si un n-uplet contient un objet muable, de façon directe ou indirecte, il ne peut pas être utilisé comme une clé. Vous ne pouvez pas utiliser des
listes comme clés, car les listes peuvent être modifiées en place en utilisant des affectations par position, par tranches ou via des méthodes comme **append()** ou **extend()**.

Le plus simple est de considérer les dictionnaires comme des ensembles de paires clé: valeur, les clés devant être uniques (au sein d’un dictionnaire). Une paire d’accolades crée un dictionnaire vide : {}.
Placer une liste de paires clé:valeur séparées par des virgules à l’intérieur des accolades ajoute les valeurs correspondantes au dictionnaire ; c’est également de cette façon que les dictionnaires sont affichés.

Les opérations classiques sur un dictionnaire consistent à stocker une valeur pour une clé et à extraire la valeur correspondant à une clé. Il est également possible de supprimer une paire clé-valeur avec **del**.
Si vous stockez une valeur pour une clé qui est déjà utilisée, l’ancienne valeur associée à cette clé est perdue. Si vous tentez d’extraire une valeur associée à une clé qui n’existe pas, une exception est levée.

Exécuter **list(d)** sur un dictionnaire d renvoie une liste de toutes les clés utilisées dans le dictionnaire, dans l’ordre d’insertion (si vous voulez qu’elles soient ordonnées, utilisez **sorted(d))**. Pour tester si une clé est dans le dictionnaire, utilisez le mot-clé in.

Voici un petit exemple utilisant un dictionnaire :

```
>>> téléphone = {'daniel': 4098, 'paul': 4139}
>>> téléphone['luc'] = 4127
>>> téléphone
{'daniel': 4098, 'paul': 4139, 'luc': 4127}
>>> téléphone['daniel']
4098
>>> del téléphone['paul']
>>> téléphone['sami'] = 4127
>>> téléphone
{'daniel': 4098, 'luc': 4127, 'sami': 4127}
>>> list(téléphone)
['daniel', 'luc', 'sami']
>>> sorted(téléphone)
['daniel', 'luc', 'sami']
>>> 'luc' in téléphone
True
>>> 'daniel' not in téléphone
False
```

Le constructeur **dict()** fabrique un dictionnaire directement à partir d’une liste de paires clé-valeur stockées sous la forme de n-uplets :

```
>>> dict([('paul', 4139), ('luc', 4127), ('daniel', 4098)])
{'paul': 4139, 'luc': 4127, 'daniel': 4098}
```

De plus, il est possible de créer des dictionnaires par compréhension depuis un jeu de clef et valeurs :

```
>>> {x: x**2 for x in (2, 4, 6)}
{2: 4, 4: 16, 6: 36}
```

Lorsque les clés sont de simples chaînes de caractères, il est parfois plus facile de spécifier les paires en utilisant des paramètres nommés :

```
>>> dict(paul=4139, luc=4127, daniel=4098)
{'paul': 4139, 'luc': 4127, 'daniel': 4098}
```

### Autres

#### Fichier

```
File
```

#### Absence de type

```
NoneType
```

#### Absence d’implémentation

```
NotImplementedType
```

#### fonction

```
Function
```

#### module

```
module
```

## Les fonctions intégrées

Lexique pédagogique à fournir «Les fonctions de base» :

### Détermination du type d’une variable

```
type()
```

### Conversion de types

```
bool()
```

Convertit en booléen : `"0"`, `""` et `"None"` donnent `"False"` et le reste `"True"`.

```
int()
```

Permet de modifier une variable en entier. Provoque une erreur si cela n’est pas possible.

```
str()
```

Permet de transformer la plupart des variables d’un autre type en chaînes de caractère.

```
float()
```

Permet la transformation en flottant.

```
repr()
```

Similaire à « str ». Voir la partie sur les objets.

```
eval()
```

Évalue le contenu de son argument comme si c’était du code Python.

```
long() # Python 2
```

Transforme une valeur en long.

### Voir les propriétés des fonctions

#### Fonction d’aide sur les fonctions Python

```
help()
```

#### Fonction de visualisation des propriétés et méthodes des fonctions Python

```
dir()
```

La fonction interne dir() est utilisée pour trouver quels noms sont définis par un module. Elle donne une liste de chaînes classées par ordre lexicographique :

```
>>> import math, sys
>>> dir(math)
['__doc__', '__file__', '__loader__', '__name__', '__package__', '__spec__', 'acos', 'acosh', 'asin', 'asinh', 'atan', 'atan2', 'atanh', 'ceil', 'comb', 'copysign', 'cos', 'cosh', 'degrees', 'dist', 'e', 'erf', 'erfc', 'exp', 'expm1', 'fabs', 'factorial', 'floor', 'fmod', 'frexp', 'fsum', 'gamma', 'gcd', 'hypot', 'inf', 'isclose', 'isfinite', 'isinf', 'isnan', 'isqrt', 'lcm', 'ldexp', 'lgamma', 'log', 'log10', 'log1p', 'log2', 'modf', 'nan', 'nextafter', 'perm', 'pi', 'pow', 'prod', 'radians', 'remainder', 'sin', 'sinh', 'sqrt', 'tan', 'tanh', 'tau', 'trunc', 'ulp']
```

Sans paramètre, **dir()** liste les noms actuellement définis :

```
>>> a = [1, 2, 3, 4, 5]
>>> import math
>>> cos = math.cos
>>> dir()
['__builtins__', '__doc__', '__loader__', '__name__', '__package__', '__spec__', 'a', 'b', 'cos', 'cubes', 'fruits', 'help', 'i', 'lettres', 'math', 'n', 'sys', 'téléphone', 'x']
```

Notez qu’elle liste tous les types de noms : les variables, fonctions, modules, etc.

**dir()** ne liste ni les fonctions primitives, ni les variables internes. Si vous voulez les lister, elles sont définies dans le module **builtins** :

```
>>> import builtins
>>> dir(builtins)
['ArithmeticError', 'AssertionError', 'AttributeError', 'BaseException', 'BlockingIOError', 'BrokenPipeError', 'BufferError', 'BytesWarning', 'ChildProcessError', 'ConnectionAbortedError', 'ConnectionError', 'ConnectionRefusedError', 'ConnectionResetError', 'DeprecationWarning', 'EOFError', 'Ellipsis', 'EnvironmentError', 'Exception', 'False', 'FileExistsError', 'FileNotFoundError', 'FloatingPointError', 'FutureWarning', 'GeneratorExit',   'IOError', 'ImportError', 'ImportWarning', 'IndentationError', 'IndexError', 'InterruptedError', 'IsADirectoryError', 'KeyError', 'KeyboardInterrupt', 'LookupError', 'MemoryError', 'ModuleNotFoundError', 'NameError', 'None', 'NotADirectoryError', 'NotImplemented', 'NotImplementedError', 'OSError', 'OverflowError', 'PendingDeprecationWarning', 'PermissionError', 'ProcessLookupError', 'RecursionError', 'ReferenceError', 'ResourceWarning', 'RuntimeError', 'RuntimeWarning', 'StopAsyncIteration', 'StopIteration', 'SyntaxError', 'SyntaxWarning', 'SystemError', 'SystemExit', 'TabError', 'TimeoutError', 'True', 'TypeError', 'UnboundLocalError', 'UnicodeDecodeError', 'UnicodeEncodeError', 'UnicodeError', 'UnicodeTranslateError', 'UnicodeWarning', 'UserWarning', 'ValueError', 'Warning', 'ZeroDivisionError', '_', '__build_class__', '__debug__', '__doc__', '__import\__', '__loader__', '__name__', '__package__', '__spec__', 'abs', 'all', 'any', 'ascii', 'bin', 'bool', 'breakpoint', 'bytearray', 'bytes', 'callable', 'chr', 'classmethod', 'compile', 'complex', 'copyright', 'credits', 'delattr', 'dict', 'dir', 'divmod', 'enumerate', 'eval', 'exec', 'exit', 'filter', 'float', 'format', 'frozenset', 'getattr', 'globals', 'hasattr', 'hash', 'help', 'hex', 'id', 'input', 'int', 'isinstance', 'issubclass', 'iter', 'len', 'license', 'list', 'locals', 'map', 'max', 'memoryview', 'min', 'next', 'object', 'oct', 'open', 'ord', 'pow', 'print', 'property', 'quit', 'range', 'repr', 'reversed', 'round', 'set', 'setattr', 'slice', 'sorted', 'staticmethod', 'str', 'sum', 'super', 'tuple', 'type', 'vars', 'zip']
```

#### L’identification des objets

```
id()
```

```
>>> id(cos)
140293507063376
```

### Zoom sur fonctions et propriétés

#### Range

Si vous devez itérer sur une suite de nombres, la fonction native **range()** est faite pour cela. Elle génère des suites arithmétiques :

```
>>> for i in range(5):
... print(i)
...
...
0
1
2
3
4
```

Le dernier élément fourni en paramètre ne fait jamais partie de la liste générée ; **range(10)** génère une liste de 10 valeurs, dont les valeurs
vont de **0 à 9**. Il est possible de spécifier une valeur de début et une valeur d’incrément différentes (y compris négative pour cette dernière, que l’on appelle également parfois le “pas”) :

```
>>> tuple(range(5, 10))
(5, 6, 7, 8, 9)
>>> tuple(range(0, 10, 3))
(0, 3, 6, 9)
>>> tuple(range(-10, -100, -30))
(-10, -40, -70)
```

Une chose étrange se produit lorsqu’on affiche un range :

```
>>> print(range(10))
range(0, 10)
```

L’objet renvoyé par **range()** se comporte presque comme une liste, mais ce n’en est pas une. Cet objet génère les éléments de la séquence au fur et à mesure de l’itération, sans réellement produire la liste en tant que telle, économisant ainsi de l’espace.

On appelle de tels objets des **iterable**, c’est-à-dire des objets qui conviennent à des fonctions ou constructions qui s’attendent à quelque chose duquel ils peuvent tirer des éléments, successivement, jusqu’à épuisement. Nous avons vu que l’instruction **for** est une de ces constructions, et un exemple de fonction qui prend un itérable en paramètre est sum() :

```
>>> sum(range(4))  # 0 + 1 + 2 + 3
6
```

Plus loin nous voyons d’autres fonctions qui donnent des itérables ou en prennent en paramètre. Si vous vous demandez comment obtenir une liste à partir d’un range, voilà la solution :

```
>>> list(range(4))
[0, 1, 2, 3]
```

#### Chaînes de caractères

##### split() : sépare une chaîne en liste

Fractionne une chaîne de caractères Python suivant un délimiteur. Si le paramètre du nombre de divisions est spécifié split() retourne seulement dans la liste les premiers élément fractionnés suivant la quantité demandée.

**Syntaxe :**

```
str.split(str="", num=string.count(str))
```

**Paramètres :**


* str : séparateur, espaces par défaut.


* num : le nombre de divisions.

**Valeur de retour :**

> Renvoie une liste de chaînes après division.

Exemples

L’exemple suivant montre la distribution de **split()** :

```
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

chaine = "Ligne1-abcdef \\nLigne2-abc \\nLigne3-abcd"
print(chaine.split())
print(chaine.split(' ', 1))
```

Exemple du résultat de sortie ci-dessus :

```
['Ligne1-abcdef', 'Ligne2-abc', 'Ligne3-abcd']
['Ligne1-abcdef', '\nLigne2-abc \\nLigne3-abcd']
```

##### join() : Concatène une liste de caractères

Transforme une liste en chaîne avec le séparateur en préfixe (`"".join(MaListe)`).

**Syntaxe :**

```
string.join(iterable)
```

**Paramètres :**

La méthode join() prend un seul paramètre.


* iterable(Obligatoire) : Tout objet itérable où toutes les valeurs renvoyées sont des chaînes

**Valeur de retour :**

> La méthode join() renvoie une chaîne créée en joignant les éléments d’un itérable par un séparateur.

Exemple

Joindre tous les éléments d’un tuple dans une chaîne, en utilisant le caractère «|» comme séparateur :

```
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

tupl = ("Python", "Rust", "Julia")
résultat = "|".join(tupl)
print(résultat)
```

Sortie :

```
Python|Rust|Julia
```

Joignez tous les éléments d’un dictionnaire dans une chaîne, en utilisant le caractère «#» comme séparateur :

```
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

dict = {"nom": "Alexandre", "age": "25"}
résultat = "#".join(dict)
print(résultat)
résultat = "#".join(dict.values())
print(résultat)
```

Sortie :

```
nom#age
Alexandre#25
```

## Les modules

Lorsque vous quittez et entrez à nouveau dans l’interpréteur Python, tout ce que vous avez déclaré dans la session précédente est perdu. Afin de rédiger des programmes plus longs, vous devez utiliser un éditeur de texte, préparer votre code dans un fichier et exécuter Python avec ce fichier en paramètre. Cela s’appelle créer un script. Lorsque votre programme grandit, vous pouvez séparer votre code dans plusieurs fichiers. Ainsi, il vous est facile de réutiliser du code écrit pour un programme dans un autre sans avoir à les copier.

Pour gérer cela, Python vous permet de placer des définitions dans un fichier et de les utiliser dans un script ou une session interactive. Un tel fichier est appelé un module et les définitions d’un module peuvent être importées dans un autre module ou dans le module main (qui est le module qui contient vos variables et définitions lors de l’exécution d’un script au niveau le plus haut ou en mode interactif).

Un module est un fichier contenant des définitions et des instructions (des fonctions, des classes et des variables.). Son nom de fichier est le nom du module suffixé de «**.py**».

À l’intérieur d’un module, son propre nom est accessible par la variable `__name__`. Ce module, avec ses variables, fonctions ou classes, peut être chargé à partir d’un autre module ; c’est ce que l’on appelle l’importation.

### __name__ et __main__

Lorsque l’interpréteur exécute un module, la variable **__name__** sera définie comme **__main__** si le module en cours d’exécution est le programme principal.

```
>>> print(" __name__ est défini à {}".format(__name__))
__name__ est défini à __main__
```

Mais si le code importe le module depuis un autre module, la variable **__name__** sera définie sur le nom de ce module. Jetons un œil à un exemple.

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ mkdir 7_Modules ; cd 7_Modules
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ nano ./mon_module.py
```

Créez un module Python nommé **mon_module.py** et saisissez ce code à l’intérieur:

```
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Module de fichier Python
print("Mon module __name__ est défini à {}".format(__name__))
```

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ python3
```

```
Python 3.9.4 (default, Apr 4 2021, 19:38:44)
[GCC 10.2.1 20210401] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> import mon_module
Mon module __name__ est défini à mon_module
>>> quit()
```

### Gestion des imports

La façon habituelle d’utiliser **__name__** et **__main__** ressemble à ceci avec le script **mon_module_2.py**:

```
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Module de fichier Python
print("Mon module __name__ est défini à {}".format(__name__))
if __name__ == "__main__":
    print("Fichier exécuté directement")
else:
    print("Fichier exécuté comme importé")
```

Ce qui nous donne à l’exécution directe du module python :

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ chmod u+x ./mon_module_2.py
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./mon_module_2.py
Mon module __name__ est défini à __main__
Fichier exécuté directement
```

Et à l’exécution comme module :

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ python3
```

```
Python 3.9.4 (default, Apr 4 2021, 19:38:44)
[GCC 10.2.1 20210401] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> import mon_module_2
Mon module __name__ est défini à mon_module
Fichier exécuté comme importé
>>> quit()
```

## Les bibliothèques de fonctions ou d’objets

### Les modules PYTHON

Lexique pédagogique à fournir **Les bibliothèques de fonctions ou
d’objets.**

### Le dépôt de modules Python

#### Pip

Une des forces de **Python** est la multitude de bibliothèques disponibles (près de 6000 bibliothèques gravitent autour du projet **Django**).

Par exemple installer une bibliothèque peut vite devenir ennuyeux:


* trouver le bon site,


* la bonne version de la bibliothèque,


* l’installer,


* trouver ses dépendances,


* etc.

Il existe une solution qui vous permet de télécharger très simplement une bibliothèque **pip**.

##### PIP c’est quoi ?

**Pip** est un **système de gestion de paquets** utilisé pour installer et gérer des librairies écrites en Python. Vous pouvez trouver une grande partie de ces librairies dans le [Python Package Index](https://pypi.python.org/pypi) (ou PyPI). **Pip** empêche les installations partielles en annonçant toutes les exigences avant l’installation.

```
pip install librairie
```

Vous pouvez choisir la version qui vous intéresse :

```
pip install librairie==2.2
```

Supprimer une librairie :

```
pip uninstall librairie
```

Mettre à jour une librairie :

```
pip install librairie --upgrade
```

Revenir sur une version antérieure :

```
pip install librairie==2.1 --upgrade
```

Rechercher une nouvelle librairie :

```
pip search librairie
```

Vous indiquer quelles librairies ne sont plus à jour :

```
pip list --outdated
```

Afficher toutes les librairies installées et leur version :

```
pip freeze
```

Exporter la liste des librairies, vous pourrez la réimporter ailleurs :

```
pip freeze > lib.txt
```

Importer la liste de librairie comme ceci :

```
pip install -r lib.txt
```

Créer un gros zip qui contient toutes les dépendances :

```
pip bundle <nom_du_bundle>.pybundle -r lib.txt
```

Pour installer les librairies :

```
pip install <nom_du_bundle>.pybundle
```

Pour installer depuis un dépôt distant ([Voir la section du support VCS](https://pip.pypa.io/en/stable/reference/pip_install/#vcs-support)) :

```
pip install git+https://github.com/chemin/monmodule.git#egg=monmodule
```

Pour le lien ver le support VCS : [https://pip.pypa.io/en/stable/reference/pip_install/#vcs-support](https://pip.pypa.io/en/stable/reference/pip_install/#vcs-support)

Voir plus d’informations [https://docs.python.org/fr/3.6/installing/index.html](https://docs.python.org/fr/3.6/installing/index.html)

### Les modules de gestion des paramètres de la ligne de commande

#### Modules dépréciés

Python fourni un module **getopt**(déprécié depuis Python 3.7) ou **optparse** (déprécié depuis Python 3.2) qui vous aident à analyser les options et les arguments de la ligne de commande. Le module **getopt** fournit deux fonctions et une exception pour activer l’analyse des arguments de ligne de commande.

Supposons que nous voulions passer deux noms de fichiers via la ligne de commande et que nous voulions également donner une option pour vérifier l’utilisation du script. L’utilisation en ligne de commande du script est la suivante :

```
test.py -i <fichier_en_entrée> -o <fichier_de_sortie>
```

```
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys, getopt

def main(argv):
    fichierentre = ''
    fichiersortie = ''
    try:
        opts, args = getopt.getopt(argv,"hi:o:",["ifile=","ofile="])
    except getopt.GetoptError:
        print('utilisation : test.py -i <fichier_en_entrée> -o <fichier_de_sortie>')
        sys.exit(2)

    for opt, arg in opts:
        if opt == '-h':
            print('utilisation : test.py -i <fichier_en_entrée> -o <fichier_de_sortie>')
            sys.exit()
        elif opt in ("-i", "--ifile"):
            fichierentre = arg
        elif opt in ("-o", "--ofile"):
            fichiersortie = arg

    print('Le fichier en entré est', fichierentre)
    print('Le fichier en sortie est', fichiersortie)

if __name__ == "__main__":
    main(sys.argv[1:])
```

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ test.py -h
utilisation: test.py -i <fichier_en_entrée> -o <fichier_en_entrée>
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ test.py -i BMP -o
utilisation: test.py -i <fichier_en_entrée> -o <fichier_en_entrée>
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ test.py -i input.txt -o output.cvs
Le fichier en entré est input.txt
Le fichier en sortie est output.cvs
```

#### Argparse

Le [module argparse](https://docs.python.org/fr/3/library/argparse.html#module-argparse) remplace *getopt* et *optparse*. Il facilite l’écriture d’interfaces de ligne de commande conviviales. Le programme définit les arguments dont il a besoin et **argparse** trouvera comment les analyser à partir de **sys.argv**. Le module **argparse** génère également automatiquement des messages d’aide et d’utilisation, et émet des erreurs lorsque les utilisateurs donnent au programme des arguments non valides.

##### Utilisation du module


1. Construction du parseur.

> `parser = argparse.ArgumentParser(description = 'ma description')`

> On peut donner une description qui terminera dans l’aide d’usage.


1. Ajout d’un argument de la ligne de commande.

> `parser.add_argument('-foo')`

> `'-foo'` est un argument optionnel (non obligatoire). C’est parce que cela commence par «**-**» ou «**--**».

> Pour avoir un argument positionnel (obligatoire), saisir `'foo'`.


1. Parcourir les arguments.

> `args = parser.parse_args()`

> Agit automatiquement sur **sys.argv**

On peut alors accéder aux valeurs des arguments en faisant directement : `args.foo`

On peut aussi récupérer les valeurs sous forme de dictionnaire avec : `vars(args)`

On peut explicitement imprimer l’aide avec : `parser.print_help()`

On peut explicitement imprimer l’usage simplifié de la commande par : `parser.print_usage()`

##### Ajout d’options

`parser.add_argument('-f', '--foo')`

On peut utiliser l’arguments optionnel simplifié **-f** ou nommé **--foo** en ligne de commande.

`parser.add_argument('-foo', help='what -foo does', metavar='fValue')`

Nom de l’argument **-foo** dans les messages d’utilisation avec l’option `metavar=` ainsi que l’aide sur l’option avec `help=`.

`parser.add_argument('-foo', dest='fVal')`

La valeur pourra être accédée après parsing avec `args.fVal` plutôt qu’avec `args.foo`.

`parser.add_argument('-foo', required=True)`

L’argument est obligatoire.

`parser.add_argument('-foo', action='store_true')`

L’argument ne prend pas de valeur et renvoi **True** si présent (**False** sinon).

`parser.add_argument('-foo', action='append')`

L’argument renvoi une liste de valeurs avec autant d’éléments que le nombre de fois où l’argument est présent.

Par exemple : **2** valeurs si «**-foo a -foo b**».

`parser.add_argument('-foo', choices=['a', 'b', 'c'])`

L’argument doit prendre l’une des valeurs indiquée.

`parser.add_argument('-foo', default='test')`

Donne une valeur par défaut.

`parser.add_argument('-foo', type=int)`

Indique que l’argument doit être un entier plutôt qu’une chaîne de caractères (défaut).
On peut utiliser **int**, **float**, **str**, **complex**.

##### Exemples

Fichier **argparse1.py** :

Implémentation minimale.

```
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse

parser = argparse.ArgumentParser()
parser.parse_args()
```

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse1.py
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7\_Modules$ ./argparse1.py --help
usage: argparse1.py [-h]

optional arguments:
    -h, --help show this help message and exit
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse1.py foo
usage: argparse1.py [-h]
argparse1.py: error: unrecognized arguments: foo
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse1.py --verbose
usage: argparse1.py [-h]
argparse1.py: error: unrecognized arguments: --verbose
```

Voilà ce qu’il se passe :


1. Exécuter le script sans aucun paramètre a pour effet de ne rien afficher sur la sortie d’erreur. Ce n’est pas très utile.


2. La deuxième commande commence à montrer l’intérêt du module **argparse**. On n’a quasiment rien fait mais on a déjà un beau message d’aide . L’option **--help** (pas besoin de la préciser), que l’on peut aussi raccourcir en **-h**.


3. Préciser quoi que ce soit d’autre comme argument de la ligne de commande entraîne une erreur.


4. Même si on reçoit aussi un argument optionnel non défini.

Fichier **argparse2.py** :

Comment passer un argument de ligne de commande ?

```
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse

parser = argparse.ArgumentParser()
parser.add_argument("echo")
args = parser.parse_args()
print(args.echo)
```

On a ajouté la méthode `add_argument()` que l’on utilise pour préciser quels paramètres de lignes de commandes le programme peut accepter. Dans le cas présent, c’est **echo** pour que cela corresponde à sa fonction. Utiliser le programme nécessite maintenant que l’on précise un paramètre.

La méthode `parse_args()` renvoie les données des arguments de la ligne de commande, dans le cas présent : **echo**.

**argparse** affecte automatiquement la variable comme par «magie». C’est à dire que nous n’avons pas besoin de préciser dans quelle variable la valeur est stockée. Vous pouvez remarquer aussi que le nom de variable `args.echo` est le même que l’argument en chaîne de caractères donné à la méthode `add_argument("echo")`.

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse2.py
usage: argparse2.py [-h] echo
argparse2.py: error: the following arguments are required: echo
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse2.py --help
usage: argparse2.py [-h] echo

positional arguments:
 echo

optional arguments:
    -h, --help show this help message and exit
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse2.py monparamètre
monparamètre
```

Voilà ce qu’il se passe :


1. Sans arguments la commande renvoie l’aide simplifiée avec le message d’erreur.


2. Nous voyons l’aide du programme avec la commande «**--help**».


3. Avec le bon argument on affiche la valeur de l’argument saisie.

Notez cependant que, même si l’affichage d’aide paraît bien , il n’est pas aussi utile qu’il pourrait l’être. Par exemple, on peut lire que **echo** est un argument positionnel mais on ne peut pas savoir ce que cela fait autrement qu’en le devinant ou en lisant le code source.

Fichier **argparse3.py** :

Comment afficher une aide plus précise pour un argument de la ligne de commande ?

```
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse

parser = argparse.ArgumentParser()
parser.add_argument("echo", help="renvoi la valeur du paramètre que vous avez passé")
args = parser.parse_args()
print(args.echo)
```

Nous ajoutons simplement le paramètre `help=""` à la méthode `add_argument()`.

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse3.py -h
usage: argparse3.py [-h] echo

positional arguments:
    echo    echo renvoi la valeur du paramètre que vous avez passé

optional arguments:
    -h, --help show this help message and exit
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse3.py monparamètre
monparamètre
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse3.py monparamètre etunautre
usage: argparse3.py [-h] echo
argparse3.py: error: argument square: invalid int value: 'etunautre'
```


1. Nous observons bien que le message d’aide est plus précis.


2. Cela fonctionne avec une valeur atribuée au paramètre positioné «**echo**».


3. Cela ne prend qu’un paramètre.

Fichier **argparse4.py** :

Comment calculer le carré d’un nombre en ligne de commande ?

```
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse

parser = argparse.ArgumentParser()
parser.add_argument("carré", help="affiche le carré du nombre passé en argument")
args = parser.parse_args()
print(args.carré**2)
```

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse4.py 4
Traceback (most recent call last):
    File "./argparse4.py", line 8, in <module>
        print(args.carré**2)
    TypeError: unsupported operand type(s) for \*\* or pow(): 'str' and 'int'
```

Cela n’a pas très bien fonctionné. C’est parce que **argparse** traite les paramètres que l’on donne comme des chaînes de caractères, à moins qu’on ne lui indique de faire autrement.

Fichier **argparse5.py** :

Comment traiter le paramètre d’entrée comme un entier ?

```
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse

parser = argparse.ArgumentParser()
parser.add_argument("carré", help="affiche le carré du nombre passé en argument", type=int)
args = parser.parse_args()
print(args.carré**2)
```

Nous ajoutons simplement le paramètre `type=int` à la méthode `add_argument()`.

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse5.py 4
16
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7\_Modules$ ./argparse5.py quatre
usage: argparse5.py [-h] carré

argparse5.py: error: argument carré: invalid int value: 'quatre'
```

Cela a bien fonctionné. Maintenant le programme va même s’arrêter si l’entrée n’est pas un entier avant de procéder à l’exécution.

Fichier **argparse6.py** :

Comment ajouter un paramètre optionnel ?

```
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import** **argparse**

parser = argparse.ArgumentParser()
parser.add_argument("--verbosity", help="augmente la verbosité de sortie")
args = parser.parse_args()

if args.verbosity :
    print("verbosité activée")
```

On rajoute «**-**» ou «**--**» pour montrer que l’argument de ligne de commande est bien optionnel, il n’y aura alors pas d’erreur si on exécute le programme sans celui-ci.

Notez que par défaut, si une option n’est pas utilisée, la variable associée, dans le cas présent `args.verbosity`, prend la valeur `None`. C’est pour cela quelle échoue au test [if](https://docs.python.org/fr/3/reference/compound_stmts.html#if).

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse6.py --verbosity 1
verbosité activée
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse6.py
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse6.py --help
usage: argparse6.py [-h] [--verbosity VERBOSITY]

optional arguments:
    -h, --help            show this help message and exit
    --verbosity VERBOSITY
                          augmente la verbosité de sortie
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse6.py --verbosity
usage: argparse6.py [-h] [--verbosity VERBOSITY]
argparse6.py: error: argument --verbosity: expected one argument
```


1. Validation de la verbosité


2. La commande sans paramètre ne retourne rien et n’est pas en erreur.


3. Le message d’aide est un peu différent quand on utilise l’option **--verbosity** si on ne précise pas une valeur.


4. Le paramètre optionnel **--verbosity** demande impérativement une valeur d’attribution.

L’exemple ci-dessus accepte obligatoirement une valeur entière arbitraire pour **--verbosity**, mais seul l’état (vrai/faux) de présence du paramètre est réellement utile pour notre commande.

Fichier **argparse7.py** :

Comment prendre en compte l’état booléen de présence d’un argument ?

```
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse

parser = argparse.ArgumentParser()
parser.add_argument("--verbose", help="augmente la verbosité de sortie", action="store_true")
args = parser.parse_args()

if args.verbose:
    print("verbosité activée")
```

Notez que maintenant on précise avec le paramètre `action=` dans `add_argument()` un état booléen. Et on lui donne la valeur **"store_true"**. Cela signifie que si l’argument de ligne de commande est précisée, la valeur `True` est assignée à `args.verbose`. Ne rien préciser renvoie la valeur `False`.

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse7.py --verbose
verbosité activée
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse7.py
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse7.py --help
usage: argparse7.py [-h] [--verbose]

optional arguments:
    -h, --help    show this help message and exit
    --verbose     augmente la verbosité de sortie
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse7.py --verbose 1
usage: argparse7.py [-h] [--verbose]
argparse7.py: error: unrecognized arguments: 1
```


1. Maintenant le paramètre est plus une option qu’un paramètre qui nécessite une valeur. On a même changé le nom du paramètre pour qu’il corresponde à cette idée.


2. Pas d’aide retournée.


3. Notez que l’aide est différente avec l’option «**--verbose**».


4. Dans l’esprit de ce que sont vraiment les options de la ligne de commande, pas des paramètres, quand vous tentez de préciser une valeur de paramètre l’aide simplifiée d’usage et une erreur sont renvoyées.

Si vous êtes familier avec l’utilisation de la ligne de commande, vous avez dû remarquer que nous n’avons pas abordé les raccourcies des paramètres.

Fichier **argparse8.py** :

Comment ajouter un raccourcie de paramètre de la ligne de commande ?

```
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse

parser = argparse.ArgumentParser()
parser.add_argument("-v", "--verbose", help="augmente la verbosité de sortie", action="store_true")
args = parser.parse_args()

if args.verbose:
    print("verbosité activée")
```

Nous allons simplement en ajouter un au code avec `"-v"` en amont de `"--verbose"` dans les options de `add_argument()`. Sachez que le dernier paramètre saisi est la clé de paramètre, ici c’est `"--verbose"`, les autres en amont sont des raccourcies, ici `"-v"`.

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse8.py -v
verbosité activée
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse8.py --help
usage: argparse8.py [-h] [-v]

optional arguments:

    -h, --help    show this help message and exit
    -v, --verbose augmente la verbosité de sortie
```

Notez que la nouvelle option est aussi indiquée dans l’aide.

Fichier **argparse9.py** :

Comment maintenant ajouter un argument positionné (obligatoire) supplémentaire ?

```
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse

parser = argparse.ArgumentParser()
parser.add_argument("carré", type=int, help="affiche le carré du nombre passé en argument")
parser.add_argument("-v", "--verbose", action="store_true", help="augmente la verbosité de sortie")
args = parser.parse_args()
reponse = args.carré**2

if args.verbose:
    print("le carré de {} est égal à {}".format(args.carré, reponse))
else:
    print(reponse)
```

Nous avons ajouté un argument positionné de type entier «**carré**» dans le code en ajoutant un autre appel à la méthode `add_argument()`.

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse9.py
usage: argparse9.py [-h] [-v] carré
argparse9.py: error: the following arguments are required: carré
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse9.py 4
16
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse9.py 4 --verbose
le carré de 4 est égal à 16
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse9.py --verbose 4
le carré de 4 est égal à 16
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse9.py -v 4
le carré de 4 est égal à 16
```


1. L’option d’argument positionné apparaît dans l’aide, et on remarque que les argument optionnels sont entre crochets. L’argument positionné n’étant pas saisie l’aide renvoie un message d’erreur.


2. Le calcul de la valeur fonctionne bien


3. Notez que l’ordre importe peu avec les autres commandes passées.

Fichier **argparse10.py** :

Qu’en est-il si nous donnons à ce programme la possibilité d’avoir plusieurs niveaux de verbosité, et que celui-ci les prend en compte ?

```
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse

parser = argparse.ArgumentParser()
parser.add_argument("carré", type=int, help="affiche le carré du nombre passé en argument")
parser.add_argument("-v", "--verbosity", type=int, help="augmente la verbosité de sortie")
args = parser.parse_args()
reponse = args.carré**2

if args.verbosity == 2:
    print("le carré de {} est égal à {}".format(args.carré, reponse))
elif args.verbosity == 1:
    print("{}² = {}".format(args.carré, reponse))
else:
    print(reponse)
```

Ajout dans le code de tests de niveau de verbosité.

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$  ./argparse10.py 4
16
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse10.py 4 -v
usage: argparse10.py [-h] [-v VERBOSITY] carré
argparse10.py: error: argument -v/--verbosity: expected one argument
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse10.py 4 -v 1
4² = 16
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse10.py 4 -v 2
le carré de 4 est égal à 16
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse10.py 4 -v 3
16
```

Tout semble bon sauf pour le dernier cas. Notre programme contient un bogue.

Fichier **argparse11.py** :

Comment restreindre les valeurs que **--verbosity** accepte ?

```
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse

parser = argparse.ArgumentParser()
parser.add_argument("carré", type=int, help="affiche le carré du nombre passé en argument")
parser.add_argument("-v", "--verbosity", type=int, choices=[0, 1, 2], help="augmente la verbosité de sortie")
args = parser.parse_args()
reponse = args.carré**2

if args.verbosity == 2:
    print("le carré de {} est égal à {}".format(args.carré, reponse))
elif args.verbosity == 1:
    print("{}² = {}".format(args.carré, reponse))
else:
    print(reponse)
```

On ajoute l’option `choices=[]` à la méthode `add_argument()`.

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse11.py 4 -v 3
usage: argparse11.py [-h] [-v {0,1,2}] carré
argparse11.py: error: argument -v/--verbosity: invalid choice: 3 (choose from 0, 1, 2)
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse11.py 4 -h
usage: argparse11.py [-h] [-v {0,1,2}] carré
positional arguments:
    carré                              affiche le carré du nombre passé en argument
optional arguments:
    -h, --help                         show this help message and exit
    -v {0,1,2}, --verbosity {0,1,2}    augmente la verbosité de sortie
```

Notez que ce changement est pris en compte à la fois dans le message d’erreur et dans le texte d’aide.

Essayons maintenant une approche différente pour jouer sur la verbosité. Cela correspond également à comment le programme CPython gère ses propres paramètres de verbosité (jetez un œil sur la sortie de la commande **python --help**) :

Fichier **argparse12.py** :

Comment compter le nombre fois où un paramètre est saisi ?

```
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse

parser = argparse.ArgumentParser()
parser.add_argument("carré", type=int, help="affiche le carré du nombre passé en argument")
parser.add_argument("-v", "--verbosity", action="count", help="augmente la verbosité de sortie")
args = parser.parse_args()
reponse = args.carré**2
if args.verbosity == 2:
    print("le carré de {} est égal à {}".format(args.carré, reponse))
elif args.verbosity == 1:
    print("{}² = {}".format(args.carré, reponse))
else:
    print(reponse)
```

Nous avons introduit une autre action `"count"` à la méthode `add_argument()`, pour compter le nombre d’occurrences d’un argument optionnel en particulier :

Oui, c’est maintenant d’avantage une option (similaire à `action="store_true"`) de la version précédente de notre script. C’est plus logique pour comprendre le message d’erreur.

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse12.py 4
16
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7\_Modules$ ./argparse12.py 4 -v
4² == 16
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse12.py 4 -vv
le carré de 4 est égal à 16
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse12.py 4 --verbosity --verbosity
le carré de 4 est égal à 16
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse12.py 4 -v 1
usage: argparse12.py [-h] [-v] carré
argparse12.py: error: unrecognized arguments: 1
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse12.py 4 -h
usage: argparse12.py [-h] [-v] carré

positional arguments:
    carré               affiche le carré du nombre passé en argument

optional arguments:
    -h, --help show     this help message and exit
    -v, --verbosity     augmente la verbosité de sortie
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse12.py 4 -vvv
16
```


1. La commande calcule le carré


2. Cela se comporte de la même manière que l’action `"store_true"`.


3. Maintenant voici une démonstration de ce que l’action `"count"` fait. Vous avez sûrement vu ce genre d’utilisation auparavant. Et si vous ne spécifiez pas l’option **-v**, cette option prendra la valeur `None`.


4. Comme on s’y attend, en spécifiant l’option dans sa forme longue, on devrait obtenir la même sortie.


5. Une valeur passé en paramètre génère une erreur.


6. Affiche l’aide normalement


7. La dernière sortie du programme montre que celui-ci contient un bogue.

Malheureusement, notre sortie d’aide n’est pas très informative à propos des nouvelles possibilités de notre programme, mais cela peut toujours être corrigé en améliorant sa documentation (en utilisant l’argument **help**).

Fichier **argparse13.py** :

Comment améliorer la documentation de l’exercice précédent et corriger le bogue ?

```
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse

parser = argparse.ArgumentParser()
parser.add_argument("carré", type=int, help="affiche le carré du nombre passé en argument")
parser.add_argument("-v", "--verbosity", action="count", help="augmente la verbosité de sortie")
args = parser.parse_args()
reponse = args.carré**2

# corection: remplacer == avec >=
if args.verbosity >= 2:
    print("le carré de {} est égal à {}".format(args.carré , reponse))
elif args.verbosity >= 1:
    print("{}² = {}".format(args.carré , reponse))
else:
    print(reponse)
```

Il suffit de changer le test «**==**» par «**>=**».

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse13.py 4 -vvv
le carré de 4 est égal à 16
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse13.py 4 -vvvv
le carré de 4 est égal à 16
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse13.py 4
Traceback (most recent call last):
    File "argparse13.py", line 12, in <module>
        if args.verbosity >= 2:
TypeError: '>=' not supported between instances of 'NoneType' and 'int'
```

Les premières exécutions du programme sont correctes, et le bogue que nous avons eu précédemment est corrigé.

La troisième sortie du programme est un autre bogue introduit par la modification.

Nous voulons que pour n’importe quelle valeur `>= 2` le programme soit verbeux tout en calculant le carré sans ce paramètre.

Fichier **argparse14.py** :

Comment corriger le nouveau bogue du code de l’exemple précédent pour avoir la sortie du carré ?

```
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse

parser = argparse.ArgumentParser()
parser.add_argument("carré", type=int, help="affiche le carré du nombre passé en argument")
parser.add_argument("-v", "--verbosity", action="count", default=0, help="augmente la verbosité de sortie")
args = parser.parse_args()
reponse = args.carré**2

# corection: remplacer == avec >=
if args.verbosity >= 2:
    print("le carré de {} est égal à {}".format(args.carré , reponse))
elif args.verbosity >= 1:
    print("{}² = {}".format(args.carré , reponse))
else:
    print(reponse)
```

Nous introduisons une nouvelle option `default=` dans la méthode `add_argument()`. Nous la définisons à l’entier **0** pour la rendre compatible avec les autres valeurs entières de l’option `count=`. Rappelez-vous que par défaut, si un argument optionnel n’est pas spécifié, il sera définit à `None` une valeur booléenne, et ne pourra donc pas être comparé à une valeur de type entier. Une erreur [TypeError](https://docs.python.org/fr/3/library/exceptions.html#TypeError) sera alors levée.

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse14.py 4
16
```

Fichier **argparse15.py** :

Qu’en est-il si nous souhaitons étendre notre mini programme pour le rendre capable de calculer d’autres puissances, et pas seulement des carrés?

```
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse

parser = argparse.ArgumentParser()
parser.add_argument("x", type=int, help="la base")
parser.add_argument("y", type=int, help="l’exposant")
parser.add_argument("-v", "--verbosity", action="count", default=0)
args = parser.parse_args()
reponse = args.x**args.y

if args.verbosity >= 2:
    print("{} à la puissance {} est égal à {}".format(args.x, args.y, reponse))
elif args.verbosity >= 1:
    print("{}^{} = {}".format(args.x, args.y, reponse))
else:
    print(reponse)
```

Nous modifions les arguments de saisies et l’opération de calcul.

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse15.py
usage: argparse15.py [-h] [-v] x y
argparse15.py: error: the following arguments are required: x, y
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse15.py -h
usage: argparse15.py [-h] [-v] x y

positional arguments:
    x          la base
    y          l’exposant

optional arguments:
    -h, --help show this help message and exit
    -v, --verbosity
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse15.py 4 2 -v
4^2 = 16
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse15.py 4 2 -vv
4 à la puissance 2 est égal à 16
```

Il est à noter que jusqu’à présent nous avons utilisé le niveau de verbosité pour **changer** le texte qui est affiché.

Fichier **argparse16.py** :

Comment utiliser le principe du niveau de verbosité pour **changer** de sens de texte ?

```
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse

parser = argparse.ArgumentParser()
parser.add_argument("x", type=int, help="la base")
parser.add_argument("y", type=int, help="l’exposant")
parser.add_argument("-v", "--verbosity", action="count", default=0)
args = parser.parse_args()
reponse = args.x**args.y

if args.verbosity >= 2:
    print("Exécution de '{}'".format(__file__))
if args.verbosity >= 1:
    print("{}^{} = ".format(args.x, args.y), end="")
print(reponse)
```

Modifions le texte affiché par les options `args.verbosity` pour afficher la commande exécutée.

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse16.py 4 2
16
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse16.py 4 2 -v
4^2 = 16
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7\_Modules$ ./argparse16.py 4 2 -vv
Exécution de './argparse16.py'
4^2 = 16
```

Jusque là, nous avons travaillé avec deux méthodes `parse_args()` et `add_argument()` d’une instance de `argparse.ArgumentParser`.

Voyons maintenant l’utilisation de la méthode `add_mutually_exclusive_group()`. Cette méthode nous permet de spécifier des paramètres qui sont en conflit entre eux.

Fichier **argparse17.py** :

Comment utiliser `add_mutually_exclusive_group()` ?

```
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse

parser = argparse.ArgumentParser()
group = parser.add_mutually_exclusive_group()
group.add_argument("-v", "--verbose", action="store_true")
group.add_argument("-q", "--quiet", action="store_true")
parser.add_argument("x", type=int, help="la base")
parser.add_argument("y", type=int, help="l’exposant")
args = parser.parse_args()
reponse = args.x**args.y

if args.quiet:
    print(reponse)
elif args.verbose:
    print("{} à la puissance {} est égal à {}".format(args.x, args.y, reponse))
else:
    print("{}^{} = {}".format(args.x, args.y, reponse))
```

Changeons aussi le reste du programme de telle sorte que la nouvelle fonctionnalité fasse sens. Nous allons introduire l’option **--quiet**, qui va avoir l’effet opposé de l’option **--verbose** :

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse17.py 4 2
4^2 == 16
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse17.py 4 2 -q
16
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse17.py 4 2 -v
4 à la puissance 2 est égal à 16
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse17.py 4 2 -vq
usage: argparse17.py [-h] [-v | -q] x y
argparse17.py: error: argument -q/--quiet: not allowed with argument -v/--verbose
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse17.py 4 2 -v --quiet
usage: argparse17.py [-h] [-v | -q] x y
test.py: error: argument -q/--quiet: not allowed with argument -v/--verbose
```

Avant d’en finir, vous voudrez certainement dire à vos utilisateurs de votre outil de ligne de commande quel est le but principal du programme. Juste dans le cas ou ils ne le sauraient pas :-)

Fichier **argparse18.py** :

Comment modifier l’aide d’un programme en ligne de commande avec un titre général ?

```
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse

parser = argparse.ArgumentParser(description="calcule X à la puissance Y")
group = parser.add_mutually_exclusive_group()
group.add_argument("-v", "--verbose", action="store_true")
group.add_argument("-q", "--quiet", action="store_true")
parser.add_argument("x", type=int, help="la base")
parser.add_argument("y", type=int, help="l’exposant")
args = parser.parse_args()
reponse = args.x**args.y

if args.quiet:
    print(reponse)
elif args.verbose:
    print("{} à la puissance {} est égal à {}".format(args.x, args.y, reponse))
else:
    print("{}^{} = {}".format(args.x, args.y, reponse))
```

On ajoute l’option `description=` lors de la création de l’objet `argparse.ArgumentParser()`

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse18.py --help
usage: argparse18.py [-h] [-v \| -q] x y

calcule X à la puissance Y

positional arguments:
    x                 la base
    y                 l’exposant

optional arguments:
    -h, --help show   this help message and exit
    -v, --verbose
    -q, --quiet
```

C’est bien jolie tout cela mais on mélange de l’anglais avec du Français.

Fichier **argparse19.py** :

Comment faire pour traduire les messages d’aide en Français ?

```
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import gettext

__TRANSLATIONS = {
    'ambiguous option: %(option)s could match %(matches)s': 'option ambiguë: %(option)s parmi %(matches)s', 'argument "-" with mode %r': 'argument "-" en mode %r', 'cannot merge actions - two groups are named %r': 'cannot merge actions - two groups are named %r', "can't open '%(filename)s': %(error)s": "can't open '%(filename)s': %(error)s", 'dest= is required for options like %r': 'dest= is required for options like %r', 'expected at least one argument': 'au moins un argument est attendu', 'expected at most one argument': 'au plus un argument est attendu', 'expected one argument': 'un argument est nécessaire', 'ignored explicit argument %r': 'ignored explicit argument %r', 'invalid choice: %(value)r (choose from %(choices)s)': 'choix invalide: %(value)r (parmi %(choices)s)', 'invalid conflict_resolution value: %r': 'invalid conflict_resolution value: %r', 'invalid option string %(option)r: must start with a character %(prefix_chars)r': 'invalid option string %(option)r: must start with a character %(prefix_chars)r', 'invalid %(type)s value: %(value)r': 'valeur invalide de type %(type)s: %(value)r', 'mutually exclusive arguments must be optional': 'mutually exclusive arguments must be optional', 'not allowed with argument %s': "pas permis avec l'argument %s", 'one of the arguments %s is required': 'au moins un argument requis parmi %s', 'optional arguments': 'arguments optionnels', 'positional arguments': 'arguments positionnels', "'required' is an invalid argument for positionals": "'required' is an invalid argument for positionals", 'show this help message and exit': 'afficher ce message d\’aide', 'unrecognized arguments: %s': 'argument non reconnu: %s', 'unknown parser %(parser_name)r (choices: %(choices)s)': 'unknown parser %(parser_name)r (choices: %(choices)s)', 'usage: ': 'utilisation: ', '%(prog)s: error: %(message)s\n': '%(prog)s: erreur: %(message)s\n', '%r is not callable': '%r is not callable', }

gettext.gettext = lambda text: __TRANSLATIONS[text] or text

import argparse

parser = argparse.ArgumentParser(description="calcule X à la puissance Y")
group = parser.add_mutually_exclusive_group()
group.add_argument("-v", "--verbose", action="store_true")
group.add_argument("-q", "--quiet", action="store_true")
parser.add_argument("x", type=int, help="la base")
parser.add_argument("y", type=int, help="l’exposant")
args = parser.parse_args()
reponse = args.x**args.y

if args.quiet:
    print(reponse)
elif args.verbose:
    print("{} à la puissance {} est égal à {}".format(args.x, args.y, reponse))
else:
    print("{}^{} = {}".format(args.x, args.y, reponse))
```

La traduction se fait avec gettext.

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse19.py --help
utilisation: argparse19.py [-h] [-v | -q] x y

calcule X à la puissance Y

arguments positionnels:
    x             la base
    y             l’exposant

arguments optionnels:
    -h, --help    affiche ce message d’aide
    -v, --verbose
    -q, --quiet
utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ cd ..
```

### Gestion des dates et du temps

#### Date et heure

**Datetime** est un module qui permet de manipuler des dates et des durées sous forme d’objets. L’idée est simple: vous manipulez l’objet pour faire tous vos calculs, et quand vous avez besoin de l’afficher, vous formatez l’objet en chaîne de caractères.

On peut créer artificiellement un objet **datetime** , ses paramètres sont:

```
datetime (année, mois, jour, heure, minute, seconde, microseconde, fuseau horaire)
```

Mais seuls «année», «mois» et «jour» sont obligatoires.

```
>>> from datetime import datetime
>>> datetime(2000, 1, 1)
datetime.datetime(2000, 1, 1, 0, 0)
```

Nous sommes ici le premier janvier 2000, à la seconde et la minute zéro, de l’heure zéro.

On peut bien entendu récupérer l’heure et la date du jour:

```
>>> actuellement = datetime.now()
>>> actuellement
datetime.datetime(2021, 7, 9, 10, 13, 1, 25073)
>>> actuellement.year
2021
>>> actuellement.month
7
>>> actuellement.day
9
>>> actuellement.hour
10
>>> actuellement.minute
13
>>> actuellement.second
1
>>> actuellement.microsecond
25073
>>> actuellement.isocalendar() # année, semaine, jour
datetime.IsoCalendarDate(year=2021, week=27, weekday=5)
>>> maintenant = datetime.now # obtenir l’heure avec une variable
>>> print(maintenant())
2021-07-09 10:14:51.359460
>>> print(maintenant())
2021-07-09 10:15:0.918195
```

Enfin, si vous souhaitez uniquement vous occuper de la date ou de l’heure:

```
>>> print(maintenant().strftime('%Hh %Mmin %Ss %d/%m/%Y')) # change une date en chaîne.
10h 16min 22s 09/07/2021
>>> from datetime import date, time, datetime
>>> maDate = datetime.strptime('2021-06-05 12:30:00', '%Y-%m-%d %H:%M:%S') # change une chaîne en date.
>>> print(maDate)
2021-06-05 12:30:00
>>> maDate
datetime.datetime(2021, 6, 5, 12, 30)
```

#### Durée

En plus de pouvoir récupérer la date du jour, on peut calculer la différence entre deux dates. Par exemple, combien de temps y a-t-il entre aujourd’hui et le premier jour de l’an 2000 ?

```
>>> duree = maintenant() - datetime(2000, 1, 1)
>>> duree
datetime.timedelta(days=7860, seconds=39227, microseconds=140524)
```

Et vous découvrez ici un autre objet, le **timedelta**. Cet objet représente une durée en jours, secondes et microsecondes.

```
>>> duree.days
7860
>>> duree.seconds
39227
>>> duree.microseconds
140524
>>> duree.total_seconds
<built-in method total_seconds of datetime.timedelta object at 0x7efc4f7655d0>
>>> duree.total_seconds()
679144227.140524
```

On peut créer son propre **timedelta** :

```
>>> from datetime import timedelta
>>> print(timedelta(days=3, seconds=100))
3 days, 0:01:40
```

Cela permet de répondre à la question : «Quelle date serons-nous dans 2 jours, 4 heures, 3 minutes, et 12 secondes ?»:

```
>>> print(maintenant() + timedelta(days=2, hours=4, minutes=3, seconds=12))
2021-07-11 15:12:00.371922
```

Les objets **datetime** et **timedelta** sont immutables. Ainsi si vous voulez utiliser une version légèrement différente d’un objet **datetime** , il faudra toujours en créer un nouveau. Par exemple:

```
>>> actuellement.replace(year=1995) # on créer un nouvel objet
datetime.datetime(1995, 7, 9, 10, 13, 1, 25073)
```

Vous noterez que je ne parles pas de fuseau horaire. Et bien c’est parce que l’implémentation Python est particulièrement ratée : l’API est compliquée et les données ne sont pas à jour. Il faut dire que la mesure du temps, contrairement à ce qu’on pourrait penser, n’est pas vraiment le truc le plus stable du monde, et des pays changent régulièrement leur manière de faire.

#### Calendrier

Le module **calendar**.

Il permet de manipuler un calendrier comme un objet, et de déterminer les jours d’un mois, les semaines, vérifier les caractéristiques d’un jour en particulier, etc. :

```
>>> import calendar
>>> calendar.mdays # combien de jour par mois ?
[0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
>>> calendar.isleap(2000) # est-ce une année bissextile ?
True
>>> calendar.weekday(2000, 1, 1) # quel jour était cette date ?
5
>>> calendar.MONDAY, calendar.TUESDAY, calendar.WEDNESDAY, calendar.THURSDAY, calendar.FRIDAY
(0, 1, 2, 3, 4)
```

On peut instancier un calendrier et itérer dessus:

```
>>> cal = calendar.Calendar()
>>> cal.getfirstweekday()
0
>>> list(cal.iterweekdays())
[0, 1, 2, 3, 4, 5, 6]
>>> list(cal.itermonthdays(2000, 1))
[0, 0, 0, 0, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 0, 0, 0, 0, 0, 0]
>>> list(cal.itermonthdates(2000, 1))
[datetime.date(1999, 12, 27), datetime.date(1999, 12, 28), datetime.date(1999, 12, 29), datetime.date(1999, 12, 30), datetime.date(1999, 12, 31), datetime.date(2000, 1, 1), datetime.date(2000, 1, 2), datetime.date(2000, 1, 3), datetime.date(2000, 1, 4), datetime.date(2000, 1, 5), datetime.date(2000, 1, 6), datetime.date(2000, 1, 7), datetime.date(2000, 1, 8), datetime.date(2000, 1, 9), datetime.date(2000, 1, 10), datetime.date(2000, 1, 16), datetime.date(2000, 1, 17), datetime.date(2000, 1, 18), datetime.date(2000, 1, 19), datetime.date(2000, 1, 20),     datetime.date(2000, 1, 21), datetime.date(2000, 1, 22), datetime.date(2000, 1, 23), datetime.date(2000, 1, 24), datetime.date(2000, 1, 25), datetime.date(2000, 1, 26), datetime.date(2000, 1, 27), datetime.date(2000, 1, 28), datetime.date(2000, 1, 29), datetime.date(2000, 1, 30), datetime.date(2000, 1, 31), datetime.date(2000, 2, 1), datetime.date(2000, 2, 2), datetime.date(2000, 2, 3), datetime.date(2000, 2, 4), datetime.date(2000, 2, 5), datetime.date(2000, 2, 6)]
>>> cal.monthdayscalendar(2000, 1)
[[0, 0, 0, 0, 0, 1, 2], [3, 4, 5, 6, 7, 8, 9], [10, 11, 12, 13, 14, 15, 16], [17, 18, 19, 20, 21, 22, 23], [24, 25, 26, 27, 28, 29, 30], [31, 0, 0, 0, 0, 0, 0]]
```

Comme souvent Python vient aussi avec de très bons modules tierces pour manipuler les dates :


* **dateutils** est un datetime boosté aux hormones qui permet notamment de donner des durées floues comme “+ 1 mois” et de gérer des événements qui se répètent.


* **babel** n’est pas spécialisé dans les dates mais dans la localisation. Le module possède des outils pour formater des dates selon le format de chaque pays, et aussi avec des formats naturels comme “il y a une minute”.


* **pytz** est une implémentation saine de gestion des fuseaux horaires en Python.

### Module Windows

```
pip install pywin32
```

Vous pouvez écrire des logs dans le gestionnaire d’évènements windows. Pour cela vous devez utiliser le module **win32evlog**

```
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import win32evtlog
import pprint
import sys

# S'abonne aux événements de l'application et les enregistre.
# Pour déclencher manuellement un nouvel événement, ouvrez une console d'administration et tapez: (remplacez 125 par tout autre ID qui vous convient)
#     eventcreate.exe /L "application" /t warning /id 125 /d "Ceci est un avertissement de test"
#
# event_context peut être `None` si ce n'est pas obligatoire, c'est juste pour montrer comment cela fonctionne
event_context = {"info": "cet objet est toujours passé à votre retour"}
# Event log source to listen to
event_source = 'application'

def new_logs_event_handler(raison, contexte, evnmt):
    """
    Appelé lorsque de nouveaux événements sont enregistrés.

    raison - raison pour laquelle l'événement a été enregistré?
    contexte- contexte dans lequel le gestionnaire d'événements a été enregistré
    evnmt - événement capturé
    """
    # Imprimez simplement quelques informations sur l'événement
    print('raison', raison, 'contexte', contexte, 'événement capturé', evnmt)

    # Rendre l'événement en XML, il y a peut-être un moyen d'obtenir un objet mais je ne l'ai pas trouvé
    print('Événement rendu :', win32evtlog.EvtRender(evt, win32evtlog.EvtRenderEventXml))

    # ligne vide pour séparer les journaux
    print(' - ')

    # Assurez-vous que tout le texte imprimé est réellement imprimé sur la console maintenant
    sys.stdout.flush()

    return 0

# Abonnez-vous aux futurs événements
subscription = win32evtlog.EvtSubscribe(event_source, win32evtlog.EvtSubscribeToFutureEvents, None, Callback=new_logs_event_handler, Context=event_context, Query=None)
```

Exemple plus complet

```
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import win32evtlog
import win32api
import win32con
import win32security # Pour traduire NT Sids en noms de compte.
import win32evtlogutil

def ReadLog(computer, logType="Application", dumpEachRecord = 0):
    # Lit l'intégralité du journal.
    h=win32evtlog.OpenEventLog(computer, logType)
    numRecords = win32evtlog.GetNumberOfEventLogRecords(h)
    print("Il y a% enregistrements" % numRecords)

    num=0
    while 1:
        objects = win32evtlog.ReadEventLog(h, win32evtlog.EVENTLOG_BACKWARDS_READ|win32evtlog.EVENTLOG_SEQUENTIAL_READ, 0)
        if not objects:
            break
        for object in objects:
            # À des fins de test, mais ne l'imprime pas.
            msg = win32evtlogutil.SafeFormatMessage(object, logType)
            if object.Sid is not None:
                try:
                    domain, user, typ = win32security.LookupAccountSid(computer, object.Sid)
                    sidDesc = "%s/%s" % (domain, user)
                except win32security.error:
                    sidDesc = str(object.Sid)
                    user_desc = "Événement associé à l'utilisateur %s" % (sidDesc,)
            else:
                user_desc = None
            if dumpEachRecord:
                print("Enregistrement d'événement de %r généré à %s"\ % (object.SourceName, object.TimeGenerated.Format()))
                if user_desc:
                    print user_desc
                    try:
                        print msg
                    except UnicodeError:
                        print("(message d'impression d'erreur unicode: repr() suit…)")
                        print(repr(msg))
        num = num + len(objects)

    if numRecords == num:
        print("Succès de la lecture complète", numRecords, "enregistrements")
    else:
        print("Impossible d'obtenir tous les enregistrements - signalé %d, mais trouvé %d" % (numRecords, num))
        print ("(Notez d'autres applications peuvent avoir écrit des enregistrements pendant l'exécution!)")
    win32evtlog.CloseEventLog(h)

def usage():
    print("Écrit un événement dans le journal des événements.")
    print("-w : N'écrire aucun enregistrement de test.")
    print("-r : Ne pas lire le journal des événements")
    print("-c : nomOrdinateur : Traiter le journal sur l'ordinateur spécifié")
    print("-v : Verbeux")
    print("-t : LogType - Utiliser le journal spécifié - défaut = 'Application'")

def test():
    # vérifier s'il fonctionne sous Windows NT, sinon, afficher un avis et terminer
    if win32api.GetVersion() & 0x80000000:
        print("Cet exemple ne fonctionne que sur NT")
        return

    import sys, getopt
    opts, args = getopt.getopt(sys.argv[1:], "rwh?c:t:v")
    computer = None
    do_read = do_write = 1
    logType = "Application"
    verbose = 0

    if len(args)>0:
        print("Arguments non valides")
        usage()
        return 1

    for opt, val in opts:
        if opt == '-t':
            logType = val
        if opt == '-c':
            computer = val
        if opt in ['-h', '-?']:
            usage()
            return
        if opt == '-r':
            do_read = 0
        if opt == '-w':
            do_write = 0
        if opt == '-v':
            verbose = verbose + 1

    if do_write:
        ph = win32api.GetCurrentProcess()
        th = win32security.OpenProcessToken(ph,win32con.TOKEN_READ)
        my_sid = win32security.GetTokenInformation(th,win32security.TokenUser)[0]

        win32evtlogutil.ReportEvent(logType, 2,
            strings=["Le texte du message pour l'événement 2", "Un autre insert"],
            data="Raw\0Data".encode("ascii"), sid=my_sid)
        win32evtlogutil.ReportEvent(logType, 1, eventType=win32evtlog.EVENTLOG_WARNING_TYPE,
            strings=["Un avertissement", "Un avertissement encore plus grave"],
            data="Raw\0Data".encode("ascii"), sid=my_sid)
        win32evtlogutil.ReportEvent(logType, 1, eventType=win32evtlog.EVENTLOG_INFORMATION_TYPE,
            strings=["Une info", "Trop d'informations"],
            data="Raw\0Data".encode("ascii"), sid=my_sid)
        print("Écriture réussie de 3 enregistrements dans le journal")

    if do_read:
        ReadLog(computer, logType, verbose > 0)

if __name__ == '__main__':
    test()
```

## L’organisation du code

### Structures de contrôles

#### Structures alternatives

##### If, else, elif

Les instructions [if, else, elif](https://courspython.com/tests.html#if) sont sans doute les plus connues.

Par exemple :

```
>>> import os
>>> x = int(input("SVP entrez un entier: "))
SVP entrez un entier: 42
>>> if x < 0:
...     x = 0
...     print('Nombre négatif remplacé par zéro')
... elif x == 0:
...     print('Zéro')
... elif x == 1:
...     print('Unité')
... else:
...     print('Plus grand')
...
Plus grand
```

Il peut y avoir un nombre quelconque de parties `elif` et la partie `else` est facultative. Le mot clé `elif` est un raccourci pour `else if`, mais permet de gagner un niveau d’indentation. Une séquence `if ... elif ... elif ...` est par ailleurs équivalente aux instructions «**switch**» ou «**case**» disponibles dans d’autres langages.

#### Structures itératives

##### For in

Les instructions [for in](https://courspython.com/boucles.html#for) que propose Python sont un peu différente de celle que l’on peut trouver en C ou en Pascal.

Au lieu de toujours itérer sur une suite arithmétique de nombres (comme en Pascal), ou de donner à l’utilisateur la possibilité de définir le pas d’itération et la condition de fin (comme en C), l’instruction `for` en Python itère sur les éléments d’une séquence (qui peut être une liste, une chaîne de caractères…), dans l’ordre dans lequel ils apparaissent dans la séquence.

Par exemple :

```
>>> # Mesure quelques chaînes de caractères
>>> mots = ['fenêtre', 'chat', 'quantique']
>>> for l in mots:
...     print(l, len(l))
...
fenêtre 7
chat 4
quantique 9
```

Le code qui modifie une collection tout en itérant sur cette même collection peut être délicat à mettre en place.

```
>>> # Strategie:  Itérer sur une copie
>>> utilisateurs = {'Vador': 'inactif', 'Luc': 'actif', 'Padawan': 'actif'}
>>> for utilisateur, statut in utilisateurs.copy().items():
...     if statut == 'inactif':
...         del utilisateurs[utilisateur]
...
...
>>> utilisateurs
{'Luc': 'actif', 'Padawan': 'actif'}
```

Au lieu de cela, il est généralement plus simple de boucler sur une copie de la collection ou de créer une nouvelle collection :

```
>>> # Strategie: Créer une nouvelle collection utilisateurs
>>> utilisateurs = {'Vador': 'inactif', 'Luc': 'actif', 'Padawan': 'actif'}
>>> utilisateurs_actifs = utilisateurs.copy()
>>> utilisateurs_vivants = []
>>> for utilisateur, statut in utilisateurs.items():
...     if statut == 'inactif':
...         del utilisateurs_actifs[utilisateur]
... else:
...          utilisateurs_vivants.append(utilisateur)
...
>>> utilisateurs_actifs
{'Luc': 'actif', 'Padawan': 'actif'}
>>> utilisateurs_vivants
['Luc', 'Padawan']
>>> utilisateurs
{'Vador': 'inactif', 'Luc': 'actif', 'padawan': 'actif'}
```

Pour itérer sur les indices d’une séquence, on peut combiner les fonctions `range()` et `len()` :

```
>>> phrase = ['Luc', 'a', 'un', 'petit', 'laser']
>>> for mot in range(len(phrase)):
...     print(mot, phrase[mot])
...
0 Luc
1 a
2 un
3 petit
4 laser
```

Cependant, dans la plupart des cas, il est plus pratique d’utiliser la fonction `enumerate()`.

```
>>> for mot in enumerate(phrase):
...     print(mot[0], mot[1])
...
0 Luc
1 a
2 un
3 petit
4 laser
```

##### While

L’instruction [while](https://courspython.com/boucles.html#while) permet de faire des boucle suivant une condition.

###### break, continue

L’instruction `break`, comme en C, interrompt la boucle `for` ou `while`.

Les boucles peuvent également disposer d’une instruction `else`. Celle-ci est exécutée lorsqu’une boucle se termine alors que tous ses éléments ont été traités (dans le cas d’un `for`) ou que la condition devient fausse (dans le cas d’un `while`), **mais pas lorsque la boucle est interrompue par une instruction** `break`.

L’exemple suivant, qui effectue une recherche de nombres premiers, en est une démonstration :

```
>>> for n in range(2, 10):
...     for x in range(2, n):
...         if n % x == 0:
...             print(n, 'égal', x, '*', n//x)
...             break
...     else:
...         # la boucle s’est terminée sans trouver de facteur
...         print(n, 'est un nombre premier')
...
...
2 est un nombre premier
3 est un nombre premier
4 égal 2 * 2
5 est un nombre premier
6 égal 2 * 3
7 est un nombre premier
8 égal 2 * 4
9 égal 3 * 3
```

Oui, ce code est correct. Regardez attentivement : l’instruction `else` est rattachée à la boucle `for`, et non à l’instruction `if`.

Lorsqu’elle est utilisée dans une boucle, la clause `else` est donc plus proche de celle associée à une instruction `try` que de celle associée à une instruction `if`: la clause `else` d’une instruction `try` s’exécute lorsqu’aucune exception n’est déclenchée, et celle d’une boucle lorsqu’aucun `break` n’intervient. Nous verrons plus ultérieurement dans ce cours l’instruction `try` et le traitement des exceptions.

L’instruction `continue`, également empruntée au C, fait passer la boucle à son itération suivante :

```
>>> for num in range(2, 10):
...     if num % 2 == 0:
...         print("Un nombre pair a été trouvé : ", num)
...         continue
...     print("Un nombre impair a été trouvé : ", num)
...
Un nombre pair a été trouvé :  2
Un nombre impair a été trouvé :  3
Un nombre pair a été trouvé :  4
Un nombre impair a été trouvé :  5
Un nombre pair a été trouvé :  6
Un nombre impair a été trouvé :  7
Un nombre pair a été trouvé :  8
Un nombre impair a été trouvé :  9
```

###### pass

L’instruction `pass` ne fait rien. Elle peut être utilisée lorsqu’une instruction est nécessaire pour fournir une syntaxe correcte, mais qu’aucune action ne doit être effectuée.

Par exemple :

```
>>> while True:
...     pass  # Attente occupée pour l'interruption du clavier (Ctrl + C)
...
^CTraceback (most recent call last):
 File "<stdin>", line 2, in <module>
KeyboardInterrupt
```

On utilise couramment cette instruction pour créer des classes minimales d’objets :

```
>>> class MaClasseVide:
...     pass
...
>>>
```

Un autre cas d’utilisation du `pass` est de réserver un espace en phase de développement pour une fonction ou un traitement conditionnel, vous permettant ainsi de construire votre code à un niveau plus abstrait.

L’instruction **pass** est alors ignorée silencieusement :

```
>>> def initlog(*args):
...     pass   # N'oubliez pas de mettre en œuvre cela!
...
```

##### alternative do while

```
>>> while True:
...     #… code
...     if cond :
...         break
```

##### Techniques de boucles des dictionnaires

Lorsque vous faites une boucle sur un dictionnaire, les clés et leurs valeurs peuvent être récupérées en même temps en utilisant la méthode `items()` :

```
>>> chevaliers_jedi = {'Luc': 'le padawan', 'Yoda': 'le grand maître', 'Obi-Wan Kenobi': 'le guerrier'}
>>> for k, v in chevaliers_jedi.items(): print(k, v)
...
Luc le padawan
Yoda le grand maître
Obi-Wan Kenobi le guerrier
```

Lorsque vous faites une boucle sur une séquence, la position et la valeur correspondante peuvent être récupérées en même temps en utilisant la fonction `enumerate()` :

```
>>> for i, v in enumerate(['tic', 'tac', 'toe']): print(i, v)
...
0 tic
1 tac
2 toe
```

Pour faire une boucle sur deux séquences, ou plus en même temps, les éléments peuvent être associés en utilisant la fonction `zip()` :

```
>>> questions = ['nom', 'côté de la force', 'couleur du sabre']
>>> réponses = ['luc', 'la lumière', 'le vert']
>>> for q, r in zip(questions, réponses):
...     print('Quel est votre {0} ? C\’est {1}.'.format(q, r))
...
Quel est votre nom ? C’est luc.
Quel est votre côté de la force ? C’est la lumière.
Quel est votre couleur du sabre ? C’est le vert.
```

Pour faire une boucle en sens inverse sur une séquence, commencez par spécifier la séquence dans son ordre normal, puis appliquez la fonction `reversed()` :

```
>>> for n in reversed(range(1, 10, 2)):  print(n)
...
9
7
5
3
1
```

Pour faire une boucle sur une séquence de manière ordonnée, utilisez la fonction `sorted()` qui renvoie une nouvelle liste ordonnée sans altérer la source :

```
>>> panier = ['pomme', 'orange', 'pomme', 'poire', 'orange', 'banane']
>>> for f in sorted(panier): print(f)
...
banane
orange
orange
poire
pomme
pomme
```

L’utilisation de la fonction `set()` sur une séquence élimine les doublons. L’utilisation de la fonction `sorted()` en combinaison avec `set()` sur une séquence est une façon idiomatique de boucler sur les éléments uniques d’une séquence dans l’ordre :

```
>>> for f in sorted(set(panier)): print(f)
...
banane
orange
poire
pomme
```

Il est parfois tentant de modifier une liste pendant son itération. Cependant, c’est souvent plus simple et plus sûr de créer une nouvelle liste à la place. :

```
>>> import math
>>> données_brutes = [56.2, float('NaN'), 51.7, 55.3, 52.5, float('NaN'), 47.8]
>>> données_filtrées = []
>>> for valeur in données_brutes:
...     if not math.isnan(valeur):
...         données_filtrées.append(valeur)
...
...
>>> données_filtrées
[56.2, 51.7, 55.3, 52.5, 47.8]
```
