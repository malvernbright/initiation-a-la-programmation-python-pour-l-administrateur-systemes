# Les objets

## Objet et caractéristiques

Plus qu’un simple langage de script, Python est aussi un langage orienté objet.

Ce langage moderne et puissant est né au début des années 1990 sous l’impulsion de Guido van Rossum.

Apparue dans les années 60 quant à elle, la programmation orientée objet (POO) est un paradigme de programmation ; c’est-à-dire une façon de concevoir un programme informatique, reposant sur l’idée qu’un programme est composé d’objets interagissant les uns avec les autres.

En définitive, **un objet est une donnée**. Une donnée constituée de diverses propriétés, et pouvant être manipulée par différentes opérations.

La programmation orientée objet est le paradigme qui nous permet de définir nos propres types d’objets, avec leurs propriétés et opérations.
Ce paradigme vient avec de nombreux concepts qui seront explicités le long de ce cour.

À travers ce cour, nous allons nous intéresser à cette façon de penser et à le programmer avec le langage Python.

### Type

Ainsi, **tout objet est associé à un type**. Un type définit la sémantique d’un objet. On sait par exemple que les objets de type `int` sont des nombres entiers, que l’on peut les additionner, les soustraire, etc.

Pour la suite de ce cours, nous utiliserons un type `User` représentant un utilisateur sur un quelconque logiciel. Nous pouvons créer ce nouveau type à l’aide du code suivant :

```
class User:
    pass
```

Nous reviendrons sur ce code par la suite, retenez simplement que nous avons maintenant à notre disposition un type `User`.

Pour créer un objet de type `User`, il nous suffit de procéder ainsi :

```
john = User()
```

On dit alors que `john` est **une instance** de `User`.

### Les attributs

nous avons dit qu’un objet était constitué d’attributs. **Ces derniers représentent des valeurs propres à l’objet**.

Nos objets de type `User` pourraient par exemple contenir un identifiant (`id`), un nom (`name`) et un mot de passe (`password`).

En Python, nous pouvons facilement associer des valeurs à nos objets :

```
class User:
    pass

# Instanciation d'un objet de type User
john = User()

# Définition d'attributs pour cet objet
john.id = 1
john.name = 'john'
john.password = '12345'

print('Bonjour, je suis {}.'.format(john.name))
print('Mon id est le {}.'.format(john.id))
print('Mon mot de passe est {}.'.format(john.password))
```

Le code ci-dessus affiche :

```
Bonjour, je suis john.
Mon id est le 1.
Mon mot de passe est 12345.
```

Nous avons instancié un objet nommé `john`, de type `User`, auquel nous avons attribué trois attributs. Puis nous avons affiché les valeurs de ces attributs.

Notez que l’on peut redéfinir la valeur d’un attribut, et qu’un attribut peut aussi être supprimé à l’aide de l’opérateur `del`.

```
>>> john.password = 'mot de passe plus sécurisé !'
>>> john.password
'mot de passe plus sécurisé !'
>>> del john.password
>>> john.password
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
AttributeError: 'User' object has no attribute 'password'
```

Il est généralement déconseillé de nommer une valeur de la même manière qu’une fonction python:built-in. On évitera par exemple d’avoir une variable `id`, `type` ou `list`.

Dans le cas d’un attribut, cela n’est pas gênant car cela ne fait pas partie du même espace de noms. En effet, `john.id` n’entre pas en conflit avec `id`.

### Les méthodes

Les **méthodes sont les opérations applicables sur les objets**. Ce sont en fait **des fonctions qui recoivent notre objet en premier paramètre**.

Nos objets `User` ne contiennent pas encore de méthode, nous découvrirons comment en ajouter dans le chapitre suivant. Mais nous pouvons déjà imaginer une méthode `check_pwd` (check password) pour vérifier qu’un mot de passe entré correspond bien au mot de passe de notre utilisateur.

```
def user_check_pwd(user, password):
    return user.password == password
```

```
>>> user_check_pwd(john, 'toto')
False
>>> user_check_pwd(john, '12345')
True
```

Les méthodes recevant l’objet en paramètre, elles peuvent en lire et modifier les attributs. Souvenez-vous par exemple de la méthode `append()` des listes, qui permet d’insérer un nouvel élément, elle modifie bien la liste en question.

À travers cette partie nous avons défini et exploré la notion d’objet.

Un terme a pourtant été omis, le terme «**classe**». Il s’agit en Python d’un synonyme de «**type**». Un objet étant le fruit d’une classe, il est temps de nous intéresser à cette dernière et à sa construction.

## Classes

On définit une classe à l’aide du mot-clef `class` survolé plus tôt :

```
class User:
    pass
```

(l’instruction `pass` sert ici à indiquer à Python que le corps de notre classe est vide)

Il est conseillé en Python de nommer sa classe suivant `MonNomDeClasse`, c’est à dire qu’un nom est composé d’une suite de mots dont la première lettre est une capitale. On préférera par exemple une classe `MonNomDeClasse` que `mon_nom_de_classe`. Exception faite des types `builtins` qui sont couramment écrits en lettres minuscules.

On **instancie** une classe de la même manière qu’on appelle une fonction, **en suffixant son nom d’une paire de parenthèses**. Cela est valable pour notre classe `User`, mais aussi pour les autres classes évoquées plus haut.

```
>>> User()
<__main__.User object at 0x7fc28e538198>
>>> int()
0
>>> str()
''
>>> list()
[]
```

La classe `User` est identique, elle ne comporte aucune méthode. Pour définir une méthode dans une classe, il suffit de procéder comme pour une définition de fonction, mais dans le corps de la classe en question.

```
class User:
    def check_pwd(self, password):
        return self.password == password
```

Notre nouvelle classe `User` possède maintenant une méthode `check_pwd` applicable sur tous ses objets.

```
>>> john = User()
>>> john.id = 1
>>> john.name = 'john'
>>> john.password = '12345'
>>> john.check_pwd('toto')
False
>>> john.check_pwd('12345')
True
```

Quel est ce `self` reçu en premier paramètre par `check_pwd` ? Il s’agit simplement de l’objet sur lequel on applique la méthode, comme expliqué dans le chapitre précédent. Les autres paramètres de la méthode arrivent après.

La méthode étant définie au niveau de la classe, elle n’a que ce moyen pour savoir quel objet est utilisé. C’est un comportement particulier de Python, mais retenez simplement qu’appeler `john.check_pwd('12345')` équivaut à l’appel `User.check_pwd(john, '12345')`. C’est pourquoi `john` correspondra ici au paramère `self` de notre méthode.

`self` n’est pas un mot-clef du langage Python, le paramètre pourrait donc prendre n’importe quel autre nom. Mais il conservera toujours ce nom par convention.

Notez aussi, dans le corps de la méthode `check_pwd`, que `password` et `self.password` sont bien deux valeurs distinctes : la première est le paramètre reçu par la méthode, tandis que la seconde est l’attribut de notre objet.

### Portées et espaces de nommage en Python

Les définitions de classes font d’habiles manipulations avec les espaces de nommage, vous devez donc savoir comment les portées et les espaces de nommage fonctionnent.

Commençons par quelques définitions.

**Un espace de nommage est une table de correspondance entre des noms et des objets**. La plupart des espaces de nommage sont actuellement implémentés sous forme de dictionnaires Python, mais ceci n’est normalement pas visible (sauf pour les performances) et peut changer dans le futur. Comme exemples d’espaces de nommage, nous pouvons citer les primitives (fonctions comme `abs()` et les noms des exceptions de base) ; les noms globaux dans un module ; et les noms locaux lors d’un appel de fonction. D’une certaine manière, l’ensemble des attributs d’un objet forme lui-même un espace de nommage. L’important à retenir concernant les espaces de nommage est qu’il n’y a absolument aucun lien entre les noms de différents espaces de nommage ; par exemple, deux modules différents peuvent définir une fonction `maximize` sans qu’il n’y ait de confusion. Les utilisateurs des modules doivent préfixer le nom de la fonction avec celui du module.

À ce propos, nous utilisons le mot «**attribut**» pour tout nom suivant un point. Par exemple, dans l’expression `z.real`, `real` est un attribut de l’objet `z`. Rigoureusement parlant, les références à des noms dans des modules sont des références d’attributs : dans l’expression `nommodule.nomfonction`, `nommodule` est un objet module et `nomfonction` est un attribut de cet objet. Dans ces conditions, il existe une correspondance directe entre les attributs du module et les noms globaux définis dans le module : ils partagent le même espace de nommage!

Les attributs peuvent être en lecture seule ou modifiables. S’ils sont modifiables, l’affectation à un attribut est possible. Les attributs de modules sont modifiables : vous pouvez écrire `nommodule.la_reponse = 42`.
Les attributs modifiables peuvent aussi être effacés avec l’instruction `del`. Par exemple, `del nommodule.la_reponse` supprime l’attribut
`la_reponse` de l’objet nommé `nommodule`.

Les espaces de nommage sont créés à différents moments et ont différentes durées de vie. L’espace de nommage contenant les primitives est créé au démarrage de l’interpréteur Python et n’est jamais effacé. L’espace de nommage globaux pour un module est créé lorsque la définition du module est lue. Habituellement, les espaces de nommage des modules durent aussi jusqu’à l’arrêt de l’interpréteur. Les instructions exécutées par la première invocation de l’interpréteur, qu’elles soient lues depuis un fichier de script ou de manière interactive, sont considérées comme faisant partie d’un module appelé `__main__`, de façon qu’elles possèdent leur propre espace de nommage (les primitives vivent elles-mêmes dans un module, appelé `builtins`).

L’espace des noms locaux d’une fonction est créé lors de son appel, puis effacé lorsqu’elle renvoie un résultat ou lève une exception non prise en charge (en fait, «oublié» serait une meilleure façon de décrire ce qui se passe réellement). Bien sûr, des invocations récursives ont chacune leur propre espace de nommage.

**La portée** est la zone textuelle d’un programme Python où un espace de nommage est directement accessible. «Directement accessible» signifie ici qu’une référence non qualifiée à un nom est cherchée dans l’espace de nommage. Bien que les portées soient déterminées de manière statique, elles sont utilisées de manière dynamique. À n’importe quel moment de l’exécution, il y a au minimum trois ou quatre portées imbriquées dont les espaces de nommage sont directement accessibles :


* la portée la plus au centre, celle qui est consultée en premier, contient les noms locaux ;


* les portées des fonctions englobantes, qui sont consultées en commençant avec la portée englobante la plus proche, contiennent des noms non-locaux mais aussi non-globaux ;


* l’avant-dernière portée contient les noms globaux du module courant ;


* la portée englobante, consultée en dernier, est l’espace de nommage contenant les primitives.

Si un nom est déclaré comme `global`, alors toutes les références et affectations vont directement dans la portée intermédiaire contenant les noms globaux du module. Pour pointer une variable qui se trouve en dehors de la portée la plus locale, vous pouvez utiliser l’instruction `nonlocal`. Si une telle variable n’est pas déclarée `nonlocal`, elle est en lecture seule (toute tentative de la modifier crée simplement une nouvelle variable dans la portée la plus locale, en laissant inchangée la variable du même nom dans sa portée d’origine).

Habituellement, la portée locale référence les noms locaux de la fonction courante. En dehors des fonctions, la portée locale référence le même espace de nommage que la portée globale : l’espace de nommage du module. Les définitions de classes créent un nouvel espace de nommage dans la portée locale.

Il est important de réaliser que les portées sont déterminées de manière textuelle : la portée globale d’une fonction définie dans un module est l’espace de nommage de ce module, quelle que soit la provenance de l’appel à la fonction. En revanche, la recherche réelle des noms est faite dynamiquement au moment de l’exécution. Cependant la définition du langage est en train d’évoluer vers une résolution statique des noms au moment de la «compilation», donc ne vous basez pas sur une résolution dynamique (en réalité, les variables locales sont déjà déterminées de manière statique)!

Une particularité de Python est que, si aucune instruction `global` ou `nonlocal` n’est active, les affectations de noms vont toujours dans la
portée la plus proche. Les affectations ne copient aucune donnée : elles se contentent de lier des noms à des objets. Ceci est également vrai pour l’effacement : l’instruction del x supprime la liaison de x dans l’espace de nommage référencé par la portée locale. En réalité, toutes les opérations qui impliquent des nouveaux noms utilisent la portée locale : en particulier, les instructions import et les définitions de fonctions effectuent une liaison du module ou du nom de fonction dans la portée locale.

L’instruction `global` peut être utilisée pour indiquer que certaines variables existent dans la portée globale et doivent être reliées en local ; l’instruction `nonlocal` indique que certaines variables existent dans une portée supérieure et doivent être reliées en local.

#### Exemple de portées et d’espaces de nommage

Ceci est un exemple montrant comment utiliser les différentes portées et espaces de nommage, et comment `global` et `nonlocal` modifient l’affectation de variable :

```
def scope_test():
    def do_local():
        spam = "local spam"

    def do_nonlocal():
        nonlocal spam
        spam = "nonlocal spam"

    def do_global():
        global spam
        spam = "global spam"

    spam = "test spam"
    do_local()
    print("Après affectation locale:", spam)
    do_nonlocal()
    print("Après affectation non locale:", spam)
    do_global()
    print("Après affectation générale:", spam)

scope_test()
print("A portée générale:", spam)
```

Ce code donne le résultat suivant :

```
Après affectation locale: test spam
Après affectation non locale: nonlocal spam
Après affectation générale: nonlocal spam
A portée générale: global spam
```

Vous pouvez constater que l’affectation locale (qui est effectuée par défaut) n’a pas modifié la liaison de `spam` dans `scope_test`. L’affectation `nonlocal` a changé la liaison de `spam` dans `scope_test` et l’affectation `global` a changé la liaison au niveau du module.

Vous pouvez également voir qu’aucune liaison pour spam n’a été faite avant l’affectation `global`.

### Passages d’arguments

Nous avons vu qu’instancier une classe était semblable à un appel de fonction. Dans ce cas, comment passer des arguments à une classe, comme on le ferait pour une fonction ?

Il faut pour cela comprendre les bases du mécanisme d’instanciation de Python. Quand on appelle une classe, un nouvel objet de ce type est construit en mémoire, puis initialisé. Cette initialisation permet d’assigner des valeurs à ses attributs.

L’objet est initialisé à l’aide d’une méthode spéciale de sa classe, la méthode `__init__`. Cette dernière recevra les arguments passés lors de l’instanciation.

```
class User:
    def __init__(self, id, name, password):
        self.id = id
        self.name = name
        self.password = password

    def check_pwd(self, password):
        return self.password == password
```

Nous retrouvons dans cette méthode le paramètre `self`, qui est donc utilisé pour modifier les attributs de l’objet.

```
>>> john = User(1, 'john', '12345')
>>> john.check_pwd('toto')
False
>>> john.check_pwd('12345')
True
```

### Méthodes spéciales __repr__ et __str__

Nous avons vu précédemment la méthode `__init__`, permettant d’initialiser les attributs d’un objet. On appelle cette méthode une méthode spéciale, il y en a encore beaucoup d’autres en Python. Elles sont reconnaissables par leur nom débutant et finissant par deux underscores.

Vous vous êtes peut-être déjà demandé d’où provenait le résultat affiché sur la console quand on entre simplement le nom d’un objet.

```
>>> import datetime
>>> aujourdhui = datetime.datetime.now()
>>> str(aujourdhui)
'2020-10-06 12:19:45.099479'
>>> repr(aujourdhui)
'datetime.datetime(2020, 10, 6, 12, 19, 45, 99479)'
>>> resultat = eval(repr(aujourdhui))
>>> print(resultat)
2020-10-06 12:19:45.099479
```

```
>>> john = User(1, 'john', '12345')
>>> john
<__main__.User object at 0x7fefd77fae10>
```

Il s’agit en fait de la représentation d’un objet, calculée à partir de sa méthode spéciale `__repr__`.

```
>>> john.__repr__()
'<__main__.User object at 0x7fefd77fae10>'
```

À noter qu’une méthode spéciale n’est presque jamais directement appelée en Python, on lui préférera dans le cas présent la fonction builtin `repr`.

```
>>> repr(john)
'<__main__.User object at 0x7fefd77fae10>'
```

Il nous suffit alors de redéfinir cette méthode `__repr__` pour bénéficier de notre propre représentation.

```
>>> class User:
...     def __repr__(self):
...         return '<User: {}, {}>'.format(self.id, self.name)
>>> User(1, 'john', '12345')
<User: 1, john>
```

Une autre opération courante est la conversion de notre objet en chaîne de caractères afin d’être affiché via print par exemple. Par défaut, la conversion en chaîne correspond à la représentation de l’objet, mais elle peut être surchargée par la méthode `__str__`.

```
>>> class User:
...
... def __repr__(self):
...     return '<User: {}, {}>'.format(self.id, self.name)
...
... def __str__(self):
...     return '{}-{}'.format(self.id, self.name)
>>> john = User(1, 'john', 12345)
>>> john
<User: 1, john>
>>> repr(john)
'<User: 1, john>'
>>> str(john)
'1-john'
>>> print(john)
1-john
```

Exemple :

```
>>> class Fraction:
...     def __init__(self, num, den):
...         self.__num = num
...         self.__den = den
...
...     def resultat(self):
...         return 1/2
...
...     def __str__(self):
...         return str(self.resultat())
...
...     def __repr__(self):
...         return str(self.__num) + '/' + str(self.__den)
>>> f = Fraction(1,2)
>>> f.resultat()
0.5
>>> print(f)
>>> print('Valeur numérique de ma fraction : ' + str(f))
Valeur numérique de ma fraction : 0.5
>>> print('Représentation de ma fraction : ', repr(f))
Représentation de ma fraction :  1/2
```

Exercice :

```
>>> from math import *
>>> class Racine:
...     def __init__(self, valeur):
...         self.__valeur = valeur
...
...     def resultat(self):
...         return sqrt(self.__valeur)
...
...     def __str__(self):
...         return str(self.resultat())
...
...     def __repr__(self):
...         return '√' + str(self.__valeur)
...
>>> nombre = Racine(2)
>>> nombre.resultat()
1.4142135623730951
>>> print(nombre)
1.4142135623730951
>>> repr(nombre)
'√2'
```

### Variables privées, l’encapsulation

Au commencement étaient les invariants

Les différents attributs de notre objet forment un état de cet objet, normalement stable. Ils sont en effet liés les uns aux autres, la modification d’un attribut pouvant avoir des conséquences sur un autre. Les invariants correspondent aux relations qui lient ces différents attributs.

Imaginons que nos objets `User` soient dotés d’un attribut contenant une évaluation du mot de passe (savoir si ce mot de passe est assez sécurisé ou non), il doit alors être mis à jour chaque fois que nous modifions l’attribut `password` d’un objet `User`.

Dans le cas contraire, le mot de passe et l’évaluation ne seraient plus corrélés, et notre objet `User` ne serait alors plus dans un état stable. Il est donc important de veiller à ces invariants pour assurer la stabilité de nos objets.

Protège-moi

Au sein d’un objet, les attributs peuvent avoir des sémantiques différentes. Certains attributs vont représenter des propriétés de l’objet et faire partie de son interface (tels que le prénom et le nom de nos objets `User`). Ils pourront alors être lus et modifiés depuis l’extérieur de l’objet, on parle dans ce cas d’attributs publics.

D’autres vont contenir des données internes à l’objet, n’ayant pas vocation à être accessibles depuis l’extérieur. Nous allons sécuriser notre stockage du mot de passe en ajoutant une méthode pour le hasher (à l’aide du module `crypt`), afin de ne pas stocker d’informations sensibles dans l’objet. Ce condensat du mot de passe ne devrait pas être accessible de l’extérieur, et encore moins modifié (ce qui en altérerait la sécurité).

De la même manière que pour les attributs, certaines méthodes vont avoir une portée publique et d’autres privée (on peut imaginer une méthode interne de la classe pour générer notre identifiant unique). On nomme **encapsulation** cette notion de protection des attributs et méthodes d’un objet, dans le respect de ses invariants.

Certains langages implémentent dans leur syntaxe des outils pour gérer la visibilité des attributs et méthodes, mais il n’y a rien de tel en Python. Il existe à la place des conventions, qui indiquent aux développeurs quels attributs/méthodes sont publics ou privés. Quand vous voyez un nom d’attribut ou méthode débuter par un «**_**» au sein d’un objet, il indique quelque chose d’interne à l’objet (privé), dont la modification peut avoir des conséquences graves sur la stabilité.

```
>>> import crypt
>>> class User:
...     def __init__(self, id, name, password):
...         self.id = id
...         self.name = name
...         self._salt = crypt.mksalt() # sel utilisé pour le hash du mot de passe
...         self._password = self._crypt_pwd(password)
...
...     def _crypt_pwd(self, password):
...         return crypt.crypt(password, self._salt)
...
...     def check_pwd(self, password):
...         return self._password == self._crypt_pwd(password)
...
>>> john = User(1, 'john', '12345')
>>> john.check_pwd('12345')
True
```

On note toutefois qu’il ne s’agit que d’une convention, l’attribut `_password` étant parfaitement visible depuis l’extérieur.

```
>>> john._password
'$6$DwdvE5H8sT71Huf/$9a.H/VIK4fdwIFdLJYL34yml/QC3KZ7'
```

Il reste possible de masquer un peu plus l’attribut à l’aide du préfixe `__`. Ce préfixe a pour effet de renommer l’attribut en y insérant le nom de la classe courante.

```
>>> class User:
...     def __init__(self, id, name, password):
...         self.id = id
...         self.name = name
...         self.__salt = crypt.mksalt()
...         self.__password = self.__crypt_pwd(password)
...
...     def __crypt_pwd(self, password):
...         return crypt.crypt(password, self.__salt)
...
...     def check_pwd(self, password):
...         return self.__password == self.__crypt_pwd(password)
>>> john = User(1, 'john', '12345')
>>> john.__password
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
AttributeError: 'User' object has no attribute '__password'
>>> john._User__password
'$6$kjwoqPPHRQAamRHT$591frrNfNNb3.RdLXYiB/bgdCC4Z0p.B'
```

Ce comportement pourra surtout être utile pour éviter des conflits de noms entre attributs internes de plusieurs classes sur un même objet, que nous verrons lors de l’héritage.

Le hashage d’un mot de passe correspond à une opération non-réversible qui permet de calculer un condensat (hash) du mot de passe. Ce condensat peut-être utilisé pour vérifier la validité d’un mot de passe, mais ne permet pas de retrouver le mot de passe d’origine.

C++, Java, Ruby, etc.

### Duck-typing

Un objet en Python est défini par sa structure (les attributs qu’il contient et les méthodes qui lui sont applicables) plutôt que par son type.

Ainsi, pour faire simple, un fichier sera un objet possédant des méthodes `read`, `write` et `close`. Tout objet respectant cette définition sera considéré par Python comme un fichier.

```
class FakeFile:
    def read(self, size=0):
        return ''

    def write(self, s):
        return 0

    def close(self):
        pass

f = FakeFile()
print('foo', file=f)
```

Python est entièrement construit autour de cette idée, appelée **duck-typing** : «**Si je vois un animal qui vole comme un canard, cancane comme un canard, et nage comme un canard, alors j’appelle cet oiseau un canard**» (James Whitcomb Riley)

Exercice :

Pour ce premier exercice, nous allons nous intéresser aux classes d’un forum. Forts de notre type `User` pour représenter un utilisateur, nous souhaitons ajouter une classe `Post`, correspondant à un quelconque message.

Cette classe sera inititalisée avec un auteur (un objet `User`) et un contenu textuel (le corps du message). Une date sera de plus générée lors de la création.

Un Post possèdera une méthode format pour retourner le message formaté, correspondant au HTML suivant :

```
<div>
    <span>Par NOM_DE_L_AUTEUR le DATE_AU_FORMAT_JJ_MM_YYYY à HEURE_AU_FORMAT_HH_MM_SS</span>
    <p>
        CORPS_DU_MESSAGE
    </p>
</div>
```

De plus, nous ajouterons une méthode post à notre classe User, recevant un corps de message en paramètre et retournant un nouvel objet Post.

```
import crypt
import datetime

class User:
    def __init__(self, id, name, password):
        self.id = id
        self.name = name
        self._salt = crypt.mksalt()
        self._password = self._crypt_pwd(password)

    def _crypt_pwd(self, password):
        return crypt.crypt(password, self._salt)

    def check_pwd(self, password):
        return self._password == self._crypt_pwd(password)

    def post(self, message):
        return Post(self, message)

class Post:
    def __init__(self, author, message):
        self.author = author
        self.message = message
        self.date = datetime.datetime.now()

    def format(self):
        date = self.date.strftime('le %d/%m/%Y à %H:%M:%S')
        return '<div><span>Par {} {}</span><p>{}</p></div>'.format(self.author.name, date, self.message)

if __name__ == '__main__':
    user = User(1, 'john', '12345')
    p = user.post('Salut à tous')
    print(p.format())
```

Nous savons maintenant définir une classe et ses méthodes, initialiser nos objets, et protéger les noms d’attributs/méthodes.

Mais jusqu’ici, quand nous voulons étendre le comportement d’une classe, nous la redéfinissons entièrement en ajoutant de nouveaux attributs/méthodes. Le chapitre suivant présente l’héritage, un concept qui permet d’étendre une ou plusieurs classes sans toucher au code initial.

## Documenter les classes d’objets

Maintenant qu’on sait documenter une fonction, documentons une classe d’objets.

Exemple de documentation d’une classe `MaClasse` dans un module `mon_module` d’un paquet `mon_paquet`  :

```
# -*- coding: utf-8 -*-

"""
.. sectionauthor:: Stagiaire ADMINISTRATEUR <stagiaire.administrateur@fai.fr>
:mod:`mon_module` -- Module d'exemple de documentation d'une classe
###################################################################

.. module:: mon_paquet.mon_module
   :platform: Linux
   :synopsis: Ce module illustre comment écrire votre docstring pour une classe dans Python.
.. moduleauthor:: Formateur PYTHON <formateur.python@fai.fr>
.. moduleauthor:: Stagiaire ADMINISTRATEUR <stagiaire.administrateur@fai.fr>

"""

__title__ = "Module illustration écriture docstring d'une classe Python"
__author__ = "Formateur PYTHON"
__version__ = '0.7.3'
__release_life_cycle__ = 'alpha'
# pre-alpha = faisabilité, alpha = développement, beta = test, rc = qualification, prod = production
__docformat__ = 'reStructuredText'

class MaClasse():
    """
    Exemple de classe de mon_paquet.mon_module

    :param arg: argument du constructeur MaClasse
    :type arg: int
    """

    def __init__(self, arg):
        """
        Constructeur de MaClasse

        Les propriétés de la classe sont :
        :param param: p1
        :type param: int
        """
        self.p1 = None

    def bonjour(self, nom):
        """
        Permet d'afficher le message « Bonjour à toi <nom> »

        :param nom: Nom de la personne
        :type nom: str
        :return: Message de bonjour
        :rtype: str:
        """
        print("Bonjour " + nom)
        return "Bonjour à toi " + nom

if __name__ == "__main__":
    mon_objet = MaClasse()
    print mon_objet.bonjour("padawan")
```

Finalement, il n’y a rien de bien nouveau. On a documenté les méthodes de la classe `MaClasse` comme on l’a fait avec les fonctions.

**La seule différence c’est avec la méthode constructeur** `__init__` de la classe `MaClasse` où la docstring des paramètres de classe est directement dans la déclaration de la classe (`:param arg: argument du constructeur MaClasse` et `:type arg: int`).

C’est **le fonctionnement par défaut avec autodoc**.
Cette disposition permet de séparer la déclaration des propriétés de la classe, qui sont définies dans la méthode `__init__`, avec les paramètres de création d’objets de la classe `MaClasse` qui sont définis dans la déclaration des paramètres de la méthode `__init__`.

Ce comportement est réglable via une option `autoclass_content = 'configuration'` dans le fichier «**conf.py**».
Si vous préférez documenter avec le constructeur `__init__` les paramètres de création d’objet avec les propriétés de la classe, ce paramètre vous permet de définir comment seront insérés les paramètres de la classe avec «**autoclass**». Exemple pour que cela soit avec la déclaration de propriétés dans `__init__` :

```
autoclass_content = 'init'
```

Les valeurs possibles sont :


* **« class »** : Seule la docstring de la classe est insérée. C’est la valeur par défaut. Vous pouvez toujours documenter `__init__` en tant que méthode distincte en utilisant «**automethod**» ou l’option «**members**» pour générer automatique la documentation de vos classes.


* **« both »** : La docstring de la classe et de la méthode `__init__` sont concaténées et insérées.


* **« init »** : Seule la docstring de la méthode `__init__` est insérée.

**WARNING**: Si la classe n’a pas de méthode `__init__`, ou si la docstring de la méthode `__init__` est vide, et que la classe a une docstring avec la méthode `__new__` celle-ci sera utilisée à la place.

Nous avons maintenant abordé l’essentiel pour commencer une rédaction complète de sa documentation du code Python. Vous pouvez encore approfondir avec plein de directives Sphinx utiles [avec ce lien](https://devguide.python.org/documenting/).

## Notions avancées en objet

### Extension de classes

Nous allons nous intéresser à l’extension de classes.

Imaginons que nous voulions définir une classe `Admin`, pour gérer des administrateurs, qui réutiliserait le même code que la classe `User`. Tout ce que nous savons faire actuellement c’est copier/coller le code de la classe `User` en changeant son nom pour `Admin`.

Nous allons maintenant voir comment faire ça de manière plus élégante, grâce à l’héritage. Nous étudierons de plus les relations entre classes ansi créées.

Nous utiliserons donc la classe `User` suivante pour la suite de ce chapitre.

```
class User:
    """ Défini des propriétés d'un utilisateur """
    def __init__(self, id, name, password):
        """ Initialisation de l'utilisateur """
        self.id = id
        self.name = name
        self._salt = crypt.mksalt()
        self._password = self._crypt_pwd(password)

    def _crypt_pwd(self, password):
        """ Calcule un mot de passe crypté """
        return crypt.crypt(password, self._salt)

    def check_pwd(self, password):
        """ Vérifie le mot de passe """
        return self._password == self._crypt_pwd(password)
```

#### Hériter

L’héritage simple est le mécanisme permettant d’étendre une unique classe. Il consiste à créer une nouvelle classe (fille) qui bénéficiera des mêmes méthodes et attributs que sa classe mère. Il sera aisé d’en définir de nouveaux dans la classe fille, et cela n’altèrera pas le fonctionnement de la mère.

Par exemple, nous voudrions étendre notre classe `User` pour ajouter la possibilité d’avoir des administrateurs. Les administrateurs (`Admin`) possèderaient une nouvelle méthode, `manage`, pour administrer le système.

```
class Admin(User):
    """ Défini des propriétés d'un administrateur """
    def manage(self):
        """ Ajoute des fonctions d'administrateur """
        print('Je suis un Jedi!')
```

En plus des méthodes de la classe `User` (`__init__`, `_crypt_pwd` et `check_pwd`), Admin possède aussi une méthode `manage`.

```
>>> root = Admin(1, 'root', 'toor')
>>> root.check_password('toor')
True
>>> root.manage()
Je suis un Jedi!
>>> john = User(2, 'john', '12345')
>>> john.manage()
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
AttributeError: 'User' object has no attribute 'manage'
```

Nous pouvons avoir deux classes différentes héritant d’une même mère

```
class Guest(User):
    pass
```

`Admin` et `Guest` sont alors deux classes filles de `User`.

L’héritage simple permet aussi d’hériter d’une classe qui hérite elle-même d’une autre classe.

```
class SuperAdmin(Admin):
    pass
```

`SuperAdmin` est alors la fille de `Admin`, elle-même la fille de `User`. On dit alors que `User` **est une ancêtre** de `SuperAdmin`.

On peut constater quels sont les parents d’une classe à l’aide de l’attribut spécial `__bases__` des classes :

```
>>> Admin.__bases__
(<class '__main__.User'>,)
>>> Guest.__bases__
(<class '__main__.User'>,)
>>> SuperAdmin.__bases__
(<class '__main__.Admin'>,)
```

Que vaudrait alors `User.__bases__`, sachant que la classe `User` est définie sans héritage ?

```
>>> User.__bases__
(<class 'object'>,)
```

On remarque que, sans que nous n’ayons rien demandé, `User` hérite de `object`. En fait, `object` est l’ancêtre de toute classe Python. Ainsi, quand aucune classe parente n’est définie, c’est `object` qui est choisi.

##### Sous-typage

Nous avons vu que l’héritage permettait d’étendre le comportement d’une classe, mais ce n’est pas tout. L’héritage a aussi du sens au niveau des types, en créant un nouveau type compatible avec le parent.

En Python, la fonction `isinstance` permet de tester si un objet est l’instance d’une certaine classe.

```
>>> isinstance(root, Admin)
True
>>> isinstance(root, User)
True
>>> isinstance(root, Guest)
False
>>> isinstance(root, object)
True
```

Mais gardez toujours à l’esprit qu’en Python, on préfère se référer à la structure d’un objet qu’à son type (duck-typing), les tests à base de `isinstance` sont donc à utiliser pour des cas particuliers uniquement, où il serait difficile de procéder autrement.

#### Redéfinition de méthodes, la surcharge

Nous savons hériter d’une classe pour y insérer de nouvelles méthodes, mais nous ne savons pas étendre les méthodes déjà présentes dans la classe mère. La redéfinition est un concept qui permet de remplacer une méthode du parent.

Nous voudrions que la classe `Guest` ne possède plus aucun mot de passe. Celle-ci devra modifier la méthode `check_pwd` pour accepter tout mot de passe, et simplifier la méthode `__init__`.

On ne peut pas à proprement parler étendre le contenu d’une méthode, mais on peut la redéfinir :

```
class Guest(User):
def __init__(self, id, name):
    self.id = id
    self.name = name
    self._salt = ''
    self._password = ''

def check_pwd(self, password):
    return True
```

Cela fonctionne comme souhaité, mais vient avec un petit problème, le code de la méthode `__init__` est répété. En l’occurrence il ne s’agit que de 2 lignes de code, mais lorsque nous voudrons apporter des modifications à la méthode de la classe `User`, il faudra les répercuter sur `Guest`, ce qui donne vite quelque chose de difficile à maintenir.

Heureusement, Python nous offre un moyen de remédier à ce mécanisme, `super`! Oui, `super`, littéralement, une fonction un peu spéciale en Python, qui nous permet d’utiliser la classe parente (superclass).

`super` est une fonction qui prend initialement en paramètre une classe et une instance de cette classe. Elle retourne un objet proxy (Un proxy est un intermédiaire transparent entre deux entités) qui s’utilise comme une instance de la classe parente.

```
>>> guest = Guest(3, 'Guest')
>>> guest.check_pwd('password')
True
>>> super(Guest, guest).check_pwd('password')
False
```

Au sein de la classe en question, les arguments de `super` peuvent être omis (ils correspondront à la classe et à l’instance courantes), ce qui nous permet de simplifier notre méthode `__init__` et d’éviter les répétitions.

```
class Guest(User):
def __init__(self, id, name):
    super().__init__(id, name, '')

def check_pwd(self, password):
    return True
```

On notera tout de même que contrairement aux versions précédentes, l’initialisateur de `User` est appelé en plus de celui de `Guest`, et donc qu’un `self` et un hash du mot de passe sont générés alors qu’ils ne serviront pas.

Ça n’est pas très grave dans le cas présent, mais pensez-y dans vos développements futurs, afin de ne pas exécuter d’opérations coûteuses inutilement.

#### Héritage Conditionnel

Il nous arrive souvent en Python d’être bloqué, lors de la création d’une classe, parce que l’héritage est conditionné à une variable passée en paramètre de la classe.

Se présente alors deux cas :


* Le premier est un comportement de classe complètement différent avec ses méthodes et propriétés, c’est un proxy (filtre) de classes qu’il nous faudra utiliser.


* Le deuxième est un ajout de propriétés et de méthodes à la classe avec un vrai héritage conditionnel.

Nous allons voir comment résoudre cela avec la déclaration de classe `def __new__()`

##### Proxy de classes

Dans cet exercice, nous allons créer deux classes distinctes (`A` et `B`) qui suivant un paramètre passé à une métaclasse `Proxy` va retourner la bonne classe.

```
class A:
    def __init__(self, mon_paramètreA):
        self.ma_propriétéA = mon_paramètreA

    def maMéthodeA(self):
        pass

class B:
    def __init__(self, mon_paramètreB):
        self.ma_propriétéB = mon_paramètreB

    def maMéthodeB(self):
        pass

class Proxy:
    def __new__(cls, mon_paramètre):
        if mon_paramètre == 'valeur1':
            return A(mon_paramètre)
        if mon_paramètre == 'valeur2':
            return B(mon_paramètre)
```

Nous voyons ici que la déclaration `def __new__()` nous permet de récupérer les paramètres passés à la classe `Proxy`, ce qui nous permet avec un test de renvoyer un objet créé avec la bonne classe. C’est pour cela que l’on parle de Proxy de classe. Le paramètre `cls` représente la classe qui a besoin d’être instanciée.

On peut bien sur changer les conditions de filtrage suivant le type de test que l’on veut, ou augmenter le nombre de classes en option. Mais ici dans cette section nous parlons d’héritage de classe, nous allons voir avec ce procédé comment créer un héritage conditionnel.

##### Héritage conditionnel

Nous introduisons ici un nouveau concept pour les héritages, l’héritage conditionnel de classes `ClasseAHeriter if mon_paramètre == 'Valeur' else object`. La difficulté de la solution vient du fait que la variable passée à la classe ne peut être récupérée avant l’héritage de classe, `mon_paramètre` doit donc être `global` pour que la condition d’héritage fonctionne. Avec le proxy nous outrepassons cette limite de façon élégante…

Ici dans cet exercice, nous allons créer un héritage conditionnel de la classe `A` dans la classe `B` suivant un paramètre passé au travers de la classe proxy `ProxyHeritage`.

```
class A:
    def __init__(self, mon_paramètreA):
        self.ma_propriétéA = mon_paramètreA

    def maMéthodeA(self):
        pass

class ProxyHeritage:
    def __new__(cls, mon_paramètre):
        class B(A if mon_paramètre == 'Valeur' else object):
            def __init__(self, mon_paramètreB):
                self.ma_propriétéB = mon_paramètreB

            def maMéthodeB(self):
                pass
        return B(mon_paramètre)
```

Cette solution n’est pas satisfaisante. En effet si on veut hériter la classe `ProxyHeritage` le comportement de gestion des héritages de classes normal de Python n’est plus possible. Nous verrons plus loin dans ce cour comment faire.

Tout ceci nous permet maintenant d’introduire la section suivante.

#### Héritages Multiples

Avec l’héritage simple, nous pouvions étendre le comportement d’une classe. L’héritage multiple va nous permettre de le faire pour plusieurs classes à la fois. Il nous suffit de préciser plusieurs classes séparées par des virgules lors de la création de notre classe fille.

```
class A:
    def foo(self):
        return '!'

class B:
    def bar(self):
        return '?'

class C(A, B):
    pass
```

Notre classe `C` a donc deux mères : `A` et `B`. Cela veut aussi dire que les objets de type `C` possèdent à la fois les méthodes `foo` et `bar`.

```
>>> c = C()
>>> c.foo()
'!'
>>> c.bar()
'?'
```

##### Ordre d’héritage

L’ordre dans lequel on hérite des parents est important, il détermine dans quel ordre les méthodes seront recherchées dans les classes mères.
Ainsi, dans le cas où la méthode existe dans plusieurs parents, celle de la première classe sera conservée.

```
class A:
    def foo(self):
        return '!'

class B:
    def foo(self):
        return '?'

class C(A, B):
    pass

class D(B, A):
    pass
```

```
>>> C().foo()
'!'
>>> D().foo()
'?'
```

Cet ordre dans lequel les classes parentes sont explorées pour la recherche des méthodes est appelé **Method Resolution Order (MRO)**. On peut le connaître à l’aide de la méthode `mro` des classes.

```
>>> A.mro()
[<class '__main__.A'>, <class 'object'>]
>>> B.mro()
[<class '__main__.B'>, <class 'object'>]
>>> C.mro()
[<class '__main__.C'>, <class '__main__.A'>, <class '__main__.B'>, <class 'object'>]
>>> D.mro()
[<class '__main__.D'>, <class '__main__.B'>, <class '__main__.A'>, <class 'object'>]
```

C’est aussi ce MRO qui est utilisé par super pour trouver à quelle classe faire appel. super se charge d’explorer le MRO de la classe de l’instance qui lui est donnée en second paramètre, et de retourner un proxy sur la classe juste à droite de celle donnée en premier paramètre.

Ainsi, avec `c` une instance de `C`, :python\`super(C, c)\` retournera un objet se comportant comme une instance de `A`, `super(A, c)` comme une instance de `B`, et `super(B, c)` comme une instance de `object`.

```
>>> c = C()
>>> c.foo() # C.foo == A.foo
'!'
>>> super(C, c).foo() # A.foo
'!'
>>> super(A, c).foo() # B.foo
'?'
>>> super(B, c).foo() # object.foo -> méthode introuvable
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
AttributeError: 'super' object has no attribute 'foo'
```

Les classes parentes n’ont alors pas besoin de se connaître les unes les autres pour se référencer.

```
class A:
    def __init__(self):
        print("Début initialisation d'un objet de type A")
        super().__init__()
        print("Fin initialisation d'un objet de type A")

class B:
    def __init__(self):
        print("Début initialisation d'un objet de type B")
        super().__init__()
        print("Fin initialisation d'un objet de type B")

class C(A, B):
    def __init__(self):
        print("Début initialisation d'un objet de type C")
        super().__init__()
        print("Fin initialisation d'un objet de type C")

class D(B, A):
    def __init__(self):
        print("Début initialisation d'un objet de type D")
        super().__init__()
        print("Fin initialisation d'un objet de type D")
```

```
>>> C()
Début initialisation d'un objet de type C
Début initialisation d'un objet de type A
Début initialisation d'un objet de type B
Fin initialisation d'un objet de type B
Fin initialisation d'un objet de type A
Fin initialisation d'un objet de type C
<__main__.C object at 0x7f0ccaa970b8>
>>> D()
Début initialisation d'un objet de type D
Début initialisation d'un objet de type B
Début initialisation d'un objet de type A
Fin initialisation d'un objet de type A
Fin initialisation d'un objet de type B
Fin initialisation d'un objet de type D
<__main__.D object at 0x7f0ccaa971d0>
```

La méthode `__init__` des classes parentes n’est pas appelée automatiquement, et l’appel doit donc être réalisé explicitement.

C’est ainsi le `super().__init__()` présent dans la classe `C` qui appelle l’initialiseur de la classe `A`, qui appelle lui-même celui de la classe `B`. Inversement, pour la classe `D`, `super().__init__()` appelle l’initialiseur de `B` qui appelle celui de `A`.

On notera que les exemple donnés n’utilisent jamais plus de deux classes mères, mais il est possible d’en avoir autant que vous le souhaitez.

```
class A:
    pass

class B:
    pass

class C:
    pass

class D:
    pass

class E(A, B, C, D):
    pass
```

##### Mixins

Les **mixins** sont des classes dédiées à une fonctionnalité particulière, utilisable en héritant d’une classe de base et de ce mixin.

Par exemple, plusieurs types que l’on connaît sont appelés séquences (`str`, `list`, `tuple`). Ils ont en commun le fait d’implémenter l’opérateur `[]` et de gérer le slicing. On peut ainsi obtenir l’objet en ordre inverse à l’aide de `obj[::-1]`.

Un mixin qui pourrait nous être utile serait une classe avec une méthode `reverse` pour nous retourner l’objet inversé.

```
class Reversable:
    def reverse(self):
        return self[::-1]

class ReversableStr(Reversable, str):
    pass

class ReversableTuple(Reversable, tuple):
    pass
```

```
>>> s = ReversableStr('abc')
>>> s
'abc'
>>> s.reverse()
'cba'
>>> ReversableTuple((1, 2, 3)).reverse()
(3, 2, 1)
```

Ou encore nous pourrions vouloir ajouter la gestion d’une photo de profil à nos classes `User` et dérivées.

```
class ProfilePicture:
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.picture = '{}-{}.png'.format(self.id, self.name)

class UserPicture(ProfilePicture, User):
    pass

class AdminPicture(ProfilePicture, Admin):
    pass

class GuestPicture(ProfilePicture, Guest):
    pass
```

```
>>> john = UserPicture(1, 'john', '12345')
>>> john.picture
'1-john.png'
```

Exercice : Fils de discussion

Vous vous souvenez de la classe `Post` pour représenter un message ? Nous aimerions maintenant pouvoir instancier des fils de discussion (`Thread`) sur notre forum.

Qu’est-ce qu’un fil de discussion ?

Un message associé à un auteur et à une date ;

Mais qui comporte aussi un titre ;

Et une liste de posts (les réponses).

Le premier point indique clairement que nous allons réutiliser le code de la classe `Post`, donc en hériter.

Notre nouvelle classe sera initialisée avec un titre, un auteur et un message. `Thread` sera dotée d’une méthode `answer` recevant un auteur et un texte, et s’occupant de créer le post correspondant et de l’ajouter au fil. Nous changerons aussi la méthode `format` du `Thread` afin qu’elle concatène au fil l’ensemble de ses réponses.

La classe `Post` restera inchangée. Enfin, nous supprimerons la méthode `post` de la classe `User`, pour lui en ajouter deux nouvelles :


* `new_thread(title, message)` pour créer un nouveau fil de discussion associé à cet utilisateur ;


* `answer_thread(thread, message)` pour répondre à un fil existant.

```
import crypt
import datetime

class User:
    def __init__(self, id, name, password):
        self.id = id
        self.name = name
        self._salt = crypt.mksalt()
        self._password = self._crypt_pwd(password)

    def _crypt_pwd(self, password):
        return crypt.crypt(password, self._salt)

    def check_pwd(self, password):
        return self._password == self._crypt_pwd(password)

    def new_thread(self, title, message):
        return Thread(title, self, message)

    def answer_thread(self, thread, message):
        thread.answer(self, message)

class Post:
    def __init__(self, author, message):
        self.author = author
        self.message = message
        self.date = datetime.datetime.now()

    def format(self):
        date = self.date.strftime('le %d/%m/%Y à %H:%M:%S')
        return '<div><span>Par {} {}</span><p>{}</p></div>'.format(self.author.name, date, self.message)

class Thread(Post):
    def __init__(self, title, author, message):
        super().__init__(author, message)
        self.title = title
        self.posts = []

    def answer(self, author, message):
        self.posts.append(Post(author, message))

    def format(self):
        posts = [super().format()]
        posts += [p.format() for p in self.posts]
        return '\n'.join(posts)

if __name__ == '__main__':
    john = User(1, 'john', '12345')
    peter = User(2, 'peter', 'toto')
    thread = john.new_thread('Bienvenue', 'Bienvenue à tous')
    peter.answer_thread(thread, 'Merci')
    print(thread.format())
```

### L’héritage conditionnel

Nous revenons ici sur le traitement des héritages conditionnels déjà abordés. Pour optimiser notre code, et la réutilisation de classes, nous voulons pouvoir utiliser l’héritage conditionnel dans nos classes avec la méthode `__init__`. Nous voulons aussi avoir la possibilité de filtrer les méthodes et les propriétés de la classe à hériter.

Voici comment faire avec le fichier «**heritageconditionnel.py**» :

```
class Pere(object):
    def __init__(self, paramètrepère=False):
        self.mapropriété_père = None
        if paramètrepère:
            self.mapropriété_conditionnellepère = None
            def ma_Methode_Conditionnelle_père(self):
                pass
            self.ma_Methode_Conditionnelle_père = ma_Methode_Conditionnelle_père

    def ma_Methode_Père(self):
        pass


class Enfant(Pere):
    def __init__(self, paramètreEnfant=False, banieméthodes=[]):
        Pere.__init__(Pere, paramètreEnfant)
        self.mapropriété_enfant = None
        parent = Pere
        class new_parent(parent.__base__):
            pass
        parent_list = dir(parent)
        new_parent_list = dir(new_parent)
        candidates = set(parent_list) - set(new_parent_list)
        if paramètreEnfant:
            self.mapropriété_conditionnelleenfant = None
            def ma_Methode_Conditionnelle_Enfant(self):
                pass
            self.ma_Methode_Conditionnelle_Enfant = ma_Methode_Conditionnelle_Enfant
            for candidate in candidates:
                if candidate not in banieméthodes:
                    setattr(new_parent, candidate, parent.__getattribute__(parent, candidate))

        Enfant.__bases__ = (new_parent, )

    def ma_Methode_Enfant(self):
        pass


class PetitEnfant(Enfant):
    def __init__(self, paramètreEnfant=False, options=[]):
        self.mapropriétépetitenfant = None
        if paramètreEnfant:
            self.mapropriété_conditionnellepetitenfant = None
            def ma_Methode_Conditionnelle_PetitEnfant():
                pass
            self.ma_Methode_Conditionnelle_PetitEnfant = ma_Methode_Conditionnelle_PetitEnfant
            if options == []:
                super().__init__(paramètreEnfant)
            else:
                super().__init__(paramètreEnfant, options)
        elif options != []:
            super().__init__(True, banieméthodes=options)
        else:
            super().__init__()
    def ma_Methode_PetitEnfant(self):
        pass

if __name__ == '__main__':
    print('PetitEnfant()')
    a = PetitEnfant()
    print(dir(a))
    print('PetitEnfant(True)')
    a = PetitEnfant(True)
    print(dir(a))
    print('PetitEnfant(options=[\'ma_Methode_Père\']')
    a = PetitEnfant(options=['ma_Methode_Père'])
    print(dir(a))
    print("Enfant()")
    b = Enfant()
    print(dir(b))
    print("Enfant('True')")
    b = Enfant('True')
    print(dir(b))
    print("Père()")
    c = Pere()
    print(dir(c))
```

Ce qui nous donne en sortie d’exécution

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement/9_objets$ python3 ./heritageconditionnel.py
PetitEnfant()
['__class__', '__delattr__', '__dict__', '__dir__', '__doc__', '__eq__', '__format__', '__ge__', '__getattribute__', '__gt__', '__hash__', '__init__', '__init_subclass__', '__le__', '__lt__', '__module__', '__ne__', '__new__', '__reduce__', '__reduce_ex__', '__repr__', '__setattr__', '__sizeof__', '__str__', '__subclasshook__', '__weakref__', 'ma_Methode_Enfant', 'ma_Methode_PetitEnfant', 'mapropriété_enfant', 'mapropriétépetitenfant']
PetitEnfant(True)
['__class__', '__delattr__', '__dict__', '__dir__', '__doc__', '__eq__', '__format__', '__ge__', '__getattribute__', '__gt__', '__hash__', '__init__', '__init_subclass__', '__le__', '__lt__', '__module__', '__ne__', '__new__', '__reduce__', '__reduce_ex__', '__repr__', '__setattr__', '__sizeof__', '__str__', '__subclasshook__', '__weakref__', 'ma_Methode_Conditionnelle_Enfant', 'ma_Methode_Conditionnelle_PetitEnfant', 'ma_Methode_Conditionnelle_père', 'ma_Methode_Enfant', 'ma_Methode_PetitEnfant', 'ma_Methode_Père', 'mapropriété_conditionnelleenfant', 'mapropriété_conditionnellepetitenfant', 'mapropriété_conditionnellepère', 'mapropriété_enfant', 'mapropriété_père', 'mapropriétépetitenfant']
PetitEnfant(options=['ma_Methode_Père']
['__class__', '__delattr__', '__dict__', '__dir__', '__doc__', '__eq__', '__format__', '__ge__', '__getattribute__', '__gt__', '__hash__', '__init__', '__init_subclass__', '__le__', '__lt__', '__module__', '__ne__', '__new__', '__reduce__', '__reduce_ex__', '__repr__', '__setattr__', '__sizeof__', '__str__', '__subclasshook__', '__weakref__', 'ma_Methode_Conditionnelle_Enfant', 'ma_Methode_Conditionnelle_père', 'ma_Methode_Enfant', 'ma_Methode_PetitEnfant', 'mapropriété_conditionnelleenfant', 'mapropriété_conditionnellepère', 'mapropriété_enfant', 'mapropriété_père', 'mapropriétépetitenfant']
Enfant()
['__class__', '__delattr__', '__dict__', '__dir__', '__doc__', '__eq__', '__format__', '__ge__', '__getattribute__', '__gt__', '__hash__', '__init__', '__init_subclass__', '__le__', '__lt__', '__module__', '__ne__', '__new__', '__reduce__', '__reduce_ex__', '__repr__', '__setattr__', '__sizeof__', '__str__', '__subclasshook__', '__weakref__', 'ma_Methode_Enfant', 'mapropriété_enfant']
Enfant('True')
['__class__', '__delattr__', '__dict__', '__dir__', '__doc__', '__eq__', '__format__', '__ge__', '__getattribute__', '__gt__', '__hash__', '__init__', '__init_subclass__', '__le__', '__lt__', '__module__', '__ne__', '__new__', '__reduce__', '__reduce_ex__', '__repr__', '__setattr__', '__sizeof__', '__str__', '__subclasshook__', '__weakref__', 'ma_Methode_Conditionnelle_Enfant', 'ma_Methode_Conditionnelle_père', 'ma_Methode_Enfant', 'ma_Methode_Père', 'mapropriété_conditionnelleenfant', 'mapropriété_conditionnellepère', 'mapropriété_enfant', 'mapropriété_père']
Père()
['__class__', '__delattr__', '__dict__', '__dir__', '__doc__', '__eq__', '__format__', '__ge__', '__getattribute__', '__gt__', '__hash__', '__init__', '__init_subclass__', '__le__', '__lt__', '__module__', '__ne__', '__new__', '__reduce__', '__reduce_ex__', '__repr__', '__setattr__', '__sizeof__', '__str__', '__subclasshook__', '__weakref__', 'ma_Methode_Conditionnelle_père', 'ma_Methode_Père', 'mapropriété_conditionnellepère', 'mapropriété_père']
```

### Opérateurs

Il est maintenant temps de nous intéresser aux opérateurs du langage Python (`+`, `-`, `\*`, etc.). En effet, un code respectant la philosophie du langage se doit de les utiliser à bon escient.

Ils sont une manière claire de représenter des opérations élémentaires (addition, concaténation, …) entre deux objets. `a + b` est en effet plus lisible qu’un `add(a, b)` ou encore `a.add(b)`.

Ce chapitre a pour but de vous présenter les mécanismes mis en jeu par ces différents opérateurs, et la manière de les implémenter. Les opérateurs sont un autre type de méthodes spéciales que nous découvrirons dans cette section.

En effet, les opérateurs ne sont rien d’autres en Python que des fonctions, qui s’appliquent sur leurs opérandes. On peut s’en rendre compte à l’aide du module operator, qui répertorie les fonctions associées à chaque opérateur.

```
>>> import operator
>>> operator.add(5, 6)
11
>>> operator.mul(2, 3)
6
```

Ainsi, chacun des opérateurs correspondra à une méthode de l’opérande de gauche, qui recevra en paramètre l’opérande de droite.

#### Opérateurs arithmétiques

L’addition, par exemple, est définie par la méthode `__add__`.

```
>>> class A:
... def \__add__(self, other):
... return other # on considère self comme 0
...
>>> A() + 5
5
```

Assez simple, n’est-il pas ? Mais nous n’avons pas tout à fait terminé. Si la méthode est appelée sur l’opérande de gauche, que se passe-t-il quand notre objet se trouve à droite ?

```
>>> 5 + A()
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
TypeError: unsupported operand type(s) for +: 'int' and 'A'
```

Nous ne supportons pas cette opération. En effet, l’expression fait appel à la méthode `int.__add__` qui ne connaît pas les objets de type `A`.
Heureusement, ce cas a été prévu et il existe une fonction inverse, `__radd__`, appelée si la première opération n’était pas supportée.

```
>>> class A:
... def __add__(self, other):
... return other
... def __radd__(self, other):
... return other
...
>>> A() + 5
5
>>> 5 + A()
5
```

Il faut bien noter que `A.__radd__` ne sera appelée que si `int.__add__` a échoué.

Les autres opérateurs arithmétques binaires auront un comportement similaire, voici une liste des méthodes à implémenter pour chacun d’eux :


* Addition/Concaténation **(a + b)** — `__add__`, `__radd__`


* Soustraction/Différence **(a - b)** — `__sub__`, `__rsub__`


* Multiplication **(a \* b)** — `__mul__`, `__rmul__`


* Division **(a / b)** — `__truediv__`, `__rtruediv__`


* Division entière **(a // b)** — `__floordiv__`, `__rfloordiv__`


* Modulo/Formattage **(a % b)** — `__mod__`, `__rmod__`


* Exponentiation **(a \*\* b)** — `__pow__`, `__rpow__`

On remarque aussi que chacun de ces opérateurs arithmétiques possède une version simplifiée pour l’assignation **(a += b)** qui correspond à la méthode `__iadd__`. Par défaut, les méthodes `__add__` `__radd__` sont appelées, mais définir `__iadd__` permet d’avoir un comportement différent dans le cas d’un opérateur d’assignation, par exemple sur les listes :

```
>>> l = [1, 2, 3]
>>> l2 = l
>>> l2 = l2 + [4]
>>> l2
[1, 2, 3, 4]
>>> l
[1, 2, 3]
>>> l2 = l
>>> l2 += [4]
>>> l2
[1, 2, 3, 4]
>>> l
[1, 2, 3, 4]
```

#### Opérateurs arithmétiques unaires

Voyons maintenant les **opérateurs unaires**, qui ne prennent donc pas d’autre paramètre que `self`.


* Opposé **(-a)** — `__neg__`


* Positif **(+a)** - `__pos__`


* Valeur abosule **(abs(a))** — `__abs__`

#### Opérateurs de comparaison

De la même manière que pour les opérateurs arithmétiques et unaires, nous avons une méthode spéciale par opérateur de comparaison. Ces opérateurs s’appliqueront sur l’opérande gauche en recevant le droite en paramètre. Ils devront retourner un booléen.

Contrairement aux opérateurs arithmétiques, il n’est pas nécessaire d’avoir deux versions pour chaque opérateur puisque Python saura directement quelle opération inverse tester si la première a échoué (`a == b` est équivalent à `b == a`, `a < b` à `b > a`, etc.).


* Égalité **(a == b)** — `__eq__`


* Différence **(a != b)** — `__neq__`


* Stricte infériorité **(a < b)** — `__lt__`


* Infériorité **(a <= b)** — `__le__`


* Stricte supériorité **(a > b)** — `__gt__`


* Supériorité **(a >= b)** — `__ge__`

On notera aussi que beaucoup de ces opérateurs peuvent s’inférer les uns les autres. Par exemple, il suffit de savoir calculer `a == b` et `a < b` pour définir toutes les autres opérations. Ainsi, Python dispose d’un décorateur, `total_ordering` du module `functools`, pour automatiquement générer les opérations manquantes.

```
>>> from functools import total_ordering
>>> @total_ordering
... class Inferior:
...     def __eq__(self, other):
...         return False
...     def __lt__(self, other):
...         return True
...
>>> i = Inferior()
>>> i == 5
False
>>> i > 5
False
>>> i < 5
True
>>> i <= 5
True
>>> i != 5
True
```

#### Autres opérateurs

Nous avons ici étudié les principaux opérateurs du langage. Ces listes ne sont pas exhaustives et présentent juste la méthodologie à suivre.

Pour une liste complète, je vous invite à consulter la documentation du module operator : [https://docs.python.org/3/library/operator.html](https://docs.python.org/3/library/operator.html).

#### Exercice Arithmétique simple

Oublions temporairement nos utilisateurs et notre forum, et intéressons-nous à l’évaluation mathématique.

Imaginons que nous voulions représenter une expression mathématique, qui pourrait contenir des termes variables (par exemple, `2 \* (-x + 1)`).

Il va nous falloir utiliser un type pour représenter cette variable `x`, appelé `Var`, et un second pour l’expression non évaluée, `Expr`. Les `Var` étant un type particulier d’expressions.

Nous aurons deux autres types d’expressions : les opérations arithmétiques unaires **(+, -)** et binaires **(+, -, \*, /, //, %, \*\*)**. Vous pouvez vous appuyer un même type pour ces deux types d’opérations.

L’expression précédente s’évaluerait par exemple à :

```
BinOp(operator.mul, 2, BinOp(operator.add, UnOp(operator.neg, Var('x')), 1))
```

Nous ajouterons à notre type `Expr` une méthode `compute(\*\*values)`, qui permettra de calculer l’expression suivant une valeur donnée, de façon à ce que `Var('x').compute(x=5)` retourne **5**.

Enfin, nous pourrons ajouter une méthode `__repr__` pour obtenir une représentation lisible de notre expression.

```
import operator

def compute(expr, **values):
    if not isinstance(expr, Expr):
        return expr
    return expr.compute(**values)

class Expr:
    def compute(self, **values):
        raise NotImplementedError

    def __pos__(self):
        return UnOp(operator.pos, self, '+')

    def __neg__(self):
        return UnOp(operator.neg, self, '-')

    def __add__(self, rhs):
        return BinOp(operator.add, self, rhs, '+')

    def __radd__(self, lhs):
        return BinOp(operator.add, lhs, self, '+')

    def __sub__(self, rhs):
        return BinOp(operator.sub, self, rhs, '-')

    def __rsub__(self, lhs):
        return BinOp(operator.sub, lhs, self, '-')

    def __mul__(self, rhs):
        return BinOp(operator.mul, self, rhs, '*')

    def __rmul__(self, lhs):
        return BinOp(operator.mul, lhs, self, '*')

    def __truediv__(self, rhs):
        return BinOp(operator.truediv, self, rhs, '/')

    def __rtruediv__(self, lhs):
        return BinOp(operator.truediv, lhs, self, '/')

    def __floordiv__(self, rhs):
        return BinOp(operator.floordiv, self, rhs, '//')

    def __rfloordiv__(self, lhs):
        return BinOp(operator.floordiv, lhs, self, '//')

    def __mod__(self, rhs):
        return BinOp(operator.mod, self, rhs, '*')

    def __rmod__(self, lhs):
        return BinOp(operator.mod, lhs, self, '*')

class Var(Expr):
    def __init__(self, name):
        self.name = name

    def compute(self, **values):
        if self.name in values:
            return values[self.name]
        return self

    def __repr__(self):
        return self.name

class Op(Expr):
    def __init__(self, op, *args):
        self.op = op
        self.args = args

    def compute(self, **values):
        args = [compute(arg, **values) for arg in self.args]
        return self.op(*args)

class UnOp(Op):
    def __init__(self, op, expr, symbol=None):
        super().__init__(op, expr)
        self.symbol = symbol

    def __repr__(self):
        if self.symbol is None:
            return super().__repr__()
        return '{}{!r}'.format(self.symbol, self.args[0])

class BinOp(Op):
    def __init__(self, op, expr1, expr2, symbol=None):
        super().__init__(op, expr1, expr2)
        self.symbol = symbol

    def __repr__(self):
        if self.symbol is None:
            return super().__repr__()
        return '({!r} {} {!r})'.format(self.args[0], self.symbol, self.args[1])

if __name__ == '__main__':
    x = Var('x')
    expr = 2 * (-x + 1)
    print(expr)
    print(compute(expr, x=1))

    y = Var('y')
    expr += y
    print(compute(expr, x=0, y=10))
```

Les opérateurs sont une notion importante en Python, mais ils sont loin d’être la seule. Le chapitre suivant vous présentera d’autres concepts avancés du Python, qu’il est important de connaître, pour être en mesure de les utiliser quand cela s’avère nécessaire.

### Les attributs de classe

Nous avons déjà rencontré un attribut de classe, quand nous nous intéressions aux parents d’une classe. Souvenez-vous de `__bases__`, nous ne l’utilisions pas sur des instances mais sur notre classe directement.

En Python, les classes sont des objets comme les autres, et peuvent donc posséder leurs propres attributs.

```
>>> class User:
...     pass
...
>>> User.type = 'simple_user'
>>> User.type
'simple_user'
```

Les attributs de classe peuvent aussi se définir dans le corps de la classe, de la même manière que les méthodes.

```
class User:
    type = 'simple_user'
```

On notera à l’inverse qu’il est aussi possible de définir une méthode de la classe depuis l’extérieur :

```
>>> def User_repr(self):
...     return '<User>'
...
>>> User.__repr_\_ = User_repr
>>> User()
<User>
```

L’avantage des attributs de classe, c’est qu’ils sont aussi disponibles pour les instances de cette classe. Ils sont partagés par toutes les instances.

```
>>> john = User()
>>> john.type
'simple_user'
>>> User.type = 'admin'
>>> john.type
'admin'
```

C’est le fonctionnement du MRO de Python, il cherche d’abord si l’attribut existe dans l’objet, puis si ce n’est pas le cas, le cherche dans les classes parentes.

Attention donc, quand l’attribut est redéfini dans l’objet, il sera trouvé en premier, et n’affectera pas la classe.

```
>>> john = User()
>>> john.type
'admin'
>>> john.type = 'superadmin'
>>> john.type
'superadmin'
>>> User.type
'admin'
>>> joe = User()
>>> joe.type
'admin'
```

Attention aussi, quand l’attribut de classe est un objet mutable { Un objet mutable est un objet que l’on peut modifier (liste, dictionnaire) par opposition à un objet immutable (nombre, chaîne de caractères, tuple) }, il peut être modifié par n’importe quelle instance de la classe.

```
>>> class User:
...     users = []
...
>>> john, joe = User(), User()
>>> john.users.append(john)
>>> joe.users.append(joe)
>>> john.users
[<__main__.User object at 0x7f3b7acf8b70>, <__main__.User object at 0x7f3b7acf8ba8>]
```

L’attribut de classe est aussi conservé lors de l’héritage, et partagé avec les classes filles (sauf lorsque les classes filles redéfinissent l’attribut, de la même manière que pour les instances).

```
>>> class Guest(User):
...     pass
...
>>> Guest.users
[<__main__.User object at 0x7f3b7acf8b70>, <__main__.User object at 0x7f3b7acf8ba8>]
>>> class Admin(User):
...     users = []
...
>>> Admin.users
[]
```

### Méthodes de Classes

Comme pour les attributs, des méthodes peuvent être définies au niveau de la classe. C’est par exemple le cas de la méthode `mro`.

```
int.mro()
```

**Les méthodes de classe constituent des opérations relatives à la classe mais à aucune instance**. Elles recevront la classe courante en premier paramètre (nommé `cls`, correspondant au `self` des méthodes d’instance), et auront donc accès aux autres attributs et méthodes de classe.

Reprenons notre classe `User`, à laquelle nous voudrions ajouter le stockage de tous les utilisateurs, et la génération automatique de l’id.
Il nous suffirait d’une même méthode de classe pour stocker l’utilisateur dans un attribut de classe `users`, et qui lui attribuerait un `id` en fonction du nombre d’utilisateurs déjà enregistrés.

```
>>> root = Admin('root', 'toor')
>>> root
<User: 1, root>
>>> User('john', '12345')
<User: 2, john>
>>> guest = Guest('guest')
<User: 3, guest>
```

Les méthodes de classe se définissent comme les méthodes habituelles, à la différence près qu’elles sont précédées du décorateur `classmethod`.

```
import crypt

class User:
    users = []

    def __init__(self, name, password):
        self.name = name
        self._salt = crypt.mksalt()
        self._password = self._crypt_pwd(password)
        self.register(self)

    @classmethod
    def register(cls, user):
        cls.users.append(user)
        user.id = len(cls.users)

    def _crypt_pwd(self, password):
        return crypt.crypt(password, self._salt)

    def check_pwd(self, password):
        return self._password == self._crypt_pwd(password)

    def __repr__(self):
        return '<User: {}, {}>'.format(self.id, self.name)

class Guest(User):
    def __init__(self, name):
        super().__init__(name, '')

    def check_pwd(self, password):
        return True

class Admin(User):
    def manage(self):
        print('Je suis un Jedi!')
```

Vous pouvez constater le résultat en réessayant le code donné plus haut.

### Méthodes statiques

Les méthodes statiques sont très proches des méthodes de classe, mais sont plus **à considérer comme des fonctions au sein d’une classe**.

Contrairement aux méthodes de classe, elles ne recevront pas le paramètre `cls`, et n’auront donc pas accès aux attributs de classe,
méthodes de classe ou méthodes statiques.

Les méthodes statiques sont plutôt dédiées à des comportements annexes en rapport avec la classe, par exemple on pourrait remplacer notre attribut `id` par un `uuid` aléatoire, dont la génération ne dépendrait de rien d’autre dans la classe.

Elles se définissent avec le décorateur `staticmethod`.

```
import uuid

class User:
    def __init__(self, name, password):
        self.id = self._gen_uuid()
        self.name = name
        self._salt = crypt.mksalt()
        self._password = self._crypt_pwd(password)

    @staticmethod
    def _gen_uuid():
        return str(uuid.uuid4())

    def _crypt_pwd(self, password):
        return crypt.crypt(password, self._salt)

    def check_pwd(self, password):
        return self._password == self._crypt_pwd(password)
```

```
>>> john = User('john', '12345')
>>> john.id
'69ef1327-3d96-42a9-94e6-622619fbf666'
```

### Recherche d’attributs

Nous savons récupérer et assigner un attribut dont le nom est fixé, cela se fait facilement à l’aide des instructions `obj.foo` et `obj.foo = value`.

Mais nous est-il possible d’accéder à des attributs dont le nom est variable ?

Prenons une instance `john` de notre classe `User`, et le nom d’un attribut que nous voudrions extraire :

```
>>> john = User('john', '12345')
>>> attr = 'name'
```

La fonction `getattr` nous permet alors de récupérer cet attribut.

```
>>> getattr(john, attr)
'john'
```

Ainsi, `getattr(obj, 'foo')` est équivalent à `obj.foo`.

On trouve aussi une fonction `hasattr` pour tester la présence d’un attribut dans un objet. Elle est construite comme `getattr` mais retourne un booléen pour savoir si l’attribut est présent ou non.

```
>>> hasattr(john, 'name')
True
>>> hasattr(john, 'last_name')
False
>>> hasattr(john, 'id')
True
```

De la même manière, les fonctions `setattr` et `delattr` servent respectivement à modifier et supprimer un attribut.

```
>>> setattr(john, 'name', 'peter') # équivalent à `john.name = 'peter'`
>>> john.name
'peter'
>>> delattr(john, 'name') # équivalent à \`del john.name\`
>>> john.name
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
AttributeError: 'User' object has no attribute 'name'
```

### Les propriétés

Les propriétés sont une manière en Python de «dynamiser» les attributs d’un objet. Ils permettent de générer des attributs à la volée à partir de méthodes de l’objet.

Un exemple vaut mieux qu’un long discours :

```
class ProfilePicture:
    @property
    def picture(self):
        return '{}-{}.png'.format(self.id, self.name)

class UserPicture(ProfilePicture, User):
    pass
```

On définit donc une propriété `picture` avec `@property`, qui s’utilise comme un attribut. Chaque fois qu’on appelle `picture`, la méthode correspondante est appelée et le résultat est calculé.

```
>>> john = UserPicture('john', '12345')
>>> john.picture
'1-john.png'
>>> john.name = 'John'
>>> john.picture
'1-John.png'
```

Il s’agit là d’une propriété en lecture seule, il nous est en effet impossible de modifier la valeur de l’attribut `picture`.

```
>>> john.picture = 'toto.png'
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
AttributeError: can't set attribute
```

Pour le rendre modifiable, il faut ajouter à notre classe la méthode permettant de gérer la modification, à l’aide du décorateur `@picture.setter` (le décorateur setter de notre propriété picture, donc).

On utilisera ici un attribut `_picture`, qui pourra contenir l’adresse de l’image si elle a été définie, et `None` le cas échéant.

```
class ProfilePicture:
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._picture = None

    @property
    def picture(self):
        if self._picture is not None:
            return self._picture
        return '{}-{}.png'.format(self.id, self.name)

    @picture.setter
    def picture(self, value):
        self._picture = value

class UserPicture(ProfilePicture, User):
    pass
```

```
>>> john = UserPicture('john', '12345')
>>> john.picture
'1-john.png'
>>> john.picture = 'toto.png'
>>> john.picture
'toto.png'
```

Enfin, on peut aussi coder la suppression de l’attribut à l’aide de `@picture.deleter`, ce qui revient à réaffecter `None` à l’attribut `_picture`.

```
class ProfilePicture:
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._picture = None

    @property
    def picture(self):
        if self._picture is not None:
            return self._picture
        return '{}-{}.png'.format(self.id, self.name)

    @picture.setter
    def picture(self, value):
        self._picture = value

    @picture.deleter
    def picture(self):
        self._picture = None

class UserPicture(ProfilePicture, User):
    pass
```

```
>>> john = UserPicture('john', '12345')
>>> john.picture
'1-john.png'
>>> john.picture = 'toto.png'
>>> john.picture
'toto.png'
>>> del john.picture
>>> john.picture
'1-john.png'
```

### Classes abstraites

La notion de classes abstraites est utilisée lors de l’héritage pour forcer les classes filles à implémenter certaines méthodes (dites méthodes abstraites) et donc respecter une interface.

Les classes abstraites ne font pas partie du cœur même de Python, mais sont disponibles via un module de la bibliothèque standard, `abc` (Abstract Pere Classes). Ce module contient notamment la classe `ABC` et le décorateur `@abstractmethod`, pour définir respectivement une classe abstraite et une méthode abstraite de cette classe.

Une classe abstraite doit donc hériter d’`ABC`, et utiliser le décorateur cité pour définir ses méthodes abstraites.

```
import abc

class MyABC(abc.ABC):
    @abc.abstractmethod
    def foo(self):
        pass
```

Il nous est impossible d’instancier des objets de type `MyABC`, puisqu’une méthode abstraite n’est pas implémentée :

```
>>> MyABC()
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
TypeError: Can't instantiate abstract class MyABC with abstract methods foo
```

Il en est de même pour une classe héritant de `MyABC` sans redéfinir la méthode.

```
>>> class A(MyABC):
... pass
...
>>> A()
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
TypeError: Can't instantiate abstract class A with abstract methods foo
```

Aucun problème par contre avec une autre classe qui redéfinit bien la méthode.

```
>>> class B(MyABC):
... def foo(self):
...     return 7
...
>>> B()
<__main__.B object at 0x7f33065316a0>
>>> B().foo()
7
```

#### Exercice : Base de données

nous aborderons les méthodes de classe et les propriétés.

Reprenons notre forum, auquel nous souhaiterions ajouter la gestion d’une base de données.

Notre base de données sera une classe avec deux méthodes, `insert` et `select`. Son implémentation est libre, elle doit juste respecter l’interface suivante :

```
>>> class A: pass
...
>>> class B: pass
...
>>>
>>> db = Database()
>>> obj = A()
>>> obj.value = 42
>>> db.insert(obj)
>>> obj = A()
>>> obj.value = 5
>>> db.insert(obj)
>>> obj = B()
>>> obj.value = 42
>>> obj.name = 'foo'
>>> db.insert(obj)
>>>
>>> db.select(A)
<__main__.A object at 0x7f033697f358>
>>> db.select(A, value=5)
<__main__.A object at 0x7f033697f3c8>
>>> db.select(B, value=42)
<__main__.B object at 0x7f033697f438>
>>> db.select(B, value=42, name='foo')
<__main__.B object at 0x7f033697f438>
>>> db.select(B, value=5)
ValueError: item not found
```

Nous ajouterons ensuite une classe `Model`, qui se chargera de stocker dans la base toutes les instances créées. `Model` comprendra une méthode de classe `get(\*\*kwargs)` chargée de réaliser une requête select sur la base de données et de retourner l’objet correspondant. Les objets de type `Model` disposeront aussi d’une propriété `id`, retournant un identifiant unique de l’objet.

On pourra alors faire hériter nos classes `User` et `Post` de `Model`, afin que les utilisateurs et messages soient stockés en base de données. Dans un second temps, on pourra faire de `Model` une classe abstraite, par exemple en rendant abstraite la méthode `__init__`.

```
import abc
import datetime

class Database:
    data = []

    def insert(self, obj):
        self.data.append(obj)

    def select(self, cls, **kwargs):
        items = (item for item in self.data
            if isinstance(item, cls)
            and all(hasattr(item, k) and getattr(item, k) == v
            for (k, v) in kwargs.items()))
        try:
            return next(items)
        except StopIteration:
            raise ValueError('item not found')

class Model(abc.ABC):
    db = Database()
    @abc.abstractmethod
    def __init__(self):
        self.db.insert(self)
    @classmethod
    def get(cls, \**kwargs):
        return cls.db.select(cls, \**kwargs)
    @property
    def id(self):
        return id(self)

class User(Model):
    def __init__(self, name):
        super().__init__()
        self.name = name

class Post(Model):
    def __init__(self, author, message):
        super().__init__()
        self.author = author
        self.message = message
        self.date = datetime.datetime.now()

    def format(self):
        date = self.date.strftime('le %d/%m/%Y à %H:%M:%S')
        return '<div><span>Par {} {}</span><p>{}</p></div>'.format(self.author.name, date, self.message)

if __name__ == '__main__':
    john = User('john')
    peter = User('peter')
    Post(john, 'salut')
    Post(peter, 'coucou')

    print(Post.get(author=User.get(name='peter')).format())
    print(Post.get(author=User.get(id=john.id)).format())
```

Ces dernières notions ont dû compléter vos connaissances du modèle objet de Python, et vous devriez maintenant être prêts à vous lancer dans un projet exploitant ces concepts.

## La visualisation de l’architecture objets

Le programme `pylint`, que nous avons installé pour tester le code Python, est livré avec un outil de ligne de commande fort pratique `pyreverse`. Celui-ci permet d’imager les classes Python et de créer des diagrammes [UML](https://www.ibm.com/docs/fr/rational-soft-arch/9.5?topic=diagrams-uml-models) des classes Python.

### Pyreverse

**Pyreverse** permet de générer des diagrammes avec :


* des attributs de classes et si possible avec leurs types,


* des méthodes de classes avec leurs paramètres,


* la représentation des exceptions,


* les liens d’héritages de classes,


* et les liens d’association de classes.

#### Les options de la ligne de commande

#### Options de la ligne de commande de Pyreverse

| Option courte

 | Option verbeuse

 | Description

 |
| ------------------------------------------------------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |  |  |  |  |  |  |  |  |  |  |  |  |  |  |
| `-p <nom du projet>`

                                                                                                                                            | `--project=<nom du projet>`

                                                                                                                                                                                                                                                                         | Nom du fichier en sortie

                                                                                                                                                                                                                                                |
| `-o <format>`

                                                                                                                                                   | `--output=<format>`

                                                                                                                                                                                                                                                                                 | Format de sortie de fichier (**svg**, **svgz**, **png**, **jpeg**/**jpg**/**jpe**, **gif**, **ps**/**ps2**/**eps**, **pdf**, pic, pcl, hpgl, gd/gd2, fig, dia, **dot**/**xdot**, **plain**/**plain-ext**, vrml/vml/vmlz, tk, wbmp, xlib, etc.)

                                                                                      |
| `-c <classe>`

                                                                                                                                                   | `--class <classe>`

                                                                                                                                                                                                                                                                                  | Afficher la classe passée en paramètre

                                                                                                                                                                                                                                  |
| `-k`

                                                                                                                                                            | `--only-classnames`

                                                                                                                                                                                                                                                                                 | Afficher seulement les noms des classes

                                                                                                                                                                                                                                 |
| `-m[y/n]`

                                                                                                                                                       | `--module-names=[y/n]`

                                                                                                                                                                                                                                                                              | Inclure le nom des modules pour la représentation des classes.

                                                                                                                                                                                                          |
| `-b`

                                                                                                                                                            | `--show-builtin`

                                                                                                                                                                                                                                                                                    | Afficher les classes des objets natifs Python

                                                                                                                                                                                                                           |
| `-a <niveau>`

                                                                                                                                                   | `--show-ancestors=<niveau>`

                                                                                                                                                                                                                                                                         | Aficher le niveaux d’héritage passé en paramètre

                                                                                                                                                                                                                        |
| `-A`

                                                                                                                                                            | `--all-ancestors`

                                                                                                                                                                                                                                                                                   | Afficher tout l’arbre d’héritage

                                                                                                                                                                                                                                        |
| `-s <niveau>`

                                                                                                                                                   | `--show-associated=<niveau>`

                                                                                                                                                                                                                                                                        | Liens des imports suivant le niveau d’imports

                                                                                                                                                                                                                           |
| `-S`

                                                                                                                                                            | `--all-associated`

                                                                                                                                                                                                                                                                                  | Toutes les liaisons d’imports

                                                                                                                                                                                                                                           |
| `-f <mode>`

                                                                                                                                                     | `--filter-mode=<mode>`

                                                                                                                                                                                                                                                                              | Filtre ce qu’il faut faire apparaître (par défaut PUB_ONLY) :


* **PUB_ONLY** : Affiche les méthodes et les propriétés publiques.


* **SPECIAL** : Affiche les attributs privés ou protégés en plus.


* **OTHER** : Affiche le méthodes privées ou protégés en plus.


* **ALL** : Affiche tout.

 |
```
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ cd 9_objets
utilisateur@MachineUbuntu:~/repertoire_de_developpement/9_objets$ pyreverse -p test -o png scene.py
```

L’option `-p test` ajout le suffixe **test** au nom du fichier qui à défaut est «**classes.ext**».
L’option `-o png` choisit le format de sortie, ici un fichier image [PNG](https://fr.wikipedia.org/wiki/Portable_Network_Graphics).

Cela génère donc le fichier «**classes_test.png**».

Qui nous donne à afficher par défaut :



![image](images/pyreverse_test1.png)

Cela nous affiche le nom de la classe `Decor` avec sa propriété `fabriqueBatiments` affectée à l’objet `FabriqueBatiments`. Ceci nous permet de comprendre que cela est en fait une méthode pour l’objet.

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement/9_objets$ pyreverse -c Decor -o png scene.py
```

L’option `-c Decour` choisit la classe à générer et va produire une sortie de nom de fichier avec le nom de la classe «**Decor.png**».

Ce qui va nous donner comme diagramme plus intéressant :



![image](images/pyreverse_test2.png)

Là nous voyons avec les **flêches** l’héritage des classes `Vegetation`, `Urbanisation`, `Hydrotopologie`, `Geomorphologie` et `Atmosphere` qui elle même hérite de `Nuages`.

Nous voyon aussi les propriétés et méthodes de `FabriqueBatiments` qui est bien affectée à la propriété `fabriqueBatiments` (en vert) de la classe `Decor`

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement/9_objets$ pyreverse -k -c Decor -o png scene.py
```

L’option `-k` permet de n’afficher que le nom des classes :



![image](images/pyreverse_test3.png)

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement/9_objets$ pyreverse -f ALL -S -c Decor -o png scene.py
```

L’option `-f ALL` permet d’afficher les propriétés et les méthodes cachées :



![image](images/pyreverse_test4.png)

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement/9_objets$ pyreverse -a 1 -c Decor -o png scene.py
```

L’option `-a 1` permet de limiter le niveau de liens en héritage par rapport à la classe :



![image](images/pyreverse_test5.png)

En limitant au premier niveau `-a 1` la classe `Nuages` n’est plus affichée.

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement/9_objets$ pyreverse -b -c Decor -o png scene.py
```

L’option `-b` permet d’afficher l’héritage des classes natives Python :



![image](images/pyreverse_test6.png)

On voit alors bien apparaître la classe `object` comme on l’attendait, mais aussi les classes `builtin.list` et `builtin.str`.

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement/9_objets$ pyreverse -mn -c Decor -o png scene.py
```

L’option `-mn` permet de supprimer le chemin d’importation des classes dans les intitulés de classes :



![image](images/pyreverse_test7.png)

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement/9_objets$ pyreverse -s 0 -c Decor -o png scene.py
```

L’option `-s 0` permet de ne visualiser que les classes importées directement :



![image](images/pyreverse_test8.png)

On voit bien que la classe `FabriqueBatiments` ne s’affiche plus avec un niveau supplémentaire d’importation.

Vous pouvez maintenant générer tous les diagrammes de classes Python que vous voulez pour agrémenter les documentations de vos programmes.

### Mise en œuvre dans GitLab
