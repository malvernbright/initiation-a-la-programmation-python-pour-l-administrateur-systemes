# L’organisation du code

## Structures de contrôles

### Structures alternatives

#### If, else, elif

Les instructions [if, else, elif](https://courspython.com/tests.html#if) sont sans doute les plus connues.

Par exemple :

```
>>> import os
>>> x = int(input("SVP entrez un entier: "))
SVP entrez un entier: 42
>>> if x < 0:
...     x = 0
...     print('Nombre négatif remplacé par zéro')
... elif x == 0:
...     print('Zéro')
... elif x == 1:
...     print('Unité')
... else:
...     print('Plus grand')
...
Plus grand
```

Il peut y avoir un nombre quelconque de parties `elif` et la partie `else` est facultative. Le mot clé `elif` est un raccourci pour `else if`, mais permet de gagner un niveau d’indentation. Une séquence `if ... elif ... elif ...` est par ailleurs équivalente aux instructions «**switch**» ou «**case**» disponibles dans d’autres langages.

### Structures itératives

#### For in

Les instructions [for in](https://courspython.com/boucles.html#for) que propose Python sont un peu différente de celle que l’on peut trouver en C ou en Pascal.

Au lieu de toujours itérer sur une suite arithmétique de nombres (comme en Pascal), ou de donner à l’utilisateur la possibilité de définir le pas d’itération et la condition de fin (comme en C), l’instruction `for` en Python itère sur les éléments d’une séquence (qui peut être une liste, une chaîne de caractères…), dans l’ordre dans lequel ils apparaissent dans la séquence.

Par exemple :

```
>>> # Mesure quelques chaînes de caractères
>>> mots = ['fenêtre', 'chat', 'quantique']
>>> for l in mots:
...     print(l, len(l))
...
fenêtre 7
chat 4
quantique 9
```

Le code qui modifie une collection tout en itérant sur cette même collection peut être délicat à mettre en place.

```
>>> # Strategie:  Itérer sur une copie
>>> utilisateurs = {'Vador': 'inactif', 'Luc': 'actif', 'Padawan': 'actif'}
>>> for utilisateur, statut in utilisateurs.copy().items():
...     if statut == 'inactif':
...         del utilisateurs[utilisateur]
...
...
>>> utilisateurs
{'Luc': 'actif', 'Padawan': 'actif'}
```

Au lieu de cela, il est généralement plus simple de boucler sur une copie de la collection ou de créer une nouvelle collection :

```
>>> # Strategie: Créer une nouvelle collection utilisateurs
>>> utilisateurs = {'Vador': 'inactif', 'Luc': 'actif', 'Padawan': 'actif'}
>>> utilisateurs_actifs = utilisateurs.copy()
>>> utilisateurs_vivants = []
>>> for utilisateur, statut in utilisateurs.items():
...     if statut == 'inactif':
...         del utilisateurs_actifs[utilisateur]
... else:
...          utilisateurs_vivants.append(utilisateur)
...
>>> utilisateurs_actifs
{'Luc': 'actif', 'Padawan': 'actif'}
>>> utilisateurs_vivants
['Luc', 'Padawan']
>>> utilisateurs
{'Vador': 'inactif', 'Luc': 'actif', 'padawan': 'actif'}
```

Pour itérer sur les indices d’une séquence, on peut combiner les fonctions `range()` et `len()` :

```
>>> phrase = ['Luc', 'a', 'un', 'petit', 'laser']
>>> for mot in range(len(phrase)):
...     print(mot, phrase[mot])
...
0 Luc
1 a
2 un
3 petit
4 laser
```

Cependant, dans la plupart des cas, il est plus pratique d’utiliser la fonction `enumerate()`.

```
>>> for mot in enumerate(phrase):
...     print(mot[0], mot[1])
...
0 Luc
1 a
2 un
3 petit
4 laser
```

#### While

L’instruction [while](https://courspython.com/boucles.html#while) permet de faire des boucle suivant une condition.

##### break, continue

L’instruction `break`, comme en C, interrompt la boucle `for` ou `while`.

Les boucles peuvent également disposer d’une instruction `else`. Celle-ci est exécutée lorsqu’une boucle se termine alors que tous ses éléments ont été traités (dans le cas d’un `for`) ou que la condition devient fausse (dans le cas d’un `while`), **mais pas lorsque la boucle est interrompue par une instruction** `break`.

L’exemple suivant, qui effectue une recherche de nombres premiers, en est une démonstration :

```
>>> for n in range(2, 10):
...     for x in range(2, n):
...         if n % x == 0:
...             print(n, 'égal', x, '*', n//x)
...             break
...     else:
...         # la boucle s’est terminée sans trouver de facteur
...         print(n, 'est un nombre premier')
...
...
2 est un nombre premier
3 est un nombre premier
4 égal 2 * 2
5 est un nombre premier
6 égal 2 * 3
7 est un nombre premier
8 égal 2 * 4
9 égal 3 * 3
```

Oui, ce code est correct. Regardez attentivement : l’instruction `else` est rattachée à la boucle `for`, et non à l’instruction `if`.

Lorsqu’elle est utilisée dans une boucle, la clause `else` est donc plus proche de celle associée à une instruction `try` que de celle associée à une instruction `if`: la clause `else` d’une instruction `try` s’exécute lorsqu’aucune exception n’est déclenchée, et celle d’une boucle lorsqu’aucun `break` n’intervient. Nous verrons plus ultérieurement dans ce cours l’instruction `try` et le traitement des exceptions.

L’instruction `continue`, également empruntée au C, fait passer la boucle à son itération suivante :

```
>>> for num in range(2, 10):
...     if num % 2 == 0:
...         print("Un nombre pair a été trouvé : ", num)
...         continue
...     print("Un nombre impair a été trouvé : ", num)
...
Un nombre pair a été trouvé :  2
Un nombre impair a été trouvé :  3
Un nombre pair a été trouvé :  4
Un nombre impair a été trouvé :  5
Un nombre pair a été trouvé :  6
Un nombre impair a été trouvé :  7
Un nombre pair a été trouvé :  8
Un nombre impair a été trouvé :  9
```

##### pass

L’instruction `pass` ne fait rien. Elle peut être utilisée lorsqu’une instruction est nécessaire pour fournir une syntaxe correcte, mais qu’aucune action ne doit être effectuée.

Par exemple :

```
>>> while True:
...     pass  # Attente occupée pour l'interruption du clavier (Ctrl + C)
...
^CTraceback (most recent call last):
 File "<stdin>", line 2, in <module>
KeyboardInterrupt
```

On utilise couramment cette instruction pour créer des classes minimales d’objets :

```
>>> class MaClasseVide:
...     pass
...
>>>
```

Un autre cas d’utilisation du `pass` est de réserver un espace en phase de développement pour une fonction ou un traitement conditionnel, vous permettant ainsi de construire votre code à un niveau plus abstrait.

L’instruction **pass** est alors ignorée silencieusement :

```
>>> def initlog(*args):
...     pass   # N'oubliez pas de mettre en œuvre cela!
...
```

#### alternative do while

```
>>> while True:
...     #… code
...     if cond :
...         break
```

#### Techniques de boucles des dictionnaires

Lorsque vous faites une boucle sur un dictionnaire, les clés et leurs valeurs peuvent être récupérées en même temps en utilisant la méthode `items()` :

```
>>> chevaliers_jedi = {'Luc': 'le padawan', 'Yoda': 'le grand maître', 'Obi-Wan Kenobi': 'le guerrier'}
>>> for k, v in chevaliers_jedi.items(): print(k, v)
...
Luc le padawan
Yoda le grand maître
Obi-Wan Kenobi le guerrier
```

Lorsque vous faites une boucle sur une séquence, la position et la valeur correspondante peuvent être récupérées en même temps en utilisant la fonction `enumerate()` :

```
>>> for i, v in enumerate(['tic', 'tac', 'toe']): print(i, v)
...
0 tic
1 tac
2 toe
```

Pour faire une boucle sur deux séquences, ou plus en même temps, les éléments peuvent être associés en utilisant la fonction `zip()` :

```
>>> questions = ['nom', 'côté de la force', 'couleur du sabre']
>>> réponses = ['luc', 'la lumière', 'le vert']
>>> for q, r in zip(questions, réponses):
...     print('Quel est votre {0} ? C\’est {1}.'.format(q, r))
...
Quel est votre nom ? C’est luc.
Quel est votre côté de la force ? C’est la lumière.
Quel est votre couleur du sabre ? C’est le vert.
```

Pour faire une boucle en sens inverse sur une séquence, commencez par spécifier la séquence dans son ordre normal, puis appliquez la fonction `reversed()` :

```
>>> for n in reversed(range(1, 10, 2)):  print(n)
...
9
7
5
3
1
```

Pour faire une boucle sur une séquence de manière ordonnée, utilisez la fonction `sorted()` qui renvoie une nouvelle liste ordonnée sans altérer la source :

```
>>> panier = ['pomme', 'orange', 'pomme', 'poire', 'orange', 'banane']
>>> for f in sorted(panier): print(f)
...
banane
orange
orange
poire
pomme
pomme
```

L’utilisation de la fonction `set()` sur une séquence élimine les doublons. L’utilisation de la fonction `sorted()` en combinaison avec `set()` sur une séquence est une façon idiomatique de boucler sur les éléments uniques d’une séquence dans l’ordre :

```
>>> for f in sorted(set(panier)): print(f)
...
banane
orange
poire
pomme
```

Il est parfois tentant de modifier une liste pendant son itération. Cependant, c’est souvent plus simple et plus sûr de créer une nouvelle liste à la place. :

```
>>> import math
>>> données_brutes = [56.2, float('NaN'), 51.7, 55.3, 52.5, float('NaN'), 47.8]
>>> données_filtrées = []
>>> for valeur in données_brutes:
...     if not math.isnan(valeur):
...         données_filtrées.append(valeur)
...
...
>>> données_filtrées
[56.2, 51.7, 55.3, 52.5, 47.8]
```
