# Tester les Pages GitLab

## Créer un projet de rendu de pages HTML

Créer un nouveau projet



![image](images/gitlab_42.png)

Création depuis un modèle



![image](images/gitlab_43.png)



![image](images/gitlab_44.png)

Choisir «**Pages/Plain HTML**» comme modèle



![image](images/gitlab_45.png)

Renseignez :


* le nom du projet «**HTML**»


* La description du projet «**Test des GitLab Pages**»


* Le niveau de sécurité «**Public**»



![image](images/gitlab_46.png)



![image](images/gitlab_47.png)

## Créer le «runner» pour ce projet



![image](images/gitlab_48.png)



![image](images/gitlab_49.png)

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ docker run --rm -it -v /etc/gitlab-runner:/etc/gitlab-runner gitlab/gitlab-runner register
Runtime platform arch=amd64 os=linux pid=7 revision=8925d9a0 version=14.2.0
Running in system-mode.

Enter the GitLab instance URL (for example, https://gitlab.com/): http://gitlab.domaine-perso.fr/
Enter the registration token: 7YBLdSA9en4NMex5zyQy
Enter a description for the runner: [75d626bde768]: Runner Test Pages GitLab
Enter tags for the runner (comma-separated): runner
Registering runner... succeeded runner=Tzzfs5xc
Enter an executor: kubernetes, custom, docker-ssh, shell, docker+machine, docker-ssh+machine, docker, parallels, ssh, virtualbox: docker
Enter the default Docker image (for example, ruby:2.6): alpine:latest
Runner registered successfully. Feel free to start it, but if it's running already the config should be automatically reloaded!
```

Changez dans «**/etc/gitlab-runner/config.toml**» :

```
concurrent = 1
check_interval = 0

[session_server]
  session_timeout = 1800

[[runners]]
  name = "Runner Developpement Python 3"
  url = "http://gitlab.domaine-perso.fr/"
  token = "9FfDsP_9Z2cXWi1Axwig"
  executor = "docker"
  [runners.custom_build_dir]
  [runners.cache]
    [runners.cache.s3]
    [runners.cache.gcs]
    [runners.cache.azure]
  [runners.docker]
    tls_verify = false
    image = "python:latest"
    privileged = false
    disable_entrypoint_overwrite = false
    oom_kill_disable = false
    disable_cache = false
    volumes = ["/var/run/docker.sock:/var/run/docker.sock", "/cache"]
    shm_size = 0

[[runners]]
  name = "Runner Test Pages GitLab"
  url = "http://gitlab.domaine-perso.fr/"
  token = "7YBLdSA9en4NMex5zyQy"
  executor = "docker"
  pull_policy = "if-not-present"
  [runners.custom_build_dir]
  [runners.cache]
    [runners.cache.s3]
    [runners.cache.gcs]
    [runners.cache.azure]
  [runners.docker]
    tls_verify = false
    image = "alpine:latest"
    privileged = false
    disable_entrypoint_overwrite = false
    oom_kill_disable = false
    disable_cache = false
    volumes = ["/var/run/docker.sock:/var/run/docker.sock", "/cache"]
    shm_size = 0
```

Vous pouvez configurer et redémarrer le Runner



![image](images/gitlab_50.png)



![image](images/gitlab_51.png)



![image](images/gitlab_52.png)

Modifiez l’option «**Indique si l’exécuteur peut choisir des tâches sans étiquettes (tags)**» pour l’activer. Et préciser une durrée maximale d’exécution de «**30m**»



![image](images/gitlab_53.png)

Enregirtrer les modifications et relancer le runner



![image](images/gitlab_54.png)

### Déployer et tester le HTML dans une Pages GitLab



![image](images/gitlab_55.png)

Éditer le fichier «**gitlab-ci.yml**» avec GitLab en cliquant sur le bouton 

![image](images/gitlab_56.png)




![image](images/gitlab_57.png)

Renseigner le Message de commit «**Mise à jour du fichier .gitlab-ci.yml pour le lancement du runner**». Puis cliquer sur le bouton «**Commit changes**»



![image](images/gitlab_58.png)



![image](images/gitlab_59.png)

Cliquer sur la tache «**Pages**» sans annuler la tache ( l’icône Cancel de l’image )



![image](images/gitlab_60.png)



![image](images/gitlab_61.png)

Dans le menu «**Dépôt**» avec le sous menu «**Commits**» on peut voir la réussite de la tâche suite au commit.



![image](images/gitlab_62.png)

Maintenant il ne manque plus qu’a récupérer le site web de la page html.

Pour cela allons dans le menu «**Paramètres**»,  le sous menu «**Pages**» du projet.



![image](images/gitlab_63.png)



![image](images/gitlab_64.png)

La présence du lien «[http://utilisateur.documentation.domaine-perso.fr/html](http://utilisateur.documentation.domaine-perso.fr/html)» nous confirme que GitLab fonctionne avec les Pages.

Un click sur ce lien et on vérifie l’accès au site web.



![image](images/gitlab_65.png)

Supprimez le projet («**Paramètres/Général/Advenced/Delete project**»), et nettoyez le runner de test «**Runner Test Pages GitLab**» du fichier «**/etc/gitlab-runner/config.toml**».
