# Le dépôt de modules Python

## Pip

Une des forces de **Python** est la multitude de bibliothèques disponibles (près de 6000 bibliothèques gravitent autour du projet **Django**).

Par exemple installer une bibliothèque peut vite devenir ennuyeux:


* trouver le bon site,


* la bonne version de la bibliothèque,


* l’installer,


* trouver ses dépendances,


* etc.

Il existe une solution qui vous permet de télécharger très simplement une bibliothèque **pip**.

### PIP c’est quoi ?

**Pip** est un **système de gestion de paquets** utilisé pour installer et gérer des librairies écrites en Python. Vous pouvez trouver une grande partie de ces librairies dans le [Python Package Index](https://pypi.python.org/pypi) (ou PyPI). **Pip** empêche les installations partielles en annonçant toutes les exigences avant l’installation.

```
pip install librairie
```

Vous pouvez choisir la version qui vous intéresse :

```
pip install librairie==2.2
```

Supprimer une librairie :

```
pip uninstall librairie
```

Mettre à jour une librairie :

```
pip install librairie --upgrade
```

Revenir sur une version antérieure :

```
pip install librairie==2.1 --upgrade
```

Rechercher une nouvelle librairie :

```
pip search librairie
```

Vous indiquer quelles librairies ne sont plus à jour :

```
pip list --outdated
```

Afficher toutes les librairies installées et leur version :

```
pip freeze
```

Exporter la liste des librairies, vous pourrez la réimporter ailleurs :

```
pip freeze > lib.txt
```

Importer la liste de librairie comme ceci :

```
pip install -r lib.txt
```

Créer un gros zip qui contient toutes les dépendances :

```
pip bundle <nom_du_bundle>.pybundle -r lib.txt
```

Pour installer les librairies :

```
pip install <nom_du_bundle>.pybundle
```

Pour installer depuis un dépôt distant ([Voir la section du support VCS](https://pip.pypa.io/en/stable/reference/pip_install/#vcs-support)) :

```
pip install git+https://github.com/chemin/monmodule.git#egg=monmodule
```

Pour le lien ver le support VCS : [https://pip.pypa.io/en/stable/reference/pip_install/#vcs-support](https://pip.pypa.io/en/stable/reference/pip_install/#vcs-support)

Voir plus d’informations [https://docs.python.org/fr/3.6/installing/index.html](https://docs.python.org/fr/3.6/installing/index.html)
