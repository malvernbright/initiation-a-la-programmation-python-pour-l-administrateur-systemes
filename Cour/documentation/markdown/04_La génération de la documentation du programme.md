# La génération de la documentation du programme

Voir [https://www.codeflow.site/fr/article/documenting-python-code](https://www.codeflow.site/fr/article/documenting-python-code)

Sphinx est un outil très complet permettant de générer des documentations riches et bien structurées. Il a originellement été créé pour la documentation du langage Python, et a très vite été utilisé pour documenter de nombreux autres projets.

Pour ce qui est de la documentation de code, il est évidemment bien adapté au Python, mais peut aussi être utilisé avec d’autres langages.

Parmi les fonctionnalités que Sphinx propose :


* la génération automatique de la documentation à partir du code (avec le support de nombreux langages),


* la possibilité de faire des références entre les pages,


* le support de plusieurs formats de sortie (HTML, PDF, LaTeX, EPUB, pages de manuel, …),


* la gestion d’extensions permettant de l’adapter à toutes les situations et langages.

## Configurer Sphinx pour Python

Le répertoire racine Sphinx d’une collection de textes bruts permettant de générer la documentation est appelé le [répertoire source](https://ugaugtjjyr2ylvegfxect62ccm--www-sphinx-doc-org.translate.goog/en/master/glossary.html#term-source-directory).
C’est le répertoire que nous avons nommé lors de l’installation «**sources-documents**».

Ce répertoire contient également le fichier de configuration Sphinx **conf.py**, où vous pouvez configurer tous les aspects de la façon dont Sphinx lit vos sources et construit votre documentation.

Pour paramétrer tous ces aspects, il faut éditer le fichier **conf.py** qui se trouve dans le dossier **~/repertoire_de_developpement/docs/sources-documents**.

### Renseigner la racine des fichiers de CODE

Indiquez où se trouve vos fichiers de code python pour générer votre documentation :

```
# -*- coding: utf-8 -*-

import os
import sys

sys.path.insert(0, os.path.abspath('../..'))
sys.setrecursionlimit(1500)

# Liste des modèles, relatifs au répertoire source, qui correspondent aux
# fichiers et répertoires à ignorer lors de la recherche de fichiers source.
# Ce modèle affecte également html_static_path et html_extra_path.
exclude_patterns = ['.env']

# Une liste de chemins contenant des modèles supplémentaires
# (ou des modèles qui remplacent les modèles intégrés/spécifiques au thème).
# Les chemins relatifs sont considérés comme relatifs au répertoire de
# configuration.
templates_path = ['_templates']
```

### Configurer les paramètres de documentation

Langue, titre, copyright et auteur :

```
langage = 'fr'

# L’internationalisation de la documentation
locale_dirs = ['locales/']
gettext_compact = False

project = "Documentation sur l’initiation à la programmation Python pour l’administrateur systèmes"
copyright = '2021, Prénom NOM'
author = 'Prénom NOM'
```

Versions du document :

```
from Documentation.mon_module import __version__, __release_life_cycle__

version = __version__ # utilisation restructuredtext |version|
release = __release_life_cycle__ # utilisation restructuredtext |release|
# pre-alpha = faisabilité, alpha = développement, beta = test, rc = qualification, prod = production
```

Extensions des fichiers de la documentation :

```
source_suffix = {
    '.rst': 'restructuredtext',
    '.txt': 'restructuredtext',
    '.md': 'markdown',
}
# ou de la forme
# source_suffix = ['.rst', '.md']
# source_suffix = '.rst'
```

Autres paramètres :

```
# Le document maître toctree.
master_doc = 'index'

# Le thème de la coloration syntaxique
pygments_style = 'sphinx'
```

### Changer de thème pour votre documentation

Le thème par défaut, Alabaster, est très minimaliste, pour avoir une documentation plus sexy, il est préférable de changer le thème par défaut.



![image](images/documentation_1.png)

#### Le thème sphinx book



![image](images/documentation_2.png)

Il existe un certain nombre de thèmes intégrés à Sphinx, et de nombreux autres disponibles. Vous pouvez consulter les thèmes disponibles sur le site [https://sphinx-themes.org/](https://sphinx-themes.org/) .

On va donc voir comment installer **sphinx-book-theme** (mais libre à vous d’en choisir un autre, le principe reste le même).

Pour utiliser le thème «**Sphinx book**», il faut commencer par l’installer, ce qui peut être fait à l’aide de la commande suivante :

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ cd docs ; sudo pip install sphinx-book-theme
Collecting sphinx-book-theme
    Downloading sphinx_book_theme-0.1.0-py3-none-any.whl (87 kB)
        |████████████████████████████████| 87 kB 813 kB/s
Requirement already satisfied: sphinx<4,>=2 in /usr/local/lib/python3.9/dist-packages (from sphinx-book-theme) (3.5.4)
Requirement already satisfied: click in /usr/lib/python3/dist-packages (from sphinx-book-theme) (7.1.2)
Requirement already satisfied: docutils>=0.15 in /usr/local/lib/python3.9/dist-packages (from sphinx-book-theme) (0.16)
Collecting pydata-sphinx-theme~=0.6.0
    Downloading pydata_sphinx_theme-0.6.3-py3-none-any.whl (1.4 MB)
        |████████████████████████████████| 1.4 MB 3.9 MB/s
Collecting beautifulsoup4<5,>=4.6.1
    Downloading beautifulsoup4-4.9.3-py3-none-any.whl (115 kB)
        |████████████████████████████████| 115 kB 4.4 MB/s
Requirement already satisfied: pyyaml in /usr/lib/python3/dist-packages (from sphinx-book-theme) (5.3.1)
Collecting soupsieve>1.2
    Downloading soupsieve-2.2.1-py3-none-any.whl (33 kB)
Requirement already satisfied: packaging in /usr/local/lib/python3.9/dist-packages (from sphinx<4,>=2->sphinx-book-theme) (20.9)
Requirement already satisfied: babel>=1.3 in /usr/local/lib/python3.9/dist-packages (from sphinx<4,>=2->sphinx-book-theme) (2.9.0)
Requirement already satisfied: imagesize in /usr/local/lib/python3.9/dist-packages (from sphinx<4,>=2->sphinx-book-theme) (1.2.0)
Requirement already satisfied: sphinxcontrib-qthelp in /usr/local/lib/python3.9/dist-packages (from sphinx<4,>=2->sphinx-book-theme) (1.0.3)
Requirement already satisfied: Pygments>=2.0 in /usr/local/lib/python3.9/dist-packages (from sphinx<4,>=2->sphinx-book-theme) (2.8.1)
Requirement already satisfied: requests>=2.5.0 in /usr/lib/python3/dist-packages (from sphinx<4,>=2->sphinx-book-theme) (2.25.1)
Requirement already satisfied: setuptools in /usr/lib/python3/dist-packages (from sphinx<4,>=2->sphinx-book-theme) (52.0.0)
Requirement already satisfied: sphinxcontrib-devhelp in /usr/local/lib/python3.9/dist-packages (from sphinx<4,>=2->sphinx-book-theme) (1.0.2)
Requirement already satisfied: alabaster<0.8,>=0.7 in /usr/local/lib/python3.9/dist-packages (from sphinx<4,>=2->sphinx-book-theme) (0.7.12)
Requirement already satisfied: snowballstemmer>=1.1 in /usr/local/lib/python3.9/dist-packages (from sphinx<4,>=2->sphinx-book-theme) (2.1.0)
Requirement already satisfied: sphinxcontrib-serializinghtml in /usr/local/lib/python3.9/dist-packages (from sphinx<4,>=2->sphinx-book-theme) (1.1.4)
Requirement already satisfied: sphinxcontrib-htmlhelp in  /usr/local/lib/python3.9/dist-packages (from sphinx<4,>=2->sphinx-book-theme) (1.0.3)
Requirement already satisfied: sphinxcontrib-jsmath in /usr/local/lib/python3.9/dist-packages (from sphinx<4,>=2->sphinx-book-theme) (1.0.1)
Requirement already satisfied: Jinja2>=2.3 in /usr/local/lib/python3.9/dist-packages (from sphinx<4,>=2->sphinx-book-theme) (2.11.3)
Requirement already satisfied: sphinxcontrib-applehelp in /usr/local/lib/python3.9/dist-packages (from sphinx<4,>=2->sphinx-book-theme) (1.0.2)
Requirement already satisfied: pytz>=2015.7 in /usr/local/lib/python3.9/dist-packages (from babel>=1.3->sphinx<4,>=2->sphinx-book-theme) (2021.1)
Requirement already satisfied: MarkupSafe>=0.23 in /usr/local/lib/python3.9/dist-packages (from Jinja2>=2.3->sphinx<4,>=2->sphinx-book-theme) (1.1.1)
Requirement already satisfied: pyparsing>=2.0.2 in /usr/local/lib/python3.9/dist-packages (from packaging->sphinx<4,>=2->sphinx-book-theme) (2.4.7)
Installing collected packages: soupsieve, beautifulsoup4, pydata-sphinx-theme, sphinx-book-theme
Successfully installed beautifulsoup4-4.9.3 pydata-sphinx-theme-0.6.3 soupsieve-2.2.1 sphinx-book-theme-0.1.0
```

Ensuite, il faut indiquer à Sphinx d’utiliser ce thème.

Il faudra remplacer la variable «**html_theme**» dans «**conf.py**» et modifier dans la section HTML les paramètres pour ce thème :

```
# -- Options pour sortie HTML -------------------------------------------------
html_title = "Documentation développement python pour l'administrateur"
html_short_title = "Développement Python 3"
html_logo = "images/logo.png"
html_favicon = "images/favicon.png"

html_theme = 'sphinx_book_theme'

# -- Options du thème
html_theme_options = {
    # Ajout du renvoie vers Gitlab
    'repository_url': "http://gitlab.domaine-perso.fr/utilisateur/initiation_developpement_python_pour_administrateur",
    'use_repository_button': True,
    # Ajout de la possibilité de laisser des retours de dysfonctionnements
    'use_issues_button': True,
    # Ajout de la possibilité de laisser des propositions de corrections à la documentation
    'use_edit_page_button': True,
    # Ajout de la possibilité de travailler sur une branche définie
    #'repository_branch': 'master',
    # Ajout du chemin relatif vers les sources de la doc
    'path_to_docs': "docs/sources-documents",
    # Ajout d'un téléchargement Rest ou PDF de la page actuelle
    'use_download_button': True,
    # Ajout d'un bouton lecture plein écran
    'use_fullscreen_button': True,
    # Ajout de la table des matières dans le panneau latéral gauche
    'home_page_in_toc': False,
    # Titre du panneau latéral droite qui sera notre table des matières
    'toc_title': "Contenu",
    # Ajout au pied de page du panneau latéral gauche
    'extra_navbar': "<p>Version " + release + "</p>",
}

#html_sidebars = {
#    "**": ["sbt-sidebar-nav.html", "sbt-sidebar-footer.html"]
#}

# Une liste de chemins contenant des fichiers statiques personnalisés
# (tels que des feuilles de style ou des fichiers de script).
# Les chemins relatifs sont considérés comme relatifs au répertoire de
# configuration.
# Ils sont copiés dans le répertoire \_static de la sortie après les fichiers
# statiques du thème.
# Par conséquent, un fichier nommé default.css écrasera le fichier default.css
# du thème.
html_static_path = ['_static']
```

### Inclure les extensions utiles pour Python

Installation des extensions non incluses de base :

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement/docs$ sudo apt install sphinx-intl texinfo xindy graphviz latexmk texlive-lang-french texlive-xetex fonts-freefont-otf ; sudo pip install sphinxcontrib-inlinesyntaxhighlight sphinx-copybutton sphinx-markdown-builder sphinx-tabs pbr
utilisateur@MachineUbuntu:~/repertoire_de_developpement/docs$ sudo pip install --upgrade sphinx wget https://github.com/mans0954/odfbuilder/releases/download/0.0.1/sphinxcontrib-odfbuilder-0.0.1.tar.gz
utilisateur@MachineUbuntu:~/repertoire_de_developpement/docs$ sudo pip install sphinxcontrib-odfbuilder
```

Il existe de nombreuses extensions intégrés à Sphinx, je vous présente ici celles qui me paraissent les plus utiles :


* [sphinx.ext.intersphinx](https://www.sphinx-doc.org/fr/master/usage/extensions/intersphinx.html) : générer des liens automatiques dans la documentation suivant des mots clés.


* [sphinx.ext.extlinks](https://www.sphinx-doc.org/en/master/usage/extensions/extlinks.html) : Fournit des alias aux URL de base de votre documentation, de sorte que vous n’avez qu’à donner le nom d’alias pour la création du lien dans votre documentation.


* [sphinx.ext.autodoc](https://www.sphinx-doc.org/en/master/usage/extensions/autodoc.html) : Insère automatiquement les docstrings des fonctions, des classes ou des modules Python dans votre documentation. Permet de construire directement de la documentation à partir de votre code Python.


* [sphinxcontrib.inlinesyntaxhighlight](https://sphinxcontrib-inlinesyntaxhighlight.readthedocs.io/en/latest/) : Insère du code d’un langage de programmation dans une ligne de texte.


* [sphinxcontrib-bibtex](https://sphinxcontrib-bibtex.readthedocs.io/en/latest/) : Insère des références bibliographiques.


* [sphinx.ext.todo](https://www.sphinx-doc.org/en/master/usage/extensions/todo.html) : Insère des taches, ou liste de taches à faire dans votre documentation.


* [sphinx.ext.githubpages](https://www.sphinx-doc.org/en/master/usage/extensions/githubpages.html) : Cette extension créée un fichier **.nojekyll** dans le répertoire HTML généré pour publier le document sur les pages GitHub.


* [sphinx.ext.imgmath](https://bizdd.readthedocs.io/en/latest/ext/math.html) : Insère des formules mathématiques dans votre documentation.


* [sphinx.ext.graphviz](https://www.sphinx-doc.org/en/master/usage/extensions/graphviz.html) : Cette extension vous permet d’intégrer des [graphiques Graphviz](https://cyberzoide.developpez.com/graphviz/) dans vos documents.


* [sphinx.ext.inheritance_diagram](https://www.sphinx-doc.org/en/master/usage/extensions/inheritance.html) : Cette extension vous permet d’inclure des diagrammes d’héritage, rendus via l’extension Graphviz.


* [sphinx_copybutton](https://sphinx-copybutton.readthedocs.io/en/latest/) : insère dans votre documentation une icône pour copier dans le presse-papiers le contenu de la directive.


* [hieroglyph](https://pvbookmarks.readthedocs.io/en/latest/documentation/doc_generators/sphinx/contributed_extensions/hieroglyph.html) : Permet de générer des slides de présentations.


* [sphinx.ext.ifconfig](https://www.sphinx-doc.org/en/master/usage/extensions/ifconfig.html) : Inclure le contenu de la directive uniquement si l’expression Python donnée en argument est `True`.


* [sphinx.ext.doctest](https://www.sphinx-doc.org/en/master/usage/extensions/doctest.html) : Cette extension vous permet de tester un code Python dans la documentation. Le constructeur doctest collectera le résultat et pourra agir suivant les retours d’exécutions obtenus.


* [sphinx_markdown_builder](https://pypi.org/project/sphinx-markdown-builder/) : Pour construire de la documentation au format markdown pour gitlab et créer les formats .odt ou .docx.

#### Ajout des extensions utiles pour Python et Sphinx

```
extensions = [
    'sphinx.ext.intersphinx',
    'sphinx.ext.extlinks',
    'sphinx.ext.autodoc',
    'sphinxcontrib.inlinesyntaxhighlight',
    'sphinx.ext.githubpages',
    'sphinx.ext.graphviz',
    'sphinx.ext.inheritance_diagram',
    'sphinx_copybutton',
    'sphinx.ext.tabs',
    'sphinx.ext.todo',
    'sphinx.ext.ifconfig',
    'sphinx.ext.doctest',
    'sphinx_markdown_builder',
    'sphinxcontrib-odfbuilder',
]
```

### Configuration des extensions

```
# -- Configuration des extensions ---------------------------------------------
```

#### Intersphinx

```
# -- Options pour intersphinx -------------------------------------------------
# Alias du lien de la documentation de Python 3
intersphinx_mapping = {'python': ('https://docs.python.org/fr/3/', None)}
# Utilisation restructuredtext:
# index de page WEB
# :ref:`python:reference-index`
# :ref:`Référence langage Python <python:reference-index>`
# lien WEB
# :doc:`python:library/enum`
# :doc:`Énumérations <python:library/enum>`
```

#### Extlinks

```
# -- Options pour extlinks ----------------------------------------------------
# Liens vers la documentation de Python 3
extlinks = {
    'docpython3': ('https://docs.python.org/fr/3/%s', 'Python'),
    'manpython3': ('https://docs.python.org/fr/3/library/%s.html', 'Manuel Python de '),
}
# utilisation restructuredtext :docpython3:`tutorial` ou :manpython3:`enum`
```

#### Autodoc

```
# -- Options pour autodoc -----------------------------------------------------
# Simule l'existence du module classes de python pour ne pas être bloqué
autodoc_mock_imports = ['classes']
```

#### Inline Syntax highlight

```
# -- Options pour inline syntax highlight --------------------------------------
# Langage défini par la directive de surbrillance si aucun langage n'est défini par le rôle
inline_highlight_respect_highlight = False
# Langage défini par la directive de surbrillance si aucun rôle n'est défini
inline_highlight_literals = False
```

#### Graphviz

```
# -- Options pour Graphviz ----------------------------------------------------
graphviz_output_format = 'svg'
# utilisation restructuredtext :
# .. graphviz::
#
#     digraph frameworks web python {
#         python [label="python", href="https://www.python.org/", target="_top"];
#         flask [label="Flask", href="https://flask.palletsprojects.com/en/1.1.x/", target="_top"];
#         django [label="Bjango", href="https://www.djangoproject.com/", target="_top"];
#         bottle [label="Bottle", href="https://bottlepy.org/", target="_top"];
#         turbogears [label="TurboGears", href="https://turbogears.org/", target="_top"];
#         web2py [label="web2py", href="http://www.web2py.com/", target="_top"];
#         cherrypy [label="CherryPy", href="https://cherrypy.org/", target="_top"];
#         quixote [label="Quixote", href="http://quixote.ca/", target="_top"];
#         python -> {flask; django; bottle; turbogears; web2py; cherrypy; quixote;};
#     }
```

#### Copybutton

```
# -- Options pour copybutton ----------------------------------------------------
# copybutton_prompt_text = '>>> '
```

#### Tabs

```
# -- Options pour tabs ----------------------------------------------------
# utilisation restructuredtext :
# .. tabs::
#     .. tab:: Python
#         Ma documentation sur Python
#         .. tabs::
#             .. code-tab:: py
#                 Fichier Python main.py
#             .. code-tab:: java
#                 Fichier java
#         .. tabs::
#             .. code-tab:: py
#                 def main():
#                     return
#             .. code-tab:: java
#                 class Main {
#                     public static void main(String[] args) {
#                     }
#                 }
#     .. tab:: Frameworks Python
#         .. tabs::
#             .. group-tab:: Flask
#                 Ma documentation sur Flask
#             .. group-tab:: Django
#                 Ma documentation sur Django
#         .. tabs::
#             .. group-tab:: Flask
#                 Le code d'exemple pour Flask
#             .. group-tab:: Django
#                 Le code d'exemple pour Django*
```

#### Ifconfig

```
# -- Options pour ifconfig  ----------------------------------------------------
def setup(app):
    app.add_config_value('niveau_developpement', 'alpha', True)
# utilisation restructuredtext :
# .. ifconfig:: niveau_developpement not in ('alpha', 'beta', 'rc')
#     En production
# .. ifconfig:: 'alpha' == niveau_developpement
#     En developpement
# .. ifconfig:: 'beta' == niveau_developpement
#     En test
# .. ifconfig:: 'rc' == niveau_developpement
#     En qualification
```

#### LaTeX

```
# -- Options pour LaTeX -------------------------------------------------------
latex_engine = 'xelatex'
latex_elements = {
    # Le format du papier('letterpaper' ou 'a4paper').
    'papersize': 'a4paper',
    #
    # La taille de la police ('10pt', '11pt' or '12pt').
    'pointsize': '12pt',
    #
    # Trucs supplémentaires pour le préambule LaTeX.
    'preamble': '',
    #
    # Alignement de la figure en LaTeX (flotteur)
    'figure_align': 'htbp',
}
#latex_show_urls = 'footnote'

# Regroupement de l'arborescence de documents en fichiers LaTeX. Liste des tuples
# (fichier source, nom du fichier cible, titre, auteur, documentclass [howto, manuel ou votre classe]).
latex_documents = [
    (master_doc, 'InitiationProgrammationPythonPourAdministrateurSystèmes.tex', "Initiation à la programmation Python pour l'administrateur systèmes", author, 'manual'),
]
```

#### Pages de manuel

```
# -- Options pour les pages de manuel ----------------------------------------
# Une entrée par page de manuel. Liste des tuples
# (fichier source, nom, description, auteurs, section du manuel).
man_pages = [
    (master_doc, 'InitiationProgrammationPythonPourAdministrateurSystèmes', "Initiation à la programmation Python pour l'administrateur systèmes", [author], 1),
]
```

#### Texinfo

```
# -- Options pour Texinfo ---------------------------------------------------
# Regroupement de l'arborescence des documents dans des fichiers Texinfo.
# Liste des tuples (fichier source, nom de la cible, titre, auteur, répertoire, description, catégorie)
texinfo_documents = [
    (master_doc, 'InitiationProgrammationPythonPourAdministrateurSystèmes', "Initiation à la programmation Python pour l'administrateur systèmes", author, 'InitiationProgrammationPythonPourAdministrateurSystèmes', "Formation d'initiation à la programmation Python pour l'administrateur systèmes.", 'Miscellaneous'),
]
```

#### Epub

```
# -- Options pour les Epub ---------------------------------------------------
# Informations bibliographiques Dublin Core.
epub_title = project
# L'identifiant unique du texte. Cela peut être un numéro ISBN
# ou la page d'accueil du projet.
# epub_identifier = ''

# Une identification unique pour le texte.
# epub_uid = ''

# A list of files that should not be packed into the epub file.
epub_exclude_files = ['search.html']
```

### Générer la documentation

**Attention des accents dans les noms de fichiers .rst font planter LaTeX.**

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement/docs$ make html
utilisateur@MachineUbuntu:~/repertoire_de_developpement/docs$ make latex
utilisateur@MachineUbuntu:~/repertoire_de_developpement/docs$ make latexpdf
utilisateur@MachineUbuntu:~/repertoire_de_developpement/docs$ make epub
utilisateur@MachineUbuntu:~/repertoire_de_developpement/docs$ make text
utilisateur@MachineUbuntu:~/repertoire_de_developpement/docs$ make markdown
utilisateur@MachineUbuntu:~/repertoire_de_developpement/docs$ make xml
utilisateur@MachineUbuntu:~/repertoire_de_developpement/docs$ make man
utilisateur@MachineUbuntu:~/repertoire_de_developpement/docs$ make texinfo
utilisateur@MachineUbuntu:~/repertoire_de_developpement/docs$ make info
utilisateur@MachineUbuntu:~/repertoire_de_developpement/docs$ make odt
```

#### Générer les formats odt ou docx

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement/docs$ sudo apt install pandoc
utilisateur@MachineUbuntu:~/repertoire_de_developpement/docs$ cd documentation/html ; pandoc ./index.html -o ../../InitiationProgrammationPythonPourAdministrateurSystèmes.odt
utilisateur@MachineUbuntu:~/repertoire_de_developpement/docs/documentation/html$ pandoc ./index.html -o ../../InitiationProgrammationPythonPourAdministrateurSystèmes.docx
```

#### Générer l’internationalisation

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement/docs$ make gettext
utilisateur@MachineUbuntu:~/repertoire_de_developpement/docs$ sphinx-intl update -p documentation/gettext -l fr -l en
```

Traduire les fichiers dans **./locales/<lang>/LC_MESSAGES/**

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement/docs$ make -e SPHINXOPTS="-Dlanguage='en'" html
```

## Rédiger la documentation

Le fichier d’entrée de votre documentation **index.rst** (toctree) se trouve dans **~/repertoire_de_developpement/docs/sources-documents**.

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement/docs$ sudo apt install retext
```

Éditer le fichier index.rst et le modifier ainsi :

```
.. Documentation sur l'initiation à la programmation Python pour l'administrateur systèmes. Document maître créé par sphinx-quickstart le Mercredi 24 Avril 2021 à 17h 27min 50s.
   Vous pouvez adapter ce fichier complètement à votre goût.
   Il contient la racine `toctree` de votre documentation.

.. toctree::
   :caption: Contenu :
   :maxdepth: 2

Initiation à la programmation Python pour l'administrateur systèmes
===================================================================


Index
=====

* :ref:`genindex`

Index des modules
=================

* :ref:`modindex`

.. * :ref:`search\`
```

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement/docs$ make html
```



![image](images/documentation_3.png)

### Parties, chapitres, sections, paragraphes

Par convention pour Python :


* «**#**» : Parties


* «**\***» : Chapitres


* «**=**» : Sections


* «**-**» : Sous-sections


* «**^**» : Sous-sous-section


* «**"**» : Paragraphes

```
.. Documentation sur l'initiation à la programmation Python pour l'administrateur systèmes. Document maître créé par sphinx-quickstart le Mercredi 24 Avril 2021 à 17h 27min 50s.
   Vous pouvez adapter ce fichier complètement à votre goût.
   Il contient la racine `toctree` de votre documentation.

.. sectionauthor:: Prénom NOM <prenom.nom@fai.fr>

.. codeauthor:: Geek DEVELOPPEUR <geek.developpeur@fai.fr>

.. toctree::
   :caption: Contenu :
   :maxdepth: 5

Programmation Python 3
######################

Un texte d’introduction sur la partie Python 3.

Initiation à la programmation Python pour l'administrateur systèmes
*******************************************************************

Un texte d’introduction pour mon chapitre.

Section 1
=========

Un texte pour la section 1.

Sous-section 1
--------------

Du texte pour la sous-section 1

    Du texte pour une sous-partie de la sous-section 1

        Du texte pour une sous sous partie de la sous-section 1

Sous-section 2
--------------

Du texte pour la sous-section 2

Exemple de titrages
###################

Chapitre 1
**********

Section 1
=========

Sous-section 1
--------------

Sous-sous-section 1
^^^^^^^^^^^^^^^^^^^

Paragraphe 1
""""""""""""

Sous-paragraphe 1
+++++++++++++++++
```

```
```

**utilisateur@MachineUbuntu:~/repertoire_de_developpement/docs$** make
html



![image](images/documentation_4.png)



![image](images/documentation_5.png)

### Mettre en forme du texte

```
.. Documentation sur l'initiation à la programmation Python pour l'administrateur systèmes. Document maître créé par sphinx-quickstart le Mercredi 24 Avril 2021 à 17h 27min 50s.
   Vous pouvez adapter ce fichier complètement à votre goût.
   Il contient la racine `toctree` de votre documentation.

.. toctree::
   :caption: Contenu :
   :maxdepth: 2

Initiation à la programmation Python pour l'administrateur systèmes
###################################################################

**Tout en gras.**

Du texte \ **en gras**\ pour ma documentation.

*Tout en italique.*

Du texte \ *en italique*\ pour ma documentation.

.. only:: html

    .. raw:: html

        Une phrase avec un <font color="Red">mot</font> en rouge.

.. only:: latex

    .. raw:: latex

        Une phrase avec un \textcolor{red}{mot} en rouge.

.. only:: odt

    Une phrase avec un mot non en rouge.
```

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement/docs$ make html
utilisateur@MachineUbuntu:~/repertoire_de_developpement/docs$ make latexpdf
```



![image](images/documentation_6.png)

### Insertion de texte d’échappement

```
.. Documentation sur l'initiation à la programmation Python pour l'administrateur systèmes. Document maître créé par sphinx-quickstart le Mercredi 24 Avril 2021 à 17h 27min 50s.
   Vous pouvez adapter ce fichier complètement à votre goût.
   Il contient la racine `toctree` de votre documentation.

.. toctree::
   :caption: Contenu :
   :maxdepth: 2

Initiation à la programmation Python pour l'administrateur systèmes
###################################################################

Le caractère \\

Du texte \
sur une seule ligne.

Du texte sans qu’il soit interprété ``\n, \r, \t, \\``.

``**Une phrase non en gras**``

``*Une phrase non en italique*``
```

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement/docs$ make html
```



![image](images/documentation_7.png)

### Les listes

```
.. Documentation sur l'initiation à la programmation Python pour l'administrateur systèmes. Document maître créé par sphinx-quickstart le Mercredi 24 Avril 2021 à 17h 27min 50s.
   Vous pouvez adapter ce fichier complètement à votre goût.
   Il contient la racine `toctree` de votre documentation.

.. toctree::
   :caption: Contenu :
   :maxdepth: 2

Initiation à la programmation Python pour l'administrateur systèmes
###################################################################

Liste à puces

* élément 1
* élément 2
* élément 3

La liste numérotée

1. élément 1
2. élément 2
3. élément 3

La liste numérotée automatiquement

#. élément 1
#. élément 2
#. élément 3
```

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement/docs$ make html
```



![image](images/documentation_8.png)

### Les tableaux

```
.. Documentation sur l'initiation à la programmation Python pour l'administrateur systèmes. Document maître créé par sphinx-quickstart le Mercredi 24 Avril 2021 à 17h 27min 50s.
   Vous pouvez adapter ce fichier complètement à votre goût.
   Il contient la racine `toctree` de votre documentation.

.. toctree::
   :caption: Contenu :
   :maxdepth: 2

Initiation à la programmation Python pour l'administrateur systèmes
###################################################################

+-----+-----------+
|  A  |     B     |
+=====+=====+=====+
|  1  |  2  |  3  |
+-----+-----+-----+

==== ==== ====
    A    B
--------- ----
 A0   A1   B0
==== ==== ====
 01   02   03
 04   05   06
==== ==== ====

.. csv-table:: Personnel
  :header: "Prénom", "Nom"
  :widths: 40, 40

  "Franc", "GEEK"
  "Emmanuel", "DICTATOR"

.. list-table:: Lettres
  :widths: 10 10 20
  :header-rows: 1
  :stub-columns: 1

  * - Lettre
    - A
    - B
  * - Nombre
    - 25
    - 5
```

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement/docs$ make html
```



![image](images/documentation_9.png)

### Insertion de blocs, code, image, graphviz, liens, mathématiques, notes, citations

```
.. Documentation sur l'initiation à la programmation Python pour l'administrateur systèmes. Document maître créé par sphinx-quickstart le Mercredi 24 Avril 2021 à 17h 27min 50s.
   Vous pouvez adapter ce fichier complètement à votre goût.
   Il contient la racine `toctree` de votre documentation.

.. role:: python(code)
   :language: python

.. toctree::
   :caption: Contenu :
   :maxdepth: 2

Initiation à la programmation Python pour l'administrateur systèmes
###################################################################

Pour afficher un texte en Python on utilise la fonction :python:`print("Mon texte")`.

.. literalinclude:: ../../1_Mode_interprété/mon_1er_programme.py

.. code-block:: python

    print("Bonjour les zouzous")

.. image:: ../../../Images/tux.svg
   :alt: Sphinx c’est cool
   :align: center
   :width: 120px

.. graphviz::

    digraph "frameworks web python" {
        python [label="Python", href="https://www.python.org/", target="_top"];
        flask [label="Flask", href="https://flask.palletsprojects.com/en/1.1.x/", target="_top"];
        django [label="Django", href="https://www.djangoproject.com/", target="_top"];
        bottle [label="Bottle", href="https://bottlepy.org/", target="_top"];
        turbogears [label="TurboGears", href="https://turbogears.org/", target="_top"];
        web2py [label="web2py", href="http://www.web2py.com/", target="_top"];
        cherrypy [label="CherryPy", href="https://cherrypy.org/", target="_top"];
        quixote [label="Quixote", href="http://quixote.ca/", target="_top"];
        python -> {flask; django; bottle; turbogears; web2py; cherrypy; quixote;};
    }

`Python <https://www.python.org>`_

- :ref:`python:reference-index`
- :ref:`Référence langage Python <python:reference-index>`
- :doc:`python:library/enum`
- :doc:`Énumérasions <python:library/enum>`
- :docpython3:`tutorial`
- :manpython3:`enum`

:download: `Téléchargement <http://gitlab.domaine-perso.fr/utilisateur/initiation_developpement_python_pour_administrateur/-/raw/master/README.md?inline=false>`_

.. math::
   :nowrap:

    \begin{gather*}
    (a + b)² = a² + 2ab + b² \\
    \sqrt{\frac{n}{n-\sqrt[3]{2}} S} \\
    \int_a^b x \, \mathrm dx = [x^2]_a^b = [(b)^2 – (a)²] = b² – a² \\
    \mathrm{2~H_{2(g)}+O_{2(g)}=2~H_2O_{(1)}}
    \end{gather*}

L'équation d'Euler :eq:`euler` est utile en mathématiques.

Référence à la citation du formateur [citation]_.

Première note [#n1]_, deuxième note [#n2]_

Et encore une troisième note [#n3]_

.. [citation] «Python que oui, ou Python que non…».

.. math:: e^{i\pi} + 1 = 0
   :label: euler

.. rubric:: Notes de bas de page

.. [#n1] Note 1
.. [#n2] Note 2
.. [#n3] Note 3
```

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement/docs$ make html
```



![image](images/documentation_10.png)

### Boites et conditions d’affichages

Il faut d’abord modifier l’extension «**sphinx.ext.todo**» au fichier **conf.py**.

```
[extensions]
todo_include_todos = True
```

Puis modifier **index.rst** comme suit :

```
.. Documentation sur l'initiation à la programmation Python pour l'administrateur systèmes. Document maître créé par sphinx-quickstart le Mercredi 24 Avril 2021 à 17h 27min 50s.
   Vous pouvez adapter ce fichier complètement à votre goût.
   Il contient la racine `toctree` de votre documentation.

.. toctree::
   :maxdepth: 2
   :caption: Contenu :

Initiation à la programmation Python pour l'administrateur systèmes
###################################################################

.. ifconfig:: niveau_developpement == 'alpha'

    .. only :: format_html and not builder_epub

        .. raw:: html

            <font color="Red">Liste des TODOs à faire</font>

    .. only :: latex

        .. raw:: latex

            \textcolor{red}{Liste des TODOs à faire}

    .. todolist::

.. ifconfig:: niveau_developpement not in ( 'pre-alpha', 'alpha', 'beta', 'rc')

    En production
.. ifconfig:: niveau_developpement == 'pre-alpha'

    En faisabilité
.. ifconfig:: niveau_developpement == 'alpha'

    En développement
.. ifconfig:: niveau_developpement == 'beta'

    En test
.. ifconfig:: niveau_developpement == 'rc'

    En qualification

Les boîtes :

.. ifconfig:: niveau_developpement == 'alpha'

    .. todo:: Compléter la boîte voir aussi

.. seealso:: Ceci est une boîte

.. ifconfig:: niveau_developpement == 'alpha'

    .. todo:: Compléter la boîte note

.. note:: Ceci est une boîte

.. sidebar:: Ceci est une boîte

    Contenu de la barre de côté

.. warning:: Ceci est une boîte

.. important:: Ceci est une boîte

.. ifconfig:: niveau_developpement == 'alpha'

    .. todo:: Compléter la boîte Remarque

.. topic:: **Remarque**

    Ceci est le contenu du sujet

.. only:: format_html and not builder_epub

    Les onglets à n'utiliser que pour une documentation purement html

    .. tabs::

        .. tab:: Code

            Ma documentation sur le code

            .. tabs::

                .. code-tab:: py

                    python AfficheArguments.py Bonjour à tous

                .. code-tab:: java

                    java AfficheArguments Bonjour à tous

            .. tabs::

                .. code-tab:: py

                    import sys

                    for arg in sys.argv:
                        print(arg)

                .. code-tab:: java

                    public class AfficheArguments {
                        public static void main(String[] args) {
                            int i;
                            for (String s : args) System.out.println(s);
                        }
                    }

        .. tab:: Group

            Ma documentation sur le code

            .. tabs::

                .. group-tab:: Python

                    Exécuter le programme :

                    .. code-block:: shell

                        python AfficheArguments.py Bonjour à tous

                .. group-tab:: Java

                    Exécuter le programme :

                    .. code-block:: shell

                        java AfficheArguments Bonjour à tous

            .. tabs::

                .. group-tab:: Python

                    Le code Python :

                    .. code-block:: python

                        import sys

                        for arg in sys.argv:
                            print(arg)

                .. group-tab:: Java

                    Le code java :

                    .. code-block:: java

                        public class AfficheArguments {
                            public static void main(String[] args) {
                                int i;
                                for (String s : args) System.out.println(s);
                            }
                        }
```

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement/docs$ make html
utilisateur@MachineUbuntu:~/repertoire_de_developpement/docs$ make epub
utilisateur@MachineUbuntu:~/repertoire_de_developpement/docs$ make latexpdf
```



![image](images/documentation_11.png)

### Documenter le code Python

Modifier le fichier «**index.rst**» :

```
.. Documentation sur l'initiation à la programmation Python pour l'administrateur systèmes. Document maître créé par sphinx-quickstart le Mercredi 24 Avril 2021 à 17h 27min 50s.
   Vous pouvez adapter ce fichier complètement à votre goût.
   Il contient la racine `toctree` de votre documentation.

.. toctree::
   :maxdepth: 2
   :caption: Contenu :

Initiation à la programmation Python pour l'administrateur systèmes
###################################################################

Approche manuelle :

.. py:module:: module
   :platform: Linux
   :synopsis: Un court résumé du périmètre d’utilisation du module

.. py:function:: fonction(paramètres)

    .. py:class:: Classe(paramètres)

        .. py:method:: méthode(paramètres)

            .. py:attribute:: attribut

.. py:decorator:: décorateur(paramètres)

.. py:exception:: exception
```

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement/docs$ make html
```



![image](images/documentation_12.png)

Pour l’approche automatique, dont nous verrons l’utilisation un peu plus loin, il faut utiliser :

```
.. Documentation sur l'initiation à la programmation Python pour l'administrateur systèmes. Document maître créé par sphinx-quickstart le Mercredi 24 Avril 2021 à 17h 27min 50s.
   Vous pouvez adapter ce fichier complètement à votre goût.
   Il contient la racine `toctree` de votre documentation.

.. toctree::
   :maxdepth: 2
   :caption: Contenu :

Initiation à la programmation Python pour l'administrateur systèmes
###################################################################

Approche automatique :

.. automodule:: Documentation.mon_module
   :members:
```

Rôle des membres de **automodule** dans nos fichiers «**.rst**» :


* **:members:** affiche les éléments publics (qui ne débute pas par «**_**»).


* **:special-members:** Affiche aussi les constructeurs des éléments publics.


* **:undoc-members:** Affiche aussi les éléments sans docstring.


* **:private-members:** Affiche aussi les éléments privés (qui commencent par «**_**»).


* **:inherited-members:** Affiche les éléments non hérités.


* **:show-inheritance:** Affiche les classes mères dont hérite les classes Python.

Ces définitions vont se trouver dans la structure de la documentation «**.rst**».

Pour l’écriture de la documentation directement dans notre code il faudra renseigner nos docstring. Sphinx ajoute de nombreuses nouvelles directives, et rôles de texte interprétés, au balisage reST standard.

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement/docs$ cd .. ; mkdir Documentation ; cd Documentation ; touch mon_module.py
```

#### Balise de méta-information

##### sectionauthor

Lorsque vous commencez l’écriture de la documentation d’un code Python, il est très utile pour les autres rédacteurs, ou les développeurs, de savoir qui l’a saisi dans le code. La directive `.. sectionauthor:: Auteur Documentation <auteur.documentation@fai.fr>` identifie l’auteur de la documentation de la section actuelle. La partie d’adresse courriel doit être en minuscules.

Actuellement, ce balisage n’est pas interprété dans la sortie documentaire, mais elle permet de garder une trace des contributions à la documentation dans le code.

Exemple à saisir dans «**mon_module.py**» :

```
# -*- coding: utf-8 -*-

"""
.. sectionauthor:: Stagiaire ADMINISTRATEUR <stagiaire.administrateur@fai.fr>
"""
```

#### Balisages spécifiques au module

Lorque vous commencez l’écriture d’un module Python (fichier «**mon_module.py**»), la première des informations à fournir aux autres développeurs est sur le module lui même. Le balisage décrit dans cette section est utilisé pour fournir ces informations sur le module pour la documentation. **Chaque module doit être documenté dans son propre fichier**. Normalement, ce balisage apparaît après le titre du module dans le fichier ; un fichier typique peut commencer comme ceci :

```
# -*- coding: utf-8 -*-

"""
.. sectionauthor:: Stagiaire ADMINISTRATEUR <stagiaire.administrateur@fai.fr>
:mod:`mon_module` -- Module d'exemple
#####################################

.. module:: mon_paquet.mon_module
   :synopsis:: Ce module illustre comment écrire une docstring de module avec Python
   :platform: Linux
"""
```

**NOTE**: Il est important de donner un titre de module puisque cela sera inséré dans l’arborescence de la table des matières pour la documentation.

##### module

La directive `.. module:: nom_du_module` marque le début de la description d’un module, d’un package ou d’un sous-module. **Le nom doit être entièrement qualifié** (c’est-à-dire incluant le nom du package pour les sous-modules). Il est paramétrable avec :


* L’option `:synopsis: Résumé rapide` qui doit consister en une phrase décrivant l’objectif du module. Elle n’est actuellement utilisée que dans l’index global des modules.


* L’option `:platform: Unix, Mac, Windows`, si elle est présente, qui indique les plateformes compatibles avec le code Python. C’est une liste séparée par des virgules des plates-formes. Si le code est disponible pour toutes les plates-formes, l’option doit être omise. Les clés sont des identifiants courts ; les exemples utilisés incluent «**Linux**», «**Unix**», «**Mac**» et «**Windows**». Il est important d’utiliser une clé qui a déjà été utilisée le cas échéant.


* L’option `:deprecated:` (sans valeur) peut être donnée pour marquer un module comme obsolète ; il sera alors désigné comme tel à divers endroits.

##### moduleauthor

```
# -*- coding: utf-8 -*-

"""
.. sectionauthor:: Stagiaire ADMINISTRATEUR <stagiaire.administrateur@fai.fr>
:mod:`mon_module` -- Module d'exemple
#####################################

.. module:: mon_module
   :platform: Unix, Windows
   :synopsis: Ce module illustre comment écrire votre docstring dans Python.
.. moduleauthor:: Formateur PYTHON <formateur.python@fai.fr>
.. moduleauthor:: Stagiaire ADMINISTRATEUR <stagiaire.administrateur@fai.fr>

"""
```

La directive `.. moduleauthor::`, qui peut apparaître plusieurs fois, nomme les auteurs du code Python du module, tout comme `.. sectionauthor::` nomme le(s) auteur(s) d’une documentation. Elle suit les même règles de syntaxe que `.. sectionauthor::`.

#### Pour le code, nos fonctions et nos classes


* «**:var/ivar/cvar nom_variable: description**» : Pour nos variables.


* «**:param/parameter/arg/argument/key/keywords nom_paramètre: description**» : Pour décrire un paramètre d’une fonction ou d’un objet.


* «**:type element: type**» : Pour décrite le type d’une variable ou d’un paramètre (Callable, int, float, long, str, tuple, list, dict, None, True, False, boolean).


* «**:returns/return: description**» : Décrit ce qui est retourné par une fonction ou un objet.


* «**:rtype: type**» : Pour décrite le type de ce qui est retourné par une fonction ou un objet (Callable, int, float, long, str, tuple, list, dict, None,
True, False, boolean).


* «**:raises/raise/except/exception nom_exception: description**» : Décrit une exception dans votre code.

Tout ceci nous permet d’écrire un code de documentation minimal final (avec des variables Python et Sphinx utiles) :

```
# -*- coding: utf-8 -*-

"""
.. sectionauthor:: Stagiaire ADMINISTRATEUR <stagiaire.administrateur@fai.fr>
:mod:`mon_module` -- Module d'exemple
#####################################

.. module:: mon_module
   :platform: Unix, Windows
   :synopsis: Ce module illustre comment écrire votre docstring dans Python.
.. moduleauthor:: Formateur PYTHON <formateur.python@fai.fr>
.. moduleauthor:: Stagiaire ADMINISTRATEUR <stagiaire.administrateur@fai.fr>

"""

__title__ = "Module illustration écriture docstring Python"
__author__ = "Formateur PYTHON"
__version__ = '0.7.3'
__release_life_cycle__ = 'alpha'
# pre-alpha = faisabilité, alpha = développement, beta = test, rc = qualification, prod = production
__docformat__ = 'reStructuredText'
```

Fichier **mon_module.py** avec une classe Python d’exemple :

```
# -*- coding: utf-8 -*-

"""
.. sectionauthor:: Stagiaire ADMINISTRATEUR <stagiaire.administrateur@fai.fr>
:mod:`mon_module` -- Module d'exemple
#####################################

.. module:: mon_module
   :platform: Unix, Windows
   :synopsis: Ce module illustre comment écrire votre docstring dans Python.
.. moduleauthor:: Formateur PYTHON <formateur.python@fai.fr>
.. moduleauthor:: Stagiaire ADMINISTRATEUR <stagiaire.administrateur@fai.fr>

"""

__title__ = "Module illustration écriture docstring Python"
__author__ = "Formateur PYTHON"
__version__ = '0.7.3'
__release_life_cycle__ = 'alpha'
# pre-alpha = faisabilité, alpha = développement, beta = test, rc = qualification, prod = production
__docformat__ = 'reStructuredText'

class ClasseExemple(object):
    """Cette classe docstring montre comment utiliser sphinx et la syntaxe rst.
    La première ligne est une brève explication\ de la classe avec ses paramètres
    et ce que renvoi l’objet.
    Cela doit être complété par une description plus précise des méthodes et des
    attributs dans le code de la classe dans le code.
    La seule méthode ici est :func:`mafonction`.

    - **paramètres**, **types**, **retour** et **type de retours**::

        :param arg1: description
        :param arg2: description
        :type arg1: type arg1
        :type arg2: type arg2
        :return: description du retour
        :rtype: type du retour

    - Documentez des sections **Exemples** en utilisant la syntaxe des doubles points ``:``
        ::

            :Exemple:

                suivi d'une ligne vierge!

        qui apparaît comme suit :

        :Exemple:

            suivi d'une ligne vierge!

    - Des sections spéciales telles que **Voir aussi**, **Avertissements**, **Notes** avec la syntaxe sphinx (*directives de paragraphe*)::

        .. seealso:: blabla
        .. warnings:: blabla
        .. note:: blabla
        .. todo:: blabla

    .. warning::
       Il existe de nombreux autres champs Info mais ils peuvent être redondants:

           * param, parameter, arg, argument, key, keyword: Description d'un paramètre.
           * type: Type de paramètre.
           * raises, raise, except, exception: Quand une exception spécifique est levée.
           * var, ivar, cvar: Description d'une variable.
           * returns, return: Description de la valeur de retour.
           * rtype: Type de retour.

    .. note::
        Il existe de nombreuses autres directives telles que :
        versionadded, versionchanged, rubric, cent\ ered\ , ...
        Voir la documentation sphinx pour plus de détails.

    Voici ci-dessous les résultats pour :func:`mafonction` docstring.

    """

def maméthode(self, arg1, arg2, arg3):
    """Retourne (arg1 / arg2) + arg3

    Ceci est une explication plus précise, qui peut inclure des mathématiques
    avec la syntaxe latex :math:`\\alpha`.
    Ensuite, vous devez fournir une sous-section facultative (juste pour être
    cohérent et avoir une documentation uniforme. Rien ne vous empêche de
    changer l'ordre):

        - paramètres utilisés ``:param <nom>: <description>``
        - type des paramètres ``:type <nom>: <description>``
        - retours de la méthode ``:returns: <description>``
        - exemples (doctest)
        - utilisation de voir aussi ``.. seealso:: texte``
        - utilisation des notes ``.. note:: texte``
        - utilisation des alertes ``.. warning:: texte``
        - liste des restes à faire ``.. todo:: texte``

    **Avantages**:
        - Utilise les balises sphinx.
        - Belle sortie HTML avec les directives Seealso, Note, Warning.

    **Désavantages**:
        - En regardant simplement la docstring, les sections de paramètres, de
          types et de retours n'apparaissent pas bien dans le code.

    :param arg1: la première valeur
    :param arg2: la première valeur
    :param arg3: la première valeur
    :type arg1: int, float,...
    :type arg2: int, float,...
    :type arg3: int, float,...
    :returns: arg1/arg2 +arg3
    :rtype: int, float

    :Example:

    .. code-block:: pycon

        >>> import template
        >>> a = template.ClasseExemple()
        >>> a.mafonction(1,1,1)
        2

    .. note:: il peut être utile de souligner une caractéristique importante

    .. seealso:: :class:`AutreClasseExemple`
    .. warning:: arg2 doit être différent de zéro.
    .. todo:: vérifier que arg2 est non nul.
    """
    return arg1 / arg2 + arg3
```

Générer la documentation pour voir le rendu :

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement/Documentation$ cd ../docs
utilisateur@MachineUbuntu:~/repertoire_de_developpement/docs$ make html
utilisateur@MachineUbuntu:~/repertoire_de_developpement/docs$ cd ..
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git add .
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git status
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git commit -m "Configuration de la documentation du projet"
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git push
```



![image](images/documentation_13.png)

## Mise en œuvre de la documentation avec Gitlab

### Définition de la structure du document

Supposons que vous ayez exécuté **sphinx-quickstart**. Il a créé un répertoire source avec **conf.py** et un document maître, **index.rst**.

La fonction principale du [document maître](https://ugaugtjjyr2ylvegfxect62ccm--www-sphinx-doc-org.translate.goog/en/master/glossary.html#term-master-document) (ou *toctree*) est de servir de page d’accueil et de contenir la racine de «**l’arborescence de la documentation du projet**». C’est l’une des principales choses que Sphinx ajoute à reStructuredText, un moyen de connecter plusieurs fichiers à une seule hiérarchie de documents.

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ makedir docs/sources-documents/classes docs/sources-documents/cours
```

Modifiez **index.rst** :

```
.. |date| date::

:Date: |date|
:Revision: 1.0
:Author: Prénom NOM <prénom.nom@fai.fr>
:Description: Documentation sur l'initiation à la programmation Python pour l'administrateur systèmes
:Info: Voir <http://gitlab.domaine-perso.fr/utilisateur/initiation_developpement_python_pour_administrateur> pour la mise à jour de ce cours.

.. toctree::
   :maxdepth: 2
   :caption: Contenu

.. include:: cours/InitiationProgrammationPythonPourAdministrateurSystemes.rst

Modules
*******

.. automodule:: Documentation.mon_module
   :members:
```

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ touch docs/sources-documents/cours/InitiationProgrammationPythonPourAdministrateurSystemes.rst
```

Modifiez **InitiationProgrammationPythonPourAdministrateurSystemes.rst** :

```
.. Cours Initiation à la programmation Python pour l'administrateur systèmes.

Initiation à la programmation Python pour l'administrateur systèmes
###################################################################
```

### Générer la documentation avec un script

Créer un fichier **makedocs** pour générer la documentation, et **makediagrammes** pour générer ultérieurement les diagrammes de Classes Python.

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ touch makedocs makediagrammes
```

Modifier **makedocs** avec le contenu ci-dessous :

```
#!/bin/bash

titre=$(tput bold ; tput setaf 1 ; tput setab 53)
section=$(tput bold ; tput setaf 3 ; tput setab 240)
soussection=$(tput bold ; tput setaf 4 ; tput setab 0)
ordinaire=$(tput sgr0)

echo -e "$titre""Création de la documentation du projet""$ordinaire"

echo -e "$section""Création des diagrammes de classes""$ordinaire"
( exec "./makediagrammes" )

cd docs
echo -e "$section""Création des fichiers de documentations""$ordinaire"
echo -e "$soussection""Format html""$ordinaire"
make html
echo -e "$soussection""Format LaTeX""$ordinaire"
make latex >/dev/null
echo -e "$soussection""Format epub""$ordinaire"
make epub
echo -e "$soussection""Format pdf""$ordinaire"
make latexpdf >/dev/null
echo -e "$soussection""Format texte""$ordinaire"
make text
echo -e "$soussection""Format xml""$ordinaire"
make xml
echo -e "$soussection""Format markdown""$ordinaire"
make markdown
echo -e "$soussection""Création des pages de manuel""$ordinaire"
make man
echo -e "$soussection""Création des pages texinfo""$ordinaire"
make texinfo
echo -e "$soussection""Création des pages info""$ordinaire"
make info
echo -e "$soussection""Format ODT""$ordinaire"
make odt
pandoc ./documentation/html/index.html -o InitiationProgrammationPythonPourAdministrateurSystèmes.odt
echo -e "$soussection""Format DOCX""$ordinaire"
pandoc ./documentation/html/index.html -o InitiationProgrammationPythonPourAdministrateurSystèmes.docx

echo -e "$section""Création du README.md du projet""$ordinaire"
cp ./documentation/markdown/index.md ../README.md

echo -e "$section""Changement du chemin des images dans README.md""$ordinaire"
cd..
sed -i 's/classes\//docs\/source\/classes\//g'\ README.md
sed -i 's/images\//docs\/source\/images\//g'\ README.md
```

Modifier **makediagrammes** avec le contenu ci-dessous :

```
#!/bin/bash
```

Rendre exécutable.

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ chmod u+x makedocs makediagrammes
```

Créez les documents de la documentation :

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ ./makedocs
```

Sauvegarder les documents dans le dépôt git et dans Gitlab :

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git add .
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git status
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git commit -m "Génération par un script de la documentation du projet"
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git push
```



![image](images/documentation_14.png)

### Générer la documentation avec Gitlab

Créer un fichier vide **requirements.txt** pour préparer à l’installation des modules Python et **packages.txt** pour installer les applications utiles dans l’environnement de test Python. Créer aussi des fichiers vides **docs-requirements.txt** et **docs-packages.txt** pour la construction de la documentation.

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ touch requirements.txt packages.txt docs-requirements.txt docs-packages.txt
```

#### Génération de la documentation dans Gitlab

Modifier **.gitlab-ci.yml** :

Remarque : il faut impérativement saisir «**pages**» comme section

```
image: python:latest

stages:
    - deploy

pages:
    stage: deploy
    script:
        - echo "$GITLAB_USER_LOGIN déploiement de la documentation"
        - echo "** Mises à jour et installation des applications supplémentaires **"
        - echo "Mises à jour système"
        - apt -y update
        - apt -y upgrade
        - echo "Installation des applications supplémentaires"
        - cat docs-packages.txt | xargs apt -y install
        - echo "Mise à jour de PIP"
        - pip install --upgrade pip
        - echo "Installation des dépendances de modules python"
        - pip install -U -r docs-requirements.txt
        - echo "** Génération des diagrammes de classes **"
        - ./makediagrammes
        - echo "** Génération de la documentation HTML **"
        - sphinx-build -b html ./docs/sources-document public
    artifacts:
        paths:
            - public
    only:
        - master
```

Modifier **docs-packages.txt**:

```
dnsutils
sphinx-intl
```

Modifier **docs-requirements.txt**:

```
sphinx
sphinx-intl
sphinxcontrib-inlinesyntaxhighlight
sphinx-copybutton
sphinx-tabs
sphinx-markdown-builder
sphinx-book-theme
```

Modifier **conf.py** pour supprimer les extensions de sortie inutiles:

```
extensions = [
    'sphinx.ext.intersphinx',
    'sphinx.ext.extlinks',
    'sphinx.ext.autodoc',
    'sphinx.ext.githubpages',
    'sphinx.ext.graphviz',
    'sphinx.ext.inheritance_diagram',
    'sphinx_copybutton',
    'sphinx.ext.tabs',
    'sphinx.ext.todo',
    'sphinx.ext.ifconfig',
    'sphinx.ext.doctest',
    'sphinx_markdown_builder',
    'sphinxcontrib-odfbuilder',
]
```

Modifiez **cours/InitiationProgrammationPythonPourAdministrateurSystemes.rst** :

```
.. Cours Initiation à la programmation Python pour l'administrateur systèmes.

`Initiation à la programmation Python pour l'administrateur systèmes <http://utilisateur.documentation.domaine-perso.fr/initiation_developpement_python_pour_administrateur/>`_
###############################################################################################################################################################################
```



![image](images/documentation_15.png)



![image](images/documentation_16.png)



![image](images/documentation_17.png)



![image](images/documentation_18.png)



![image](images/documentation_19.png)



![image](images/documentation_20.png)
