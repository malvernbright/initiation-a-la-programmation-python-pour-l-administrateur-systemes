# La visualisation de l’architecture objets

Le programme `pylint`, que nous avons installé pour tester le code Python, est livré avec un outil de ligne de commande fort pratique `pyreverse`. Celui-ci permet d’imager les classes Python et de créer des diagrammes [UML](https://www.ibm.com/docs/fr/rational-soft-arch/9.5?topic=diagrams-uml-models) des classes Python.

## Pyreverse

**Pyreverse** permet de générer des diagrammes avec :


* des attributs de classes et si possible avec leurs types,


* des méthodes de classes avec leurs paramètres,


* la représentation des exceptions,


* les liens d’héritages de classes,


* et les liens d’association de classes.

### Les options de la ligne de commande

### Options de la ligne de commande de Pyreverse

| Option courte

 | Option verbeuse

 | Description

 |
| ------------------------------------------------------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ------------------------------------------ |  |  |  |  |  |  |  |  |  |  |  |
| `-p <nom du projet>`

                                                                                                                                            | `--project=<nom du projet>`

                                                                                                                                                                                                                                                                         | Nom du fichier en sortie

                   |
| `-o <format>`

                                                                                                                                                   | `--output=<format>`

                                                                                                                                                                                                                                                                                 | Format de sortie de fichier (**svg**, **svgz**, **png**, **jpeg**/**jpg**/**jpe**, **gif**, **ps**/**ps2**/**eps**, **pdf**, pic, pcl, hpgl, gd/gd2, fig, dia, **dot**/**xdot**, **plain**/**plain-ext**, vrml/vml/vmlz, tk, wbmp, xlib, etc.)

 |
| `-c <classe>`

                                                                                                                                                   | `--class <classe>`

                                                                                                                                                                                                                                                                                  | Afficher la classe passée en paramètre

                                                                                                                                             |
| `-k`

                                                                                                                                                            | `--only-classnames`

                                                                                                                                                                                                                                                                                 | Afficher seulement les noms des classes

                                                                                                                                            |
| `-m[y/n]`

                                                                                                                                                       | `--module-names=[y/n]`

                                                                                                                                                                                                                                                                              | Inclure le nom des modules pour la représentation des classes.

                                                                                                                     |
| `-b`

                                                                                                                                                            | `--show-builtin`

                                                                                                                                                                                                                                                                                    | Afficher les classes des objets natifs Python

                                                                                                                                      |
| `-a <niveau>`

                                                                                                                                                   | `--show-ancestors=<niveau>`

                                                                                                                                                                                                                                                                         | Aficher le niveaux d’héritage passé en paramètre

                                                                                                                                   |
| `-A`

                                                                                                                                                            | `--all-ancestors`

                                                                                                                                                                                                                                                                                   | Afficher tout l’arbre d’héritage

                                                                                                                                                   |
| `-s <niveau>`

                                                                                                                                                   | `--show-associated=<niveau>`

                                                                                                                                                                                                                                                                        | Liens des imports suivant le niveau d’imports

                                                                                                                                      |
| `-S`

                                                                                                                                                            | `--all-associated`

                                                                                                                                                                                                                                                                                  | Toutes les liaisons d’imports

                                                                                                                                                      |
| `-f <mode>`

                                                                                                                                                     | `--filter-mode=<mode>`

                                                                                                                                                                                                                                                                              | Filtre ce qu’il faut faire apparaître (par défaut PUB_ONLY) :


* **PUB_ONLY** : Affiche les méthodes et les propriétés publiques.


* **SPECIAL** : Affiche les attributs privés ou protégés en plus.


* **OTHER** : Affiche le méthodes privées ou protégés en plus.


* **ALL** : Affiche tout.

 |
```
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ cd 9_objets
utilisateur@MachineUbuntu:~/repertoire_de_developpement/9_objets$ pyreverse -p test -o png scene.py
```

L’option `-p test` ajout le suffixe **test** au nom du fichier qui à défaut est «**classes.ext**».
L’option `-o png` choisit le format de sortie, ici un fichier image [PNG](https://fr.wikipedia.org/wiki/Portable_Network_Graphics).

Cela génère donc le fichier «**classes_test.png**».

Qui nous donne à afficher par défaut :



![image](images/pyreverse_test1.png)

Cela nous affiche le nom de la classe `Decor` avec sa propriété `fabriqueBatiments` affectée à l’objet `FabriqueBatiments`. Ceci nous permet de comprendre que cela est en fait une méthode pour l’objet.

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement/9_objets$ pyreverse -c Decor -o png scene.py
```

L’option `-c Decour` choisit la classe à générer et va produire une sortie de nom de fichier avec le nom de la classe «**Decor.png**».

Ce qui va nous donner comme diagramme plus intéressant :



![image](images/pyreverse_test2.png)

Là nous voyons avec les **flêches** l’héritage des classes `Vegetation`, `Urbanisation`, `Hydrotopologie`, `Geomorphologie` et `Atmosphere` qui elle même hérite de `Nuages`.

Nous voyon aussi les propriétés et méthodes de `FabriqueBatiments` qui est bien affectée à la propriété `fabriqueBatiments` (en vert) de la classe `Decor`

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement/9_objets$ pyreverse -k -c Decor -o png scene.py
```

L’option `-k` permet de n’afficher que le nom des classes :



![image](images/pyreverse_test3.png)

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement/9_objets$ pyreverse -f ALL -S -c Decor -o png scene.py
```

L’option `-f ALL` permet d’afficher les propriétés et les méthodes cachées :



![image](images/pyreverse_test4.png)

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement/9_objets$ pyreverse -a 1 -c Decor -o png scene.py
```

L’option `-a 1` permet de limiter le niveau de liens en héritage par rapport à la classe :



![image](images/pyreverse_test5.png)

En limitant au premier niveau `-a 1` la classe `Nuages` n’est plus affichée.

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement/9_objets$ pyreverse -b -c Decor -o png scene.py
```

L’option `-b` permet d’afficher l’héritage des classes natives Python :



![image](images/pyreverse_test6.png)

On voit alors bien apparaître la classe `object` comme on l’attendait, mais aussi les classes `builtin.list` et `builtin.str`.

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement/9_objets$ pyreverse -mn -c Decor -o png scene.py
```

L’option `-mn` permet de supprimer le chemin d’importation des classes dans les intitulés de classes :



![image](images/pyreverse_test7.png)

```
utilisateur@MachineUbuntu:~/repertoire_de_developpement/9_objets$ pyreverse -s 0 -c Decor -o png scene.py
```

L’option `-s 0` permet de ne visualiser que les classes importées directement :



![image](images/pyreverse_test8.png)

On voit bien que la classe `FabriqueBatiments` ne s’affiche plus avec un niveau supplémentaire d’importation.

Vous pouvez maintenant générer tous les diagrammes de classes Python que vous voulez pour agrémenter les documentations de vos programmes.

## Mise en œuvre dans GitLab
