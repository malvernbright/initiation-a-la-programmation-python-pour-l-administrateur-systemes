# Les procédures et fonctions

## Définir une procédure/fonction

La syntaxe Python pour la définition d’une fonction est la suivante :

```
def nom_fonction(liste de paramètres):
    """ bloc d'instructions """
```

Vous pouvez choisir n’importe quel nom pour la fonction que vous créez, **à l’exception des mots-clés réservés du langage, et à la condition de n’utiliser aucun caractère spécial ou accentué** (le caractère souligné «**_**» est permis). Comme c’est le cas pour les noms de variables, **on utilise par convention des minuscules**, notamment au début du nom (**les noms commençant par une majuscule seront réservés aux classes**).

### Corps de la procédure/fonction

Comme les instructions `if`, `for` et `while`, l’instruction [def](https://courspython.com/fonctions.html#def) est une instruction composée. La ligne contenant cette instruction se termine obligatoirement par un deux-points «**:**», qui introduisent un bloc d’instructions qui est précisé grâce à l’indentation. Ce bloc d’instructions constitue le **corps de la fonction**.

### Procédure sans paramètres

S’il n’y a pas de valeur retournée nous avons à faire à une procédure.

```
>>> def message():
...     print('Bonjour tout le monde')
...
...
>>> message()
Bonjour tout le monde
```

### Fonction sans paramètres

Pour retourner une valeur, afin d’avoir une fonction, il faut utiliser le paramètre **return**.

```
>>> def mafonction():
...     montexte = 'Bonjour tout le monde'
...     return montexte
...
>>> print(mafonction())
'Bonjour tout le monde'
```

Pour retourner plusieurs paramètres il suffit de les séparer avec «**,**».

```
>>> def mafonction():
...     monprenier_paramètre = 'premier paramètre'
...     mondeuxième_paramètre = 'deuxième paramètre'
...     montroisième_paramètre = 'troisième paramètre'
...     return monprenier_paramètre, mondeuxième_paramètre, montroisième_paramètre
...
>>> mafonction()
('premier paramètre', 'deuxième paramètre', 'troisième paramètre')
```

### Paramètres d’une fonction/procédure

#### Utilisation d’une variable comme paramètre

Passer une paramètre obligatoire à une fonction (ou procédure) s’appelle un **argument positionné**. Il suffit de mettre son nom en argument dans la fonction lors de sa déclaration `def mafonctionouprocédure(monparamètre):`.

Exemple :

```
>>> def compteur(fin):
...     début = 0
...     indice = début
...     while indice < fin:
...         print(indice)
...         indice = indice + 1
...
...
...
>>> compteur(2)
0
1
>>> compteur(5)
0
1
2
3
4
```

#### Plusieurs paramètres

Pour passer plusieurs arguments positionnés il faut les séparer avec «**,**».

```
>>> def compteur_complet(début, fin, pas):
...     indice = début
...     while indice < fin:
...         print(indice)
...         indice = indice + pas
...
...
...
>>> compteur_complet(2, 10, 2)
2
4
6
8
```

#### Valeurs par défaut des paramètres

Pour certains paramètres, la forme la plus utile consiste à indiquer **une valeur par défaut**. C’est ce que l’on appelle **des arguments nommés**.

Les arguments nommés sont sous la forme `kwarg=valeur`. Le paramètre **peut alors devenir optionnel**, et la fonction (ou procédure) peut être appelée avec moins de paramètres.

Dans un appel de fonction (ou de procédure), les **arguments nommés doivent suivre les arguments positionnés**.

```
>>> def compteur_complet(fin, début=0, pas=1):
...     indice = début
...     while indice <= fin:
...         print(indice)
...         indice = indice + pas
...
...
...
>>> compteur_complet(10, 2, 2)
2
4
6
8
10
>>> compteur_complet(10)
0
1
2
3
4
5
6
7
8
9
10
```

On peut aussi utiliser des arguments nommés pour passer des valeurs aux paramètres en les nommant.

```
>>> compteur_complet(10, pas=2)
0
2
4
6
8
10
>>> compteur_complet(début=2, fin=11, pas=2)
2
4
6
8
10
```

Les valeurs par défaut sont évaluées au moment de la définition de la fonction (ou procédure).

```
>>> valeur = 5
>>> def mafonction(arg=valeur):
...     print(arg)
...
...
>>> valeur = 6
>>> mafonction()
5
```

Lorsque cette valeur par défaut est un objet mutable (qui peut-être modifié), comme une liste, un dictionnaire ou des instances de classes, la valeur par défaut est aussi évaluée une seule fois au moment de sa création. Cette valeur sera alors partagée entre les différents appels de la fonction (ou procédure).

```
>>> def mafonction(valeur, maliste=[]):
...     maliste.append(valeur)
...     return(maliste)
...
>>> mafonction(1)
[1]
>>> mafonction(2)
[1, 2]
>>> mafonction(3)
[1, 2, 3]
```

Si vous souhaitez que cette valeur ne soit pas partagée entre les appels successifs de la fonction.

```
>>> def mafonction(valeur, maliste=None):
...     if not maliste:
...         maliste = []
...     maliste.append(valeur)
...     return(maliste)
...
>>> mafonction(1)
[1]
>>> mafonction(2)
[2]
>>> mafonction(3)
[3]
>>> def mafonction(valeur, maliste=[]):
...     malisteinterne = maliste.copy()
...     malisteinterne.append(valeur)
...     return(malisteinterne)
...
>>> mafonction(1)
[1]
>>> mafonction(2)
[2]
>>> mafonction(3)
[3]
```

### Variables locales ou globales

Lorsqu’une fonction (procédure) est appelée, Python réserve pour elle (dans la mémoire de l’ordinateur) **un espace de noms**. Cet **espace de noms local** à la fonction (procédure) est à distinguer de l’**espace de noms global** où se trouvait les variables du programme principal.

Dans l’espace de noms local, nous aurons des variables qui ne sont accessibles qu’au sein de la fonction (procédure). C’est par exemple le cas des variables **début**, **fin**, **pas** et **indice** dans l’exemple précédent de la fonction `compteur_complet()`. A chaque fois que nous définissons des **variables à l’intérieur du corps d’une fonction (ou procédure)**, ces variables ne sont accessibles qu’à la fonction (procédure) elle-même. On dit que **ces variables sont des variables locales** à la fonction (procédure). Une variable locale peut avoir le même nom qu’une variable de l’espace de noms global mais elle reste néanmoins indépendante. Les contenus des variables locales sont stockés dans l’espace de noms local qui est inaccessible depuis l’extérieur de la fonction (procédure).

Les **variables définies à l’extérieur d’une fonction (procédure) sont des variables globales**. Leur contenu est «visible» de l’intérieur d’une fonction (procédure), mais la fonction (procédure) ne peut pas le modifier.

```
>>> def test():
...     variable_locale = 5
...     print(variable_globale, variable_locale)
...
...
>>> variable_globale = 3
>>> variable_locale = 4
>>> test()
3 5
>>> print(variable_globale, variable_locale)
3 4
```

#### Utilisation d’une variable globale

Vous avez besoin de définir une fonction qui soit capable de modifier une variable globale. Il vous suffira alors d’utiliser l’instruction `global`. Cette instruction permet d’indiquer à l’intérieur de la définition d’une fonction (procédure) quelles sont les variables à traiter globalement.

```
>>> def test():
...     global variable_globale
...     variable_globale = 3
...     variable_locale = 4
...     print(variable, variable_locale, variable_globale)
...
...
>>> variable = 0
>>> variable_globale = 1
>>> variable_locale = 2
>>> test()
0 4 3
>>> print(variable, variable_locale, variable_globale)
0 2 3
```

### Gestion des arguments nommés

Les noms des paramètres affectés avec leurs valeurs passés aux fonctions (ou aux procédures) sont ce que l’on appelle des arguments nommés.

Toutes les valeurs de paramètres positionnés ou tous les arguments nommés doivent correspondre à l’un des arguments acceptés par la fonction.

```
>>> def mafonction(valeur1positionnée, valeur2='Défaut', valeur3positionnée):
  File "<input>", line 1
    def mafonction(valeur1positionnée, valeur2='Défaut', valeur3positionnée):
                                                                           ^
SyntaxError: non-default argument follows default argument
>>> def mafonction(valeur1positionnée, valeur2='Défaut2', valeur3='Défaut3'):
...     print(valeur1positionnée)
...     print(valeur2)
...     print(valeur3)
...
...
>>> mafonction()
Traceback (most recent call last):
  File "<input>", line 1, in <module>
     mafonction()
TypeError: mafonction() missing 1 required positional argument: 'valeur1positionnée'
>>> mafonction('premier paramètre')
premier paramètre
Défaut2
Défaut3
>>> mafonction('premier paramètre', valeur4='quatrième paramètre')
Traceback (most recent call last):
  File "<input>", line 1, in <module>
    mafonction('premier paramètre', valeur4='quatrième paramètre')
TypeError: mafonction() got an unexpected keyword argument 'valeur4'
```

Aucun argument ne peut recevoir une valeur plus d’une fois.

```
>>> def mafonction(valeur1positionnée, valeur2='Défaut2', *valeurs, **paramètres):
...     print(valeur1positionnée)
...     print(valeur2)
...     print(valeurs)
...     print(paramètres)
...
...
>>> mafonction()
Traceback (most recent call last):
  File "<input>", line 1, in <module>
    mafonction()
TypeError: mafonction() missing 1 required positional argument: 'valeur1positionnée'
>>> mafonction('premier paramètre')
premier paramètre
Défaut2
()
{}
>>> mafonction('premier paramètre', valeur4='quatrième paramètre')
premier paramètre
Défaut2
()
{'valeur4': 'quatrième paramètre'}
>>> mafonction('valeur1', 'valeur2', troisième_paramètre='valeur3')
valeur1
valeur2
()
{'troisième_paramètre': 'valeur3'}
>>> mafonction('valeur1', 'test', valeur2='valeur2', troisième_paramètre='valeur3')
Traceback (most recent call last):
  File "<input>", line 1, in <module>
    mafonction('valeur1', 'test', valeur2='valeur2', troisième_paramètre='valeur3')
TypeError: mafonction() got multiple values for argument 'valeur2'
>>> mafonction('valeur1', valeur2='valeur2', troisième_paramètre='valeur3')
valeur1
valeur2
()
{'troisième_paramètre': 'valeur3'}
>>> mafonction('valeur1', 'valeur2', 'valeur3', troisième_paramètre='valeur3')
valeur1
valeur2
('valeur3',)
{'troisième_paramètre': 'valeur3'}
```

### Paramètres spéciaux

Par défaut, les arguments peuvent être passés à une fonction Python par position, ou explicitement par mot-clé (les arguments nommés). On peut récupérer les valeurs et les arguments nommés passés à la fonction (ou à la procédure) avec le préfixe «**\***» et «**\*\***». **Splat** et **double splat** dans une formation plus avancée de Python. Exemple de premier terme : `\*lereste=range(10)`

```
>>> def mafonction(*valeurs, **arguments_et_valeurs):
...     print(valeurs)
...     print(arguments_et_valeurs)
...
...
>>> mafonction()
()
{}
>>> mafonction('valeur1', 'valeur2', 'valeur3')
('valeur1', 'valeur2', 'valeur3')
{}
>>> mafonction(premier_argument='valeur1', deuxième_argument='valeur2', troisième_argument='valeur3')
()
{'premier_argument': 'valeur1', 'deuxième_argument': 'valeur2', 'troisième_argument': 'valeur3'}
>>> mafonction('valeur1', 'valeur2', troisième_argument='valeur3' )
('valeur1', 'valeur2')
{'troisième_argument': 'valeur3'}
```

Pour la lisibilité et la performance, il est logique de restreindre la façon dont les arguments peuvent être transmis afin qu’un développeur n’ait qu’à regarder la définition de la fonction pour déterminer si les éléments sont transmis par position seule, par position ou par mot-clé, ou par mot-clé seul.

```
def fonc(arg_position, /, arg_position_ou_kwd, *, kwd1):
         ──────┬─────     ──────────────┬────     ──┬─
               │                        │           │
               │  Positionnés et nommés ┘           │
               │                   nommés seulement ┘
               └── Positionnés seulement
```

où «**/**» et «**\***» sont facultatifs. S’ils sont utilisés, ces symboles indiquent par quel type de paramètre un argument peut être transmis à la fonction : position seule, position ou mot-clé, et mot-clé seul.

```
>>> def mafonction(valeurpositionnée, /, valeur='Valeur', *, argument='Argument', **paramètres):
...     print(valeurpositionnée)
...     print(valeur)
...     print(argument)
...     print(paramètres)
...
...
>>> mafonction()
Traceback (most recent call last):
  File "<input>", line 1, in <module>
    mafonction()
TypeError: mafonction() missing 1 required positional argument: 'valeurpositionnée'
>>> mafonction('Premier')
Premier
Valeur
Argument
{}
>>> mafonction(valeurpositionnée='Premier')
Traceback (most recent call last):
  File "<input>", line 1, in <module>
    mafonction(valeurpositionnée='Premier')
TypeError: mafonction() missing 1 required positional argument: 'valeurpositionnée'
>>> mafonction('Premier', 'deuxième')
Premier
deuxième
Argument
{}
>>> mafonction('Premier', 'deuxième', 'troisième')
Traceback (most recent call last):
  File "<input>", line 1, in <module>
    mafonction('Premier', 'deuxième', 'troisième')
TypeError: mafonction() takes from 1 to 2 positional arguments but 3 were given
>>> mafonction('Premier', 'deuxième', argument='troisième')
Premier
deuxième
troisième
{}
>>> mafonction('Premier', valeur='deuxième', argument='troisième', bidon='bidon')
Premier
deuxième
troisième
{'bidon': 'bidon'}
```

Si on veut récupérer les paramètres positionnés supplémentaires

```
>>> def mafonction(valeurpositionnée, /, valeur='Valeur', *valeurs, argument='Argument', **paramètres):
...     print(valeurpositionnée)
...     print(valeur)
...     print(valeurs)
...     print(argument)
...     print(paramètres)
...
...
>>> mafonction('Premier')
Premier
Valeur
()
Argument
{}
>>> mafonction('Premier', valeur='deuxième', argument='troisième', bidon='bidon')
Premier
deuxième
()
troisième
{'bidon': 'bidon'}
>>> mafonction('Premier', 'deuxième', 'troisième', argument='quatrième', bidon='bidon')
Premier
deuxième
('troisième',)
quatrième
{'bidon': 'bidon'}
>>> mafonction('Premier', 'deuxième', 'troisième', bidon='bidon')
Premier
deuxième
('troisième',)
Argument
{'bidon': 'bidon'}
```

Voir [https://docs.python.org/fr/3/tutorial/controlflow.html#special-parameters](https://docs.python.org/fr/3/tutorial/controlflow.html#special-parameters) et Voir [https://docs.python.org/fr/3/tutorial/controlflow.html#arbitrary-argument-lists](https://docs.python.org/fr/3/tutorial/controlflow.html#arbitrary-argument-lists)

## Documenter les fonctions

Voici quelques conventions concernant le contenu et le format des chaînes de documentation.

Il convient que la première ligne soit toujours courte et résume de manière concise l’utilité de l’objet. Afin d’être bref, nul besoin de rappeler le nom de l’objet ou son type, qui sont accessibles par d’autres moyens (sauf si le nom est un verbe qui décrit une opération).
La convention veut que la ligne commence par une majuscule et se termine par un point.

```
def ma_fonction():
    """Ne fait rien."""
```

S’il y a d’autres lignes dans la chaîne de documentation, la deuxième ligne devrait être vide, pour la séparer visuellement du reste de la description. Les autres lignes peuvent alors constituer un ou plusieurs paragraphes décrivant le mode d’utilisation de l’objet, ses effets de bord, etc.

```
def ma_fonction():
    """Ne fait rien.

    C'est du texte d'aide seulement.
    """
```

L’analyseur de code Python ne supprime pas l’indentation des chaînes de caractères littérales multi-lignes, donc les outils qui utilisent la documentation doivent si besoin faire cette opération eux-mêmes. La convention suivante s’applique :


* la première ligne non vide après la première détermine la profondeur d’indentation de l’ensemble de la chaîne de documentation (on ne peut pas utiliser la première ligne qui est généralement accolée aux guillemets d’ouverture de la chaîne de caractères et dont l’indentation n’est donc pas visible).


* Les espaces «correspondant» à cette profondeur d’indentation sont alors supprimées du début de chacune des lignes de la chaîne. Aucune ligne ne devrait présenter un niveau d’indentation inférieur mais, si cela arrive, toutes les espaces situées en début de ligne doivent être supprimées. L’équivalent des espaces doit être testé après expansion des tabulations (normalement remplacées par 8 espaces).

Voici un exemple de chaîne de documentation multi-lignes :

```
>>> def ma_fonction():
...     """Ne fait rien, c'est pour la doc.
...
...     Cela ne fait vraiment rien!
...     """
...     pass
...
>>> print(ma_fonction.__doc__)
Ne fait rien, c'est pour la doc.

Cela ne fait vraiment rien!
```

### Annotations de fonctions

Les annotations de fonction sont des **métadonnées optionnelles décrivant les types utilisés par les paramètres** de la fonction (procédure) définie par l’utilisateur (voir les [PEP 3107](https://www.python.org/dev/peps/pep-3107/) et [PEP 484](https://www.python.org/dev/peps/pep-0484/) pour plus d’informations).

Les annotations sont stockées dans l’attribut `__annotations__` de la fonction, sous la forme d’un dictionnaire, et n’ont aucun autre effet.

Les annotations sur les paramètres sont définies par deux points «**:**» après le nom du paramètre suivi d’une expression donnant la valeur de l’annotation.

Les annotations de retour sont définies par «**->**» suivi d’une expression, entre la liste des paramètres et les deux points de fin de l’instruction def.

L’exemple suivant a un paramètre positionnel, un paramètre nommé et la valeur de retour annotés :

```
>>> def f(ham: str, eggs: str='eggs') -> str:
...     print("Annotations:", f.__annotations__)
...     print("Arguments:", ham, eggs)
...     return ham + ' and ' + eggs
...
>>> f('spam')
Annotations: {'ham': <class 'str'>, 'return': <class 'str'>, 'eggs': <class 'str'>}
Arguments: spam eggs
'spam and eggs'
```

## Création de bibliothèques de fonctions

Prenez votre éditeur favori et créez un fichier «**fibo.py**» dans le répertoire courant qui contient :

```
# Fibonacci numbers module
def fib(n): # write Fibonacci series up to n
    a, b = 0, 1
    while a < n:
        print(a, end=' ')
        a, b = b, a+b
    print()
def fib2(n): # return Fibonacci series up to n
    result = []
    a, b = 0, 1
    while a < n:
        result.append(a)
        a, b = b, a+b
    return result
```

Maintenant, ouvrez un interpréteur et importez le module en tapant :

```
>>> import fibo
```

Cela n’importe pas les noms des fonctions définies dans fibo directement dans **la table des symboles courants** mais y ajoute simplement fibo. Vous pouvez donc appeler les fonctions via le nom du module :

```
>>> fibo.fib(1000)
0 1 1 2 3 5 8 13 21 34 55 89 144 233 377 610 987
>>> fibo.fib2(100)
[0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89]
>>> fibo.__name__
'fibo'
```

Si vous avez l’intention d’utiliser souvent une fonction, il est possible de lui assigner un nom local :

```
>>> fib = fibo.fib
>>> fib(500)
0 1 1 2 3 5 8 13 21 34 55 89 144 233 377
```

### Les modules en détail

Un module peut contenir aussi bien des instructions que des déclarations de fonctions. Ces instructions permettent d’initialiser le module. Elles ne sont exécutées que la première fois lorsque le nom d’un module est trouvé dans un `import` (elles sont aussi exécutées lorsque le fichier est exécuté en tant que script).

**Chaque module possède sa propre table de symboles**, utilisée comme table de symboles globaux par toutes les fonctions définies par le module. Ainsi l’auteur d’un module peut utiliser des variables globales dans un module sans se soucier de collisions de noms avec des variables globales définies par l’utilisateur du module. Cependant, si vous savez ce que vous faites, vous pouvez modifier une variable globale d’un module avec la même notation que pour accéder aux fonctions :

```
nommodule.nomelement
```

Des modules peuvent importer d’autres modules. **Il est courant, mais pas obligatoire, de ranger tous les import au début du module (ou du script)**. Les noms des modules importés sont insérés dans la table des symboles globaux du module qui importe.

La variante de l’instruction `import`, `from ... import ...` qui importe les noms d’un module directement dans la table de symboles du module qui l’importe, par exemple :

```
>>> from fibo import fib, fib2
>>> fib(500)
0 1 1 2 3 5 8 13 21 34 55 89 144 233 377
```

N’insère pas le nom du module depuis lequel les définitions sont récupérées dans la table des symboles locaux (dans cet exemple, fibo n’est pas défini).

On peut aussi tout importer d’un module avec :

```
>>> from fibo import *
>>> fib(500)
0 1 1 2 3 5 8 13 21 34 55 89 144 233 377
```

Tous les noms ne commençant pas par un tiret bas «**_**» sont importés. Dans la grande majorité des cas, **les développeurs n’utilisent pas cette syntaxe** puisqu’en important un ensemble indéfini de noms, des noms déjà définis peuvent se retrouver masqués.

Notez qu’en général, `import \*` d’un module ou d’un paquet est déconseillé. Souvent, le code devient difficilement lisible. **Son utilisation en mode interactif est acceptée pour gagner quelques secondes**.

Si le nom du module est suivi par `as`, alors le nom suivant `as` est directement lié au module importé.

```
>>> import fibo as fib
>>> fib.fib(500)
0 1 1 2 3 5 8 13 21 34 55 89 144 233 377
```

Dans les faits, le module est importé de la même manière qu’avec `import fibo`, la seule différence est qu’il sera disponible sous le nom de «**fib**».

C’est aussi valide en utilisant `from`, et a le même effet :

```
>>> from fibo import fib as fibonacci
>>> fibonacci(500)
0 1 1 2 3 5 8 13 21 34 55 89 144 233 377
```

**NOTE**: Pour des raisons de performance, chaque module n’est importé qu’une fois par session. Si vous changez le code d’un module vous devez donc redémarrer l’interpréteur afin d’en voir l’impact ; ou, s’il s’agit simplement d’un seul module que vous voulez tester en mode interactif, vous pouvez le ré-importer explicitement en utilisant `importlib.reload()`, par exemple : `import importlib; importlib.reload(nommodule)`.

### Exécuter des modules comme des scripts

Lorsque vous exécutez un module Python avec `python fibo.py <arguments>`

le code du module est exécuté comme si vous l’aviez importé mais son `__name__` vaut «**__main__** ». Donc, en ajoutant ces lignes à la fin du module :

```
if __name__ == "__main__":
    import sys
    fib(int(sys.argv[1]))
```

vous pouvez rendre le fichier utilisable comme script aussi bien que comme module importable. Car le code qui analyse la ligne de commande n’est lancé que si le module est exécuté comme fichier «**main**» :

```
$ python fibo.py 50
0 1 1 2 3 5 8 13 21 34
```

Si le fichier est importé, le code n’est pas exécuté :

```
>>> import fibo
```

C’est typiquement utilisé soit pour proposer une interface utilisateur pour un module, soit pour lancer les tests sur le module (exécuter le module en tant que script lance les tests).

### Les dossiers de recherche de modules

Lorsqu’un module nommé par exemple spam est importé, **il est d’abord recherché parmi les modules natifs**. Puis, s’il n’est pas trouvé, **l’interpréteur cherche un fichier nommé spam.py** dans une liste de dossiers donnée par la variable `sys.path`.

Par défaut, `sys.path` est initialisée :


* sur le dossier contenant le script courant (ou le dossier courant si aucun script n’est donné) ;


* avec la variable système `PYTHONPATH` (une liste de dossiers, utilisant la même syntaxe que la variable shell PATH) ;


* à la valeur par défaut du répertoire d’installation des modules de Python (par convention le répertoire site-packages où l’on trouve les modules Python)

**NOTE**: Sur les systèmes qui gèrent les liens symboliques, le dossier contenant le script courant est résolu après avoir suivi le lien symbolique du script. Autrement dit, le dossier contenant le lien symbolique n’est pas ajouté aux dossiers de recherche de modules.

Après leur initialisation, les programmes Python peuvent modifier leur `sys.path`. Le dossier contenant le script courant est placé au début de la liste des dossiers à rechercher, avant les dossiers de bibliothèques. Cela signifie qu’un module dans ce dossier, ayant le même nom qu’un module Python, sera chargé à sa place. C’est une erreur typique du débutant, à moins que ce ne soit voulu.

### Modules Python «compilés»

Pour accélérer le chargement des modules, Python met en cache une version compilée de chaque module dans un fichier nommé «**module(version).pyc**». Où «version» représente typiquement une version de Python, donc le format du fichier compilé. Cette compilation est stockée dans le dossier «**__pycache__**». Par exemple, avec la version Python CPython 3.3, la version compilée de **spam.py** serait «**__pycache__/spam.cpython-33.pyc**». Cette règle de nommage permet à des versions compilées d’un code pour des versions différentes de Python de coexister.

Python compare les dates de modification du fichier source et de sa version compilée pour voir si le module doit être recompilé. Ce processus est entièrement automatique. Par ailleurs, les versions compilées sont indépendantes de la plateforme et peuvent donc être partagées entre des systèmes d’architectures différentes.

**Il existe deux situations où Python ne vérifie pas le cache** :


* le premier cas est lorsque le module est donné par la ligne de commande (cas où le module est toujours recompilé, sans même cacher sa version compilée) ;


* le second cas est lorsque le module n’a pas de source. Pour gérer un module sans source (où seule la version compilée est fournie), le module compilé doit se trouver dans le dossier source, et sa source ne doit pas être présente.

### Astuces pour les experts

Vous pouvez utiliser les options «**-O**» ou «**-OO**» lors de l’appel à Python pour réduire la taille des modules compilés. L’option «**-O**» supprime les instructions `assert` et l’option «**-OO**» supprime aussi les documentations rinohtype `__doc__`. Cependant, puisque certains programmes ont besoin de ces `__doc__`, vous ne devriez utiliser «**-OO**» que si vous savez ce que vous faites.

Les modules «optimisés» sont marqués d’un «**opt-**» et sont généralement plus petits. Les versions futures de Python pourraient changer les effets de l’optimisation ;

Un programme ne s’exécute pas plus vite lorsqu’il est lu depuis un .pyc, il est juste chargé plus vite ;

le module `compileall` peut créer des fichiers .pyc pour tous les modules d’un dossier ; vous trouvez plus de détails sur ce processus, ainsi qu’un organigramme des décisions, dans la [PEP 3147](https://www.python.org/dev/peps/pep-3147/).

### Les paquets

Les paquets sont un moyen de structurer les espaces de nommage des modules Python en utilisant une notation «pointée». Par exemple, le nom de module **A.B** désigne le **sous-module B** du **paquet A**. De la même manière que l’utilisation des modules évite aux auteurs de différents modules d’avoir à se soucier des noms de variables globales des autres, l’utilisation des noms de modules avec des points évite aux auteurs de paquets contenant plusieurs modules tel que **NumPy** ou **Pillow** d’avoir à se soucier des noms des modules des autres.

Imaginez que vous voulez construire un ensemble de modules (un «paquet») pour gérer uniformément les fichiers contenant du son et des données sonores. Il existe un grand nombre de formats de fichiers pour stocker du son (généralement identifiés par leur extension, par exemple .wav, .aiff, .au), vous avez donc besoin de créer et maintenir un nombre croissant de modules pour gérer la conversion entre tous ces formats.

Vous voulez aussi pouvoir appliquer un certain nombre d’opérations sur ces sons : mixer, ajouter de l’écho, égaliser, ajouter un effet stéréo artificiel, etc. Donc, en plus des modules de conversion, vous allez écrire une myriade de modules permettant d’effectuer ces opérations.

Voici une structure possible pour votre paquet (exprimée sous la forme d’une arborescence de fichiers) :

```
sound                         Niveau supérieur du package
    ├───__init__.py           Initialize the sound package
    │   formats               Subpackage for file format conversions
    │       └───__init__.py
    │           wavread.py
    │           wavwrite.py
    │           aiffread.py
    │           aiffwrite.py
    │           auread.py
    │           auwrite.py
    │           ...
    ├───effects               Subpackage for sound effects
    │       └───__init__.py
    │           echo.py
    │           surround.py
    │           reverse.py
    │           ...
    └───filters               Subpackage for filters
            └───__init__.py
                equalizer.py
                vocoder.py
                karaoke.py
                ...
```

Lorsqu’il importe des paquets, Python cherche dans chaque dossier de `sys.path` un sous-dossier du nom du paquet.

Les fichiers «**__init__.py**» sont nécessaires pour que Python considère un dossier contenant ce fichier comme un paquet. Cela évite que des dossiers ayant des noms courants comme string ne masquent des modules qui auraient été trouvés plus tard dans la recherche des dossiers. Dans le plus simple des cas, «**__init__.py**» peut être vide, mais il peut aussi exécuter du code d’initialisation pour son paquet ou configurer la variable `__all__`.

Les utilisateurs d’un module peuvent importer ses modules individuellement, par exemple :

```
import sound.effects.echo
```

charge le sous-module `sound.effects.echo`. Il doit alors être référencé par son nom complet.

```
sound.effects.echo.echofilter(input, output, delay=0.7, atten=4)
```

Une autre manière d’importer des sous-modules est :

```
from sound.effects import echo
```

charge aussi le sous-module `echo` et le rend disponible sans avoir à indiquer le préfixe du paquet. Il peut donc être utilisé comme ceci :

```
echo.echofilter(input, output, delay=0.7, atten=4)
```

Une autre méthode consiste à importer la fonction ou la variable désirée directement :

```
from sound.effects.echo import echofilter
```

Le sous-module `echo` est toujours chargé mais ici la fonction `echofilter()` est disponible directement :

```
echofilter(input, output, delay=0.7, atten=4)
```

Notez que lorsque vous utilisez `from package import element`, «element» peut aussi bien être un sous-module, un sous-paquet ou simplement un nom déclaré dans le paquet (une variable, une fonction ou une classe).
L’instruction `import` cherche en premier si «element» est défini dans le paquet ; s’il ne l’est pas, elle cherche à charger un module et, si elle n’en trouve pas, une exception `ImportError` est levée.

Au contraire, en utilisant la syntaxe `import element.souselement.soussouselement`, chaque element sauf le dernier doit être un paquet. Le dernier element peut être un module ou un paquet, **mais ne peut être ni une fonction, ni une classe, ni une variable** définie dans l’élément précédent.

#### Importer un paquet

Qu’arrive-t-il lorsqu’un utilisateur écrit `from sound.effects import \*` ?

Idéalement, on pourrait espérer que Python aille chercher tous les sous-modules du paquet sur le système de fichiers et qu’ils seraient tous importés. Cela pourrait être long, et importer certains sous-modules pourrait avoir des effets secondaires indésirables ou, du moins, désirés seulement lorsque le sous-module est importé explicitement.

La seule solution, pour l’auteur du paquet, est de fournir un index explicite du contenu du paquet. L’instruction `import` utilise la convention suivante : si le fichier «**__init__.py**» du paquet définit une liste nommée `__all__`, cette liste est utilisée comme liste des noms de modules devant être importés lorsque `from package import \*` est utilisé. Il est de la responsabilité de l’auteur du paquet de maintenir cette liste à jour lorsque de nouvelles versions du paquet sont publiées.

Un auteur de paquet peut aussi décider de ne pas autoriser d’importer «**\***» pour son paquet. Par exemple, le fichier «**sound/effects/__init__.py**» peut contenir le code suivant :

```
__all__ = ["echo", "surround", "reverse"]
```

Cela signifie que `from sound.effects import \*` importe les trois sous-modules explicitement désignés du paquet `sound`.

Si `__all__` n’est pas définie, l’instruction `from sound.effects import \*` n’importe pas tous les sous-modules du paquet `sound.effects` dans l’espace de nommage courant, mais s’assure seulement que le paquet `sound.effects` a été importé (c.-à-d. que tout le code du fichier «**__init__.py**» a été exécuté), et importe ensuite les noms définis dans le paquet. Cela inclut tous les noms définis (et sous-modules chargés explicitement) par «**__init__.py**». Sont aussi inclus tous les sous-modules du paquet ayant été chargés explicitement par une instruction `import`.

Typiquement :

```
import sound.effects.echo
import sound.effects.surround
from sound.effects import *
```

Dans cet exemple, les modules `echo` et `surround` sont importés dans l’espace de nommage courant lorsque `from ... import` est exécuté, parce qu’ils sont définis dans le paquet `sound.effects` (cela fonctionne aussi lorsque `__all__` est définie).

Bien que certains modules ont été pensés pour n’exporter que les noms respectant une certaine structure lorsque `import \*` est utilisé, `import \*` reste considéré comme **une mauvaise pratique dans du code** à destination d’un environnement de production.

Rappelez-vous que rien ne vous empêche d’utiliser `from paquet import sous_module_specifique` ! C’est d’ailleurs la manière recommandée, à moins que le module qui fait les importations ait besoin de sous-modules ayant le même nom mais provenant de paquets différents.

#### Références internes dans un paquet

Lorsque les paquets sont organisés en sous-paquets (comme le paquet `sound` par exemple), vous pouvez utiliser des importations absolues pour cibler des paquets voisins. Par exemple, si le module `sound.filters.vocoder` a besoin du module `echo` du paquet `sound.effects`, il peut utiliser `from sound.effects import echo`.

Il est aussi possible d’écrire des importations relatives de la forme `from .module import name`.

Ces importations relatives sont préfixées par des points pour indiquer leur origine (paquet courant ou parent). Depuis le module `surround`, par exemple vous pouvez écrire :

```
from . import echo
from .. import formats
from ..filters import equalizer
```

Notez que les importations relatives se fient au nom du module actuel. Puisque le nom du module principal est toujours `__main__`, les modules utilisés par le module principal d’une application ne peuvent être importés que par des importations absolues.

#### Paquets dans plusieurs dossiers

Les paquets possèdent un attribut supplémentaire, `__path__`, qui est une liste initialisée avant l’exécution du fichier «**__init__.py**», contenant le nom de son dossier dans le système de fichiers. Cette liste peut être modifiée, altérant ainsi les futures recherches de modules et sous-paquets contenus dans le paquet.

Bien que cette fonctionnalité ne soit que rarement utile, elle peut servir à élargir la liste des modules trouvés dans un paquet.
