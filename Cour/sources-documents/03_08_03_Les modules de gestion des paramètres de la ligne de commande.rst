.. role:: python(code)
  :language: python

Les modules de gestion des paramètres de la ligne de commande
=============================================================

Modules dépréciés
-----------------

Python fourni un module **getopt**\ (déprécié depuis Python 3.7) ou **optparse** (déprécié depuis Python 3.2) qui vous aident à analyser les options et les arguments de la ligne de commande. Le module **getopt** fournit deux fonctions et une exception pour activer l'analyse des arguments de ligne de commande.

Supposons que nous voulions passer deux noms de fichiers via la ligne de commande et que nous voulions également donner une option pour vérifier l'utilisation du script. L'utilisation en ligne de commande du script est la suivante :

.. code-block:: console

  test.py -i <fichier_en_entrée> -o <fichier_de_sortie>


.. code-block:: python

  #!/usr/bin/env python3
  # -*- coding: utf-8 -*-

  import sys, getopt

  def main(argv):
      fichierentre = ''
      fichiersortie = ''
      try:
          opts, args = getopt.getopt(argv,"hi:o:",["ifile=","ofile="])
      except getopt.GetoptError:
          print('utilisation : test.py -i <fichier_en_entrée> -o <fichier_de_sortie>')
          sys.exit(2)

      for opt, arg in opts:
          if opt == '-h':
              print('utilisation : test.py -i <fichier_en_entrée> -o <fichier_de_sortie>')
              sys.exit()
          elif opt in ("-i", "--ifile"):
              fichierentre = arg
          elif opt in ("-o", "--ofile"):
              fichiersortie = arg

      print('Le fichier en entré est', fichierentre)
      print('Le fichier en sortie est', fichiersortie)

  if __name__ == "__main__":
      main(sys.argv[1:])

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ test.py -h
  utilisation: test.py -i <fichier_en_entrée> -o <fichier_en_entrée>
  utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ test.py -i BMP -o
  utilisation: test.py -i <fichier_en_entrée> -o <fichier_en_entrée>
  utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ test.py -i input.txt -o output.cvs
  Le fichier en entré est input.txt
  Le fichier en sortie est output.cvs

Argparse
--------

Le `module argparse <https://docs.python.org/fr/3/library/argparse.html#module-argparse>`_ remplace *getopt* et *optparse*. Il facilite l'écriture d'interfaces de ligne de commande conviviales. Le programme définit les arguments dont il a besoin et **argparse** trouvera comment les analyser à partir de **sys.argv**. Le module **argparse** génère également automatiquement des messages d'aide et d'utilisation, et émet des erreurs lorsque les utilisateurs donnent au programme des arguments non valides.

Utilisation du module
^^^^^^^^^^^^^^^^^^^^^

1. Construction du parseur.

  :python:`parser = argparse.ArgumentParser(description = 'ma description')`

  On peut donner une description qui terminera dans l'aide d'usage.

2. Ajout d'un argument de la ligne de commande.

  :python:`parser.add_argument('-foo')`

  :python:`'-foo'` est un argument optionnel (non obligatoire). C'est parce que cela commence par «**-**» ou «**\--**».

  Pour avoir un argument positionnel (obligatoire), saisir :python:`'foo'`.

3. Parcourir les arguments.

  :python:`args = parser.parse_args()`

  Agit automatiquement sur **sys.argv**

On peut alors accéder aux valeurs des arguments en faisant directement : :python:`args.foo`

On peut aussi récupérer les valeurs sous forme de dictionnaire avec : :python:`vars(args)`

On peut explicitement imprimer l'aide avec : :python:`parser.print_help()`

On peut explicitement imprimer l'usage simplifié de la commande par : :python:`parser.print_usage()`


Ajout d'options
^^^^^^^^^^^^^^^

:python:`parser.add_argument('-f', '--foo')`

On peut utiliser l'arguments optionnel simplifié **-f** ou nommé **\--foo** en ligne de commande.

:python:`parser.add_argument('-foo', help='what -foo does', metavar='fValue')`

Nom de l'argument **-foo** dans les messages d'utilisation avec l'option :python:`metavar=` ainsi que l'aide sur l'option avec :python:`help=`.

:python:`parser.add_argument('-foo', dest='fVal')`

La valeur pourra être accédée après parsing avec :python:`args.fVal` plutôt qu'avec :python:`args.foo`.

:python:`parser.add_argument('-foo', required=True)`

L'argument est obligatoire.

:python:`parser.add_argument('-foo', action='store_true')`

L'argument ne prend pas de valeur et renvoi **True** si présent (**False** sinon).

:python:`parser.add_argument('-foo', action='append')`

L'argument renvoi une liste de valeurs avec autant d'éléments que le nombre de fois où l'argument est présent.

Par exemple : **2** valeurs si «**-foo a -foo b**».

:python:`parser.add_argument('-foo', choices=['a', 'b', 'c'])`

L'argument doit prendre l'une des valeurs indiquée.

:python:`parser.add_argument('-foo', default='test')`

Donne une valeur par défaut.

:python:`parser.add_argument('-foo', type=int)`

Indique que l'argument doit être un entier plutôt qu'une chaîne de caractères (défaut).
On peut utiliser **int**, **float**, **str**, **complex**.


Exemples
^^^^^^^^

Fichier **argparse1.py** :

Implémentation minimale.

.. code-block:: python

  #!/usr/bin/env python3
  # -*- coding: utf-8 -*-

  import argparse

  parser = argparse.ArgumentParser()
  parser.parse_args()

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse1.py
  utilisateur@MachineUbuntu:~/repertoire_de_developpement/7\_Modules$ ./argparse1.py --help
  usage: argparse1.py [-h]

  optional arguments:
      -h, --help show this help message and exit
  utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse1.py foo
  usage: argparse1.py [-h]
  argparse1.py: error: unrecognized arguments: foo
  utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse1.py --verbose
  usage: argparse1.py [-h]
  argparse1.py: error: unrecognized arguments: --verbose

Voilà ce qu'il se passe :

1. Exécuter le script sans aucun paramètre a pour effet de ne rien afficher sur la sortie d'erreur. Ce n'est pas très utile.

2. La deuxième commande commence à montrer l'intérêt du module **argparse**. On n'a quasiment rien fait mais on a déjà un beau message d'aide . L'option **\--help** (pas besoin de la préciser), que l'on peut aussi raccourcir en **-h**.

3. Préciser quoi que ce soit d'autre comme argument de la ligne de commande entraîne une erreur.

4. Même si on reçoit aussi un argument optionnel non défini.


Fichier **argparse2.py** :

Comment passer un argument de ligne de commande ?

.. code-block:: python

  #!/usr/bin/env python3
  # -*- coding: utf-8 -*-

  import argparse

  parser = argparse.ArgumentParser()
  parser.add_argument("echo")
  args = parser.parse_args()
  print(args.echo)

On a ajouté la méthode :python:`add_argument()` que l'on utilise pour préciser quels paramètres de lignes de commandes le programme peut accepter. Dans le cas présent, c'est **echo** pour que cela corresponde à sa fonction. Utiliser le programme nécessite maintenant que l'on précise un paramètre.

La méthode :python:`parse_args()` renvoie les données des arguments de la ligne de commande, dans le cas présent : **echo**.

**argparse** affecte automatiquement la variable comme par «magie». C'est à dire que nous n'avons pas besoin de préciser dans quelle variable la valeur est stockée. Vous pouvez remarquer aussi que le nom de variable :python:`args.echo` est le même que l'argument en chaîne de caractères donné à la méthode :python:`add_argument("echo")`.

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse2.py
  usage: argparse2.py [-h] echo
  argparse2.py: error: the following arguments are required: echo
  utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse2.py --help
  usage: argparse2.py [-h] echo

  positional arguments:
   echo

  optional arguments:
      -h, --help show this help message and exit
  utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse2.py monparamètre
  monparamètre

Voilà ce qu'il se passe :

1. Sans arguments la commande renvoie l'aide simplifiée avec le message d'erreur.

2. Nous voyons l'aide du programme avec la commande «**\--help**».

3. Avec le bon argument on affiche la valeur de l'argument saisie.

Notez cependant que, même si l'affichage d'aide paraît bien , il n'est pas aussi utile qu'il pourrait l'être. Par exemple, on peut lire que **echo** est un argument positionnel mais on ne peut pas savoir ce que cela fait autrement qu'en le devinant ou en lisant le code source.


Fichier **argparse3.py** :

Comment afficher une aide plus précise pour un argument de la ligne de commande ?

.. code-block:: python

  #!/usr/bin/env python3
  # -*- coding: utf-8 -*-

  import argparse

  parser = argparse.ArgumentParser()
  parser.add_argument("echo", help="renvoi la valeur du paramètre que vous avez passé")
  args = parser.parse_args()
  print(args.echo)

Nous ajoutons simplement le paramètre :python:`help=""` à la méthode :python:`add_argument()`.

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse3.py -h
  usage: argparse3.py [-h] echo

  positional arguments:
      echo    echo renvoi la valeur du paramètre que vous avez passé

  optional arguments:
      -h, --help show this help message and exit
  utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse3.py monparamètre
  monparamètre
  utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse3.py monparamètre etunautre
  usage: argparse3.py [-h] echo
  argparse3.py: error: argument square: invalid int value: 'etunautre'

1. Nous observons bien que le message d'aide est plus précis.

2. Cela fonctionne avec une valeur atribuée au paramètre positioné «**echo**».

3. Cela ne prend qu'un paramètre.


Fichier **argparse4.py** :

Comment calculer le carré d'un nombre en ligne de commande ?

.. code-block:: python

  #!/usr/bin/env python3
  # -*- coding: utf-8 -*-

  import argparse

  parser = argparse.ArgumentParser()
  parser.add_argument("carré", help="affiche le carré du nombre passé en argument")
  args = parser.parse_args()
  print(args.carré**2)

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse4.py 4
  Traceback (most recent call last):
      File "./argparse4.py", line 8, in <module>
          print(args.carré**2)
      TypeError: unsupported operand type(s) for \*\* or pow(): 'str' and 'int'

Cela n'a pas très bien fonctionné. C'est parce que **argparse** traite les paramètres que l'on donne comme des chaînes de caractères, à moins qu'on ne lui indique de faire autrement.


Fichier **argparse5.py** :

Comment traiter le paramètre d'entrée comme un entier ?

.. code-block:: python

  #!/usr/bin/env python3
  # -*- coding: utf-8 -*-

  import argparse

  parser = argparse.ArgumentParser()
  parser.add_argument("carré", help="affiche le carré du nombre passé en argument", type=int)
  args = parser.parse_args()
  print(args.carré**2)

Nous ajoutons simplement le paramètre :python:`type=int` à la méthode :python:`add_argument()`.

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse5.py 4
  16
  utilisateur@MachineUbuntu:~/repertoire_de_developpement/7\_Modules$ ./argparse5.py quatre
  usage: argparse5.py [-h] carré

  argparse5.py: error: argument carré: invalid int value: 'quatre'

Cela a bien fonctionné. Maintenant le programme va même s'arrêter si l'entrée n'est pas un entier avant de procéder à l'exécution.


Fichier **argparse6.py** :

Comment ajouter un paramètre optionnel ?

.. code-block:: python

  #!/usr/bin/env python3
  # -*- coding: utf-8 -*-

  import** **argparse**

  parser = argparse.ArgumentParser()
  parser.add_argument("--verbosity", help="augmente la verbosité de sortie")
  args = parser.parse_args()

  if args.verbosity :
      print("verbosité activée")

On rajoute «**-**» ou «**\--**» pour montrer que l'argument de ligne de commande est bien optionnel, il n'y aura alors pas d'erreur si on exécute le programme sans celui-ci.

Notez que par défaut, si une option n'est pas utilisée, la variable associée, dans le cas présent :python:`args.verbosity`, prend la valeur :python:`None`. C'est pour cela quelle échoue au test `if <https://docs.python.org/fr/3/reference/compound_stmts.html#if>`_.

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse6.py --verbosity 1
  verbosité activée
  utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse6.py
  utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse6.py --help
  usage: argparse6.py [-h] [--verbosity VERBOSITY]

  optional arguments:
      -h, --help            show this help message and exit
      --verbosity VERBOSITY
                            augmente la verbosité de sortie
  utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse6.py --verbosity
  usage: argparse6.py [-h] [--verbosity VERBOSITY]
  argparse6.py: error: argument --verbosity: expected one argument

1. Validation de la verbosité

2. La commande sans paramètre ne retourne rien et n'est pas en erreur.

3. Le message d'aide est un peu différent quand on utilise l'option **\--verbosity** si on ne précise pas une valeur.

4. Le paramètre optionnel **\--verbosity** demande impérativement une valeur d'attribution.

L'exemple ci-dessus accepte obligatoirement une valeur entière arbitraire pour **\--verbosity**, mais seul l’état (vrai/faux) de présence du paramètre est réellement utile pour notre commande.


Fichier **argparse7.py** :

Comment prendre en compte l'état booléen de présence d'un argument ?

.. code-block:: python

  #!/usr/bin/env python3
  # -*- coding: utf-8 -*-

  import argparse

  parser = argparse.ArgumentParser()
  parser.add_argument("--verbose", help="augmente la verbosité de sortie", action="store_true")
  args = parser.parse_args()

  if args.verbose:
      print("verbosité activée")

Notez que maintenant on précise avec le paramètre :python:`action=` dans :python:`add_argument()` un état booléen. Et on lui donne la valeur **\"store_true\"**. Cela signifie que si l'argument de ligne de commande est précisée, la valeur :python:`True` est assignée à :python:`args.verbose`. Ne rien préciser renvoie la valeur :python:`False`.

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse7.py --verbose
  verbosité activée
  utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse7.py
  utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse7.py --help
  usage: argparse7.py [-h] [--verbose]

  optional arguments:
      -h, --help    show this help message and exit
      --verbose     augmente la verbosité de sortie
  utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse7.py --verbose 1
  usage: argparse7.py [-h] [--verbose]
  argparse7.py: error: unrecognized arguments: 1

1. Maintenant le paramètre est plus une option qu’un paramètre qui nécessite une valeur. On a même changé le nom du paramètre pour qu'il corresponde à cette idée.

2. Pas d'aide retournée.

3. Notez que l'aide est différente avec l'option «**\--verbose**».

4. Dans l'esprit de ce que sont vraiment les options de la ligne de commande, pas des paramètres, quand vous tentez de préciser une valeur de paramètre l'aide simplifiée d'usage et une erreur sont renvoyées.

Si vous êtes familier avec l'utilisation de la ligne de commande, vous avez dû remarquer que nous n'avons pas abordé les raccourcies des paramètres.


Fichier **argparse8.py** :

Comment ajouter un raccourcie de paramètre de la ligne de commande ?

.. code-block:: python

  #!/usr/bin/env python3
  # -*- coding: utf-8 -*-

  import argparse

  parser = argparse.ArgumentParser()
  parser.add_argument("-v", "--verbose", help="augmente la verbosité de sortie", action="store_true")
  args = parser.parse_args()

  if args.verbose:
      print("verbosité activée")

Nous allons simplement en ajouter un au code avec :python:`"-v"` en amont de :python:`"--verbose"` dans les options de :python:`add_argument()`. Sachez que le dernier paramètre saisi est la clé de paramètre, ici c'est :python:`"--verbose"`, les autres en amont sont des raccourcies, ici :python:`"-v"`.

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse8.py -v
  verbosité activée
  utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse8.py --help
  usage: argparse8.py [-h] [-v]

  optional arguments:

      -h, --help    show this help message and exit
      -v, --verbose augmente la verbosité de sortie

Notez que la nouvelle option est aussi indiquée dans l'aide.


Fichier **argparse9.py** :

Comment maintenant ajouter un argument positionné (obligatoire) supplémentaire ?

.. code-block:: python

  #!/usr/bin/env python3
  # -*- coding: utf-8 -*-

  import argparse

  parser = argparse.ArgumentParser()
  parser.add_argument("carré", type=int, help="affiche le carré du nombre passé en argument")
  parser.add_argument("-v", "--verbose", action="store_true", help="augmente la verbosité de sortie")
  args = parser.parse_args()
  reponse = args.carré**2

  if args.verbose:
      print("le carré de {} est égal à {}".format(args.carré, reponse))
  else:
      print(reponse)

Nous avons ajouté un argument positionné de type entier «**carré**» dans le code en ajoutant un autre appel à la méthode :python:`add_argument()`.

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse9.py
  usage: argparse9.py [-h] [-v] carré
  argparse9.py: error: the following arguments are required: carré
  utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse9.py 4
  16
  utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse9.py 4 --verbose
  le carré de 4 est égal à 16
  utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse9.py --verbose 4
  le carré de 4 est égal à 16
  utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse9.py -v 4
  le carré de 4 est égal à 16

1. L'option d'argument positionné apparaît dans l'aide, et on remarque que les argument optionnels sont entre crochets. L'argument positionné n'étant pas saisie l'aide renvoie un message d'erreur.

2. Le calcul de la valeur fonctionne bien

3. Notez que l'ordre importe peu avec les autres commandes passées.


Fichier **argparse10.py** :

Qu'en est-il si nous donnons à ce programme la possibilité d'avoir plusieurs niveaux de verbosité, et que celui-ci les prend en compte ?

.. code-block:: python

  #!/usr/bin/env python3
  # -*- coding: utf-8 -*-

  import argparse

  parser = argparse.ArgumentParser()
  parser.add_argument("carré", type=int, help="affiche le carré du nombre passé en argument")
  parser.add_argument("-v", "--verbosity", type=int, help="augmente la verbosité de sortie")
  args = parser.parse_args()
  reponse = args.carré**2

  if args.verbosity == 2:
      print("le carré de {} est égal à {}".format(args.carré, reponse))
  elif args.verbosity == 1:
      print("{}² = {}".format(args.carré, reponse))
  else:
      print(reponse)

Ajout dans le code de tests de niveau de verbosité.

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$  ./argparse10.py 4
  16
  utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse10.py 4 -v
  usage: argparse10.py [-h] [-v VERBOSITY] carré
  argparse10.py: error: argument -v/--verbosity: expected one argument
  utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse10.py 4 -v 1
  4² = 16
  utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse10.py 4 -v 2
  le carré de 4 est égal à 16
  utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse10.py 4 -v 3
  16

Tout semble bon sauf pour le dernier cas. Notre programme contient un bogue.


Fichier **argparse11.py** :

Comment restreindre les valeurs que **\--verbosity** accepte ?

.. code-block:: python

  #!/usr/bin/env python3
  # -*- coding: utf-8 -*-

  import argparse

  parser = argparse.ArgumentParser()
  parser.add_argument("carré", type=int, help="affiche le carré du nombre passé en argument")
  parser.add_argument("-v", "--verbosity", type=int, choices=[0, 1, 2], help="augmente la verbosité de sortie")
  args = parser.parse_args()
  reponse = args.carré**2

  if args.verbosity == 2:
      print("le carré de {} est égal à {}".format(args.carré, reponse))
  elif args.verbosity == 1:
      print("{}² = {}".format(args.carré, reponse))
  else:
      print(reponse)

On ajoute l'option :python:`choices=[]` à la méthode :python:`add_argument()`.

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse11.py 4 -v 3
  usage: argparse11.py [-h] [-v {0,1,2}] carré
  argparse11.py: error: argument -v/--verbosity: invalid choice: 3 (choose from 0, 1, 2)
  utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse11.py 4 -h
  usage: argparse11.py [-h] [-v {0,1,2}] carré
  positional arguments:
      carré                              affiche le carré du nombre passé en argument
  optional arguments:
      -h, --help                         show this help message and exit
      -v {0,1,2}, --verbosity {0,1,2}    augmente la verbosité de sortie

Notez que ce changement est pris en compte à la fois dans le message d'erreur et dans le texte d'aide.

Essayons maintenant une approche différente pour jouer sur la verbosité. Cela correspond également à comment le programme CPython gère ses propres paramètres de verbosité (jetez un œil sur la sortie de la commande **python \--help**) :


Fichier **argparse12.py** :

Comment compter le nombre fois où un paramètre est saisi ?

.. code-block:: python

  #!/usr/bin/env python3
  # -*- coding: utf-8 -*-

  import argparse

  parser = argparse.ArgumentParser()
  parser.add_argument("carré", type=int, help="affiche le carré du nombre passé en argument")
  parser.add_argument("-v", "--verbosity", action="count", help="augmente la verbosité de sortie")
  args = parser.parse_args()
  reponse = args.carré**2
  if args.verbosity == 2:
      print("le carré de {} est égal à {}".format(args.carré, reponse))
  elif args.verbosity == 1:
      print("{}² = {}".format(args.carré, reponse))
  else:
      print(reponse)

Nous avons introduit une autre action :python:`"count"` à la méthode :python:`add_argument()`, pour compter le nombre d’occurrences d'un argument optionnel en particulier :

Oui, c'est maintenant d'avantage une option (similaire à :python:`action="store_true"`) de la version précédente de notre script. C'est plus logique pour comprendre le message d'erreur.

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse12.py 4
  16
  utilisateur@MachineUbuntu:~/repertoire_de_developpement/7\_Modules$ ./argparse12.py 4 -v
  4² == 16
  utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse12.py 4 -vv
  le carré de 4 est égal à 16
  utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse12.py 4 --verbosity --verbosity
  le carré de 4 est égal à 16
  utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse12.py 4 -v 1
  usage: argparse12.py [-h] [-v] carré
  argparse12.py: error: unrecognized arguments: 1
  utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse12.py 4 -h
  usage: argparse12.py [-h] [-v] carré

  positional arguments:
      carré               affiche le carré du nombre passé en argument

  optional arguments:
      -h, --help show     this help message and exit
      -v, --verbosity     augmente la verbosité de sortie
  utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse12.py 4 -vvv
  16

1. La commande calcule le carré

2. Cela se comporte de la même manière que l'action :python:`"store_true"`.

3. Maintenant voici une démonstration de ce que l'action :python:`"count"` fait. Vous avez sûrement vu ce genre d'utilisation auparavant. Et si vous ne spécifiez pas l'option **-v**, cette option prendra la valeur :python:`None`.

4. Comme on s'y attend, en spécifiant l'option dans sa forme longue, on devrait obtenir la même sortie.

5. Une valeur passé en paramètre génère une erreur.

6. Affiche l'aide normalement

7. La dernière sortie du programme montre que celui-ci contient un bogue.

Malheureusement, notre sortie d'aide n'est pas très informative à propos des nouvelles possibilités de notre programme, mais cela peut toujours être corrigé en améliorant sa documentation (en utilisant l'argument **help**).


Fichier **argparse13.py** :

Comment améliorer la documentation de l'exercice précédent et corriger le bogue ?

.. code-block:: python

  #!/usr/bin/env python3
  # -*- coding: utf-8 -*-

  import argparse

  parser = argparse.ArgumentParser()
  parser.add_argument("carré", type=int, help="affiche le carré du nombre passé en argument")
  parser.add_argument("-v", "--verbosity", action="count", help="augmente la verbosité de sortie")
  args = parser.parse_args()
  reponse = args.carré**2

  # corection: remplacer == avec >=
  if args.verbosity >= 2:
      print("le carré de {} est égal à {}".format(args.carré , reponse))
  elif args.verbosity >= 1:
      print("{}² = {}".format(args.carré , reponse))
  else:
      print(reponse)

Il suffit de changer le test «**==**» par «**>=**».

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse13.py 4 -vvv
  le carré de 4 est égal à 16
  utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse13.py 4 -vvvv
  le carré de 4 est égal à 16
  utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse13.py 4
  Traceback (most recent call last):
      File "argparse13.py", line 12, in <module>
          if args.verbosity >= 2:
  TypeError: '>=' not supported between instances of 'NoneType' and 'int'

Les premières exécutions du programme sont correctes, et le bogue que nous avons eu précédemment est corrigé.

La troisième sortie du programme est un autre bogue introduit par la modification.

Nous voulons que pour n'importe quelle valeur :python:`>= 2` le programme soit verbeux tout en calculant le carré sans ce paramètre.


Fichier **argparse14.py** :

Comment corriger le nouveau bogue du code de l'exemple précédent pour avoir la sortie du carré ?

.. code-block:: python

  #!/usr/bin/env python3
  # -*- coding: utf-8 -*-

  import argparse

  parser = argparse.ArgumentParser()
  parser.add_argument("carré", type=int, help="affiche le carré du nombre passé en argument")
  parser.add_argument("-v", "--verbosity", action="count", default=0, help="augmente la verbosité de sortie")
  args = parser.parse_args()
  reponse = args.carré**2

  # corection: remplacer == avec >=
  if args.verbosity >= 2:
      print("le carré de {} est égal à {}".format(args.carré , reponse))
  elif args.verbosity >= 1:
      print("{}² = {}".format(args.carré , reponse))
  else:
      print(reponse)

Nous introduisons une nouvelle option :python:`default=` dans la méthode :python:`add_argument()`. Nous la définisons à l'entier **0** pour la rendre compatible avec les autres valeurs entières de l'option :python:`count=`. Rappelez-vous que par défaut, si un argument optionnel n'est pas spécifié, il sera définit à :python:`None` une valeur booléenne, et ne pourra donc pas être comparé à une valeur de type entier. Une erreur `TypeError <https://docs.python.org/fr/3/library/exceptions.html#TypeError>`_ sera alors levée.

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse14.py 4
  16


Fichier **argparse15.py** :

Qu'en est-il si nous souhaitons étendre notre mini programme pour le rendre capable de calculer d'autres puissances, et pas seulement des carrés?

.. code-block:: python

  #!/usr/bin/env python3
  # -*- coding: utf-8 -*-

  import argparse

  parser = argparse.ArgumentParser()
  parser.add_argument("x", type=int, help="la base")
  parser.add_argument("y", type=int, help="l’exposant")
  parser.add_argument("-v", "--verbosity", action="count", default=0)
  args = parser.parse_args()
  reponse = args.x**args.y

  if args.verbosity >= 2:
      print("{} à la puissance {} est égal à {}".format(args.x, args.y, reponse))
  elif args.verbosity >= 1:
      print("{}^{} = {}".format(args.x, args.y, reponse))
  else:
      print(reponse)

Nous modifions les arguments de saisies et l'opération de calcul.

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse15.py
  usage: argparse15.py [-h] [-v] x y
  argparse15.py: error: the following arguments are required: x, y
  utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse15.py -h
  usage: argparse15.py [-h] [-v] x y

  positional arguments:
      x          la base
      y          l’exposant

  optional arguments:
      -h, --help show this help message and exit
      -v, --verbosity
  utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse15.py 4 2 -v
  4^2 = 16
  utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse15.py 4 2 -vv
  4 à la puissance 2 est égal à 16

Il est à noter que jusqu'à présent nous avons utilisé le niveau de verbosité pour **changer** le texte qui est affiché.


Fichier **argparse16.py** :

Comment utiliser le principe du niveau de verbosité pour **changer** de sens de texte ?

.. code-block:: python

  #!/usr/bin/env python3
  # -*- coding: utf-8 -*-

  import argparse

  parser = argparse.ArgumentParser()
  parser.add_argument("x", type=int, help="la base")
  parser.add_argument("y", type=int, help="l’exposant")
  parser.add_argument("-v", "--verbosity", action="count", default=0)
  args = parser.parse_args()
  reponse = args.x**args.y

  if args.verbosity >= 2:
      print("Exécution de '{}'".format(__file__))
  if args.verbosity >= 1:
      print("{}^{} = ".format(args.x, args.y), end="")
  print(reponse)

Modifions le texte affiché par les options :python:`args.verbosity` pour afficher la commande exécutée.

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse16.py 4 2
  16
  utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse16.py 4 2 -v
  4^2 = 16
  utilisateur@MachineUbuntu:~/repertoire_de_developpement/7\_Modules$ ./argparse16.py 4 2 -vv
  Exécution de './argparse16.py'
  4^2 = 16


Jusque là, nous avons travaillé avec deux méthodes :python:`parse_args()` et :python:`add_argument()` d'une instance de :python:`argparse.ArgumentParser`.

Voyons maintenant l'utilisation de la méthode :python:`add_mutually_exclusive_group()`. Cette méthode nous permet de spécifier des paramètres qui sont en conflit entre eux.


Fichier **argparse17.py** :

Comment utiliser :python:`add_mutually_exclusive_group()` ?

.. code-block:: python

  #!/usr/bin/env python3
  # -*- coding: utf-8 -*-

  import argparse

  parser = argparse.ArgumentParser()
  group = parser.add_mutually_exclusive_group()
  group.add_argument("-v", "--verbose", action="store_true")
  group.add_argument("-q", "--quiet", action="store_true")
  parser.add_argument("x", type=int, help="la base")
  parser.add_argument("y", type=int, help="l’exposant")
  args = parser.parse_args()
  reponse = args.x**args.y

  if args.quiet:
      print(reponse)
  elif args.verbose:
      print("{} à la puissance {} est égal à {}".format(args.x, args.y, reponse))
  else:
      print("{}^{} = {}".format(args.x, args.y, reponse))

Changeons aussi le reste du programme de telle sorte que la nouvelle fonctionnalité fasse sens. Nous allons introduire l'option **\--quiet**, qui va avoir l'effet opposé de l'option **\--verbose** :

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse17.py 4 2
  4^2 == 16
  utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse17.py 4 2 -q
  16
  utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse17.py 4 2 -v
  4 à la puissance 2 est égal à 16
  utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse17.py 4 2 -vq
  usage: argparse17.py [-h] [-v | -q] x y
  argparse17.py: error: argument -q/--quiet: not allowed with argument -v/--verbose
  utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse17.py 4 2 -v --quiet
  usage: argparse17.py [-h] [-v | -q] x y
  test.py: error: argument -q/--quiet: not allowed with argument -v/--verbose


Avant d'en finir, vous voudrez certainement dire à vos utilisateurs de votre outil de ligne de commande quel est le but principal du programme. Juste dans le cas ou ils ne le sauraient pas :-)


Fichier **argparse18.py** :

Comment modifier l'aide d'un programme en ligne de commande avec un titre général ?

.. code-block:: python

  #!/usr/bin/env python3
  # -*- coding: utf-8 -*-

  import argparse

  parser = argparse.ArgumentParser(description="calcule X à la puissance Y")
  group = parser.add_mutually_exclusive_group()
  group.add_argument("-v", "--verbose", action="store_true")
  group.add_argument("-q", "--quiet", action="store_true")
  parser.add_argument("x", type=int, help="la base")
  parser.add_argument("y", type=int, help="l’exposant")
  args = parser.parse_args()
  reponse = args.x**args.y

  if args.quiet:
      print(reponse)
  elif args.verbose:
      print("{} à la puissance {} est égal à {}".format(args.x, args.y, reponse))
  else:
      print("{}^{} = {}".format(args.x, args.y, reponse))

On ajoute l'option :python:`description=` lors de la création de l'objet :python:`argparse.ArgumentParser()`

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse18.py --help
  usage: argparse18.py [-h] [-v \| -q] x y

  calcule X à la puissance Y

  positional arguments:
      x                 la base
      y                 l’exposant

  optional arguments:
      -h, --help show   this help message and exit
      -v, --verbose
      -q, --quiet


C’est bien jolie tout cela mais on mélange de l’anglais avec du Français.


Fichier **argparse19.py** :

Comment faire pour traduire les messages d'aide en Français ?

.. only:: latex

  .. code-block:: python

    #!/usr/bin/env python3
    # -*- coding: utf-8 -*-

    import gettext

    __TRANSLATIONS = {
        'ambiguous option: %(option)s could match %(matches)s': 'option ambiguë: %(option)s parmi %(matches)s', 'argument "-" with mode %r': 'argument "-" en mode %r', 'cannot merge actions - two groups are named %r': 'cannot merge actions - two groups are named %r', "can't open '%(filename)s': %(error)s": "can't open '%(filename)s': %(error)s", 'dest= is required for options like %r': 'dest= is required for options like %r', 'expected at least one argument': 'au moins un argument est attendu', 'expected at most one argument': 'au plus un argument est attendu', 'expected one argument': 'un argument est nécessaire', 'ignored explicit argument %r': 'ignored explicit argument %r', 'invalid choice: %(value)r (choose from %(choices)s)': 'choix invalide: %(value)r (parmi %(choices)s)', 'invalid conflict_resolution value: %r': 'invalid conflict_resolution value: %r', 'invalid option string %(option)r: must start with a character %(prefix_chars)r': 'invalid option string %(option)r: must start with a character %(prefix_chars)r', 'invalid %(type)s value: %(value)r': 'valeur invalide de type %(type)s: %(value)r', 'mutually exclusive arguments must be optional': 'mutually exclusive arguments must be optional', 'not allowed with argument %s': "pas permis avec l'argument %s", 'one of the arguments %s is required': 'au moins un argument requis parmi %s', 'optional arguments': 'arguments optionnels', 'positional arguments': 'arguments positionnels', "'required' is an invalid argument for positionals": "'required' is an invalid argument for positionals", 'show this help message and exit': 'afficher ce message d\’aide',
        'unrecognized arguments: %s': 'argument non reconnu: %s', 'unknown parser %(parser_name)r (choices: %(choices)s)': 'unknown parser %(parser_name)r (choices: %(choices)s)', 'usage: ': 'utilisation: ', '%(prog)s: error: %(message)s\n': '%(prog)s: erreur: %(message)s\n', '%r is not callable': '%r is not callable', }

    gettext.gettext = lambda text: __TRANSLATIONS[text] or text

    import argparse

    parser = argparse.ArgumentParser(description="calcule X à la puissance Y")
    group = parser.add_mutually_exclusive_group()
    group.add_argument("-v", "--verbose", action="store_true")
    group.add_argument("-q", "--quiet", action="store_true")
    parser.add_argument("x", type=int, help="la base")
    parser.add_argument("y", type=int, help="l’exposant")
    args = parser.parse_args()
    reponse = args.x**args.y

    if args.quiet:
        print(reponse)
    elif args.verbose:
        print("{} à la puissance {} est égal à {}".format(args.x, args.y, reponse))
    else:
        print("{}^{} = {}".format(args.x, args.y, reponse))

.. only:: not latex

  .. code-block:: python

    #!/usr/bin/env python3
    # -*- coding: utf-8 -*-

    import gettext

    __TRANSLATIONS = {
        'ambiguous option: %(option)s could match %(matches)s': 'option ambiguë: %(option)s parmi %(matches)s', 'argument "-" with mode %r': 'argument "-" en mode %r', 'cannot merge actions - two groups are named %r': 'cannot merge actions - two groups are named %r', "can't open '%(filename)s': %(error)s": "can't open '%(filename)s': %(error)s", 'dest= is required for options like %r': 'dest= is required for options like %r', 'expected at least one argument': 'au moins un argument est attendu', 'expected at most one argument': 'au plus un argument est attendu', 'expected one argument': 'un argument est nécessaire', 'ignored explicit argument %r': 'ignored explicit argument %r', 'invalid choice: %(value)r (choose from %(choices)s)': 'choix invalide: %(value)r (parmi %(choices)s)', 'invalid conflict_resolution value: %r': 'invalid conflict_resolution value: %r', 'invalid option string %(option)r: must start with a character %(prefix_chars)r': 'invalid option string %(option)r: must start with a character %(prefix_chars)r', 'invalid %(type)s value: %(value)r': 'valeur invalide de type %(type)s: %(value)r', 'mutually exclusive arguments must be optional': 'mutually exclusive arguments must be optional', 'not allowed with argument %s': "pas permis avec l'argument %s", 'one of the arguments %s is required': 'au moins un argument requis parmi %s', 'optional arguments': 'arguments optionnels', 'positional arguments': 'arguments positionnels', "'required' is an invalid argument for positionals": "'required' is an invalid argument for positionals", 'show this help message and exit': 'afficher ce message d\’aide', 'unrecognized arguments: %s': 'argument non reconnu: %s', 'unknown parser %(parser_name)r (choices: %(choices)s)': 'unknown parser %(parser_name)r (choices: %(choices)s)', 'usage: ': 'utilisation: ', '%(prog)s: error: %(message)s\n': '%(prog)s: erreur: %(message)s\n', '%r is not callable': '%r is not callable', }

    gettext.gettext = lambda text: __TRANSLATIONS[text] or text

    import argparse

    parser = argparse.ArgumentParser(description="calcule X à la puissance Y")
    group = parser.add_mutually_exclusive_group()
    group.add_argument("-v", "--verbose", action="store_true")
    group.add_argument("-q", "--quiet", action="store_true")
    parser.add_argument("x", type=int, help="la base")
    parser.add_argument("y", type=int, help="l’exposant")
    args = parser.parse_args()
    reponse = args.x**args.y

    if args.quiet:
        print(reponse)
    elif args.verbose:
        print("{} à la puissance {} est égal à {}".format(args.x, args.y, reponse))
    else:
        print("{}^{} = {}".format(args.x, args.y, reponse))

La traduction se fait avec gettext.

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./argparse19.py --help
  utilisation: argparse19.py [-h] [-v | -q] x y

  calcule X à la puissance Y

  arguments positionnels:
      x             la base
      y             l’exposant

  arguments optionnels:
      -h, --help    affiche ce message d’aide
      -v, --verbose
      -q, --quiet
  utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ cd ..
