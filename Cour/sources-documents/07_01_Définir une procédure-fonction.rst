.. role:: python(code)
  :language: python

.. role:: terminal(code)
  :language: console

Définir une procédure/fonction
******************************

La syntaxe Python pour la définition d’une fonction est la suivante :

.. code-block:: python

  def nom_fonction(liste de paramètres):
      """ bloc d'instructions """

Vous pouvez choisir n’importe quel nom pour la fonction que vous créez, **à l’exception des mots-clés réservés du langage, et à la condition de n’utiliser aucun caractère spécial ou accentué** (le caractère souligné «**\_**» est permis). Comme c’est le cas pour les noms de variables, **on utilise par convention des minuscules**, notamment au début du nom (**les noms commençant par une majuscule seront réservés aux classes**).

Corps de la procédure/fonction
==============================

Comme les instructions :python:`if`, :python:`for` et :python:`while`, l’instruction `def <https://courspython.com/fonctions.html#def>`_ est une instruction composée. La ligne contenant cette instruction se termine obligatoirement par un deux-points «**:**», qui introduisent un bloc d’instructions qui est précisé grâce à l’indentation. Ce bloc d’instructions constitue le **corps de la fonction**.

Procédure sans paramètres
=========================

S’il n’y a pas de valeur retournée nous avons à faire à une procédure.

.. code-block:: pycon

  >>> def message():
  ...     print('Bonjour tout le monde')
  ...
  ...
  >>> message()
  Bonjour tout le monde

Fonction sans paramètres
========================

Pour retourner une valeur, afin d’avoir une fonction, il faut utiliser le paramètre **return**.

.. code-block:: pycon

  >>> def mafonction():
  ...     montexte = 'Bonjour tout le monde'
  ...     return montexte
  ...
  >>> print(mafonction())
  'Bonjour tout le monde'

Pour retourner plusieurs paramètres il suffit de les séparer avec «**,**».

.. code-block:: pycon

  >>> def mafonction():
  ...     monprenier_paramètre = 'premier paramètre'
  ...     mondeuxième_paramètre = 'deuxième paramètre'
  ...     montroisième_paramètre = 'troisième paramètre'
  ...     return monprenier_paramètre, mondeuxième_paramètre, montroisième_paramètre
  ...
  >>> mafonction()
  ('premier paramètre', 'deuxième paramètre', 'troisième paramètre')

Paramètres d'une fonction/procédure
===================================

Utilisation d’une variable comme paramètre
------------------------------------------

Passer une paramètre obligatoire à une fonction (ou procédure) s'appelle un **argument positionné**. Il suffit de mettre son nom en argument dans la fonction lors de sa déclaration :python:`def mafonctionouprocédure(monparamètre):`.

Exemple :

.. code-block:: pycon

  >>> def compteur(fin):
  ...     début = 0
  ...     indice = début
  ...     while indice < fin:
  ...         print(indice)
  ...         indice = indice + 1
  ...
  ...
  ...
  >>> compteur(2)
  0
  1
  >>> compteur(5)
  0
  1
  2
  3
  4

Plusieurs paramètres
--------------------

Pour passer plusieurs arguments positionnés il faut les séparer avec «**,**».

.. code-block:: pycon

  >>> def compteur_complet(début, fin, pas):
  ...     indice = début
  ...     while indice < fin:
  ...         print(indice)
  ...         indice = indice + pas
  ...
  ...
  ...
  >>> compteur_complet(2, 10, 2)
  2
  4
  6
  8

Valeurs par défaut des paramètres
---------------------------------

Pour certains paramètres, la forme la plus utile consiste à indiquer **une valeur par défaut**. C’est ce que l’on appelle **des arguments nommés**.

Les arguments nommés sont sous la forme :python:`kwarg=valeur`. Le paramètre **peut alors devenir optionnel**, et la fonction (ou procédure) peut être appelée avec moins de paramètres.

Dans un appel de fonction (ou de procédure), les **arguments nommés doivent suivre les arguments positionnés**.

.. code-block:: pycon

  >>> def compteur_complet(fin, début=0, pas=1):
  ...     indice = début
  ...     while indice <= fin:
  ...         print(indice)
  ...         indice = indice + pas
  ...
  ...
  ...
  >>> compteur_complet(10, 2, 2)
  2
  4
  6
  8
  10
  >>> compteur_complet(10)
  0
  1
  2
  3
  4
  5
  6
  7
  8
  9
  10

On peut aussi utiliser des arguments nommés pour passer des valeurs aux paramètres en les nommant.

.. code-block:: pycon

  >>> compteur_complet(10, pas=2)
  0
  2
  4
  6
  8
  10
  >>> compteur_complet(début=2, fin=11, pas=2)
  2
  4
  6
  8
  10

Les valeurs par défaut sont évaluées au moment de la définition de la fonction (ou procédure).

.. code-block:: pycon

  >>> valeur = 5
  >>> def mafonction(arg=valeur):
  ...     print(arg)
  ...
  ...
  >>> valeur = 6
  >>> mafonction()
  5

Lorsque cette valeur par défaut est un objet mutable (qui peut-être modifié), comme une liste, un dictionnaire ou des instances de classes, la valeur par défaut est aussi évaluée une seule fois au moment de sa création. Cette valeur sera alors partagée entre les différents appels de la fonction (ou procédure).

.. code-block:: pycon

  >>> def mafonction(valeur, maliste=[]):
  ...     maliste.append(valeur)
  ...     return(maliste)
  ...
  >>> mafonction(1)
  [1]
  >>> mafonction(2)
  [1, 2]
  >>> mafonction(3)
  [1, 2, 3]

Si vous souhaitez que cette valeur ne soit pas partagée entre les appels successifs de la fonction.

.. code-block:: pycon

  >>> def mafonction(valeur, maliste=None):
  ...     if not maliste:
  ...         maliste = []
  ...     maliste.append(valeur)
  ...     return(maliste)
  ...
  >>> mafonction(1)
  [1]
  >>> mafonction(2)
  [2]
  >>> mafonction(3)
  [3]
  >>> def mafonction(valeur, maliste=[]):
  ...     malisteinterne = maliste.copy()
  ...     malisteinterne.append(valeur)
  ...     return(malisteinterne)
  ...
  >>> mafonction(1)
  [1]
  >>> mafonction(2)
  [2]
  >>> mafonction(3)
  [3]

Variables locales ou globales
=============================

Lorsqu’une fonction (procédure) est appelée, Python réserve pour elle (dans la mémoire de l’ordinateur) **un espace de noms**. Cet **espace de noms local** à la fonction (procédure) est à distinguer de l’**espace de noms global** où se trouvait les variables du programme principal.

Dans l’espace de noms local, nous aurons des variables qui ne sont accessibles qu’au sein de la fonction (procédure). C’est par exemple le cas des variables **début**, **fin**, **pas** et **indice** dans l’exemple précédent de la fonction :python:`compteur_complet()`. A chaque fois que nous définissons des **variables à l’intérieur du corps d’une fonction (ou procédure)**, ces variables ne sont accessibles qu’à la fonction (procédure) elle-même. On dit que **ces variables sont des variables locales** à la fonction (procédure). Une variable locale peut avoir le même nom qu’une variable de l’espace de noms global mais elle reste néanmoins indépendante. Les contenus des variables locales sont stockés dans l’espace de noms local qui est inaccessible depuis l’extérieur de la fonction (procédure).

Les **variables définies à l’extérieur d’une fonction (procédure) sont des variables globales**. Leur contenu est «visible» de l’intérieur d’une fonction (procédure), mais la fonction (procédure) ne peut pas le modifier.

.. code-block:: pycon

  >>> def test():
  ...     variable_locale = 5
  ...     print(variable_globale, variable_locale)
  ...
  ...
  >>> variable_globale = 3
  >>> variable_locale = 4
  >>> test()
  3 5
  >>> print(variable_globale, variable_locale)
  3 4

Utilisation d’une variable globale
----------------------------------

Vous avez besoin de définir une fonction qui soit capable de modifier une variable globale. Il vous suffira alors d’utiliser l’instruction :python:`global`. Cette instruction permet d’indiquer à l’intérieur de la définition d’une fonction (procédure) quelles sont les variables à traiter globalement.

.. code-block:: pycon

  >>> def test():
  ...     global variable_globale
  ...     variable_globale = 3
  ...     variable_locale = 4
  ...     print(variable, variable_locale, variable_globale)
  ...
  ...
  >>> variable = 0
  >>> variable_globale = 1
  >>> variable_locale = 2
  >>> test()
  0 4 3
  >>> print(variable, variable_locale, variable_globale)
  0 2 3

Gestion des arguments nommés
============================

Les noms des paramètres affectés avec leurs valeurs passés aux fonctions (ou aux procédures) sont ce que l’on appelle des arguments nommés.

Toutes les valeurs de paramètres positionnés ou tous les arguments nommés doivent correspondre à l’un des arguments acceptés par la fonction.

.. only:: latex

  .. code-block:: pycon

    >>> def mafonction(valeur1positionnée, valeur2='Défaut', valeur3positionnée):
      File "<input>", line 1
        def mafonction(valeur1positionnée, valeur2='Défaut', valeur3positionnée):
                       ^
    SyntaxError: non-default argument follows default argument
    >>> def mafonction(valeur1positionnée, valeur2='Défaut2', valeur3='Défaut3'):
    ...     print(valeur1positionnée)
    ...     print(valeur2)
    ...     print(valeur3)
    ...
    ...
    >>> mafonction()
    Traceback (most recent call last):
      File "<input>", line 1, in <module>
         mafonction()
    TypeError: mafonction() missing 1 required positional argument: 'valeur1positionnée'
    >>> mafonction('premier paramètre')
    premier paramètre
    Défaut2
    Défaut3
    >>> mafonction('premier paramètre', valeur4='quatrième paramètre')
    Traceback (most recent call last):
      File "<input>", line 1, in <module>
        mafonction('premier paramètre', valeur4='quatrième paramètre')
    TypeError: mafonction() got an unexpected keyword argument 'valeur4'

.. only::  not latex

  .. code-block:: pycon

    >>> def mafonction(valeur1positionnée, valeur2='Défaut', valeur3positionnée):
      File "<input>", line 1
        def mafonction(valeur1positionnée, valeur2='Défaut', valeur3positionnée):
                                                                               ^
    SyntaxError: non-default argument follows default argument
    >>> def mafonction(valeur1positionnée, valeur2='Défaut2', valeur3='Défaut3'):
    ...     print(valeur1positionnée)
    ...     print(valeur2)
    ...     print(valeur3)
    ...
    ...
    >>> mafonction()
    Traceback (most recent call last):
      File "<input>", line 1, in <module>
         mafonction()
    TypeError: mafonction() missing 1 required positional argument: 'valeur1positionnée'
    >>> mafonction('premier paramètre')
    premier paramètre
    Défaut2
    Défaut3
    >>> mafonction('premier paramètre', valeur4='quatrième paramètre')
    Traceback (most recent call last):
      File "<input>", line 1, in <module>
        mafonction('premier paramètre', valeur4='quatrième paramètre')
    TypeError: mafonction() got an unexpected keyword argument 'valeur4'

Aucun argument ne peut recevoir une valeur plus d’une fois.

.. code-block:: pycon

  >>> def mafonction(valeur1positionnée, valeur2='Défaut2', *valeurs, **paramètres):
  ...     print(valeur1positionnée)
  ...     print(valeur2)
  ...     print(valeurs)
  ...     print(paramètres)
  ...
  ...
  >>> mafonction()
  Traceback (most recent call last):
    File "<input>", line 1, in <module>
      mafonction()
  TypeError: mafonction() missing 1 required positional argument: 'valeur1positionnée'
  >>> mafonction('premier paramètre')
  premier paramètre
  Défaut2
  ()
  {}
  >>> mafonction('premier paramètre', valeur4='quatrième paramètre')
  premier paramètre
  Défaut2
  ()
  {'valeur4': 'quatrième paramètre'}
  >>> mafonction('valeur1', 'valeur2', troisième_paramètre='valeur3')
  valeur1
  valeur2
  ()
  {'troisième_paramètre': 'valeur3'}
  >>> mafonction('valeur1', 'test', valeur2='valeur2', troisième_paramètre='valeur3')
  Traceback (most recent call last):
    File "<input>", line 1, in <module>
      mafonction('valeur1', 'test', valeur2='valeur2', troisième_paramètre='valeur3')
  TypeError: mafonction() got multiple values for argument 'valeur2'
  >>> mafonction('valeur1', valeur2='valeur2', troisième_paramètre='valeur3')
  valeur1
  valeur2
  ()
  {'troisième_paramètre': 'valeur3'}
  >>> mafonction('valeur1', 'valeur2', 'valeur3', troisième_paramètre='valeur3')
  valeur1
  valeur2
  ('valeur3',)
  {'troisième_paramètre': 'valeur3'}

Paramètres spéciaux
===================

Par défaut, les arguments peuvent être passés à une fonction Python par position, ou explicitement par mot-clé (les arguments nommés). On peut récupérer les valeurs et les arguments nommés passés à la fonction (ou à la procédure) avec le préfixe «**\***» et «**\*\***». **Splat** et **double splat** dans une formation plus avancée de Python. Exemple de premier terme : :python:`*lereste=range(10)`

.. code-block:: pycon

  >>> def mafonction(*valeurs, **arguments_et_valeurs):
  ...     print(valeurs)
  ...     print(arguments_et_valeurs)
  ...
  ...
  >>> mafonction()
  ()
  {}
  >>> mafonction('valeur1', 'valeur2', 'valeur3')
  ('valeur1', 'valeur2', 'valeur3')
  {}
  >>> mafonction(premier_argument='valeur1', deuxième_argument='valeur2', troisième_argument='valeur3')
  ()
  {'premier_argument': 'valeur1', 'deuxième_argument': 'valeur2', 'troisième_argument': 'valeur3'}
  >>> mafonction('valeur1', 'valeur2', troisième_argument='valeur3' )
  ('valeur1', 'valeur2')
  {'troisième_argument': 'valeur3'}

Pour la lisibilité et la performance, il est logique de restreindre la façon dont les arguments peuvent être transmis afin qu'un développeur n'ait qu'à regarder la définition de la fonction pour déterminer si les éléments sont transmis par position seule, par position ou par mot-clé, ou par mot-clé seul.

.. code-block:: text

  def fonc(arg_position, /, arg_position_ou_kwd, *, kwd1):
           ──────┬─────     ──────────────┬────     ──┬─
                 │                        │           │
                 │  Positionnés et nommés ┘           │
                 │                   nommés seulement ┘
                 └── Positionnés seulement

où «**/**» et «**\***» sont facultatifs. S'ils sont utilisés, ces symboles indiquent par quel type de paramètre un argument peut être transmis à la fonction : position seule, position ou mot-clé, et mot-clé seul.

.. code-block:: pycon

  >>> def mafonction(valeurpositionnée, /, valeur='Valeur', *, argument='Argument', **paramètres):
  ...     print(valeurpositionnée)
  ...     print(valeur)
  ...     print(argument)
  ...     print(paramètres)
  ...
  ...
  >>> mafonction()
  Traceback (most recent call last):
    File "<input>", line 1, in <module>
      mafonction()
  TypeError: mafonction() missing 1 required positional argument: 'valeurpositionnée'
  >>> mafonction('Premier')
  Premier
  Valeur
  Argument
  {}
  >>> mafonction(valeurpositionnée='Premier')
  Traceback (most recent call last):
    File "<input>", line 1, in <module>
      mafonction(valeurpositionnée='Premier')
  TypeError: mafonction() missing 1 required positional argument: 'valeurpositionnée'
  >>> mafonction('Premier', 'deuxième')
  Premier
  deuxième
  Argument
  {}
  >>> mafonction('Premier', 'deuxième', 'troisième')
  Traceback (most recent call last):
    File "<input>", line 1, in <module>
      mafonction('Premier', 'deuxième', 'troisième')
  TypeError: mafonction() takes from 1 to 2 positional arguments but 3 were given
  >>> mafonction('Premier', 'deuxième', argument='troisième')
  Premier
  deuxième
  troisième
  {}
  >>> mafonction('Premier', valeur='deuxième', argument='troisième', bidon='bidon')
  Premier
  deuxième
  troisième
  {'bidon': 'bidon'}

Si on veut récupérer les paramètres positionnés supplémentaires

.. code-block:: pycon

  >>> def mafonction(valeurpositionnée, /, valeur='Valeur', *valeurs, argument='Argument', **paramètres):
  ...     print(valeurpositionnée)
  ...     print(valeur)
  ...     print(valeurs)
  ...     print(argument)
  ...     print(paramètres)
  ...
  ...
  >>> mafonction('Premier')
  Premier
  Valeur
  ()
  Argument
  {}
  >>> mafonction('Premier', valeur='deuxième', argument='troisième', bidon='bidon')
  Premier
  deuxième
  ()
  troisième
  {'bidon': 'bidon'}
  >>> mafonction('Premier', 'deuxième', 'troisième', argument='quatrième', bidon='bidon')
  Premier
  deuxième
  ('troisième',)
  quatrième
  {'bidon': 'bidon'}
  >>> mafonction('Premier', 'deuxième', 'troisième', bidon='bidon')
  Premier
  deuxième
  ('troisième',)
  Argument
  {'bidon': 'bidon'}

Voir https://docs.python.org/fr/3/tutorial/controlflow.html#special-parameters et Voir https://docs.python.org/fr/3/tutorial/controlflow.html#arbitrary-argument-lists
