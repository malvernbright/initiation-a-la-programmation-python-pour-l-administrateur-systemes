.. role:: python(code)
  :language: python

.. role:: terminal(code)
  :language: console

L’affichage d'informations
**************************

Il existe bien des moyens de présenter les sorties d'un programmes ; les données peuvent être affichées sous une forme lisible par un être humain ou sauvegardées dans un fichier pour une utilisation future. Cette partie présente quelques possibilités.

À l’écran du terminal 
=====================

.. code-block:: python

  print()

Formatage de données
--------------------

Souvent vous voudrez plus de contrôle sur le formatage de vos sorties chaîne de caractères et aller au delà d’un affichage de valeurs séparées par des espaces. **Il y a plusieurs moyens de formater ces chaînes de caractères pour l'affichage**

Les expressions formatées f\' {} \'
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Commencez une chaîne de caractère avec **f** ou **F** avant d’ouvrir vos guillemets doubles ou simple. Dans ces chaînes de caractère, vous pouvez entrer des expressions Python entre les accolades :python:`{}` qui peuvent contenir des variables ou des valeurs littérales.

.. code-block:: pycon

  >>> année = 2005
  >>> évènement = 'Sky'
  >>> f'En {année} : {évènement}'
  'En 2005 : Sky'

La méthode str.format()
^^^^^^^^^^^^^^^^^^^^^^^

Les chaînes de caractères exige un plus grand effort manuel. Vous utiliserez toujours les accolades :python:`{}` pour indiquer où une variable sera substituée et suivant des détails sur son formatage. Vous devrez également fournir les informations à formater.

.. code-block:: pycon

  >>> votes_oui = 42572654
  >>> votes_non = 43132495
  >>> pourcentage = votes_oui / (votes_oui + votes_non)
  >>> '{:-9} votes OUI {:2.2%}'.format(votes_oui, pourcentage)
  ' 42572654 votes OUI 49.67%'

Concaténations de tranches de chaînes
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Enfin, vous pouvez construire des concaténations de chaînes vous-même et modifier leur format texte, et ainsi créer n'importe quel agencement.

Le type des chaînes a des méthodes utiles pour aligner des chaînes, pour afficher suivant une taille fixe, suivant la casse, etc.

.. code-block:: pycon

  >>> s = 'coucou mon texte'
  >>> dir(s)
  ['__add__', '__class__', '__contains__', '__delattr__', '__dir__',   '__doc__', '__eq__', '__format__', '__ge__', '__getattribute__', '__getitem__', '__getnewargs__', '__gt__', '__hash__', '__init__', '__init_subclass__', '__iter__', '__le__', '__len__', '__lt__', '__mod__', '__mul__', '__ne__', '__new__', '__reduce__', '__reduce_ex__', '__repr__', '__rmod__', '__rmul__', '__setattr__', '__sizeof__', '__str__', '__subclasshook__', 'capitalize', 'casefold', 'center', 'count', 'encode', 'endswith', 'expandtabs', 'find', 'format', 'format_map', 'index', 'isalnum', 'isalpha', 'isascii', 'isdecimal', 'isdigit', 'isidentifier', 'islower', 'isnumeric', 'isprintable', 'isspace', 'istitle', 'isupper', 'join', 'ljust', 'lower', 'lstrip', 'maketrans', 'partition', 'replace', 'rfind', 'rindex', 'rjust', 'rpartition', 'rsplit', 'rstrip', 'split', 'splitlines', 'startswith', 'strip', 'swapcase', 'title', 'translate', 'upper', 'zfill']

La bibliothèque de fonctions «**string**\ » contient une classe **Template** qui permet aussi de remplacer des valeurs au sein de chaînes de caractères, en utilisant des marqueurs comme :python:`$x`, et en les remplaçant par les valeurs d'un dictionnaire, mais sa capacité à formater les chaînes est plus limitée.

La sortie en chaînes de caractères
----------------------------------

Lorsqu'un affichage basique suffit, pour afficher simplement une variable pour en inspecter le contenu, vous pouvez convertir n'importe quelle valeur ou objet en chaîne de caractères en utilisant la fonction :python:`repr()` ou la fonction :python:`str()`.

**La fonction** :python:`str()` est destinée à représenter les valeurs sous une forme lisible par un être humain.

**La fonction** :python:`repr()` est destinée à générer des représentations qui puissent être lues par l'interpréteur (ou qui lèvera une **SyntaxError** s'il n'existe aucune syntaxe équivalente).

Pour les objets qui n'ont pas de représentation humaine spécifique, :python:`str()` renvoie la même valeur que :python:`repr()`.

Beaucoup de valeurs, comme les nombres ou les structures telles que les listes ou les dictionnaires, ont la même représentation en utilisant les deux fonctions. Les chaînes de caractères, en particulier, ont deux représentations distinctes.

Quelques exemples :

.. code-block:: pycon

  >>> s = 'Bonjour à tous :-)'
  >>> str(s)
  'Bonjour à tous :-)'
  >>> repr(s)
  "'Bonjour à tous :-)'"
  >>> str(1/7)
  '0.14285714285714285'
  >>> x = 10 * 3.25
  >>> y = 200 * 200
  >>> s = 'La valeur de x est ' + repr(x) + ', et celle de y est ' + repr(y) + '...'
  >>> print(s)
  La valeur de x est 32.5, et celle de y est 40000...
  >>> # repr() ajoute les guillemets et les barres obliques inverses d'une chaîne
  >>> salut = 'Bonjour à tous :-)\n'
  >>> salutations = repr(salut)
  >>> print(salutations)
  'Bonjour à tous :-)\n'
  >>> # L'argument de repr() peut être n'importe quel objet Python
  >>> repr((x, y, ('bidon', 'œufs')))
  "(32.5, 40000, ('bidon', 'œufs'))"

Les *chaînes* de caractères formatées (f-strings)
-------------------------------------------------

**Les chaînes de caractères formatées** :python:`f''` (aussi appelées **f-strings**) vous permettent d’inclure la valeur d’expressions Python dans des chaînes de caractères en les préfixant avec «**f**» ou «**F**» et écrire des expressions comme {expression}.

L'expression peut être suivie d'un spécificateur de format. Cela permet un plus grand contrôle sur la façon dont la valeur est rendue. L'exemple suivant arrondit pi à trois décimales après la virgule :

.. code-block:: pycon

  >>> import math
  >>> print(f'La valeur de pi est approximativement {math.pi:.3f}.')
  La valeur de pi est approximativement 3.142.

Donner un entier après les ':' :python:`f'{variable:10}'` indique la largeur minimale de ce champ en nombre de caractères. C'est utile pour faire de jolis tableaux :

.. code-block:: pycon

  >>> table = {'Sylvie': 4127, 'Jacques': 4098, 'David': 7678}
  >>> for nom, téléphone in table.items():
  ...     print(f'{nom:10} ==> {téléphone:10d}')
  ...
  ...
  Sylvie     ==>       4127
  Jacques    ==>       4098
  David      ==>       7678

D’autres modificateurs peuvent être utilisés pour convertir la valeur avant son formatage. «**!a**\ » applique la fonction :python:`ascii()`, «**!s**\ » applique la fonction ::python:`str()`, et «**!r**\ » applique la fonction :python:`repr()` :

.. code-block:: pycon

  >>> animaux = 'rats Womp'
  >>> print(f'Mon aéroglisseur est plein de {animaux}.')
  Mon aéroglisseur est plein de rats Womp.
  >>> print(f'Mon aéroglisseur est plein de {animaux!r}.')
  Mon aéroglisseur est plein de 'rats Womp'.

Pour plus d’informations sur ces spécifications de formats, voir dans le guide Python en ligne «`Mini-langage de spécification de format <https://docs.python.org/fr/3/library/string.html#formatspec>`_».

La méthode de chaîne de caractères format()
---------------------------------------------

L'utilisation de base de la méthode str.format() ressemble à ceci :

.. code-block:: pycon

  >>> print('Le {} nous dit "{}!"'.format('Sith', 'utilise le coté obscur de la force'))
  Le Sith nous dit "utilise le coté obscur de la force!"

Les accolades et les caractères à l'intérieur (appelés les champs de formatage) sont remplacés par les objets passés en paramètres à la méthode :python:`str.format()`. Un nombre entre accolades se réfère à la position de l'objet passé à la méthode :python:`str.format()`.

.. code-block:: pycon

  >>> print('{0} et {1}'.format('bidon', 'pub'))
  bidon et pub
  >>> print('{1} et {0}'.format('bidon', 'pub'))
  pub et bidon

Si des arguments nommés sont utilisés dans la méthode str.format(), leurs valeurs sont utilisées en se basant sur le nom des arguments

.. code-block:: pycon

  >>> print('Cet aliment {nourriture} est {qualité}.'.format(nourriture = 'hamburger', qualité='chimique'))
  Cet aliment hamburger est chimique.

Les arguments positionnés et nommés peuvent être combinés arbitrairement :

.. code-block:: pycon

  >>> print('L’histoire de {0}, {1}, et {autre}.'.format('Bernard', 'Martin', autre='George'))
  L’histoire de Bernard, Martin, et George.

Si vous avez une chaîne de formatage vraiment longue que vous ne voulez pas découper, il est possible de référencer les variables à formater par leur nom plutôt que par leur position. Utilisez simplement un dictionnaire et la notation entre crochets «**[]**\ » pour accéder aux clés.

.. code-block:: pycon

  >>> table = {'Sylvie': 4127, 'Jacques': 4098, 'Daniel': 8637678}
  >>> print('Jacques: {0[Jacques]:d}; Sylvie: {0[Sylvie]:d}; ' 'Daniel: {0[Daniel]:d}'.format(table))
  Jacques: 4098; Sylvie: 4127; Daniel: 8637678

Vous pouvez obtenir le même résultat en passant le tableau comme des arguments nommés en utilisant la notation «**\*\***\ ».

.. code-block:: pycon

  >>> table = {'Sylvie': 4127, 'Jacques': 4098, 'Daniel': 8637678}
  >>> print('Jacques: {Jacques:d}; Sylvie: {Sylvie:d}; Daniel: {Daniel:d}'.format(**table))
  Jacques: 4098; Sylvie: 4127; Daniel: 8637678

C'est particulièrement utile en combinaison avec la fonction native :python:`vars()` qui renvoie un dictionnaire contenant toutes les variables locales.

A titre d’exemple, les lignes suivantes produisent un ensemble de colonnes alignées de façon ordonnée donnant les entiers, leurs carrés et leurs cubes :

.. code-block:: pycon

  >>> for x in range(1, 11):
  ...     print('{0:2d} {1:3d} {2:4d}'.format(x, x*x, x*x*x))
  ...
  ...
  1   1    1
  2   4    8
  3   9   27
  4  16   64
  5  25  125
  6  36  216
  7  49  343
  8  64  512
  9  81  729
  10 100 1000

Pour avoir une description complète du formatage des chaînes de caractères avec la méthode :python:`str.format()`, lisez «Syntaxe de formatage de chaîne».


.. only:: latex

  .. raw:: latex

    \newpage


Logs Système
============

Utiliser logging
----------------

Nous allons aborder ici en avance un module *logging* qui fournit un ensemble de fonctions pour une utilisation simple d’affichages de logs dans une application avec une possibilité de filtrage de la verbosité.
Ces fonctions sont :python:`debug()`, :python:`info()`, :python:`warning()`, :python:`error()` et :python:`critical()`.

Pour déterminer quand employer la journalisation, voyez la table ci-dessous, qui vous indique, pour chaque tâche parmi les plus communes, l'outil approprié.

.. list-table:: Choix du niveau d'information
  :widths: 50 60
  :header-rows: 1

  * - Tâche que vous souhaitez mener
    - Le meilleur outil pour cette tâche
  * - Affiche la sortie console d'un script en ligne de commande ou d'un programme lors de son utilisation ordinaire
    - `print() <https://docs.python.org/fr/3/library/functions.html#print>`_
  * - Rapporter des évènements qui ont lieu au cours du fonctionnement normal d'un programme (par exemple pour suivre un statut ou examiner des dysfonctionnements)
    - `logging.info() <https://docs.python.org/fr/3/library/logging.html#logging.info>`_

      ou

      `logging.debug() <https://docs.python.org/fr/3/library/logging.html#logging.debug>`_

      pour une sortie très détaillée à visée diagnostique
  * - Émettre un avertissement (*warning* en anglais) en relation avec un évènement particulier au cours du fonctionnement d’un programme
    - `warnings.warn() <https://docs.python.org/fr/3/library/warnings.html#warnings.warn>`_

      dans le code de la bibliothèque si le problème est évitable et l'application cliente doit être modifiée pour éliminer cet avertissement

      `logging.warning() <https://docs.python.org/fr/3/library/logging.html#logging.warning>`_

      si l'application cliente ne peut rien faire pour corriger la situation mais l'évènement devrait quand même être noté
  * - Rapporter une erreur lors d'un évènement particulier en cours d'exécution
    - Lever une exception
  * - Rapporter la suppression d'une erreur sans lever d'exception (par exemple pour la gestion d'erreur d'un processus de long terme sur un serveur)
    - `logging.error() <https://docs.python.org/fr/3/library/logging.html#logging.error>`_,

      `logging.exception() <https://docs.python.org/fr/3/library/logging.html#logging.exception>`_

      ou `logging.critical() <https://docs.python.org/fr/3/library/logging.html#logging.critical>`_,

      au mieux, selon l'erreur spécifique et le domaine d'application


Les fonctions de journalisation sont nommées d'après le niveau ou la sévérité des évènements qu'elles suivent. Les niveaux standards et leurs applications sont décrits ci-dessous (par ordre croissant de sévérité) :

.. list-table:: Typologie des journalisations
  :widths: 16 80
  :header-rows: 1

  * - Niveau
    - Pourqoi c'est utilisé
  * - **DEBUG**
    - Information détaillée, intéressante seulement lorsqu'on diagnostique un problème.
  * - **INFO**
    - Confirmation que tout fonctionne comme prévu.
  * - **WARNING**
    - L'indication que quelque chose d'inattendu a eu lieu, ou de la possibilité d'un problème dans un futur proche (par exemple « espace disque faible »). Le logiciel fonctionne encore normalement.
  * - **ERROR**
    - Du fait d'un problème plus sérieux, le logiciel n'a pas été capable de réaliser une tâche.
  * - **CRITICAL**
    - Une erreur sérieuse, indiquant que le programme lui-même pourrait être incapable de continuer à fonctionner.


Le niveau par défaut est WARNING, ce qui signifie que seuls les évènements de ce niveau et au-dessus sont suivis, sauf si le paquet logging est configuré pour faire autrement.

Les évènements suivis peuvent être gérés de différentes façons. La manière la plus simple est de les afficher dans la console. Une autre méthode commune est de les écrire dans un fichier.

Un exemple simple
-----------------

Un exemple très simple est :

.. code-block:: pycon

  >>> import logging
  >>> logging.warning('Attention!') # affiche un message dans la console
  WARNING:root:Attention!
  >>> logging.info('C’est bon relâche la pression') # n’imprime rien

Si vous entrez ces lignes dans un script que vous exécutez, vous verrez :terminal:`WARNING:root:Attention!` » affiché dans la console. Le message INFO n'apparaît pas parce que le niveau par défaut est WARNING. Le message affiché inclut l'indication du niveau et la description de l'évènement fournie dans l'appel à logging, ici «Attention!». Ne vous préoccupez pas de la partie «root» pour le moment : nous détaillerons ce point plus bas. La sortie elle-même peut être formatée de multiples manières si besoin. Les options de formatage seront aussi expliquées plus bas.

Enregistrer les évènements dans un fichier
------------------------------------------

Il est très commun d'enregistrer les évènements dans un fichier, c'est donc ce que nous allons regarder maintenant. Il faut essayer ce qui suit avec un interpréteur Python nouvellement démarré, ne poursuivez pas la session commencée ci-dessus :

.. code-block:: pycon

  >>> import logging
  >>> logging.basicConfig(filename='./exemple.log', encoding='utf-8', level=logging.DEBUG)
  >>> logging.debug('Ce message doit aller dans le fichier journal')
  >>> logging.info('Celui là aussi')
  >>> logging.warning('Et encore celui-ci')
  >>> logging.error('Et des trucs non-ASCII aussi, comme Fêtes de Noël')

**Modifié dans la version 3.9**: L'argument d'encodage a été ajouté.

Dans les versions antérieures de Python ou lorsqu'il n'est pas spécifié, l'encodage utilisé est la valeur par défaut utilisée par :python:`open()`.
Bien que cela ne soit pas montré dans l'exemple ci-dessus, un argument d'erreurs peut également maintenant être passé, qui détermine comment les erreurs de codage sont gérées. Pour les valeurs disponibles et les valeurs par défaut, consultez la documentation de :python:`open()`.

Maintenant, si nous ouvrons le fichier «**exemple.log**» et lisons ce qui s'y trouve, on trouvera les messages de log :

.. code-block:: text

  DEBUG:root:Ce message doit aller dans le fichier journal
  INFO:root:Celui là aussi
  WARNING:root:Et encore celui-ci
  ERROR:root:Et des trucs non-ASCII aussi, comme Fêtes de Noël

Cet exemple montre aussi comment on peut régler le niveau de journalisation qui sert de seuil pour le suivi. Dans ce cas, comme nous avons réglé le seuil à DEBUG, tous les messages ont été écrits.

Régler le niveau de journalisation d’un script
----------------------------------------------

Si vous souhaitez régler le niveau de journalisation à partir d'une option de la ligne de commande comme :

.. code-block:: console

  --log=INFO

Créer avec votre éditeur de texte le fichier «**niveau_journalisation.py**».

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement$ mkdir 5_Niveau_journalisation ; cd 5_Niveau_journalisation
  utilisateur@MachineUbuntu:~/repertoire_de_developpement/5_Niveau_journalisation$ nano niveau_journalisation.py ; chmod u+x niveau_journalisation.py

.. code-block:: python

  #! /usr/bin/env python3
  # -*- coding: utf8 -*-

  import argparse, logging

  # Récupère l'argument de la ligne de commande du paramètre log et le met dans la variable loglevel
  params = argparse.ArgumentParser()
  params.add_argument('--log')
  args = params.parse_args()
  loglevel = args.log

  # Défini le niveau de journalisation
  if loglevel:
      numeric_level = getattr(logging, loglevel.upper())
  else:
      numeric_level = logging.DEBUG

  # Teste si le paramètre est valide
  if not isinstance(numeric_level, int):
      raise ValueError('Niveau de journalisation invalide : %s' % loglevel)

  # Configure le niveau de journalisation et le fichier où journaliser
  logging.basicConfig(filename='niveau.log', filemode='w', level=numeric_level)

  # Messages de tests
  logging.error('Message erreur')
  logging.warning('Message alerte')
  logging.info('Message information')
  logging.debug('Message debug')

Vous passez la valeur du paramètre donné à l'option :terminal:`--log` dans une variable **loglevel**. L'appel à :python:`basicConfig()` doit être fait avant un appel à :python:`debug()`, :python:`info()`, etc. Si vous exécutez le script plusieurs fois **sans l’option «filemode»**, les messages des exécutions successives sont ajoutés au fichier «**niveau.log**».

Si vous voulez que chaque exécution reprenne un fichier vierge, sans conserver les messages des exécutions précédentes, vous devez spécifier l'argument **filemode** à **\'w\'**. Le texte n'est plus ajouté au fichier de log, donc les messages des exécutions précédentes sont perdus.

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement/5_Niveau_journalisation$ ./niveau_journalisation.py
  utilisateur@MachineUbuntu:~/repertoire_de_developpement/5_Niveau_journalisation$ cat niveau.log
  ERROR:root:Message erreur
  WARNING:root:Message alerte
  INFO:root:Message information
  DEBUG:root:Message debug
  utilisateur@MachineUbuntu:~/repertoire_de_developpement/5_Niveau_journalisation$ python3 ./niveau_journalisation.py --log=DEBUG
  utilisateur@MachineUbuntu:~/repertoire_de_developpement/5_Niveau_journalisation$ cat niveau.log
  ERROR:root:Message erreur
  WARNING:root:Message alerte
  INFO:root:Message information
  DEBUG:root:Message debug
  utilisateur@MachineUbuntu:~/repertoire_de_developpement/5_Niveau_journalisation$ python3 ./niveau_journalisation.py --log=INFO
  utilisateur@MachineUbuntu:~/repertoire_de_developpement/5_Niveau_journalisation$ cat niveau.log
  ERROR:root:Message erreur
  WARNING:root:Message alerte
  INFO:root:Message information
  utilisateur@MachineUbuntu:~/repertoire_de_developpement/5_Niveau_journalisation$ python3 ./niveau_journalisation.py --log=WARNING
  utilisateur@MachineUbuntu:~/repertoire_de_developpement/5_Niveau_journalisation$ cat niveau.log
  ERROR:root:Message erreur
  WARNING:root:Message alerte
  utilisateur@MachineUbuntu:~/repertoire_de_developpement/5_Niveau_journalisation$ python3 ./niveau_journalisation.py --log=ERROR
  utilisateur@MachineUbuntu:~/repertoire_de_developpement/5_Niveau_journalisation$ cat niveau.log
  ERROR:root:Message erreur
  utilisateur@MachineUbuntu:~/repertoire_de_developpement/5_Niveau_journalisation$ python3 ./niveau_journalisation.py --log=BIDON ; cd ..
  Traceback (most recent call last):
    File "./niveau_journalisation.py", line 14, in <module>
      numeric_level = getattr(logging, loglevel.upper())
  AttributeError: module 'logging' has no attribute 'BIDON'

Modifier le format du message affiché
-------------------------------------

Pour changer le format utilisé pour afficher le message, vous devez préciser le format que vous souhaitez employer :

.. code-block:: pycon

  >>> import logging
  >>> logging.basicConfig(format='%(levelname)s:%(message)s', level=logging.DEBUG)
  >>> logging.debug('Message d’analyse de code')
  DEBUG:Message d’analyse de code
  >>> logging.basicConfig(format='Mon programme %(levelname)s:%(lineno)d:%(pathname)s:%(message)s', level=logging.DEBUG, force=True)
  >>> logging.debug('Message d’analyse de code')
  Mon programme DEBUG:1:<bpython-input-7>:Message d’analyse de code
  >>> logging.info('Message d’information')
  Mon programme INFO:1:<bpython-input-8>:Message d’information
  >>> logging.warning('Attention!')
  Mon programme WARNING:1:<bpython-input-9>:Attention!


.. only:: latex

  .. raw:: latex

    \newpage


GUI
===

Gui intégré client lourd (Windows, gtk, qt, etc.).

Gui web (remi, django, etc.).

On verra cela plus loin dans le cours.
