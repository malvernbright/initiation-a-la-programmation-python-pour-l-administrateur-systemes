.. role:: python(code)
  :language: python

.. role:: terminal(code)
  :language: console

La visualisation de l’architecture objets
*****************************************

Le programme :terminal:`pylint`, que nous avons installé pour tester le code Python, est livré avec un outil de ligne de commande fort pratique :terminal:`pyreverse`. Celui-ci permet d'imager les classes Python et de créer des diagrammes `UML <https://www.ibm.com/docs/fr/rational-soft-arch/9.5?topic=diagrams-uml-models>`_ des classes Python.

Pyreverse
=========

**Pyreverse** permet de générer des diagrammes avec :

* des attributs de classes et si possible avec leurs types,
* des méthodes de classes avec leurs paramètres,
* la représentation des exceptions,
* les liens d'héritages de classes,
* et les liens d'association de classes.

Les options de la ligne de commande
-----------------------------------

.. list-table:: Options de la ligne de commande de Pyreverse
   :widths: 20 30 50
   :header-rows: 1
   :stub-columns: 2

   * - Option courte
     - Option verbeuse
     - Description
   * - :terminal:`-p <nom du projet>`
     - :terminal:`--project=<nom du projet>`
     - Nom du fichier en sortie
   * - :terminal:`-o <format>`
     - :terminal:`--output=<format>`
     - Format de sortie de fichier (**svg**, **svgz**, **png**, **jpeg**/**jpg**/**jpe**, **gif**, **ps**/**ps2**/**eps**, **pdf**, pic, pcl, hpgl, gd/gd2, fig, dia, **dot**/**xdot**, **plain**/**plain-ext**, vrml/vml/vmlz, tk, wbmp, xlib, etc.)
   * - :terminal:`-c <classe>`
     - :terminal:`--class <classe>`
     - Afficher la classe passée en paramètre
   * - :terminal:`-k`
     - :terminal:`--only-classnames`
     - Afficher seulement les noms des classes
   * - :terminal:`-m[y/n]`
     - :terminal:`--module-names=[y/n]`
     - Inclure le nom des modules pour la représentation des classes.
   * - :terminal:`-b`
     - :terminal:`--show-builtin`
     - Afficher les classes des objets natifs Python
   * - :terminal:`-a <niveau>`
     - :terminal:`--show-ancestors=<niveau>`
     - Aficher le niveaux d'héritage passé en paramètre
   * - :terminal:`-A`
     - :terminal:`--all-ancestors`
     - Afficher tout l'arbre d'héritage
   * - :terminal:`-s <niveau>`
     - :terminal:`--show-associated=<niveau>`
     - Liens des imports suivant le niveau d'imports
   * - :terminal:`-S`
     - :terminal:`--all-associated`
     - Toutes les liaisons d'imports
   * - :terminal:`-f <mode>`
     - :terminal:`--filter-mode=<mode>`
     - Filtre ce qu'il faut faire apparaître (par défaut PUB_ONLY) :

       * **PUB_ONLY** : Affiche les méthodes et les propriétés publiques.
       * **SPECIAL** : Affiche les attributs privés ou protégés en plus.
       * **OTHER** : Affiche le méthodes privées ou protégés en plus.
       * **ALL** : Affiche tout.


.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement$ cd 9_objets
  utilisateur@MachineUbuntu:~/repertoire_de_developpement/9_objets$ pyreverse -p test -o png scene.py

L'option :terminal:`-p test` ajout le suffixe **test** au nom du fichier qui à défaut est «**classes.ext**».
L'option :terminal:`-o png` choisit le format de sortie, ici un fichier image `PNG <https://fr.wikipedia.org/wiki/Portable_Network_Graphics>`_.

Cela génère donc le fichier «**classes_test.png**».

Qui nous donne à afficher par défaut :

.. image:: images/pyreverse_test1.png
   :alt: pyreverse -p test -o png scene.py
   :align: center
   :scale: 100%

Cela nous affiche le nom de la classe :python:`Decor` avec sa propriété :python:`fabriqueBatiments` affectée à l'objet :python:`FabriqueBatiments`. Ceci nous permet de comprendre que cela est en fait une méthode pour l'objet.

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement/9_objets$ pyreverse -c Decor -o png scene.py

L'option :terminal:`-c Decour` choisit la classe à générer et va produire une sortie de nom de fichier avec le nom de la classe «**Decor.png**».

Ce qui va nous donner comme diagramme plus intéressant :

.. image:: images/pyreverse_test2.png
   :alt: pyreverse -p test -o png scene.py
   :align: center
   :scale: 100%

Là nous voyons avec les **flêches** l'héritage des classes :python:`Vegetation`, :python:`Urbanisation`, :python:`Hydrotopologie`, :python:`Geomorphologie` et :python:`Atmosphere` qui elle même hérite de :python:`Nuages`.

Nous voyon aussi les propriétés et méthodes de :python:`FabriqueBatiments` qui est bien affectée à la propriété :python:`fabriqueBatiments` (en vert) de la classe :python:`Decor`

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement/9_objets$ pyreverse -k -c Decor -o png scene.py

L'option :terminal:`-k` permet de n'afficher que le nom des classes :

.. image:: images/pyreverse_test3.png
   :alt: pyreverse -p test -o png scene.py
   :align: center
   :scale: 100%


.. code-block:: console

   utilisateur@MachineUbuntu:~/repertoire_de_developpement/9_objets$ pyreverse -f ALL -S -c Decor -o png scene.py

L'option :terminal:`-f ALL` permet d'afficher les propriétés et les méthodes cachées :

.. image:: images/pyreverse_test4.png
   :alt: pyreverse -p test -o png scene.py
   :align: center
   :scale: 100%

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement/9_objets$ pyreverse -a 1 -c Decor -o png scene.py

L'option :terminal:`-a 1` permet de limiter le niveau de liens en héritage par rapport à la classe :

.. image:: images/pyreverse_test5.png
   :alt: pyreverse -p test -o png scene.py
   :align: center
   :scale: 100%

En limitant au premier niveau :terminal:`-a 1` la classe :python:`Nuages` n'est plus affichée.

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement/9_objets$ pyreverse -b -c Decor -o png scene.py

L'option :terminal:`-b` permet d'afficher l'héritage des classes natives Python :

.. image:: images/pyreverse_test6.png
   :alt: pyreverse -p test -o png scene.py
   :align: center
   :scale: 100%

On voit alors bien apparaître la classe :python:`object` comme on l'attendait, mais aussi les classes :python:`builtin.list` et :python:`builtin.str`.

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement/9_objets$ pyreverse -mn -c Decor -o png scene.py

L'option :terminal:`-mn` permet de supprimer le chemin d'importation des classes dans les intitulés de classes :

.. image:: images/pyreverse_test7.png
   :alt: pyreverse -p test -o png scene.py
   :align: center
   :scale: 100%

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement/9_objets$ pyreverse -s 0 -c Decor -o png scene.py

L'option :terminal:`-s 0` permet de ne visualiser que les classes importées directement :

.. image:: images/pyreverse_test8.png
   :alt: pyreverse -p test -o png scene.py
   :align: center
   :scale: 100%

On voit bien que la classe :python:`FabriqueBatiments` ne s'affiche plus avec un niveau supplémentaire d'importation.

Vous pouvez maintenant générer tous les diagrammes de classes Python que vous voulez pour agrémenter les documentations de vos programmes.

Mise en œuvre dans GitLab
=========================
