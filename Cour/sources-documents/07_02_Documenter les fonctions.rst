.. role:: python(code)
  :language: python

.. role:: terminal(code)
  :language: console

Documenter les fonctions
************************

Voici quelques conventions concernant le contenu et le format des chaînes de documentation.

Il convient que la première ligne soit toujours courte et résume de manière concise l'utilité de l'objet. Afin d'être bref, nul besoin de rappeler le nom de l'objet ou son type, qui sont accessibles par d'autres moyens (sauf si le nom est un verbe qui décrit une opération).
La convention veut que la ligne commence par une majuscule et se termine par un point.

.. code-block:: python

  def ma_fonction():
      """Ne fait rien."""


S'il y a d'autres lignes dans la chaîne de documentation, la deuxième ligne devrait être vide, pour la séparer visuellement du reste de la description. Les autres lignes peuvent alors constituer un ou plusieurs paragraphes décrivant le mode d'utilisation de l'objet, ses effets de bord, etc.

.. code-block:: python

  def ma_fonction():
      """Ne fait rien.

      C'est du texte d'aide seulement.
      """


L'analyseur de code Python ne supprime pas l'indentation des chaînes de caractères littérales multi-lignes, donc les outils qui utilisent la documentation doivent si besoin faire cette opération eux-mêmes. La convention suivante s'applique :

* la première ligne non vide après la première détermine la profondeur d'indentation de l'ensemble de la chaîne de documentation (on ne peut pas utiliser la première ligne qui est généralement accolée aux guillemets d'ouverture de la chaîne de caractères et dont l'indentation n'est donc pas visible).
* Les espaces «correspondant» à cette profondeur d'indentation sont alors supprimées du début de chacune des lignes de la chaîne. Aucune ligne ne devrait présenter un niveau d'indentation inférieur mais, si cela arrive, toutes les espaces situées en début de ligne doivent être supprimées. L'équivalent des espaces doit être testé après expansion des tabulations (normalement remplacées par 8 espaces).

Voici un exemple de chaîne de documentation multi-lignes :

.. code-block:: pycon

  >>> def ma_fonction():
  ...     """Ne fait rien, c'est pour la doc.
  ...
  ...     Cela ne fait vraiment rien!
  ...     """
  ...     pass
  ...
  >>> print(ma_fonction.__doc__)
  Ne fait rien, c'est pour la doc.

  Cela ne fait vraiment rien!


Annotations de fonctions
========================

Les annotations de fonction sont des **métadonnées optionnelles décrivant les types utilisés par les paramètres** de la fonction (procédure) définie par l'utilisateur (voir les `PEP 3107 <https://www.python.org/dev/peps/pep-3107/>`_ et `PEP 484 <https://www.python.org/dev/peps/pep-0484/>`_ pour plus d'informations).

Les annotations sont stockées dans l'attribut :python:`__annotations__` de la fonction, sous la forme d'un dictionnaire, et n'ont aucun autre effet.

Les annotations sur les paramètres sont définies par deux points «**:**» après le nom du paramètre suivi d'une expression donnant la valeur de l'annotation.

Les annotations de retour sont définies par «**->**» suivi d'une expression, entre la liste des paramètres et les deux points de fin de l'instruction def.

L'exemple suivant a un paramètre positionnel, un paramètre nommé et la valeur de retour annotés :

.. code-block:: pycon

  >>> def f(ham: str, eggs: str='eggs') -> str:
  ...     print("Annotations:", f.__annotations__)
  ...     print("Arguments:", ham, eggs)
  ...     return ham + ' and ' + eggs
  ...
  >>> f('spam')
  Annotations: {'ham': <class 'str'>, 'return': <class 'str'>, 'eggs': <class 'str'>}
  Arguments: spam eggs
  'spam and eggs'
