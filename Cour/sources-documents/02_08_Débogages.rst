.. role:: terminal(code)
  :language: console

Débogages
*********

Si vous avez un bogue non banal, c'est là que les stratégies de débogage vont rentrer en ligne de compte. Le problème doit être isolé dans un petit nombre de lignes de code, hors frameworks ou code applicatif.

Pour déboguer un problème donné :

#. Faites échouer le code de façon fiable : trouvez un cas de test qui fait échouer le code à chaque fois.

#. Diviser et conquérir : une fois que vous avez un cas de test échouant, isolez le code coupable.

    a.  Quel module.

    b.  Quelle fonction.

    c.  Quelle ligne de code.

    d.  Isolez une petite erreur reproductible (permet de définir un cas de test à implémenter).

#. Changez une seule chose à chaque fois, l’archiver dans la révision de code, et ré-exécutez le cas de test d'échec.

#. Utilisez le débogueur (pour Python pdb) pour comprendre ce qui ne va pas.

#. Prenez des notes et soyez patient, ça peut prendre un moment.

Une fois que vous avez procédé à cette étape, isolez un petit bout de code reproduisant le bogue et corrigez celui-ci en utilisant ce bout de code, ajoutez le code de test dans votre suite de test (Unittest).

Le débogueur Python pdb
=======================

Installation de pdb
-------------------

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement$sudo apt install python3-ipdb

Déboguer avec pdb
-----------------

Les façons de lancer le débogueur :

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement$ mkdir 2_Debug ; cd 2_Debug

Créer le fichier «**error.py**» dans le dossier «**repertoire_de_developpement/2_Debug**»

.. code-block:: python
  :linenos:

  #! /usr/bin/env python3
  # -*- coding: utf8 -*-

  dividende = 5
  nombres = [5, 4, 3, 2, 1, 0]
  for diviseur in nombres:
    print('Valeur du rapport : %s' % (dividende/diviseur))

Postmortem
^^^^^^^^^^

:terminal:`pdb` est invoqué (exécuté) pour déboguer un script.

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement/2_Debug$ python3 -m pdb error.py
  >/home/utilisateur/repertoire_de_developpement/2_Debug/error.py(4)<module>()
  -> dividende = 5
  (pdb) q

Pour arrêter le débogage (prompt :terminal:`pdb`) tapez :terminal:`q`.

Lancez le module avec le débogueur
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement/2_Debug$ ipython3 error.py

ou

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement/2_Debug$ ipython3
  In [1]:%run error.py

pour sortir du débogueur (prompt :terminal:`In [num]:%`) tapez :terminal:`quit`.


Exécution pas à pas du débogueur
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement/2_Debug$ ipython3 -c '%run -d error.py'

ou

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement/2_Debug$ ipython3
  In [1]: %run -d error.py

Continuez dans le code avec :terminal:`n`\ (ext), next saute à la prochaine déclaration de code dans le contexte d'exécution courant  :

.. code-block:: console

  ipdb> n

Placez un point d'arrêt à la ligne 7 en utilisant :terminal:`b 7` :

.. code-block:: console

  ipdb> b 7

Continuez l'exécution jusqu'au prochain point d'arrêt avec :terminal:`c`\ (ontinue) :

.. code-block:: console

  ipdb> c

Continuez dans le code avec :terminal:`s`\ (tep), step va traverser les contextes d'exécution, c'est-à-dire permettre l'exploration à l'intérieur des appels de fonction :

.. code-block:: console

  ipdb> s

Visualiser l'état d'une variable avec :terminal:`print()` :

.. code-block:: console

  ipdb> print(diviseur)

Arrêter le débogage :

.. code-block:: console

  ipdb> q

Quitter le débogueur :

.. code-block:: console

  In [3]: quit

Appeler le débogueur à l'intérieur du module
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block:: python

  import pdb; pdb.set_trace()

Les commandes du débogueur
^^^^^^^^^^^^^^^^^^^^^^^^^^

+----------+----------------------------------------------------------+
| l (list) | Liste le code à la position courante                     |
+----------+----------------------------------------------------------+
| u(p)     | Monte à la pile d'appel                                  |
+----------+----------------------------------------------------------+
| d(own)   | Descend à la pile d'appel                                |
+----------+----------------------------------------------------------+
| n(ext)   | Exécute la prochaine ligne (ne va pas à l'intérieur      |
|          | d'une nouvelle fonction)                                 |
+----------+----------------------------------------------------------+
| s(tep)   | Exécute la prochaine déclaration (va à l'intérieur d'une |
|          | nouvelle fonction)                                       |
+----------+----------------------------------------------------------+
| bt       | Affiche la pile d'appel                                  |
+----------+----------------------------------------------------------+
| a        | Affiche les variables locales                            |
+----------+----------------------------------------------------------+
| !command | Exécute la commande **Python** donnée (par opposition à  |
|          | une commande pdb)                                        |
+----------+----------------------------------------------------------+

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement/2_Debug$ cd ..
  utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git add .
  utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git commit -m "Ajout des exemples de débogages"


.. only:: latex

  .. raw:: latex

    \newpage

La Gestion des Warnings d’exécution
===================================

Attention cet exemple fonctionne jusqu’à Python 3.9. Pour les versions postérieures les modules obsolètes hérités de Python2 ne sont plus pris en charge.

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement$ mkdir 3_Interpreteur_alerts ; cd 3_Interpreteur_alerts

Créer le fichier «**monscript.py**» dans le dossier «**repertoire_de_developpement/3_Interpreteur_alerts**»

.. code-block:: python
  :linenos:

  #! /usr/bin/env python3
  # -*- coding: utf8 -*-

  import formatter

  print('Bonjour %s' % 'Moi')

Exécution de python avec les Warnings :

.. code-block:: shell

  python3 -Wd monscript.py


.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement/3_Interpreteur_alerts$ python3 -Wd monscript.py
  monscript.py:4: DeprecationWarning: the formatter module is deprecated
    import formatter
  Bonjour Moi

Pour l’activer par défaut pour toutes les alertes :

.. code-block:: bash

  python3 -Wa

À chaque mise à jour de version de python, pour son code il est important de vérifier les warnings.

Ceux-ci nous informe de l’obsolescence des bibliothèques ou des fonctions de python que nous utilisons. Cela permet de préparer et corriger le code python de nos applications développées pour les migrations futures de vos systèmes informatiques et de leurs bibliothèques/frameworks.

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement/3_Interpreteur_alerts$ cd ..
  utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git add .
  utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git commit -m "Ajout des exemples de warnings d’exécution"


.. only:: latex

  .. raw:: latex

    \newpage
